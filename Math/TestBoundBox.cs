﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.Test
{
    public class TestBoundBox : MonoBehaviour
    {
        [Header("Closest Point")]
        public Vector3 size = Vector3.one;
        public Transform m_Point = null;

        public bool m_ReDefine = true;
        public bool m_ContinueUpdateRotation = false;

        private bool m_Defined = false;
        private Box m_Box;


        [Header("Structure grid")]
        public Vector3 m_CellSize = Vector3.one;


        private void OnDrawGizmos()
        {
            if (m_ReDefine || !m_Defined)
            {
                m_Box = new Box(transform.position, size * .5f);
                m_Defined = true;
                m_ReDefine = false;
            }
            if (m_ContinueUpdateRotation)
            {
                m_Box.rotation = transform.rotation;
            }

            GizmosExtend.DrawCube(m_Box, Color.cyan);
            if (m_Point != null)
            {
                Vector3 p = m_Point.position;
                Vector3 closest = Closest.PointInBox(
                    Matrix4x4.TRS(m_Box.origin, m_Box.rotation, Vector3.one),
                    Vector3.zero, m_Box.Size(), p);
                GizmosExtend.DrawLine(closest, p, Color.gray);
#if false
                Plane[] planes;
                m_Box.GetPlane(out planes);
                int cpt = 0;
                Vector3 v = (p - m_Box.origin).normalized;
                Vector3 final = Vector3.zero;
                foreach (Plane plane in planes)
                {
                    if (plane.GetSide(p))
                    {
                        GizmosExtend.DrawPoint(p, m_Colors[cpt], 0.2f);
                        Vector3 ap = plane.ClosestPointOnPlane(p);
                        GizmosExtend.DrawLine(p, ap, m_Colors[cpt]);

                        Vector3 bias = ap - m_Box.origin;
                        float diff = Vector3.Dot(bias, v);
                        final += v * diff;
                    }
                    cpt++;
                }
                GizmosExtend.DrawRay(m_Box.origin, final, Color.black);
#endif
                Bounds sb = BoundsExtend.GetStructuredBounds(p, m_CellSize);
                GizmosExtend.DrawBounds(sb, Color.yellow);
            }
        }
        
        static readonly Color[] m_Colors = new Color[] { Color.red, Color.yellow, Color.green, Color.cyan, Color.blue, Color.magenta };
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public static class Closest
	{
		/// <summary>Find a closest point on a ray.
		/// <see cref="https://forum.unity.com/threads/how-do-i-find-the-closest-point-on-a-line.340058/"/>
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="directionNormalize"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static Vector3 PointOnRay(in Vector3 origin, in Vector3 directionNormalize, in Vector3 point)
		{
			Debug.Assert(directionNormalize.IsNormalized(), "Require unit vector");
			Vector3 vector = point - origin;
			float projectedDistance = Vector3.Dot(vector, directionNormalize);
			return origin + directionNormalize * projectedDistance;
		}

		/// <summary>Find a closest point between line</summary>
		/// <param name="lhs"></param>
		/// <param name="rhs"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static Vector3 PointOnLine(in Vector3 lhs, in Vector3 rhs, in Vector3 point)
        {
			if (lhs == rhs)
				return lhs; // quick return;
			return PointOnRay(lhs, (rhs - lhs).normalized, point);
        }

		/// <summary>Find closest point(s) between ray</summary>
		/// <param name="ray">first ray</param>
		/// <param name="otherRay">2nt ray</param>
		/// <param name="point1">result point on first ray</param>
		/// <param name="point2">result point on 2nt ray</param>
		/// <returns></returns>
		public static bool PointsBetweenRays(this Ray ray, Ray otherRay, out Vector3 point1, out Vector3 point2)
		{
			return PointsBetweenRays(ray.origin, ray.direction, otherRay.origin, otherRay.direction, out point1, out point2);
		}

		/// <summary>Find closest point(s) between ray</summary>
		/// <param name="ray1Origin"></param>
		/// <param name="ray1Direction"></param>
		/// <param name="ray2Origin"></param>
		/// <param name="ray2Direction"></param>
		/// <param name="point1">result point on first ray</param>
		/// <param name="point2">result point on 2nt ray</param>
		/// <returns></returns>
		public static bool PointsBetweenRays(
			in Vector3 ray1Origin, in Vector3 ray1Direction,
			in Vector3 ray2Origin, in Vector3 ray2Direction,
			out Vector3 point1, out Vector3 point2)
		{
			bool isParallel = Mathf.Abs(Vector3.Dot(ray1Direction.normalized, ray2Direction.normalized)) > 1 - Mathf.Epsilon;
			if (isParallel)
			{
				point1 = Vector3.zero;
				point2 = Vector3.zero;
				return false;
			}

			// loosely based on http://www.youtube.com/watch?v=HC5YikQxwZA
			// t is distance traveled at ray and s distance traveled at otherRay
			// PQ is vector between rays
			Vector3 PQT = -ray1Direction;
			Vector3 PQS = ray2Direction;
			Vector3 PQVal = -ray1Origin + ray2Origin;

			float PQu1S = Vector3.Dot(PQS, ray1Direction);
			float PQu1T = Vector3.Dot(PQT, ray1Direction);
			float PQu1Val = Vector3.Dot(PQVal, ray1Direction) * -1;

			float PQu2S = Vector3.Dot(PQS, ray2Direction);
			float PQu2T = Vector3.Dot(PQT, ray2Direction);
			float PQu2Val = Vector3.Dot(PQVal, ray2Direction) * -1;

			// maybe not the most efficient way to solve the system of linear equations
			Matrix4x4 mat = Matrix4x4.identity;
			mat[0, 0] = PQu1S;
			mat[0, 1] = PQu1T;
			mat[1, 0] = PQu2S;
			mat[1, 1] = PQu2T;

			mat = mat.inverse;
			Vector4 res = mat * new Vector4(PQu1Val, PQu2Val, 0, 0);

			point1 = ray1Origin + ray1Direction * res[1];
			point2 = ray2Origin + ray2Direction * res[0];

			return true;
		}


		/// <summary>Calculates the closest point on the plane.</summary>
		/// <param name="plane">Representation of a plane in 3D space.</param>
		/// <param name="point">The point to project onto the plane.</param>
		/// <returns>A point on the plane that is closest to point.</returns>
		public static Vector3 PointOnPlane(in Plane	plane, in Vector3 point)
        {
			return PointOnPlane(plane.normal, plane.distance, point);
		}

		/// <summary>Calculates the closest point on the plane.
		/// as same as <see cref="Plane.ClosestPointOnPlane(Vector3)"/></summary>
		/// <param name="planeNormal">Normal vector of the plane.</param>
		/// <param name="planeDistance">Distance from the origin to the plane.</param>
		/// <param name="point">The point to project onto the plane.</param>
		/// <returns>A point on the plane that is closest to point.</returns>
		public static Vector3 PointOnPlane(in Vector3 planeNormal, float planeDistance, in Vector3 point)
        {
			float pointToPlaneDistance = Vector3.Dot(planeNormal, point) + planeDistance;
			return point - (planeNormal * pointToPlaneDistance);
		}

        /// <summary>
        /// Determines the closest point between a point and a triangle.
        /// </summary>
        /// <param name="point">The point to test.</param>
        /// <param name="vertex1">The first vertex to test.</param>
        /// <param name="vertex2">The second vertex to test.</param>
        /// <param name="vertex3">The third vertex to test.</param>
        /// <param name="result">When the method completes, contains the closest point between the two objects.</param>
        public static void PointOnTriangle(in Vector3 point, in Vector3 vertex1, in Vector3 vertex2, in Vector3 vertex3, out Vector3 result)
        {
            //Source: Real-Time Collision Detection by Christer Ericson
            //Reference: Page 136

            //Check if P in vertex region outside A
            Vector3 ab = vertex2 - vertex1;
            Vector3 ac = vertex3 - vertex1;
            Vector3 ap = point - vertex1;

            float d1 = Vector3.Dot(ab, ap);
            float d2 = Vector3.Dot(ac, ap);
            if (d1 <= 0.0f && d2 <= 0.0f)
            {
                result = vertex1; //Barycentric coordinates (1,0,0)
                return;
            }

            //Check if P in vertex region outside B
            Vector3 bp = point - vertex2;
            float d3 = Vector3.Dot(ab, bp);
            float d4 = Vector3.Dot(ac, bp);
            if (d3 >= 0.0f && d4 <= d3)
            {
                result = vertex2; // Barycentric coordinates (0,1,0)
                return;
            }

            //Check if P in edge region of AB, if so return projection of P onto AB
            float vc = d1 * d4 - d3 * d2;
            if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
            {
                float v = d1 / (d1 - d3);
                result = vertex1 + v * ab; //Barycentric coordinates (1-v,v,0)
                return;
            }

            //Check if P in vertex region outside C
            Vector3 cp = point - vertex3;
            float d5 = Vector3.Dot(ab, cp);
            float d6 = Vector3.Dot(ac, cp);
            if (d6 >= 0.0f && d5 <= d6)
            {
                result = vertex3; //Barycentric coordinates (0,0,1)
                return;
            }

            //Check if P in edge region of AC, if so return projection of P onto AC
            float vb = d5 * d2 - d1 * d6;
            if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f)
            {
                float w = d2 / (d2 - d6);
                result = vertex1 + w * ac; //Barycentric coordinates (1-w,0,w)
                return;
            }

            //Check if P in edge region of BC, if so return projection of P onto BC
            float va = d3 * d6 - d5 * d4;
            if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f)
            {
                float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
                result = vertex2 + w * (vertex3 - vertex2); //Barycentric coordinates (0,1-w,w)
                return;
            }

            //P inside face region. Compute Q through its Barycentric coordinates (u,v,w)
            float denom = 1.0f / (va + vb + vc);
            float v2 = vb * denom;
            float w2 = vc * denom;
            result = vertex1 + ab * v2 + ac * w2; //= u*vertex1 + v*vertex2 + w*vertex3, u = va * denom = 1.0f - v - w
        }

        /// <summary>Determines the closest point between a box and a point(world space)</summary>
        /// <param name="matrix"></param>
        /// <param name="boxCenter"></param>
        /// <param name="boxSize"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 PointInBox(in Matrix4x4 matrix, in Vector3 boxCenter, in Vector3 boxSize, in Vector3 point)
		{
			Vector3 local = matrix.inverse.MultiplyPoint(point); // world to local
			Vector3 localClosest = PointInLocalBox(boxCenter, boxSize, local);
			return matrix.MultiplyPoint(localClosest); // local to world.
		}

		/***
		public static bool IsPointInBox(in Vector3 boxCenter, in Vector3 boxSize, in Quaternion rotation, in Vector3 point)
        {
			BoundBox box = new BoundBox(boxCenter, boxSize * 0.5f, rotation);
			Plane[] planes;
			box.GetPlane(out planes);
			foreach (Plane plane in planes)
				if (plane.GetSide(point))
					return true;
			return false;
		}

		public static Vector3 PointInBox(this BoundBox box, in Vector3 point)
        {
			Plane[] planes;
			// check out-side.
			box.GetPlane(out planes);
			foreach (Plane plane in planes)
				if (plane.GetSide(point))
				{
					plane.ClosestPointOnPlane(point);
				}
			// check in-side
			return point;
		}
		//**/

		/// <summary>Determines the closest point between a box and a point(local space)</summary>
		/// <param name="boxCenter"></param>
		/// <param name="boxSize"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static Vector3 PointInLocalBox(in Vector3 boxCenter, in Vector3 boxSize, in Vector3 point)
		{
			Vector3 half = boxSize * 0.5f;
			Vector3Extend.Max(point, boxCenter - half, out Vector3 tmp);
			Vector3Extend.Min(tmp, boxCenter + half, out Vector3 localClosest);
			return localClosest;
		}

		/// <summary>Locate the closest distance from point to line</summary>
		/// <param name="ray"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static float DistanceToPoint(in Ray ray, in Vector3 point)
        {
			return DistanceToPoint(ray.origin, ray.direction, point);
        }

		/// <summary>Locate the closest distance from point to line</summary>
		/// <param name="origin"></param>
		/// <param name="directionNormalize"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static float DistanceToPoint(in Vector3 origin, in Vector3 directionNormalize, in Vector3 point)
        {
			Debug.Assert(directionNormalize.IsNormalized(), "Require unit vector");
			return Vector3.Cross(directionNormalize, point - origin).magnitude;
        }


    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.Test
{
    public class TestCrossProduce : MonoBehaviour
    {
        public Transform point0 = null;
        public Transform point2 = null;

        private void OnDrawGizmos()
        {
            if (!point0 || !point2)
                return;
            Transform o = transform;
            Transform a = point0;
            Transform b = point2;
            Vector3 oa = a.position - o.position;
            Vector3 ob = b.position - o.position;


            GizmosExtend.DrawRay(o.position, oa, Color.blue);
            GizmosExtend.DrawRay(o.position, ob, Color.green);
            Vector3 oaob = Vector3.Cross(oa, ob);
            //GizmosExtend.DrawRay(o.position, oaob, Color.green);

            // Vector3 lhs = oa;
            Vector3 forward = oa;
            Vector3 up = ob;
            Vector3.OrthoNormalize(ref forward, ref up);
            // GizmosExtend.DrawRay(o.position.AppendY(1f), lhs, Color.magenta);
            GizmosExtend.DrawRay(o.position.AppendY(1f), forward, Color.cyan);
            GizmosExtend.DrawRay(o.position.AppendY(1f), up, Color.yellow);

            GizmosExtend.DrawLabel(a.position, "a");
            GizmosExtend.DrawLabel(b.position, "b");
            GizmosExtend.DrawLabel(o.position + oaob, $"a x b\nlen={oaob.magnitude:F2}");
        }
    }
}
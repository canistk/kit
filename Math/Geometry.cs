#define DRAW_DEBUG
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public static class Geometry
    {
        #region IsTriangleOrientedClockwise
        /// <summary>
        /// Test if the given points are clockwise order.
        /// </summary>
        /// <param name="p0">left vertex</param>
        /// <param name="p1">center vertex</param>
        /// <param name="p2">right vertex</param>
        /// <param name="normal">the given reference for plane's normal.</param>
        /// <returns></returns>
        public static bool IsTriangleOrientedClockwise(in Vector3 p0, in Vector3 p1, in Vector3 p2, in Vector3 normal)
        {
            var p01 = p0 - p1;
            var p21 = p2 - p1;
            var n = Vector3.Cross(p01, p21);
            return Vector3.Dot(n, normal) < 0f; // cross product - right hand rule
        }

        /// <summary>Test if the given points are clockwise order.</summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static bool IsTriangleOrientedClockwiseXZ(in Vector2 p0, in Vector2 p1, in Vector2 p2)
        {
            return IsTriangleOrientedClockwise(p0.CastXZ2XYZ(), p1.CastXZ2XYZ(), p2.CastXZ2XYZ(), Vector3.up);
        }

        /// <summary>Test if the given points are clockwise order.</summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static bool IsTriangleOrientedClockwiseXY(in Vector2 p0, in Vector2 p1, in Vector2 p2)
        {
            return IsTriangleOrientedClockwise(p0.CastXY2XYZ(), p1.CastXY2XYZ(), p2.CastXY2XYZ(), Vector3.back);
        }
        #endregion IsTriangleOrientedClockwise

        /// <summary>
        /// Triangulate convex polygon,
        /// PS: Assume the given points, are convex hull,
        /// concave polygon will be fail.
        /// </summary>
        /// <param name="convexHullpoints"></param>
        /// <returns></returns>
        public static List<Triangle> TriangulateConvexPolygon(List<Vector3> convexHullpoints)
        {
            List<Triangle> triangles = new List<Triangle>();

            for (int i = 2; i < convexHullpoints.Count; i++)
            {
                Vector3 a = convexHullpoints[0];
                Vector3 b = convexHullpoints[i - 1];
                Vector3 c = convexHullpoints[i];
                triangles.Add(new Triangle(a, b, c));
            }

            return triangles;
        }

        public static IEnumerable<Triangle> TriangulateConcavePolygon(params Vector3[] vertices)
        {
            if (vertices.Length < 3)
                throw new System.Exception("At least 3 point to draw triangle");
            if (!GeometryUtility.TryCreatePlaneFromPolygon(vertices, out var plane))
                yield break;

            Vector3 normal = plane.normal;
            foreach(var t in TriangulateConcavePolygon(normal, vertices))
            {
                yield return t;
            }
        }

        // WIP
        // https://www.habrador.com/tutorials/math/10-triangulation/
        public static IEnumerable<Triangle> TriangulateConcavePolygon(Vector3 normal, IList<Vector3> vertices)
        {
            if (vertices.Count < 3)
            {
                yield break;
            }
            if (vertices.Count == 3)
            {
                yield return new Triangle(vertices[0], vertices[1], vertices[2]);
                yield break;
            }

            // vertices = SortClockwise(normal, vertices);

            int anchor = 1;
            bool[] ear = new bool[vertices.Count];
            int totalEarCount = 0;
            int nextIdx()
            {
                if (vertices.Count - totalEarCount < 3)
                    throw new System.Exception("Handle this on higher level");
                //string log = string.Join(",", ear);
                int next = anchor;
                do
                {
                    if (++next >= vertices.Count)
                        next = 0;
                }
                while (ear[next]);
                return next;
            }
            int prevIdx()
            {
                if (vertices.Count - totalEarCount < 3)
                    throw new System.Exception("Handle this on higher level");
                //string log = string.Join(",", ear);
                int prev = anchor;
                do
                {
                    if (--prev < 0)
                        prev = vertices.Count - 1;
                }
                while (ear[prev]);
                return prev;
            }

            int times = 0;
            int earNoFoundCnt = 0;
            int earFoundCnt = 0;
            bool inverse = false;
            Vector3 sign = normal;
            while (vertices.Count > totalEarCount && 
                times < vertices.Count * 30) // dirty checking to prevent dead loop.
            {
                int remain = vertices.Count - totalEarCount;
                if (remain < 3)
                    yield break;
                if (earNoFoundCnt >= (int)remain)
                {
                    inverse         = !inverse;
                    earNoFoundCnt   = 0;
                    earFoundCnt     = 0;
                    sign            = inverse ? -normal : normal;
                }

                int prev    = prevIdx();
                int next    = nextIdx();
                var a       = vertices[prev];
                var b       = vertices[anchor];
                var c       = vertices[next];
                Vector3 n = normal * ((float)++times) * 2f;
                bool isClockwise = Geometry.IsTriangleOrientedClockwise(a, b, c, normal);
                if (remain == 3)
                {
#if DRAW_DEBUG
                    Color _color = inverse ? Color.magenta : Color.cyan;
                    DebugExtend.DrawTriangle(a + n, b + n, c + n, _color);
                    DebugExtend.DrawWireSphere(b + n, 0.1f, _color);
#endif
                    if (isClockwise)
                        yield return new Triangle(a, b, c);
                    else
                        yield return new Triangle(c, b, a);
                    yield break;
                }
                else if (isClockwise)
                {
#if DRAW_DEBUG
                    Color _color = inverse ? Color.magenta : Color.cyan;
                    DebugExtend.DrawTriangle(a + n, b + n, c + n,   _color);
                    DebugExtend.DrawWireSphere(b + n, 0.1f,         _color);
#endif
                    yield return new Triangle(a, b, c);
                    ear[anchor] = true;
                    earNoFoundCnt = 0;
                    ++earFoundCnt;
                    ++totalEarCount;
                }
                else
                {
#if DRAW_DEBUG
                    Color _color = inverse ? Color.red : Color.blue;
                    DebugExtend.DrawLine(a + n, b + n, _color);
                    DebugExtend.DrawLine(b + n, c + n, _color);
                    DebugExtend.DrawWireSphere(b + n, 0.1f, _color);
#endif
                    ++earNoFoundCnt;
                }

                if (inverse)
                {
                    anchor = prev;
                    if (earFoundCnt > 0)
                        anchor = prevIdx();
                }
                else
                {
                    anchor = next;
                    if (earFoundCnt > 0)
                        anchor = nextIdx();
                }
            }
        }


        /// <summary>
        /// Sort points in clockwise order, based on giving normal
        /// </summary>
        /// <param name="normal"></param>
        /// <param name="points"></param>
        /// <returns></returns>
        public static Vector3[] SortClockwise(Vector3 normal, IEnumerable<Vector3> points)
        {
            void InitSort(out List<ConvexHullVertex> list, out Vector3 centroid2d, out int bottomLeftIdx)
            {
                list = new List<ConvexHullVertex>(8);
                bool IsBottomLeft(Vector2 a, Vector2 b) => (a.y < b.y) || (a.y == b.y && a.x < b.x);
                // find inverse matrix based on giving normal.
                Matrix4x4 invMatrix = Matrix4x4.Inverse(Matrix4x4.TRS(Vector3.zero, Quaternion.LookRotation(normal), Vector3.one));
                list.Clear();
                bottomLeftIdx = -1;
                Vector2 sum = Vector2.zero;
                foreach (var point in points)
                {
                    // align z-axis perspective based on giving normal.
                    Vector3 p = invMatrix.MultiplyPoint3x4(Vector3.ProjectOnPlane(point, normal));
                    Vector2 projected2d = new Vector2(p.x, p.y); // ignore z-axis calculation
                    list.Add(new ConvexHullVertex(point, projected2d));
                    sum += projected2d;
                    int last = list.Count - 1;
                    if (bottomLeftIdx == -1 ||
                        IsBottomLeft(list[last].projected, list[bottomLeftIdx].projected))
                    {
                        bottomLeftIdx = last;
                    }
                }
                centroid2d = sum / (float)list.Count;
            }
            InitSort(out var list, out Vector3 centroid2d, out int bottomLeftIdx);

            Vector3[] GetResult()
            {
                Vector3[] rst = new Vector3[list.Count];
                for (int i = 0; i < list.Count; ++i)
                {
                    rst[i] = list[i].point;
                }
                return rst;
            }
            if (list.Count <= 1)
            {
                return GetResult();
            }

            list.Sort((a, b) => ConvexHullVertex.IsClockwise(centroid2d, a, b));
            return GetResult();
        }

        private class ConvexHullVertex
        {
            public Vector3 point;
            public Vector2 projected;
            public ConvexHullVertex(Vector3 point, Vector2 projected)
            {
                this.point = point;
                this.projected = projected;
            }

            /// <summary>based on centroid to compare two verties, in term of vector, 
            /// to define it's clockwise or anti-clockwise</summary>
            /// <param name="o">Centroid of Convex hull verties</param>
            /// <param name="lhs"></param>
            /// <param name="rhs"></param>
            /// <returns>1 = clockwise, -1 = anti-clockwise, 0 = same</returns>
            public static int IsClockwise(Vector2 centroid, ConvexHullVertex lhs, ConvexHullVertex rhs)
            {
                var a = lhs.projected;
                var b = rhs.projected;
                var o = centroid;
                if(a == b)
                    return 0;

                var rst = (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
                return rst > 0f ? 1 : -1;
            }
        }

        /// <summary>Calculate the surface area of triangle.</summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static float Area(Vector3 p0, Vector3 p1, Vector3 p2)
        {
            var points = new[] { p0, p1, p2 };
            const int n = 3;
            float A = 0;
            for (int p = n - 1, q = 0; q < n; p = q++) // note: p = last q
            {
                var pval = points[p];
                var qval = points[q];
                A += pval.x * qval.y - qval.x * pval.y;
                A += pval.y * qval.z - qval.y * pval.z;
                A += pval.z * qval.x - qval.z * pval.x;
            }
            return (Mathf.Abs(A) / 2f);
        }

        public static Vector3 Centroid(IList<Vector3> p)
        {
            var cnt = p.Count;
            var sum = Vector3.zero;
            for (var i = 0; i<cnt; i++)
            {
                sum += p[i];
            }
            var rst = sum / (float)cnt;
            return rst;   
        }

    }
}
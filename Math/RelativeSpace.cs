﻿using UnityEngine;

namespace Kit
{
	/// <summary>The relative space depend on "target",
	/// as long as the local offset not changed,
	/// the world offset will always depend on giving target.</summary>
	public struct RelativeSpace
	{
		public Transform target;
		public Vector3 localPosition;
		public Vector3 localForward;
		public Vector3 localUpward;
		public RelativeSpace(Transform transform, Vector3 worldPosition, Quaternion worldRotation) :
			this(transform, worldPosition, worldRotation * Vector3.forward, worldRotation * Vector3.up)
		{ }
		public RelativeSpace(Transform transform, Vector3 worldPosition, Vector3 worldForward, Vector3 worldUpward)
		{
			this.target = transform;
			localPosition = transform.InverseTransformPoint(worldPosition);
			localForward = transform.InverseTransformDirection(worldForward);
			localUpward = transform.InverseTransformDirection(worldUpward);
		}
		public Vector3 worldPosition => target.TransformPoint(localPosition);
		public Vector3 worldForward => target.TransformDirection(localForward);
		public Vector3 worldUpward => target.TransformDirection(localUpward);
		public Quaternion worldRotation => Quaternion.LookRotation(worldForward, worldUpward);
		public bool IsValid => target != null;
	}
}
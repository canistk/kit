using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;

public class TestBoundClosest : MonoBehaviour
{
    [SerializeField] BoxCollider m_Box = null;

    [SerializeField] CapsuleCollider m_Capsule = null;

    [SerializeField] SphereCollider m_Sphere = null;

    [SerializeField] Collider m_Collider = null;

    [SerializeField] Color m_ColorBound = Color.gray;
    [SerializeField] Color m_Color = Color.cyan;

    [SerializeField] Transform m_Position = null;

    private void OnDrawGizmos()
    {
        if (m_Box)
        {
            GizmosExtend.DrawBounds(m_Box.bounds, m_ColorBound);
            GizmosExtend.DrawCube(m_Box, m_Color);
            if (m_Position)
            {
                Vector3 closest = Closest.PointInBox(
                    m_Box.transform.localToWorldMatrix,
                    m_Box.center, m_Box.size,
                    m_Position.transform.position
                    );
                GizmosExtend.DrawPoint(closest, Color.red, 0.2f);
            }
        }
        if (m_Capsule)
        {
            GizmosExtend.DrawBounds(m_Capsule.bounds, m_ColorBound);
            GizmosExtend.DrawCapsule(m_Capsule, m_Color);
        }
        if (m_Sphere)
        {
            GizmosExtend.DrawBounds(m_Sphere.bounds, m_ColorBound);
            GizmosExtend.DrawWireSphere(m_Sphere, m_Color);
        }

        if (m_Collider)
        {
            GizmosExtend.DrawBounds(m_Collider.bounds, m_ColorBound);
            GizmosExtend.DrawCollider(m_Collider, m_Color);
        }
    }
}

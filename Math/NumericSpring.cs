﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	public static partial class MathKit
	{
		/// <summary>One dimensional numeric springing</summary>
		/// <param name="value">current value (input/output)</param>
		/// <param name="targetValue">target value (input)</param>
		/// <param name="velocity">object velocity (input/output)</param>
		/// <param name="zeta">Damping Ratio, between 0~1(input)</param>
		/// <param name="frequency">Oscillation Frequency in term of 2PI per cycle.(input)</param>
		/// <param name="delta">Time Step (input)</param>
		/// <remarks>
		/// In(2) & 1Hz - Natural logarithm of 2 (approximately 0.693)
		/// <see cref="https://en.wikipedia.org/wiki/Half-life"/>
		/// Implementation
		/// <see cref="http://box2d.org/files/GDC2011/GDC2011_Catto_Erin_Soft_Constraints.pdf"/>
		/// </remarks>
		public static void NumericSpring(ref float value, float targetValue, ref float velocity,
			float zeta, float frequency, float delta)
		{
			float omega = Mathf.PI * frequency;
			float factor = delta * 2f * zeta * omega + 1f;
			float o2 = omega * omega;
			float do2 = delta * o2;
			float d2o2 = delta * do2;
			float interval = 1f / (factor + d2o2);
			value = (factor * value + delta * velocity + d2o2 * targetValue) * interval;
			velocity = (velocity + do2 * (targetValue - value)) * interval;
		}

		/// <summary>Three dimensional numeric springing</summary>
		/// <param name="value">current value (input/output)</param>
		/// <param name="targetValue">target value (input)</param>
		/// <param name="velocity">object velocity (input/output)</param>
		/// <param name="zeta">Damping Ratio, between 0~1(input)</param>
		/// <param name="frequency">Oscillation Frequency in term of 2PI per cycle.(input)</param>
		/// <param name="delta">Time Step (input)</param>
		public static void NumericSpring(ref Vector3 value, Vector3 targetValue, ref Vector3 velocity,
			float zeta, float frequency, float delta)
		{
			float omega = Mathf.PI * frequency;
			float factor = delta * 2f * zeta * omega + 1f;
			float o2 = omega * omega;
			float do2 = delta * o2;
			float d2o2 = delta * do2;
			float interval = 1f / (factor + d2o2);
			value = (factor * value + delta * velocity + d2o2 * targetValue) * interval;
			velocity = (velocity + do2 * (targetValue - value)) * interval;
		}
		[System.Serializable]
		public class NumericSetting
		{
			[Range(0f,1f)] public float dampingRatio = 1f;
			[Range(0f, 1f)] public float angularFrequency = 1f;
			public float value { get; set; }
			public float velocity { get; set; }
		}

		public static void NumericSpring(NumericSetting numericSetting, float targetValue, float deltaTime)
		{
			float value = numericSetting.value;
			float velocity = numericSetting.velocity;
			NumericSpring(ref value, ref velocity, targetValue, numericSetting.dampingRatio, numericSetting.angularFrequency, deltaTime);
			numericSetting.value = value;
			numericSetting.velocity = velocity;
		}

		/// <summary>
		/// <seealso cref="http://allenchou.net/2015/04/game-math-precise-control-over-numeric-springing/?fbclid=IwAR2XHc5HeQHYTm3g1VnWoPmV3WlyIUuUebe1px264NAETyXELOUw6bnamB0"/>
		/// </summary>
		/// <param name="value"></param>
		/// <param name="velocity"></param>
		/// <param name="targetValue"></param>
		/// <param name="dampingRatio"></param>
		/// <param name="angularFrequency"></param>
		/// <param name="deltaTime"></param>
		public static void NumericSpring(ref float value, ref float velocity, float targetValue, float dampingRatio, float angularFrequency, float deltaTime)
		{
			float f = 1f + 2f * deltaTime * dampingRatio * angularFrequency;
			float oo = angularFrequency * angularFrequency;
			float hoo = deltaTime * oo;
			float hhoo = deltaTime * hoo;
			float detInv = 1f / (f + hhoo);
			float detX = f * value + deltaTime * velocity + hhoo * targetValue;
			float detV = velocity + hoo * (targetValue - value);
			value = detX * detInv;
			velocity = detV * detInv;
		}


		/// <summary>Rotation numeric springing</summary>
		/// <param name="value">current value (input/output)</param>
		/// <param name="targetValue">target value (input)</param>
		/// <param name="velocity">object velocity (input/output)</param>
		/// <param name="zeta">Damping Ratio, between 0~1(input)</param>
		/// <param name="frequency">Oscillation Frequency in term of 2PI per cycle.(input)</param>
		/// <param name="delta">Time Step (input)</param>
		[System.Obsolete("This method didn't work, wait until we find the solution.", true)]
		public static void NumericSpring(ref Quaternion value, Quaternion targetValue, ref Vector3 velocity,
			float zeta, float frequency, float delta)
		{
			if (value.Equals(targetValue))
				return;

			Vector3 valueEuler = value.eulerAngles;
			Vector3 targetEuler = targetValue.eulerAngles;
			float omega = Mathf.PI * frequency;
			float factor = delta * 2f * zeta * omega + 1f;
			float o2 = omega * omega;
			float do2 = delta * o2;
			float d2o2 = delta * do2;
			float interval = 1f / (factor + d2o2);
			NumericSpring(ref valueEuler, targetEuler, ref velocity, zeta, frequency, delta);
			value = Quaternion.Euler(valueEuler);
			/// Note: since Quaternion didn't have unique rotate order, 
			/// we didn't know following.
			/// 1) where to start
			/// 2) can't overshoot at the end
			/// 3) hard to measure the step we should move this frame.
		}
	}
}
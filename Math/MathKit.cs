﻿using System.Collections;
using System.Collections.Generic;
using Math = System.Math;
using Mathf = UnityEngine.Mathf;

namespace Kit
{
	public static partial class MathKit
	{
		/// <summary>The value for which all absolute numbers smaller than are considered equal to zero.</summary>
		public const float ZeroTolerance = 1e-6f; // Value a 8x higher than 1.19209290E-07F

		/// <summary>A value specifying the approximation of Pi which is 180 degrees.</summary>
		public const float Pi = (float)Math.PI;

		/// <summary>A value specifying the approximation of 2Pi which is 360 degrees.</summary>
		public const float TwoPi = (float)(2 * Math.PI);

		/// <summary>A value specifying the approximation of Pi/2 which is 90 degrees.</summary>
		public const float PiOverTwo = (float)(Math.PI / 2);

		/// <summary>A value specifying the approximation of Pi/4 which is 45 degrees.</summary>
		public const float PiOverFour = (float)(Math.PI / 4);

		/// <summary>Determines whether the specified value is close to zero (0.0f).</summary>
		/// <param name="f">The floating value.</param>
		/// <returns><c>true</c> if the specified value is close to zero (0.0f); otherwise, <c>false</c>.</returns>
		public static bool IsZero(float f)
		{
			return Math.Abs(f) < ZeroTolerance;
		}

		/// <summary>Determines whether the specified value is close to one (1.0f).</summary>
		/// <param name="f">The floating value.</param>
		/// <returns><c>true</c> if the specified value is close to one (1.0f); otherwise, <c>false</c>.</returns>
		public static bool IsOne(float f)
		{
			return IsZero(f - 1f);
		}

		/// <summary>
		/// Quadratic Equation Solver, try to locate root of giving equation.
		/// Solve  Ax^2 + Bx + C = 0 given B*B-4*A*C >= 0
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection"/>
		/// <see cref="https://pages.mtu.edu/~shene/COURSES/cs201/NOTES/chap03/quad-2.html"/>
		/// <see cref="https://www.youtube.com/watch?v=VdVPg04t_6w"/>
		/// </summary>
		/// <param name="a">Ax^2 take A</param>
		/// <param name="b">Bx take B</param>
		/// <param name="c">C take C</param>
		/// <param name="root0">return root</param>
		/// <param name="root1">return root</param>
		/// <returns></returns>
		public static bool SolveQuadratic(float a, float b, float c, ref float root0, ref float root1)
		{
			float discr = b * b - 4f * a * c;
			if (discr < 0f)
				return false; // No root
			else if (discr == 0)
				root0 = root1 = -0.5f * b / a;
			else
			{
				float q = (b > 0f) ?
					-.5f * (b + Mathf.Sqrt(discr)) :
					-.5f * (b - Mathf.Sqrt(discr));
				root0 = q / a;
				root1 = c / q;
			}
			// compute smallest distance value of intersection
			if (root0 > root1)
				Swap(ref root0, ref root1);
			return true;
		}


		public static void Swap<T>(ref T x, ref T y)
		{
			T tmp = x;
			x = y;
			y = tmp;
		}


		/// <summary>Convert Dot product back to degree angle.</summary>
		/// <param name="dot"></param>
		/// <returns></returns>
		public static float DotToDegree(this float dot)
		{
			return Mathf.Acos(dot) * Mathf.Rad2Deg;
		}

		/// <summary>
		/// Convert degree angle to dot product
		/// </summary>
		/// <param name="degree"></param>
		/// <returns></returns>
		public static float DegreeToDot(this float degree)
        {
			return Mathf.Cos(degree * Mathf.Deg2Rad);
        }
	}
}

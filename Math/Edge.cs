using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public struct Edge
    {
        public Vector3[] p;
        public Edge(Vector3 p0, Vector3 p1)
        {
            p = new Vector3[2] { p0, p1 };
        }
    }
}
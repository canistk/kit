using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit.GPUCollision;
using Unity.Collections;
using System.Runtime.InteropServices;
using Kit;

[ExecuteInEditMode]
public class TestOBB : MonoBehaviour
{
    [SerializeField] BoxCollider m_BoxA = null;
    [SerializeField] BoxCollider m_BoxB = null;
    [SerializeField] ComputeShader m_ComputeShader = null;

    GraphicsBuffer m_Buffer = null;
    kCube[] m_Data = null;
    int m_KernelIndex = -1;
    int m_InstanceCount;

    private void OnEnable()
    {
        if (m_ComputeShader == null)
        {
            gameObject.SetActive(false);
            return;
        }

        if (m_ComputeShader && TryInitBuffer(out m_Buffer, out m_InstanceCount))
        {
            if (!m_ComputeShader.HasKernel("GpuMesh"))
            {
                Debug.LogError("No Kernel found.");
                return;
            }

            m_KernelIndex = m_ComputeShader.FindKernel("GpuMesh");
            m_ComputeShader.SetBuffer(m_KernelIndex, "cubeBuffer", m_Buffer);
        }
    }

    private void OnDisable()
    {
        m_Buffer?.Dispose();
        m_Buffer = null;
    }

    void Update()
    {
        return;
        if (m_BoxA == null || m_BoxB == null)
            return;
        if (m_ComputeShader == null)
            return;
        if (m_KernelIndex == -1)
            return;
        m_ComputeShader.GetKernelThreadGroupSizes(m_KernelIndex, out var x, out var y, out var z);
        var threadGroupX = (int) (m_InstanceCount / x);
        m_ComputeShader.Dispatch(m_KernelIndex, threadGroupX, 1, 1);
        m_Buffer.GetData(m_Data);
    }

    private void OnDrawGizmos()
    {
        if (m_BoxA == null || m_BoxB == null)
            return;

        OBBIntersect(m_BoxA, m_BoxB);
    }

    bool TryInitBuffer(out GraphicsBuffer buffer, out int arrayCount)
    {
        buffer = null;
        arrayCount = -1;
        if (m_BoxA == null || m_BoxB == null)
            return false;

        m_Data = new kCube[]
        {
            (kCube)m_BoxA,
            (kCube)m_BoxB,
        };
        arrayCount = m_Data.Length;

        using (var arr = new NativeArray<kCube>(m_Data, Allocator.Temp))
        {
            buffer = new GraphicsBuffer(GraphicsBuffer.Target.Structured, arr.Length, Marshal.SizeOf<kCube>());
            buffer.SetData(arr);
        }

        return buffer != null;
    }

    bool OBBIntersect(kCube obb1, kCube obb2)
    {
        // transform obb2 center point into obb1 local space.
        Vector3[] vertices;
        {
            var c = obb2.Center;
            var l = obb2.HalfExtents;
            var v = new Vector3[8];
            var i = 0;
            v[i++] = c + new Vector3( l.x,  l.y,  l.z); // RTF
            v[i++] = c + new Vector3(-l.x,  l.y,  l.z); // LTF
            v[i++] = c + new Vector3( l.x, -l.y,  l.z); // RDF
            v[i++] = c + new Vector3(-l.x, -l.y,  l.z); // LDF
            v[i++] = c + new Vector3( l.x,  l.y, -l.z); // RTB
            v[i++] = c + new Vector3(-l.x,  l.y, -l.z); // LTB
            v[i++] = c + new Vector3( l.x, -l.y, -l.z); // RDB
            v[i++] = c + new Vector3(-l.x, -l.y, -l.z); // LDB
            vertices = v;
        }

        // calculate obb2 world space
        var obb2world = obb2.LocalToWorld.MultiplyPoint(obb2.Center);
        // GizmosExtend.DrawSphere(oob2world, 0.05f, Color.red.CloneAlpha(0.5f));
        var oob2worldVertices = new Vector3[8];
        for (int i=0; i<vertices.Length; ++i)
        {
            oob2worldVertices[i] = obb2.LocalToWorld.MultiplyPoint(vertices[i]);
            // GizmosExtend.DrawSphere(oob2worldVertices[i], 0.05f, Color.red.CloneAlpha(0.3f));
        }

        // Project Obb2 into Obb1 local space
        var w2l = obb1.LocalToWorld.inverse;
        Vector3 obb2Local = w2l.MultiplyPoint(obb2world);
        // GizmosExtend.DrawSphere(obb2Local, 0.05f, Color.yellow.CloneAlpha(0.5f));
        Vector3[] o2in1L = new Vector3[8];
        for(int i=0; i< oob2worldVertices.Length; ++i)
        {
            o2in1L[i] = w2l.MultiplyPoint(oob2worldVertices[i]);
            // GizmosExtend.DrawSphere(o2in1L[i], 0.05f, Color.yellow.CloneAlpha(0.5f));
        }


        bool isCollision = false;
        var b = new Bounds(obb1.Center, obb1.HalfExtents * 2f);
        for (int i=0; i< o2in1L.Length; ++i)
        {
            if (b.Contains(o2in1L[i]))
            {
                isCollision = true;
                GizmosExtend.DrawSphere(o2in1L[i], 0.1f, Color.red.CloneAlpha(0.8f));
            }
            else
            {
                GizmosExtend.DrawSphere(o2in1L[i], 0.1f, Color.gray.CloneAlpha(0.3f));
            }
        }
        // GizmosExtend.DrawSphere(obb2Local, 0.3f, Color.yellow.CloneAlpha(0.5f));

        return isCollision;
        
    }
}

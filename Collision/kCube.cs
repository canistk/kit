using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.GPUCollision
{
    /// <summary>
    /// <see cref="GraphicsBuffer"/>
    /// </summary>
    public struct kCube
    {
        public Vector3 Center;
        public Vector3 HalfExtents;
        public Matrix4x4 LocalToWorld;


        public static implicit operator kCube(BoxCollider b)
        {
            return new kCube
            {
                Center       = b.center, //.bounds.center,
                HalfExtents  = b.size * 0.5f, //.bounds.extents,
                LocalToWorld = b.transform.localToWorldMatrix,
            };
        }
    }
}
﻿using System.Collections;
using UnityEngine;
#if REWIRED
using Rewired;
#endif
namespace Kit
{
	[DisallowMultipleComponent]
	public sealed class CursorManager : Singleton<CursorManager, RemoveLateComer, SearchHierarchy>
	{
		[Header("Default status")]
		[SerializeField] bool m_Visible = false;
		[Header("Cursor (optional)")]
		[SerializeField] Texture2D m_CursorImage = null;
		[SerializeField] Vector2 m_HotSpot = Vector2.zero;
		[SerializeField] CursorMode m_CursorMode = CursorMode.Auto;

#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_LINUX)
		/// <summary> confined cursor lock mode is only supported on the standalone player platform on Windows and Linux
		/// <see cref="https://docs.unity3d.com/ScriptReference/CursorLockMode.Confined.html"/></summary>
		private readonly CursorLockMode _lockMode = CursorLockMode.Confined;
#else
		private readonly CursorLockMode _lockMode = CursorLockMode.Locked;
#endif

		/// <summary>
		/// Auto locker depended on <see cref="object"/> status
		/// </summary>
		public static HashLock<object> m_Locker = new HashLock<object>(true);

		protected override void Awake()
		{
			base.Awake();
			if (!Input.mousePresent)
			{
				enabled = false;
				return;
			}

			Cursor.visible = m_Visible;
			if (!m_Visible)
			{
				Cursor.lockState = _lockMode;
			}
		}

		private IEnumerator Start()
		{
#if REWIRED
			yield return new WaitUntil(() => ReInput.isReady);
#else
			yield return null;
#endif
			if (m_CursorImage != null)
				Cursor.SetCursor(m_CursorImage, m_HotSpot, m_CursorMode);

			Cursor.visible = m_Visible;
			Cursor.lockState = _lockMode;
		}

		private void LateUpdate()
		{
			if (Cursor.visible && m_Locker.IsLocked)
			{
				Cursor.visible = false;
				Cursor.lockState = _lockMode;
			}
			else if (!Cursor.visible &&
#if REWIRED
					 ControllerType.Mouse == ReInput.controllers.GetLastActiveControllerType() &&
#endif
					!m_Locker.IsLocked
					)
			{
				// not display but moved, shall we display ?
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
			// else, not display but locked, we keep that lock
			// else, not display but moved/not moved, we don't care
		}
	}
}
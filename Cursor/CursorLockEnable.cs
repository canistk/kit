﻿using UnityEngine;

namespace Kit
{
	public class CursorLockEnable : MonoBehaviour
	{
		private void OnEnable()
		{
			CursorManager.m_Locker.AcquireLock(this);
		}

		private void OnDisable()
		{
			CursorManager.m_Locker.ReleaseLock(this);
		}
	}
}
﻿using UnityEngine;

namespace Kit
{
	/// <summary>
	/// The motion fragment capture 2 frame's coordinate and velocity information.
	/// pre-calculate common factor for complex calculation afterward.
	/// </summary>
	public struct MotionFragment
	{
		public static readonly MotionFragment None = default;

		#region Public API
		public readonly bool Used;

		/// <summary>The snap shot for previous transform information</summary>
		public readonly TransformCache from;
		/// <summary>The snap shot for latest transform information</summary>
		public readonly TransformCache to;
		
		/// <summary>The <see cref="Time.deltaTime"/>, used for calculation.</summary>
		public readonly float deltaTime;

		/// <summary>The bias ground normal used to calcualte the local movement</summary>
		public readonly Vector3 biasGroundNormal;

		/// <summary>The forward before motion</summary>
		public readonly Vector3 fromForward;
		
		/// <summary>The forward after motion</summary>
		public readonly Vector3 toForward;

		/// <summary>The motion between last and current position</summary>
		public readonly Vector3 motionVector;

		/// <summary>The motion vector based on local space <see cref="motionVector"/></summary>
		public readonly Vector3 localMotionVector;

		/// <summary>The motion vector project on <see cref="biasGroundNormal"/></summary>
		public readonly Vector3 motionVectorIgnoreY;

		/// <summary>The local motion vector project on <see cref="biasGroundNormal"/></summary>
		public readonly Vector3 localMotionVectorIgnoreY;

		/// <summary>The motion direction (Normalize)</summary>
		public readonly Vector3 motionDirection;

		/// <summary>The moving direction on local space</summary>
		public readonly Vector3 localMotionDirection;

		public readonly Vector3 localMotionIgnoreYNormal;

		/// <summary>The distance moved within this period,
		public readonly float motionDistance;

		/// <summary>The distance moved within this period,
		/// Ignore Y - (local space), are based on <see cref="biasGroundNormal"/>.</summary>
		public readonly float motionDistanceIgnoreY;

		/// <summary>The distance moved within this period,
		/// Ignore Y - (local space), are based on <see cref="biasGroundNormal"/>.</summary>
		public readonly float localMotionDistanceIgnoreY;

		/// <summary>The speed moved from <see cref="from.Position"/> to current position.
		/// calculate by engine <see cref="Time.deltaTime"/></summary>
		public readonly float motionSpeed;

		/// <summary>The speed project on <see cref="biasGroundNormal"/> as floor,
		/// and separate into horizontal direction (XZ)</summary>
		public readonly float localMotionSpeedXZ;

		/// <summary>The speed project on <see cref="biasGroundNormal"/> itself
		/// and extract the vertical direction (Y)</summary>
		public readonly float localMotionSpeedY;

		/// <summary>Velocity - The <see cref="motionSpeed"/> represent in vector</summary>
		public readonly Vector3 motionVelocity;

		/// <summary>Velocity - The <see cref="motionSpeed"/> represent in vector
		/// Ignore Y - (local space), are based on <see cref="biasGroundNormal"/></summary>
		public readonly Vector3 localMotionVelocityXZ;

		/// <summary>The steeringRotation between <see cref="fromRotation"/> and current rotation,
		/// <seealso cref="Quaternion.FromToRotation(Vector3, Vector3)"/></summary>
		public readonly Quaternion steeringRotation;

		/// <summary>The different between <see cref="fromForward"/> and current forward</summary>
		public readonly float steeringAngle;

		/// <summary>The angularVelocity not the unity's format.</summary>
		public readonly float angularVelocity;

		/// <summary>The bias angularVelocity unity's.</summary>
		public readonly Vector3 biasAngularVelocity;

		/// <summary>The angle between
		/// moving direction <see cref="motionVector"/>
		/// and actually facing direction <see cref="Transform.forward"/> 
		/// 0 = align forward, -90 = left, +90 = right, -180/180 = backward</summary>
		public readonly float angleBetweenMotionLookAt;

		/// <summary>0 = forward, Normalize -180~180 degree to -1f ~ 1f</summary>
		public readonly float angleBetweenMotionLookAtNormalize;
		#endregion // Public API

		/// <summary>To define the motion from origin to destination</summary>
		/// <param name="fromPostion"></param>
		/// <param name="fromRotation"></param>
		/// <param name="toPosition"></param>
		/// <param name="toRotation"></param>
		/// <param name="biasGroundNormal"></param>
		/// <param name="deltaTime"></param>
		public MotionFragment(Vector3 fromPostion, Quaternion fromRotation, Vector3 toPosition, Quaternion toRotation, Vector3 biasGroundNormal, float deltaTime)
			: this(new TransformCache(fromPostion, fromRotation), new TransformCache(toPosition, toRotation), biasGroundNormal, deltaTime) { }


		/// <summary>To define the motion from origin to destination</summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="biasGroundNormal"></param>
		/// <param name="_deltaTime"></param>
		public MotionFragment(TransformCache from, TransformCache to, Vector3 biasGroundNormal, float _deltaTime)
		{
			if (_deltaTime <= 0f)
				throw new System.Exception("Invalid time flow for calculate.");

			// incoming data
			this.from = from;
			this.to = to;
			this.deltaTime = _deltaTime * Time.timeScale;
			this.biasGroundNormal = biasGroundNormal;

			// motion's
			motionVector = this.to.position - this.from.position;
			if (this.from.rotation.IsInvalid())
				localMotionVector = motionVector;
			else
			{
				// localMotionVector = Matrix4x4.Rotate(this.from.rotation).inverse.MultiplyVector(motionVector);
				localMotionVector = this.from.rotation.Inverse() * motionVector;
			}

			// distance first, we have ZERO cases.
			motionDistance = motionVector.magnitude;
			if (motionDistance == 0f)
			{
				// didn't moved, in this case we can't calculate movement direction.
				// Fallback : align to current forward.
				motionDirection = localMotionDirection = localMotionIgnoreYNormal = this.to.rotation.GetForward(); // Gain
				motionVectorIgnoreY = localMotionVectorIgnoreY = Vector3.zero; // Gain
				motionDistanceIgnoreY = localMotionDistanceIgnoreY = 0f; // Gain
			}
			else
			{
				motionDirection = motionVector / motionDistance; // Gain - quick normalize since we have distance.
				localMotionDirection = localMotionVector / motionDistance; // Gain - normalize;
				localMotionIgnoreYNormal = new Vector3(localMotionDirection.x, 0f, localMotionDirection.z).normalized;
				motionVectorIgnoreY = Vector3.ProjectOnPlane(motionVector, biasGroundNormal);
				motionDistanceIgnoreY = motionVectorIgnoreY.magnitude;
				localMotionVectorIgnoreY = new Vector3(localMotionVector.x, 0f, localMotionVector.z); // Gain - no calculation
				localMotionDistanceIgnoreY = new Vector2(localMotionVector.x, localMotionVector.z).magnitude; // Gain - Vector2
			}

			// Speed & velocity
			motionSpeed = motionDistance / deltaTime;
			motionVelocity = motionDistance > 0f ?
				motionDirection * motionSpeed :
				Vector3.zero;
			localMotionSpeedXZ = motionDistanceIgnoreY / deltaTime;
			localMotionVelocityXZ = motionDistanceIgnoreY > 0f ?
				localMotionIgnoreYNormal * localMotionSpeedXZ :
				Vector3.zero;
			// math hack, since local's Y is actually moved distance along Y axis with correct sign.
			// cost was paid on previous matrix convertion, now we gain some back.
			/*
			// full version
			Vector3 y = Vector3.Project(motionVector, biasGroundNormal);
			localMotionSpeedY = y.magnitude / deltaTime * Vector3.Dot(y.normalized, biasGroundNormal);
			*/
			localMotionSpeedY = localMotionVector.y / deltaTime; // Gain

			// Rotation
			fromForward = this.from.rotation * Vector3.forward;
			toForward = this.to.rotation * Vector3.forward;

			steeringAngle = Vector3.SignedAngle(fromForward, toForward, this.biasGroundNormal);
			//steeringRotation = from.rotation.Inverse() * to.rotation;
			steeringRotation = Quaternion.FromToRotation(fromForward, toForward);

			steeringRotation.ToAngleAxis(out float angle, out Vector3 axis);
			float angular = angle / deltaTime;
			angularVelocity = angular;
			
			{
				Vector3 fUp = from.rotation * Vector3.up;
				Vector3 fForward = from.rotation * Vector3.forward;
				Vector3 fRight = from.rotation * Vector3.right;
				Vector3 tForward = to.rotation * Vector3.forward;
				Vector3 tUp = to.rotation * Vector3.up;
                biasAngularVelocity = new Vector3
                (
					fForward.AngleProjectOnPlaneSigned(tForward, fRight),
					fForward.AngleProjectOnPlaneSigned(tForward, fUp),
					fUp.AngleProjectOnPlaneSigned(tUp, fForward)
                );
			}

			if (motionDirection == Vector3.zero)
			{
				angleBetweenMotionLookAtNormalize = angleBetweenMotionLookAt = 0f;
			}
			else
			{
				angleBetweenMotionLookAt = Vector3.SignedAngle(motionDirection, toForward, this.biasGroundNormal);
				angleBetweenMotionLookAtNormalize = angleBetweenMotionLookAt / 180f;
			}

			Used = true;
		}
	}

	/// <summary>
	/// Based on <see cref="MotionFragment"/>
	/// assume that fragment is the anchor in world space.
	/// to calculate the motion in certain period.
	/// </summary>
	public class MotionAnchor
    {
		public readonly float			startTime;
        public readonly float			duration;
        public readonly MotionFragment	initMotion;
		public readonly Vector3			upward;
		public readonly Vector3			forward;
		public Vector3					origin => initMotion.to.position;
		public float					initXZSpeed => initMotion.localMotionSpeedXZ;
		public float					initYSpeed => initMotion.localMotionSpeedY;


        public float	movedY;
        public float	movedXZ;
        public int		frameCount;
        public bool		completed;

        public MotionAnchor(MotionFragment anchor, float duration)
			: this(anchor, anchor.to.rotation * Vector3.up, duration)
        { }

        public MotionAnchor(MotionFragment anchor, Vector3 biasGroundNormal, float duration)
        {
            this.initMotion = anchor;

            this.startTime = Time.time;
            this.duration = duration;

			this.upward = biasGroundNormal;
			this.forward = Vector3.ProjectOnPlane(anchor.motionVector, this.upward);
			if (forward != Vector3.zero)
				forward.Normalize();

            movedY = movedXZ = 0f;
            frameCount = -1;
            completed = false;
        }

        public MotionAnchor(float startTime, float duration, MotionFragment initMotion, Vector3 upward, Vector3 forward, float movedY, float movedXZ, int frameCount, bool completed)
        {
            this.startTime = startTime;
            this.duration = duration;
            this.initMotion = initMotion;
            this.upward = upward;
            this.forward = forward;
            this.movedY = movedY;
            this.movedXZ = movedXZ;
            this.frameCount = frameCount;
            this.completed = completed;
        }

        public float CalculateDiffByCurve(AnimationCurve curve01, float pt, float maxValue, ref float movedValue)
        {
            float expect = curve01.Evaluate(pt) * maxValue;
            float diff = expect - movedValue;
            movedValue += diff;
            return diff;
        }

		public float timePass => Time.time - startTime;

        public void CalculateDiff(in HumanMotionConfig config, out Vector3 deltaMotion)
        {
			deltaMotion = default(Vector3);
			if (frameCount == Time.frameCount)
				return;
			if (completed)
				return;
			frameCount = Time.frameCount;

			float _timePass = timePass;
			float pt = Mathf.Clamp01(_timePass / duration);

			// when true, skip the rest on next cycle.
			completed = _timePass > duration;

			float yDiff		= CalculateDiffByCurve(config.curveY01, pt, config.maxYDistance, ref movedY);
			float xzDiff	= CalculateDiffByCurve(config.curveXZ01, pt, config.maxXZDistance, ref movedXZ);

			deltaMotion = upward * yDiff + forward * xzDiff;
		}
    }

	/// <summary>
	/// the common 3D motion for humanoid character.
	/// assume following
	/// Y is the local Y-axis
	/// xz as the steering projected on Y-plane.
	/// </summary>
	[System.Serializable]
	public class HumanMotionConfig
    {
        [Tooltip("The curve to adjust Y-axis movement")]
		[RectRange] public AnimationCurve	curveY01 = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
		[Min(0f)] public float				maxYDistance = 2f;
		[Tooltip("The curve to adjust XZ-axis movement, as projected on Y-axis plane")]
		[RectRange] public AnimationCurve	curveXZ01 = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);
		[Min(0f)] public float				maxXZDistance = 2f;

		public HumanMotionConfig(
			AnimationCurve curveY01, float maxYDistance,
			AnimationCurve curveXZ01, float maxXZDistance)
        {
			this.curveY01			= curveY01;
			this.curveXZ01			= curveXZ01;
			this.maxYDistance		= maxYDistance;
			this.maxXZDistance		= maxXZDistance;
		}

		public HumanMotionConfig() { }
    }
}
﻿using System.Collections;
using UnityEngine;

namespace Kit
{
    [System.Serializable]
    public struct ShakeParams
    {
        public static readonly AnimationCurve Bell = new AnimationCurve
        {
            keys = new[]{
                new Keyframe(0f, 0f, 0f, 5f),
                new Keyframe(0.2f, 1f, 0f, 0f),
                new Keyframe(1f, 0f, -0.2f, 0f)
            },
        };
        public static readonly ShakeParams None = default;
        public static readonly ShakeParams Default = new ShakeParams
        {
            attenuationCurve    = Bell,
            duration            = 1f,
            speed               = 1f,
            ignoreTimeScale		= false,
            magnitude           = 1f,
            narrowRangeX        = 1f,
            narrowRangeY        = 1f,
            narrowRangeZ        = 1f,
            angular             = 0f,
            pitchX              = 1f,
            yawY                = 1f,
            rollZ               = 1f,
            randomSeed          = false,
            seed                = 0,
        };

        [RectRange, Tooltip("How shake factor smooth apply on target based on giving time value reference.")]
        public AnimationCurve   attenuationCurve;
        public float            GetAttenuation(float pt) => attenuationCurve == null ? 1f : attenuationCurve.Evaluate(pt);
        public float            duration;
        public float            speed;
        public bool             ignoreTimeScale;

        [Header("Random")]
        public bool             randomSeed;
        public int              seed;
        public int              GetSeed() => randomSeed ? Random.Range(int.MinValue, int.MaxValue) : seed;

        [Min(0f), Header("Restricted range, by local axis")]
        public float            magnitude;
        [Range(0f, 1f), Tooltip("0f = mask/narrow axis movement based on directionRef, 1f = not narrow.")]
        public float            narrowRangeX;
        [Range(0f, 1f), Tooltip("0f = mask/narrow axis movement based on directionRef, 1f = not narrow.")]
        public float            narrowRangeY;
        [Range(0f, 1f), Tooltip("0f = mask/narrow axis movement based on directionRef, 1f = not narrow.")]
        public float            narrowRangeZ;
        public Vector3          narrowRange => new Vector3(narrowRangeX, narrowRangeY, narrowRangeZ);

        [Min(0f), Header("Restricted rotation, by axis normal")]
        public float            angular;
        [Range(0f, 1f)]
        public float            pitchX;
        [Range(0f, 1f)] 
        public float            yawY;
        [Range(0f, 1f)] 
        public float            rollZ;
        public Vector3          angleRatio => new Vector3(pitchX, yawY, rollZ);
    }
    public class Shakeable : MonoBehaviour
    {
#pragma warning disable CS0414
        [ContextButton(nameof(Editor_Shake))] 
        [SerializeField] bool m_Debug = false;
#pragma warning restore CS0414
        [SerializeField] Vector3 m_DebugDirection01 = Vector3.zero;
        [SerializeField] Transform m_DebugDirection = null;
        public Vector3 GetForceDir()
        {
            return m_DebugDirection == null ?
                m_DebugDirection01 :
                m_DebugDirection.forward;
        }

        [Space]
        [Header("Shake preset")]
        [SerializeField] Transform m_ShakeTarget = null;
        [SerializeField] ShakeParams m_ShakeParams = ShakeParams.Default;

        #region System
        protected void Reset()
        {
            m_ShakeTarget = transform;
        }

        private void OnValidate()
        {
            if (m_Debug)
            {
                m_Debug = false;
                Editor_Shake();
            }
        }

        private void Update()
        {
            HandleShakeing();
        }

        protected void OnDisable()
        {
            Cancel();
        }
        #endregion System

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        private void Editor_Shake() => Shake();

        /// <summary>Shake by preset config.</summary>
        public void Shake()
        {
            Shake(m_ShakeTarget, GetForceDir(), m_ShakeParams);
        }

        private ShakeTask m_ShakeTask;
        public void Shake(Transform obj, Vector3 force, ShakeParams config)
        {
            if (!Application.isPlaying)
                return;
            Cancel();
            m_ShakeTask = new ShakeTask(obj, force, config);
        }

        private void HandleShakeing()
        {
            if (m_ShakeTask != null)
                m_ShakeTask.Update();
        }

        public void Cancel()
        {
            if (m_ShakeTask != null)
            {
                m_ShakeTask.Dispose();
                m_ShakeTask = null;
            }
        }
    }

    /// <summary>
    /// A helper class to handle calculate shake pattern.
    /// design to be call every update <see cref="Update"/>
    /// </summary>
    /// <example><see cref="Shakeable"/></example>
    public class ShakeTask : System.IDisposable
    {
        private static readonly Vector3 Vector3zero = Vector3.zero;
        #region Helper Class
        private struct PosRot
        {
            public static readonly PosRot identity = new PosRot { position = Vector3.zero, rotation = Quaternion.identity };
            public Vector3 position;
            public Quaternion rotation;

            public void Copy(Transform t)
            {
                position = t.position;
                rotation = t.rotation;
            }
            public static implicit operator PosRot(Transform t)
            {
                return new PosRot
                {
                    position = t.position,
                    rotation = t.rotation,
                };
            }
            public void Assign(Transform self)
            {
                self.position = position;
                self.rotation = rotation;
            }
        }

        private struct PerlinNoiseSampler
        {
            private float anchor;
            public PerlinNoiseSampler(int seed)
            {
                var rnd = new System.Random(seed);
                anchor = (float)rnd.NextDouble();
            }

            public Vector3 GetPerlinNoise(float speed, float time, float attenuation)
            {
                var s = time * speed;
                var sx = Mathf.PerlinNoise(anchor + s, anchor);
                var sy = Mathf.PerlinNoise(anchor, anchor + s);
                var sz = Mathf.PerlinNoise(anchor + s, anchor + s);

                // remap 0 ~ 1 into -1 ~ 1
                return new Vector3(
                    (sx * 2f - 1f) * attenuation,
                    (sy * 2f - 1f) * attenuation,
                    (sz * 2f - 1f) * attenuation);
            }
        }
        #endregion Helper Class

        #region Variables
        public Transform obj                { get; private set; }
        private Matrix4x4                   objWorldToLocal;
        private Quaternion                  forceQuat;
        private float                       startTime;
        private float                       endTime               => startTime + shake.duration;

        private Vector3                     force;
        private ShakeParams                 shake;
        private PerlinNoiseSampler          sampler;
        private PosRot                      beforeShake;
        private PosRot                      beforeShakeLocal;

        #endregion Variables

        #region Constructor
        public ShakeTask(Transform obj, Vector3 force, ShakeParams shake)
        {
            this.obj                = obj;
            this.force              = force;
            this.shake              = shake;
            this.beforeShake        = obj;
            this.beforeShakeLocal   = new PosRot { position = obj.localPosition, rotation = obj.localRotation };

            if (obj == null)
                throw new System.NullReferenceException("shake target can not be null");

            if (shake.duration <= 0f)
                throw new System.Exception("Invalid duration, shake movement require duration");

            sampler                 = new PerlinNoiseSampler(shake.GetSeed());
            objWorldToLocal         = obj.worldToLocalMatrix;
            forceQuat               = Quaternion.LookRotation(force, Vector3.up);
            startTime               = GetTime();
        }
        #endregion Constructor

        private float GetTime()
        {
            return shake.ignoreTimeScale    ? 
                Time.realtimeSinceStartup   :
                Time.timeSinceLevelLoad     ;
        }

        public void Update()
        {
            Calculate(
                p => obj.localPosition = p,
                r => obj.localRotation = r);
        }

        public void Calculate(System.Action<Vector3> localPosCallback, System.Action<Quaternion> localRotCallback)
        {
            if (isDisposed)
                return;
            var curTime = GetTime();
            if (curTime >= endTime ||
                force == Vector3zero)
            {
                Dispose();
                return;
            }

            var pass                = curTime - startTime;
            var pt                  = Mathf.Clamp01(pass / shake.duration);
            var attenuation         = shake.GetAttenuation(pt);
            var perlinNoise         = sampler.GetPerlinNoise(shake.speed, pass, attenuation);

            var calcPos             = shake.magnitude   != 0f   &&  shake.narrowRange != Vector3zero;
            if (calcPos)
            {
                var forceMultiplier = shake.magnitude;
                var power           = force.magnitude * forceMultiplier;
                var localNoise = new Vector3(
                    perlinNoise.x * shake.narrowRange.x * power,
                    perlinNoise.y * shake.narrowRange.y * power,
                    perlinNoise.z * shake.narrowRange.z * power);
                var worldOffset     = forceQuat * localNoise;
                var localOffset     = objWorldToLocal.MultiplyVector(worldOffset);
                var localPos        = beforeShakeLocal.position + localOffset;
                localPosCallback?.Invoke(localPos);
            }

            var calcRot             = shake.angular     != 0f   &&  shake.angleRatio  != Vector3zero; // angle (0,0,0) = did't roll.
            if (calcRot)
            {
                float f = (shake.angular * 2f - 1f);
                var pitch           = Quaternion.AngleAxis(perlinNoise.x * shake.angleRatio.x * f, forceQuat * Vector3.right);
                var yaw             = Quaternion.AngleAxis(perlinNoise.y * shake.angleRatio.y * f, forceQuat * Vector3.up);
                var roll            = Quaternion.AngleAxis(perlinNoise.z * shake.angleRatio.z * f, forceQuat * Vector3.forward);

                //DebugExtend.DrawArrow(Vector3zero, forceQuat * Vector3.right * 5f  , Color.red);
                //DebugExtend.DrawArrow(Vector3zero, forceQuat * Vector3.up * 5f     , Color.green);
                //DebugExtend.DrawArrow(Vector3zero, forceQuat * Vector3.forward* 5f , Color.cyan);

                // Note: U3D rotation order start from right to left.
                var worldAngular    = roll * yaw * pitch * beforeShake.rotation;
                var parentRotInv    = obj.parent ? obj.parent.rotation.Inverse() : Quaternion.identity;
                var localAngular    = parentRotInv * worldAngular;
                localRotCallback?.Invoke(localAngular);
            }
        }

        #region Dispose
        private bool isDisposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    // dispose managed state (managed objects)
                    if (obj != null)
                    {
                        var parentRotInv    = obj.parent ? obj.parent.rotation.Inverse() : Quaternion.identity;
                        obj.localRotation   = parentRotInv * beforeShake.rotation;
                    }
                }
                obj = null;
                // free unmanaged resources (unmanaged objects) and override finalizer
                // set large fields to null
                isDisposed = true;
            }
        }

        ~ShakeTask()
        {
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }
}
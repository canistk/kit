using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;

namespace Kit.Test
{
    [RequireComponent(typeof(MotionCache))]
    public class TestMotionCache : MonoBehaviour
    {
        [SerializeField] Vector2 m_DebugOffset = Vector2.zero;

        [SerializeField] MotionCache m_MotionCache = null;
        [SerializeField] float m_SecondAgo = 1f;
        [SerializeField] Color m_MotionPathColor = Color.cyan;

        [SerializeField] Color m_AngularColor = Color.green.CloneAlpha(.2f);

        private void Reset()
        {
            m_MotionCache = GetComponent<MotionCache>();
        }

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying)
                return;

            float travelDistance = 0f;
            float timePassed = 0f;
            float angularDistance = 0f;
            int workCount = 0;
            foreach (MotionFragment frag in m_MotionCache.GetPeriod(m_SecondAgo))
            {
                timePassed += frag.deltaTime;
                travelDistance += frag.motionDistance;
                frag.steeringRotation.ToAngleAxis(out float aAngle, out Vector3 axis);
                angularDistance += aAngle; // assume radius = 1f, we don't care.
                workCount++;
                GizmosExtend.DrawLine(frag.from.position, frag.to.position, m_MotionPathColor);

                Vector3 pos = frag.from.position;
                Vector3 fForward = frag.from.rotation.GetForward();
                Vector3 fRight = frag.from.rotation.GetRight();
                Vector3 fUp = frag.from.rotation.GetUp();

                GizmosExtend.DrawArc(frag.from.position, axis, frag.from.rotation.GetForward(), -aAngle, 1f, m_AngularColor);

                GizmosExtend.DrawArc(pos, fRight, fForward, frag.biasAngularVelocity.x, 1f, Color.red.CloneAlpha(.2f));
                GizmosExtend.DrawArc(pos, fUp, fForward, frag.biasAngularVelocity.y, 1f, Color.green.CloneAlpha(.2f));
                GizmosExtend.DrawArc(pos, fForward, fUp, frag.biasAngularVelocity.z, 1f, Color.blue.CloneAlpha(.2f));
            }

            // Above gizmos should maintain as same as.
            // m_MotionCache.GetAvgSpeed(out float avgSpeed, out float avgAngular, m_SecondAgo);

            float avgAngularSpeed = timePassed == 0f ? 0f : angularDistance / timePassed;
            float speed = timePassed == 0f ? 0f : travelDistance / timePassed;

            string msg = $"Record = {workCount}\n" +
                $"Time pass = {timePassed:F2} * \n" +
                $"Speed = {speed:F2} =\n" +
                $"Distance = {travelDistance:F2}\n" +
                $"Angular Velocity = {avgAngularSpeed:F2}";

            GizmosExtend.DrawLabel(transform.position, msg, m_DebugOffset);
        }
    }
}
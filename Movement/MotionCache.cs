﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StringBuilder = System.Text.StringBuilder;

namespace Kit
{
	public class MotionCache : MonoBehaviour
	{
		[Header("Setting")]
		[SerializeField, Tooltip("To define the world up for this object. require normalize vector (length = 1f)")]
		Vector3 m_BiasGroundNormal = Vector3.up;

		[SerializeField, Min(1)] int m_HistorySize = 1;
		public int historySize => m_HistorySize;
		
		[SerializeField] bool m_FixedUpdate = false;

		[Header("Debug")]
		[SerializeField] DebugInfo debugInfo = null;

		[System.Flags]
		public enum eDebugFlag
		{
			motion = 1 << 0,
			rotation = 1 << 1,
			speed = 1 << 2,
			logLabel = 1 << 3,
		}

		[System.Serializable]
		public class DebugInfo
		{
			[MaskField(typeof(eDebugFlag))] public eDebugFlag flag;
			public Vector2 offset;

			public float duration = 0f;
			public Color colorMotionVelocity = Color.cyan.CloneAlpha(0.5f);
			public Color colorMotionTrack = Color.cyan;

			public float angularDrawLength = 1f;
			public Color colorAngular = Color.green.CloneAlpha(.4f);
		}

		public MotionFragment motionFragment { get; private set; }
		private MotionFragment[] m_History = null;
		private TransformCache m_Last;
		int writePt = 0;
		StringBuilder sb = null;
		public event System.Action EVENT_MotionUpdated = null;
		private void MotionUpdate()
		{
			if (Time.timeScale == 0f)
				return;
			float deltaTime = m_FixedUpdate ? Time.fixedDeltaTime : Time.deltaTime;
			if (deltaTime == 0f)
				return;

			TransformCache currentFrame = new TransformCache(transform.position, transform.rotation);
			motionFragment = new MotionFragment(m_Last, currentFrame, biasGroundNormal, deltaTime);
			if (m_History.Length != m_HistorySize)
            {
				if (m_History.Length < m_HistorySize)
					writePt = 0;
				System.Array.Resize(ref m_History, m_HistorySize);
			}

			// circular buffer.
			// https://en.wikipedia.org/wiki/Circular_buffer
			m_History[writePt] = motionFragment;
			writePt = (writePt + 1) % m_History.Length;

			m_Last = currentFrame;
			EVENT_MotionUpdated?.Invoke();
		}
		public void ResetParams()
		{
			m_BiasGroundNormal.Normalize();
			motionFragment = MotionFragment.None;
			if (m_History == null)
			{
				m_History = new MotionFragment[m_HistorySize];
			}
			else
			{
				for (int i = 0; i < m_History.Length; i++)
					m_History[i] = MotionFragment.None;
			}
			m_Last = new TransformCache(transform.position, transform.rotation);
			writePt = 0;
		}

		/// <summary>From oldest to latest history</summary>
		/// <returns></returns>
		public IEnumerable<MotionFragment> GetHistory()
		{
			int pt = writePt;
			do
			{
				pt = (pt + m_History.Length - 1) % m_History.Length;
				if (m_History[pt].Used)
					yield return m_History[pt];
			}
			while (pt != writePt);
		}

		/// <summary>
		/// The result will be inverse, since we rewind history records.
		/// </summary>
		/// <param name="secondAgo"></param>
		/// <returns></returns>
		public IEnumerable<MotionFragment> GetPeriod(float secondAgo)
        {
			secondAgo = Mathf.Max(0f, secondAgo);
			int pt = writePt;
			float timePassed = 0f;
			do
			{
				pt = (pt + m_History.Length - 1) % m_History.Length;
				timePassed += m_History[pt].deltaTime;
				if (m_History[pt].Used)
					yield return m_History[pt];
			}
			while (pt != writePt && timePassed < secondAgo);
			// Debug.Log($"run timePass {timePassed:F2}");
		}

		public AverageMotion GetAverage(float period = 1f, System.Action<MotionFragment> iterateCallback = null)
        {
			AverageMotion avg = default;
			bool started = false;
			foreach (MotionFragment frag in GetPeriod(period))
			{
				if (!started)
					avg.start = frag;
				avg.end = frag;
				
				avg.iterateCount++;
				avg.sampleDuration += frag.deltaTime;

				avg.travelDistance += frag.motionDistance;
				avg.motionVector += frag.motionVector;
				
				frag.steeringRotation.ToAngleAxis(out float angular, out Vector3 axis);
				avg.angularDistance += angular; // assume radius = 1f, since travel distance are based on arc * radius.
				avg.angularAxis += axis;

				iterateCallback?.Invoke(frag);
			}

			if (avg.iterateCount > 0 && avg.sampleDuration > 0f)
			{
				avg.speed = avg.travelDistance / avg.sampleDuration;
				avg.angularAxis /= (float)avg.iterateCount;
				avg.angular = avg.angularDistance / avg.sampleDuration;
			}

			return avg;
		}

		public MotionFragment GetLastSeconds(float secondAgo)
		{
			bool started = false;
			TransformCache to = new TransformCache(transform.position, transform.rotation);
			TransformCache from = to;
			Vector3 toNormal = default, fromNormal = default;

			float duration = 0f;
			foreach (MotionFragment frag in GetPeriod(secondAgo))
			{
				if (!started)
				{
					to = frag.to;
					toNormal = frag.biasGroundNormal;
				}
				from = frag.from;
				fromNormal = frag.biasGroundNormal;
				duration += frag.deltaTime;
			}

			Vector3 finalUp = Vector3.Slerp(fromNormal, toNormal, 0.5f);
			if (finalUp == Vector3.zero)
				finalUp = toNormal;
			return new MotionFragment(from, to, finalUp, duration);
		}

		#region System Implement
		private void OnValidate()
		{
#if UNITY_EDITOR
			if (KitBox.IsEditorModeValueChanged(this))
			{
				m_BiasGroundNormal.Normalize();
			}
#endif
		}

		private void OnEnable()
		{
			ResetParams();
		}

		private void Update()
		{
			if (!m_FixedUpdate)
				MotionUpdate();
		}

        private void FixedUpdate()
        {
			if (m_FixedUpdate)
				MotionUpdate();
        }

        private void OnDrawGizmos()
		{
			if (ReferenceEquals(debugInfo, null) ||
				debugInfo.flag == (eDebugFlag)0)
				return;
			if (sb == null)
				sb = new StringBuilder(500);
			bool logLabel = debugInfo.flag.HasFlag(eDebugFlag.logLabel);
			bool logMotion = debugInfo.flag.HasFlag(eDebugFlag.motion);
			if (logLabel && logMotion)
			{
				sb.AppendLine($"Last position : {lastPosition:F1}, WritePt = {writePt}");
				sb.AppendLine($"Motion distance : {motionDistance:F2}");
			}
            if (logMotion && !motionVector.IsZero())
            {
                DebugExtend.DrawLine(motionFragment.from.position, motionFragment.to.position, debugInfo.colorMotionTrack, debugInfo.duration);
            }

			bool logSpeed = debugInfo.flag.HasFlag(eDebugFlag.speed);
			if (logSpeed && logLabel)
			{
				sb.AppendLine($"Speed(frame) : {motionSpeed:F2}");
				sb.AppendLine($"Local Speed XZ : {localMotionSpeedXZ:F1}, Y : {localMotionSpeedY:F1}");
			}
			if (logSpeed && motionVelocity != Vector3.zero)
			{
				GizmosExtend.DrawRay(transform.position, motionVelocity, color: debugInfo.colorMotionVelocity);
			}

			bool logRotate = debugInfo.flag.HasFlag(eDebugFlag.rotation);
			if (logLabel && logRotate)
			{
				sb.AppendLine($"Steering signed angle (frame) : {steeringAngle:F2}");
				sb.AppendLine($"Projected local axis angle (X/Y/Z): {biasAngularVelocity:F2}");
				sb.AppendLine($"Angle between Motion vector & facing vector : {angleBetweenMotionLookAt:F1}");
            }
			if (logRotate)
			{
				if (m_HistorySize > 1 && m_History != null)
				{
					foreach (MotionFragment frag in GetPeriod(debugInfo.duration))
					{
						frag.steeringRotation.ToAngleAxis(out float angle, out Vector3 axis);
						GizmosExtend.DrawArc(frag.from.position, axis, frag.from.rotation * Vector3.forward, angle, debugInfo.angularDrawLength, debugInfo.colorAngular);
					}
				}
				else
				{
					steeringRotation.ToAngleAxis(out float angle, out Vector3 axis);
					GizmosExtend.DrawArc(lastPosition, axis, lastForward, angle, 1f, debugInfo.colorAngular);
				}
				//if (biasAngularVelocity != Vector3.zero)
				//{
				//	GizmosExtend.DrawArc(transform.position, transform.right, transform.forward, biasAngularVelocity.x, 1f, debugInfo.colorAxisX);
				//	GizmosExtend.DrawArc(transform.position, transform.up, transform.forward, biasAngularVelocity.y, 1f, debugInfo.colorAxisY);
				//	GizmosExtend.DrawArc(transform.position, transform.forward, transform.up, biasAngularVelocity.z, 1f, debugInfo.colorAxisZ);
				//}
			}

			if (logLabel && sb.Length > 0)
			{
				GizmosExtend.DrawLabel(transform.position,
					sb.ToString(),
					offsetX: debugInfo.offset.x,
					offsetY: debugInfo.offset.y);
				sb.Clear();
			}
		}
		#endregion System Implement

		#region API redirection
		/// <summary>To define the world up for this object. require normalize vector (length = 1f)</summary>
		public Vector3 biasGroundNormal => m_BiasGroundNormal;

		/// <summary>The <see cref="Time.deltaTime"/>, used for calculation.</summary>
		public float deltaTime => motionFragment.deltaTime;

		/// <summary>World position on last frame</summary>
		public Vector3 lastPosition => motionFragment.from.position;

		/// <summary>the rotation on last frame</summary>
		public Quaternion lastRotation => motionFragment.from.rotation;

		/// <summary>The last forward from <see cref="lastRotation"/></summary>
		public Vector3 lastForward => motionFragment.fromForward;

		/// <summary>The motion between <see cref="lastPosition"/> and current position</summary>
		public Vector3 motionVector => motionFragment.motionVector;

		/// <summary>The motion vector based on local space <see cref="motionVector"/></summary>
		public Vector3 localMotionVector => motionFragment.localMotionVector;

		/// <summary>The motion vector project on <see cref="m_BiasGroundNormal"/></summary>
		public Vector3 motionVectorIgnoreY => motionFragment.motionVectorIgnoreY;

		/// <summary>The local motion vector project on <see cref="m_BiasGroundNormal"/></summary>
		public Vector3 localMotionVectorIgnoreY => motionFragment.localMotionVectorIgnoreY;

		/// <summary>The motion direction (Normalize)</summary>
		public Vector3 motionDirection => motionFragment.motionDirection;

		/// <summary>The moving direction on local space</summary>
		public Vector3 localMotionDirection => motionFragment.localMotionDirection;

		/// <summary>The distance moved from <see cref="lastPosition"/></summary>
		public float motionDistance => motionFragment.motionDistance;

		/// <summary>The distance moved from <see cref="lastPosition"/>
		/// Ignore Y - (local space), are based on <see cref="m_BiasGroundNormal"/>.</summary>
		public float motionDistanceIgnoreY => motionFragment.motionDistanceIgnoreY;

		/// <summary>The distance moved from <see cref="lastPosition"/>
		/// Ignore Y - (local space), are based on <see cref="m_BiasGroundNormal"/>.</summary>
		public float localMotionDistanceIgnoreY => motionFragment.localMotionDistanceIgnoreY;

		/// <summary>The speed moved from <see cref="lastPosition"/> to current position.
		/// calculate by engine <see cref="Time.deltaTime"/></summary>
		public float motionSpeed => motionFragment.motionSpeed;

		/// <summary>The speed project on <see cref="m_BiasGroundNormal"/> as floor,
		/// and separate into horizontal direction (XZ)</summary>
		public float localMotionSpeedXZ => motionFragment.localMotionSpeedXZ;

		/// <summary>The speed project on <see cref="m_BiasGroundNormal"/> itself
		/// and extract the vertical direction (Y)</summary>
		public float localMotionSpeedY => motionFragment.localMotionSpeedY;

		/// <summary>Velocity - The <see cref="motionSpeed"/> represent in vector</summary>
		public Vector3 motionVelocity => motionFragment.motionVelocity;

		/// <summary>Velocity - The <see cref="motionSpeed"/> represent in vector
		/// Ignore Y - (local space), are based on <see cref="biasGroundNormal"/></summary>
		public Vector3 localMotionVelocityXZ => motionFragment.localMotionVelocityXZ;

		/// <summary>The steeringRotation between <see cref="lastRotation"/> and current rotation,
		/// <seealso cref="Quaternion.FromToRotation(Vector3, Vector3)"/></summary>
		public Quaternion steeringRotation => motionFragment.steeringRotation;

		/// <summary>The different between <see cref="lastForward"/> and current forward</summary>
		public float steeringAngle => motionFragment.steeringAngle;

		/// <summary>The angularVelocity not the unity's format.</summary>
		public float angularVelocity => motionFragment.angularVelocity;

		/// <summary>The bias angularVelocity unity's.</summary>
		public Vector3 biasAngularVelocity => motionFragment.biasAngularVelocity;

		/// <summary>The angle between
		/// moving direction <see cref="motionVector"/>
		/// and actually facing direction <see cref="Transform.forward"/> 
		/// 0 = align forward, -90 = left, +90 = right, -180/180 = backward</summary>
		public float angleBetweenMotionLookAt => motionFragment.angleBetweenMotionLookAt;

		/// <summary>0 = forward, Normalize -180~180 degree to -1f ~ 1f</summary>
		public float angleBetweenMotionLookAtNormalize => motionFragment.angleBetweenMotionLookAtNormalize;
		#endregion // API redirection
	}

	/// <summary>A snap shot for a period of motion</summary>
	public struct AverageMotion
    {
		public int iterateCount;
		public float sampleDuration;
		
		public float travelDistance;
		public float speed;

		public float angularDistance;
		public float angular;

		public MotionFragment start, end;
		public Vector3 motionVector;
		public Vector3 angularAxis; // average
    }
}
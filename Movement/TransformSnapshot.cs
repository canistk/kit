﻿using UnityEngine;
namespace Kit
{
	/// <summary>
	/// A struct to temporarily cache the snap shot of target position & rotation.
	/// </summary>
	[System.Obsolete("Use TransformCache", true)]
	public struct TransformSnapshot
	{
		public readonly Vector3 position;
		public readonly Quaternion rotation;
		public readonly Vector3 forward;
		public readonly Vector3 localScale;
		public readonly Vector3 lossyScale;

		/// <summary>Constructor for transfrom</summary>
		/// <param name="transform"></param>
		public TransformSnapshot(Transform transform) : this(transform.position, transform.rotation, transform.localScale, transform.lossyScale) { }

		public TransformSnapshot(Vector3 position, Quaternion rotation,
			Vector3 lossyScale = default, Vector3 localScale = default)
		{
			this.position = position;
			this.rotation = rotation;
			forward = rotation * Vector3.forward;
			this.localScale = localScale == default ? Vector3.one : localScale;
			this.lossyScale = lossyScale == default ? Vector3.one : lossyScale;
		}

		/// <summary>Assign cached position & rotation to giving transform.</summary>
		/// <param name="transform">giving transform</param>
		public void AssignToTransform(Transform transform)
		{
			transform.SetPositionAndRotation(position, rotation);
		}

		public static explicit operator TransformSnapshot(Transform transform) => new TransformSnapshot(transform);
	}

	public static class TransformSnapshotUtil
	{
		[System.Obsolete("Use TransformCache", true)]
		public static bool IsSameLocation(this TransformSnapshot self, TransformSnapshot other)
		{
			return self.position == other.position &&
				self.rotation == other.rotation &&
				self.lossyScale == other.lossyScale;
		}
		[System.Obsolete("Use TransformCache", true)]
		public static bool Approximately(this TransformSnapshot self, TransformSnapshot other)
		{
			return self.position.Approximately(other.position) &&
				self.rotation.Approximately(other.rotation) &&
				self.lossyScale.Approximately(other.lossyScale);
		}
		[System.Obsolete("Use TransformCache", true)]
		public static bool EqualRoughly(this TransformSnapshot self, TransformSnapshot other, float threshold = float.Epsilon)
		{
			return
				self.position.EqualRoughly(other.position, threshold) &&
				self.rotation.EqualRoughly(other.rotation, threshold) &&
				self.lossyScale.EqualRoughly(other.lossyScale);
		}
	}
}
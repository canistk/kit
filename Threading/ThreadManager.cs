using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class ThreadManager : Singleton<ThreadManager, RemoveLateComer, SearchHierarchy>
    {
        const bool s_Debug = false;

        #region System
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(this.gameObject);
            DontDestroyOnLoad(this);
        }
        private void Update()
        {
            ManageWorker();
        }
        #endregion System

        #region Manage Worker
        private List<ThreadWorker> m_WorkerThreads = new List<ThreadWorker>();
        public void Register(ThreadWorker worker)
        {
#pragma warning disable 162
            if (s_Debug) Debug.Log($"{name}.add({worker})");
#pragma warning restore 162
            if (!m_WorkerThreads.Contains(worker))
            {
                lock (m_WorkerThreads)
                {
                    m_WorkerThreads.Add(worker);
                }
            }
        }

        public void Unregister(ThreadWorker worker)
        {
            if (worker == null)
                return;
            if (!worker.IsStopping)
                worker.EndThread(); // will trigger UnRegister, and remove in ManageWorker
        }

        private void ManageWorker()
        {
            // worker update dispatcher
            lock (m_WorkerThreads)
            {
                for (int i = 0; i < m_WorkerThreads.Count; ++i)
                {
                    if (m_WorkerThreads[i] == null)
                    {
                        Debug.LogError($"Worker[{i}] has been deleted.\nConsider call {nameof(ThreadWorker)}.{nameof(ThreadWorker.EndThread)}() instead.");
                        m_WorkerThreads.RemoveAt(i--); // after remove, continue in correct order.
                        continue;
                    }

                    m_WorkerThreads[i].UnityMainThread();

                    if (m_WorkerThreads[i].IsStopping)
                    {
#pragma warning disable 162
                        if (s_Debug) Debug.Log($"{name}.RemoveAt({i}), Total={m_WorkerThreads.Count - 1} ");
#pragma warning restore 162
                        m_WorkerThreads.RemoveAt(i--); // after remove, continue in correct order.
                    }
                }
            }
        }
        #endregion Manage Worker
    }
}
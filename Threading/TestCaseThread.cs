using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Threading.Test
{
    public class TestCaseThread : MonoBehaviour
    {
        public static int s_GlobalCount = 0;
        public static readonly object s_Lock = new object();

        void Start()
        {
            int rnd = Random.Range(10, 5);
            Debug.Log("Total workers : " + rnd);
            while (rnd-- > 0)
            {
                new PoorWorker();
            }
        }

    }


    public class PoorWorker : ThreadWorker
    {
        public System.Guid m_Id;
        int m_TaskAmount = Random.Range(1, 10);
        Color m_Color;
        public PoorWorker()
        {
            m_Id = System.Guid.NewGuid();
            int interval = Random.Range(3, 2000);
            m_Color = ColorExtend.GetJetColor((float)interval / 2000.0f);
            // ColorExtend.RandomRange(Color.gray, Color.white);
            this.LogWarning($"Worker created, task={m_TaskAmount}, Id={m_Id.ToString()}, interval={interval}");
            BeginThread(interval);
        }

        protected override void OnAwake()
        {
        }

        protected override void OnThreadLoop()
        {
            if (m_TaskAmount <= 0)
                return; // skip

            int taskOrder = -1;
            lock (TestCaseThread.s_Lock)
            {
                taskOrder = ++TestCaseThread.s_GlobalCount;
            }
            --m_TaskAmount;
            string str = m_Color.ToRichText($"W[{m_Id}] - Task[{m_TaskAmount}]");
            this.Log($"[{taskOrder}] {str} - {System.DateTime.UtcNow.Ticks}");
        }

        protected override void OnUnityMainThread()
        {
            // let base class dispatch "Log"

            if (m_TaskAmount <= 0 && !IsStopping)
                EndThread();
        }
    }
}
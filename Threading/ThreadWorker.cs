using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Kit
{
    /// <summary>
    /// An abstract class for Unity3d implement multi-thread task,
    /// handle following case :
    /// 1) internal debug log <see cref="Log(string)"/>, <see cref="LogWarning(string)"/>, <see cref="LogError(string)"/> 
    ///     that already implement event dispatch method, to display U3D log safe.
    /// 2) expose <see cref="OnAwake"/><see cref="OnThreadLoop"/><see cref="OnUnityMainThread"/>
    /// </summary>
    public abstract class ThreadWorker : System.IDisposable
    {
        #region Threading
        private Thread m_Thread = null;
        private int m_MillisecondsInterval = 1000;
        private Queue<KeyValuePair<int,string>> m_LogQueue = new Queue<KeyValuePair<int, string>>(100);

        /// <summary>Start threading</summary>
        /// <param name="millisecondsInterval">1000 ~= 1sec</param>
        public void BeginThread(int millisecondsInterval)
        {
            if (m_Thread != null)
            {
                // throw new System.Exception("Multi thread double init.");
                Debug.LogError("Multi thread double init.");
                return;
            }
            m_MillisecondsInterval = millisecondsInterval;
            m_Thread = new Thread(new ThreadStart(InternalThreadLoop));
            m_Thread.IsBackground = true;
            m_Thread.Start();

            ThreadManager.Instance.Register(this);
        }

        public void EndThread()
        {
            m_DisposeRequest = true;
            if (ThreadManager.InstanceWithoutFetch)
                ThreadManager.Instance.Unregister(this);
        }

        private void InternalThreadLoop()
        {
            OnAwake();
            while (!m_DisposeRequest)
            {
                lock (m_LogQueue)
                {
                    OnThreadLoop();
                }
                Thread.Sleep(m_MillisecondsInterval);
            }

            Dispose(true);
        }

        /// <summary>
        /// May perform the init here.
        /// Unity API can not be used in here.
        /// </summary>
        protected abstract void OnAwake();

        /// <summary>Non-safe area. Unity API can not be used in here.</summary>
        protected abstract void OnThreadLoop();

        /// <summary>Should be called</summary>
        internal void UnityMainThread()
        {
            lock (m_LogQueue)
            {
                OnUnityMainThread();
                while (m_LogQueue.Count > 0)
                {
                    var kvp = m_LogQueue.Dequeue();
                    switch (kvp.Key)
                    {
                        case 0: Debug.Log(kvp.Value); break;
                        case 1: Debug.LogWarning(kvp.Value); break;
                        case 2: Debug.LogError(kvp.Value); break;
                    }
                }
            }
        }
        protected abstract void OnUnityMainThread();
        #endregion Threading

        #region Debug
        /// <summary>
        /// Due to multi-thread cannot use UnityAPI, we cache the message and dispatch later on
        /// <see cref="UnityMainThread"/>
        /// </summary>
        /// <param name="str"></param>
        protected void Log(string str) => m_LogQueue.Enqueue(new KeyValuePair<int, string>(0, str));
        protected void LogWarning(string str) => m_LogQueue.Enqueue(new KeyValuePair<int, string>(1, str));
        protected void LogError(string str) => m_LogQueue.Enqueue(new KeyValuePair<int, string>(2, str));
        #endregion Debug

        #region Dispose
        public bool IsStopping => m_DisposeRequest || m_DisposedValue;
        public bool IsDisposed => m_DisposedValue;
        private bool m_DisposeRequest = false;
        private bool m_DisposedValue = false;
        protected void Dispose(bool disposing)
        {
            if (!m_DisposedValue)
            {
                if (disposing)
                {
                    // TODO: 處置受控狀態 (受控物件)
                    if (m_Thread != null && m_Thread.ThreadState == ThreadState.Running)
                    {
                        m_Thread.Abort();
                    }
                    OnDispose();
                }

                // TODO: 釋出非受控資源 (非受控物件) 並覆寫完成項
                // TODO: 將大型欄位設為 Null
                OnDisposedFreeMemory();
                m_LogQueue = null;
                m_DisposedValue = true;
            }
        }

        protected virtual void OnDispose() { }

        protected virtual void OnDisposedFreeMemory() { }

        // // TODO: 僅有當 'Dispose(bool disposing)' 具有會釋出非受控資源的程式碼時，才覆寫完成項
        ~ThreadWorker()
        {
            // 請勿變更此程式碼。請將清除程式碼放入 'Dispose(bool disposing)' 方法
            Dispose(disposing: false);
        }

        void System.IDisposable.Dispose()
        {
            // 請勿變更此程式碼。請將清除程式碼放入 'Dispose(bool disposing)' 方法
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }
}
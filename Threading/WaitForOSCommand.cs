using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Threading
{
    public class WaitForOSCommand : CustomYieldInstruction
    {
        public class TaskResult
        {
            public string commandShell, arguments;
            public StringBuilder m_Output = new StringBuilder();
            public StringBuilder m_Error = new StringBuilder();
            public System.Exception m_Exception = null;
        }
        public override bool keepWaiting => m_Thread.IsAlive;
        private Thread m_Thread;
        public TaskResult taskResult;
        public WaitForOSCommand(string commandShell, string arguments)
        {
            m_Thread = new Thread(new ParameterizedThreadStart(ThreadProc));
            taskResult = new TaskResult
            {
                commandShell = commandShell,
                arguments = arguments,
            };
            m_Thread.Start(taskResult);
        }

        private void ThreadProc(object obj)
        {
            TaskResult taskResult = (TaskResult)obj;
            try
            {
                using (Process process = new Process())
                {
                    process.StartInfo = new ProcessStartInfo
                    {
                        FileName = taskResult.commandShell,
                        Arguments = taskResult.arguments,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        RedirectStandardError = true,
                        RedirectStandardOutput = true,
                    };
                    process.EnableRaisingEvents = true;
                    process.OutputDataReceived += OnOutputDataReceived;
                    process.ErrorDataReceived += OnErrorDataReceived;
                    process.Start();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                    process.WaitForExit();
                }
            }
            catch (System.Exception e)
            {
                taskResult.m_Exception = e;
            }
        }

        private void OnErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
                taskResult.m_Error.AppendLine(e.Data);
        }

        private void OnOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
                taskResult.m_Output.AppendLine(e.Data);
        }
    }
}
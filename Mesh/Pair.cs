﻿using System;
namespace Kit.MeshLib.Core
{
	public interface IPair<T> where T : IEquatable<T>, IComparable<T>
	{
		T p0 { get; }
		T p1 { get; }
		Pair<T> ordered { get; }
		Pair<T> sorted { get; }
		Pair<T> reversed { get; }

		/// <summary>Is contain giving reference</summary>
		/// <param name="p"></param>
		/// <returns></returns>
		bool Contains(T p);

		/// <summary>Is equal to giving reference, same order, same instance.</summary>
		/// <param name="b"></param>
		/// <returns></returns>
		bool IsEquals(IPair<T> b);

		/// <summary>Is roughly equal to reference, but may not in same order</summary>
		/// <param name="b"></param>
		/// <returns></returns>
		bool IsEqualRoughly(IPair<T> b);

		/// <summary>Get another reference, based on giving reference within this pair.</summary>
		/// <param name="p"></param>
		/// <returns></returns>
		T GetOther(T p);
	}

	public struct Pair<T> : IPair<T>, IEquatable<IPair<T>>, IComparable<IPair<T>>
		where T : IComparable<T>, IEquatable<T>
	{
		public T p0 { get; private set; } // readonly
		public T p1 { get; private set; } // readonly
		public Pair(T p0, T p1) { this.p0 = p0; this.p1 = p1; }

		public static Pair<T> newSorted(T p0, T p1)
		{
			bool isSorted = p0.CompareTo(p1) <= 0;
			return new Pair<T>(isSorted ? p0 : p1, isSorted ? p1 : p0);
		}


		public Pair<T> ordered { get { return this; } }
		public Pair<T> sorted { get { return newSorted(p0, p1); } }
		public Pair<T> reversed { get { return new Pair<T>(p1, p0); } }

		public bool Contains(T p)
		{
			return (p0.NotNull() && p0.Equals(p)) ||
					(p1.NotNull() && p1.Equals(p)) ||
					(p.IsNull() && (p0.IsNull() || p1.IsNull()));
		}

		public bool IsEquals(IPair<T> b) { return b.NotNull() && p0.Equals(b.p0) && p1.Equals(b.p1); }

		public bool IsEqualRoughly(IPair<T> b)
		{
			return b.NotNull() && (p0.Equals(b.p0) && p1.Equals(b.p1)) || (p0.Equals(b.p1) && p1.Equals(b.p0));
		}

		public T GetOther(T p) { return p0.Equals(p) ? p1 : p0; }

		// Object
		public override int GetHashCode() { return Extensions.GenerateHashCode(p0.GetHashCode(), p1.GetHashCode()); }
		public override bool Equals(object b) { return b.NotNull() && b is IPair<T> && Equals((IPair<T>)b); }

		// IEquatable<IPair>
		public bool Equals(IPair<T> b) { return IsEquals(b); }

		// IComparable<IPair>
		public int CompareTo(IPair<T> b)
		{
			return b.IsNull() ?
				1 : this.Equals(b) ?
					0 : p0.CompareTo(b.p0);
		}

		public static bool operator ==(Pair<T> a, IPair<T> b) { return a.IsNull() ? b.IsNull() : a.Equals(b); }
		public static bool operator !=(Pair<T> a, IPair<T> b) { return a.IsNull() ? b.NotNull() : !a.Equals(b); }

		public static bool operator <(Pair<T> a, IPair<T> b) { return a.IsNull() ? b.NotNull() : a.CompareTo(b) < 0; }
		public static bool operator >(Pair<T> a, IPair<T> b) { return a.IsNull() ? false : a.CompareTo(b) > 0; }
		public static bool operator <=(Pair<T> a, IPair<T> b) { return a == b || a < b; }
		public static bool operator >=(Pair<T> a, IPair<T> b) { return a == b || a > b; }
	}
}
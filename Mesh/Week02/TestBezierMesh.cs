using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;
using Kit.Coordinates;
using Kit.MeshLib.Core;

//https://catlikecoding.com/unity/tutorials/procedural-grid/
//https://www.raywenderlich.com/3169311-runtime-mesh-manipulation-with-unity
public class TestBezierMesh : MonoBehaviour
{
#pragma warning disable 414
    [SerializeField, ContextButton(nameof(UpdateMesh))] bool m_UpdateMesh = false;
    [SerializeField] Mesh m_Mesh = null;
    private void UpdateMesh() => m_Mesh = GetComponent<MeshFilter>().mesh;
    [Space(10)]
    [Header("Bezier")]
    public List<TangentPoint> m_TangentPointArr = new List<TangentPoint>();
    public List<float> m_Partitions = new List<float>
    {
        0.2f,
        0.8f,
        0.2f,
    };
    public float tolerance = 0.01f;
    [SerializeField, ContextButton(nameof(BuildBezier))] bool m_BuildBezier = false;
    public void BuildBezier()
    {
        m_BakedCurve = Bezier.BakeCurveByTolerance(transform, m_TangentPointArr, tolerance);
    }
    [Header("Debug")]
    public bool m_DebugDrawBezier = true;
    public Color m_OrgLineColor = Color.gray.CloneAlpha(0.3f);
    public Color m_BakedLineColor = Color.yellow;

    [Space]
    [Header("Bezier + Mesh")]
    [SerializeField, ContextButton(nameof(BuildMesh))] bool m_BuildMesh = false;

    [SerializeField, ContextButton(nameof(ShuffleMesh))] bool m_ShuffleMesh = false;
    [SerializeField] int m_ShuffleTriangleGroup = 4;
    [Space(20)]
    public Bezier.Baked m_BakedCurve;
#pragma warning restore 414
    
    public void BuildMesh()
    {
        if (m_Mesh != null)
            DestroyImmediate(m_Mesh);

        if (m_BakedCurve == null)
            BuildBezier();
        Bezier.GenerateCurveMesh("Week02HW", m_Partitions.ToArray(), m_BakedCurve, AnimationCurve.Constant(0f,1f,1f), ref m_Mesh);
        // MeshExtend.GeneratePlane("Custom", xSize, ySize, out m_Mesh);
        
        // m_Mesh = MeshExtend.CreatePlaneXY("TestMesh", 1f, 1f);
        MeshExtend.RenderMesh(transform, m_Mesh);
    }

    public void ShuffleMesh()
    {
        MeshExtend.SplitMeshTriangles(ref m_Mesh,
                    out Vector2[] newUv2,
                    out Vector2[] newUv3, m_ShuffleTriangleGroup);
        m_Mesh.uv2 = newUv2;
        m_Mesh.uv3 = newUv3;
        m_Mesh.RecalculateNormals();
    }

    private void OnDrawGizmos()
    {
        if (m_DebugDrawBezier)
        {
            if (m_BakedCurve != null)
            {
                m_BakedCurve.DrawGizmos(m_BakedLineColor);
            }
            if (m_TangentPointArr != null)
            {
                for (int i = 1; i < m_TangentPointArr.Count; i++)
                {
                    var p0 = m_TangentPointArr[i - 1];
                    var p1 = m_TangentPointArr[i];
                    p0.DrawGizmos(Color.red, Color.green.CloneAlpha(.3f));
                    p1.DrawGizmos(Color.red, Color.green.CloneAlpha(.3f));
                    GizmosExtend.DrawBezier(p0.position, p0.outTangentPoint, p1.position, p1.inTangentPoint, m_OrgLineColor, m_OrgLineColor);
                }
            }
        }
    }

}

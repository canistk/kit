﻿using UnityEngine;
using UnityEngine.Assertions;

using System;
using System.Collections.Generic;

namespace Kit.MeshLib.Core
{
	public class Point : IEquatable<Point>, IComparable<Point>
	{
		public readonly int index;
		public readonly Vector3 vector;

		readonly HashSet<MeshEdge>	m_Edges		= new HashSet<MeshEdge>();
		readonly HashSet<Face>		m_Faces		= new HashSet<Face>();
		readonly HashSet<Vertex>	m_Vertices	= new HashSet<Vertex>();

		public IEnumerable<MeshEdge>	edges	=> m_Edges;
		public IEnumerable<Face>		faces	=> m_Faces;
		public IEnumerable<Vertex>		vertices => m_Vertices;

		public Point(int index, Vector3 vertex)
		{
			this.index = index;
			this.vector = vertex;
		}

		public bool isIn(MeshEdge edge) { return m_Edges.Contains(edge); }

		public void addEdge(MeshEdge edge)
		{
			Assert.IsNotNull(edge);
			m_Edges.Add(edge);
		}

		public void addFace(Face face)
		{
			Assert.IsNotNull(face);
			m_Faces.Add(face);
		}

		public void addVertex(Vertex v)
		{
			Assert.IsNotNull(v);
			m_Vertices.Add(v);
		}

		public Vertex tryGetVertexFor(Face face)
		{
			// null face typically means 'no adjacent face'
			// i.e. mesh is not closed, which is ok
			if (face == null)
				return null;

			foreach (var vertex in m_Vertices)
				if (vertex.isSharedBy(face))
					return vertex;

			return null;
		}

		public Vector3 normalWith(IList<Vector3> vertices)
		{
			var sum = Vector3.zero;

			foreach (var face in m_Faces)
				sum += face.normalWith(vertices);

			return sum / m_Faces.Count;
		}

		public static implicit operator Vector3(Point p) { return p.vector; }

		// Object
		public override int GetHashCode() { return index.GetHashCode(); }
		public override bool Equals(object b) { return b.NotNull() && b is Point && Equals((Point)b); }

		// IEquatable
		public bool Equals(Point b) { return b.NotNull() && b.index == index; }

		// IComparable
		public int CompareTo(Point b) { return b.IsNull() ? 1 : this.index.CompareTo(b.index); }

		public static bool operator ==(Point a, Point b) { return a.IsNull() ? b.IsNull() : a.Equals(b); }
		public static bool operator !=(Point a, Point b) { return a.IsNull() ? b.NotNull() : !a.Equals(b); }

		public static bool operator <(Point a, Point b) { return a.IsNull() ? b.NotNull() : a.CompareTo(b) < 0; }
		public static bool operator >(Point a, Point b) { return a.IsNull() ? false : a.CompareTo(b) > 0; }
		public static bool operator <=(Point a, Point b) { return a == b || a < b; }
		public static bool operator >=(Point a, Point b) { return a == b || a > b; }
	}
}
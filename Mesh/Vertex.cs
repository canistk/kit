﻿using UnityEngine.Assertions;
using System.Collections.Generic;

namespace Kit.MeshLib.Core
{
	public class Vertex
	{
		public readonly int index;
		public readonly Point point;
		public readonly HashSet<Face> faces = new HashSet<Face>();
		// public IEnumerable<Face> faces => _faces;

		public Vertex(int index, Point point, Face face)
		{
			this.index = index;
			this.point = point;

			faces = new HashSet<Face>();
			faces.Add(face);
			point.addVertex(this);
		}

		public void addFace(Face face)
		{
			Assert.IsNotNull(face);
			faces.Add(face);
		}

		public bool isSharedBy(Face face) => faces.Contains(face);
	}
}
using Kit.Avatar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.MeshLib.Core
{
    /// <summary>
    /// <see cref="https://web.ntnu.edu.tw/~algo/ConvexHull.html"/>
    /// <see cref="https://media.steampowered.com/apps/valve/2014/DirkGregorius_ImplementingQuickHull.pdf"/>
    /// </summary>
    public class QuickHull2d : kTask
    {
        private List<Vector2> m_Input;
        public QuickHull2d(IList<Vector2> points)
        {
            if (points.Count < 3)
                throw new System.Exception($"Fail to form triangle, {points.Count} not enough point(s)");

            this.m_Input = new List<Vector2>(points.Count);
            foreach (var p in points)
            {
                if (p == null)
                    continue;
                this.m_Input.Add(p);
            }
        }

        protected override bool InternalExecute()
        {
            InitialHull();
            OnComplete();
            return false;
        }

        private class Point
        {
            public Vector2 value;
            public Point next;
            public Point(Vector2 value)
            {
                this.value = value;
            }

            public void Insert(Point _newNext)
            {
                if (next != null)
                    _newNext.Insert(next);
                next = _newNext;
            }
        }

        Point m_Hull;
        private bool InitialHull()
        {
            if (m_Input.Count < 3)
                throw new System.Exception("Handle this error early");

            var arr = m_Input.ToArray();
            Sorting.QuickSort(ref arr, 0, arr.Length - 1, Comparer<Vector2>.Create((a, b) => a.x.CompareTo(b.x)));
            var leftMost    = new Point(arr[0]);
            var rightMost   = new Point(arr[arr.Length - 1]);
            // Debug.Log($"order by X-Axis {Print(arr, xAxisOrder)}");

            m_Hull = leftMost;
            leftMost.Insert(rightMost);

            var pool = new List<Point>(arr.Length - 2);
            for (int i = 1; i <= arr.Length - 2; ++i)
            {
                pool.Add(new Point(arr[i]));
            }

            FurthestPointOnLeft(leftMost, rightMost, pool); // process points above
            FurthestPointOnLeft(rightMost, leftMost, pool); // process points below
            return false;
        }

        /// <summary>Line (lp, rp), search for furthest point</summary>
        private void FurthestPointOnLeft(Point lp,  Point rp, List<Point> conflictList)
        {
            var furthest = (-1, -1f); // index, distance
            for (int i = 0; i < conflictList.Count; ++i)
            {
                var p = conflictList[i].value;
                if (p.CrossProduct(lp.value, rp.value) <= 0)
                    continue; // ignore point(s) on right & overlap.

                var dis = p.PerpendicularDistance(lp.value, rp.value);
                if (dis < furthest.Item2)
                    continue; // ignore closer point(s)

                furthest = (i, dis);
            }
            if (furthest.Item1 > -1)
            {
                // found furthest point.
                var p = conflictList[furthest.Item1];
                conflictList.RemoveAt(furthest.Item1);
                lp.Insert(p);
                Debug.Log($"Found {p.value}, between {lp.value} & {rp.value}, conflict cnt={conflictList.Count}");

                // form triangle, remove point within triangle
                int k = conflictList.Count;
                while (k--> 0)
                {
                    if (_IsPointWithInTriangle(lp, p, rp, conflictList[k]))
                    {
                        Debug.Log($"Skipped point in triangle {conflictList[k].value}");
                        conflictList.RemoveAt(k);
                        //Debug.DrawLine(lp.value.CastXZ2XYZ(), p.value.CastXZ2XYZ(), Color.red, 1f);
                        //Debug.DrawLine(p.value.CastXZ2XYZ(), rp.value.CastXZ2XYZ(), Color.red, 1f);
                        //Debug.DrawLine(rp.value.CastXZ2XYZ(),lp.value.CastXZ2XYZ(), Color.red, 1f);
                    }
                }

                FurthestPointOnLeft(lp, p, conflictList);
                FurthestPointOnLeft(p, rp, conflictList);
            }

            bool _IsPointWithInTriangle(Point a, Point b, Point c, Point test)
            {
                // a = left, b = furthest, c = right
                // assume a,b,c in clockwise order
                return // find inside
                    test.value.CrossProduct(a.value, b.value) >= 0 &&
                    test.value.CrossProduct(b.value, c.value) >= 0 &&
                    test.value.CrossProduct(c.value, a.value) >= 0;
                //bool outside =
                //    test.value.CrossProduct(a.value, b.value) <= 0 ||
                //    test.value.CrossProduct(b.value, c.value) <= 0 ||
                //    test.value.CrossProduct(c.value, a.value) <= 0;
                //return !outside;
            }
        }


        private bool OnComplete()
        {
            var n = m_Hull;
            while (n.next != null)
            {
                var a = n.value.CastXZ2XYZ();
                var b = n.next.value.CastXZ2XYZ();
                DebugExtend.DrawLine(a, b, Color.cyan, 1f, false);
                n = n.next;
            }

            {
                var a = n.value.CastXZ2XYZ();
                var b = m_Hull.value.CastXZ2XYZ();
                DebugExtend.DrawLine(a, b, Color.yellow, 1f, false);
            }
            return false;
        }

        public IEnumerable<Vector2> GetConvex(bool includeStartPoint = true)
        {
            var n = m_Hull;
            while (n.next != null)
            {
                yield return n.value;
                n = n.next;
            }
            if (includeStartPoint)
                yield return m_Hull.value; // last point is start point (again)
        }
    }
}
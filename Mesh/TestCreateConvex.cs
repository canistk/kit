using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Kit.MeshLib.Core.Test
{
    public class TestCreateConvex : MonoBehaviour
    {
        [SerializeField] Transform[] points;

        [ContextButton(nameof(CancelTask))]
        [SerializeField] bool m_CancelTask = false;
        private void CancelTask()
        {
            EditorTaskHandler.ClearUp();
            if (m_Task != null)
            {
                m_Task.Abort();
                m_Task = null;
            }
        }

        [ContextButton(nameof(CreateTask))]
        [SerializeField] bool m_CreateTask = false;

        private void CreateTask()
        {
            if (points == null || points.Length == 0)
            {
                Debug.LogError("Not point selected.");
                return;
            }

            var arr = points.Select(o => o.position).ToArray();
            m_Task = new QuickHull3d(arr);
            EditorTaskHandler.Add(m_Task);
        }

        kTask m_Task;
        [ContextButton(nameof(NextStep))]
        [SerializeField] bool m_NextStep = false;
        private void NextStep()
        {
            if (m_Task == null || m_Task is not TaskDebug debugTask)
                return;
            debugTask.TryMoveToNextStep();
#if UNITY_EDITOR
            UnityEditor.SceneView.RepaintAll();
#endif
        }

        private void OnDrawGizmos()
        {
            if (m_Task == null || m_Task is not IDrawGizmos gizmosTask)
                return;

            gizmosTask.DrawGizmos();
        }
    }
}
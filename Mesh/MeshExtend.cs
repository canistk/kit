using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kit.MeshKit.FaceEdge;

namespace Kit
{
	public static class MeshExtend
	{
		
		/// <summary>
		/// <see cref="https://catlikecoding.com/unity/tutorials/procedural-grid/"/>
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="xSize"></param>
		/// <param name="ySize"></param>
		/// <returns></returns>
		public static void GeneratePlane(string _name, int xSize, int ySize, out Mesh mesh)
        {
			Vector3[] vertices = new Vector3[(xSize + 1) * (ySize + 1)];
			Vector2[] uv = new Vector2[vertices.Length];
			Vector4[] tangents = new Vector4[vertices.Length];
			Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
			for (int i = 0, y = 0; y <= ySize; y++)
			{
				for (int x = 0; x <= xSize; x++, i++)
				{
					vertices[i] = new Vector3(x, y);
					uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
					tangents[i] = tangent;
				}
			}

			int[] triangles = new int[xSize * ySize * 6];
			for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++)
			{
				for (int x = 0; x < xSize; x++, ti += 6, vi++)
				{
                    triangles[ti] = vi;
                    triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                    triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                    triangles[ti + 5] = vi + xSize + 2;
                }
			}

			mesh = new Mesh
			{
				name = _name,
				vertices = vertices,
				uv = uv,
				triangles = triangles,
				tangents = tangents,
			};
		}

		/// <summary>
		/// <see cref="https://catlikecoding.com/unity/tutorials/procedural-grid/"/>
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="xSize"></param>
		/// <param name="ySize"></param>
		/// <returns></returns>
		public static IEnumerator GeneratePlaneDebug(string _name, int xSize, int ySize, float step = 0.1f)
		{
#if !UNITY_EDITOR
			yield break;
#else
			if (!Application.isPlaying)
			{
				Debug.LogError("For debug purpose, PlayMode only.");
				yield break;
			}
			
			Vector3[] vertices = new Vector3[(xSize + 1) * (ySize + 1)];
			Vector2[] uv = new Vector2[vertices.Length];
			Vector4[] tangents = new Vector4[vertices.Length];
			Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
			for (int i = 0, y = 0; y <= ySize; y++)
			{
				for (int x = 0; x <= xSize; x++, i++)
				{
					vertices[i] = new Vector3(x, y);
					uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
					tangents[i] = tangent;
				}
			}

			const float dd = 5f;
			const float pSize = 0.02f;
			Color _pointColor = Color.cyan;
			Color _lineColor = Color.red;
			int[] triangles = new int[xSize * ySize * 6];
			void Draw(int from, int to)
			{
				DebugExtend.DrawCircle(vertices[triangles[from]], Vector3.forward, _pointColor, pSize, dd);
				DebugExtend.DrawLine(vertices[triangles[from]], vertices[triangles[to]], _lineColor, dd);
			}

			for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++)
			{
				for (int x = 0; x < xSize; x++, vi++)
                {
					triangles[ti] = vi;
					triangles[ti + 3] = triangles[ti + 2] = vi + 1;
					triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
					triangles[ti + 5] = vi + xSize + 2;


					Draw(ti + 0, ti + 1);
                    yield return new WaitForSeconds(step);
					Draw(ti + 1, ti + 2);
                    yield return new WaitForSeconds(step);
					Draw(ti + 2, ti + 0);
					yield return new WaitForSeconds(step);

					Draw(ti + 3, ti + 4);
					yield return new WaitForSeconds(step);
					Draw(ti + 4, ti + 5);
					yield return new WaitForSeconds(step);
					Draw(ti + 5, ti + 3);
					yield return new WaitForSeconds(step);

                    ti += 6;
                }
            }
#endif
		}

		public static Mesh CreatePlaneXY(string _name, float _width, float _height)
		{
			List<Vector3> vertices = new List<Vector3>(10);
			List<Vector2> uv = new List<Vector2>(10);
			List<int> triangles = new List<int>(10);
			
			CalcPlaneXY(vertices, uv, triangles, _width, _height, Vector3.zero);
			// CalcPlaneXY(vertices, uv, triangles, _width, _height * 0.5f, new Vector3(0f, _height, 0f));

			Mesh _mesh = new Mesh
			{
				name = _name,
				vertices = vertices.ToArray(),
				uv = uv.ToArray(),
				triangles = triangles.ToArray(),
			};
			_mesh.RecalculateNormals();
			return _mesh;
		}

		public static void CalcPlaneXY(
			List<Vector3> vertices,
			List<Vector2> uv,
			List<int> triangles,
			float width, float height, Vector3 pivotOffset = default)
		{
			/*****
			1    3

			0    2
			//*****/
			// Vertices
			float _halfW = width * .5f;
			float _halfH = height * .5f;
			vertices.Add(pivotOffset + new Vector3(-_halfW, -_halfH, 0f)); // left-Bottom
			vertices.Add(pivotOffset + new Vector3(-_halfW, _halfH, 0f)); // left-Top
			vertices.Add(pivotOffset + new Vector3(_halfW, -_halfH, 0f)); // right-Bottom
			vertices.Add(pivotOffset + new Vector3(_halfW, _halfH, 0f)); // right-Top

			// UV
            uv.Add(new Vector2(0, 0)); // left-bottom
			uv.Add(new Vector2(0, 1)); // left-top
            uv.Add(new Vector2(1, 0)); // right-bottom
            uv.Add(new Vector2(1, 1)); // right-top

			// Triangles
			// 0,1,2
			int startIndex = triangles.Count;
			triangles.Add(startIndex + 0);
			triangles.Add(startIndex + 1);
			triangles.Add(startIndex + 2);

			// 2,1,3
			triangles.Add(startIndex + 2);
			triangles.Add(startIndex + 1);
			triangles.Add(startIndex + 3);
		}

		public static void RenderMesh(Transform transform, Mesh mesh)
		{
			MeshFilter meshFilter = transform.GetOrAddComponent<MeshFilter>();
			Renderer renderer = transform.GetOrAddComponent<MeshRenderer>();
			meshFilter.mesh = mesh;
		}

		/// <summary>Left hand system,
		/// assume a clockwise order vertex, generate a normal vector for this surface.
		/// </summary>
		/// <param name="v0"></param>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		internal static Vector3 GetLeftHandFaceNormal(Vector3 v0, Vector3 v1, Vector3 v2) => Vector3.Cross(v2 - v1, v0 - v1).normalized;

		/// <summary>Find out the point nearests, based on the vertex.
		/// <see cref="http://answers.unity3d.com/questions/7788/closest-point-on-mesh-collider.html"/> 
		/// </summary>
		/// <returns>The cloeset vertex to point.</returns>
		/// <param name="point">Point.</param>
		public static Vector3 ClosestVertexOnMeshTo(this MeshFilter meshFilter, Vector3 point)
		{
			// convert point to local space
			point = meshFilter.transform.InverseTransformPoint(point);
			float minDistanceSqr = Mathf.Infinity;
			Vector3 nearestVertex = Vector3.zero;
			// scan all vertices to find nearest
			foreach (Vector3 vertex in meshFilter.mesh.vertices)
			{
				Vector3 diff = point-vertex;
				float distSqr = diff.sqrMagnitude;
				if (distSqr < minDistanceSqr)
				{
					minDistanceSqr = distSqr;
					nearestVertex = vertex;
				}
			}
			// convert nearest vertex back to world space
			return meshFilter.transform.TransformPoint(nearestVertex);
		}

		/// <summary>
		/// <see cref="https://github.com/SimpleTalkCpp/workshop-2021-07-unity-shader/blob/main/Assets/Week002/Week002%20-%20Dissolve%20Trianlges/Week002_DissolveTriangles.cs"/>
		/// Rearrange mesh triangle and index,
		/// made the triangle disconnect to it's neighbor triangle.
		/// </summary>
		/// <example>
		/// ShuffleMeshTriangles(ref mesh, out Vector2[] newUv2, out Vector2[] newUv3, 4);
		/// mesh.uv2 = newUv2; // put somewhere else, if UV2 or UV3 was occupied.
		/// mesh.uv3 = newUv3;
		/// </example>
		/// <param name="mesh"></param>
		/// <param name="newUv2"></param>
		/// <param name="newUv3"></param>
		/// <param name="groupCount"></param>
		public static void SplitMeshTriangles(ref Mesh mesh, out Vector2[] newUv2, out Vector2[] newUv3, int groupCount = 4)
		{
			Vector3[] vertices = mesh.vertices;
			Vector2[] uv = mesh.uv;
			int[] triangles = mesh.triangles;

			int[] newTriangles = new int[triangles.Length];
			Vector3[] newVertices = new Vector3[triangles.Length];
			Vector2[] newUv = new Vector2[triangles.Length];
			newUv2 = new Vector2[triangles.Length];
			newUv3 = new Vector2[triangles.Length];

			int triCount = triangles.Length / 3;
			Vector3[] triCenters = new Vector3[triCount];

			float[] groupId = new float[triCount];

			for (int i = 0; i < triCount; i++)
			{
				Vector3 v0 = vertices[triangles[i * 3]];
				Vector3 v1 = vertices[triangles[i * 3 + 1]];
				Vector3 v2 = vertices[triangles[i * 3 + 2]];

				triCenters[i] = (v0 + v1 + v2) / 3;
				groupId[i] = (float)Random.Range(0, groupCount) / groupCount;
			}

			for (int i = 0; i < triangles.Length; i++)
			{
				var vi = triangles[i];
				newTriangles[i] = i;
				newVertices[i] = vertices[vi];
				newUv[i] = uv[vi];

				var tri = i / 3; // Cast float to Int, frac
				var center = triCenters[tri];
				newUv2[i] = new Vector2(center.x, center.y);
				newUv3[i] = new Vector2(center.z, groupId[tri]);
			}
			// Notes : newTriangles will be { 0,1,2,3,4,5.... ordered }
			// When triangles may be { 0, 1, 2, 2, 3, 0, ......}

			// var newMesh = new Mesh();
			mesh.name = mesh.name + "(Shuffle)";
			mesh.vertices = newVertices;
			mesh.uv = newUv;
			mesh.triangles = newTriangles;
			//mesh.uv2 = newUv2;
			//mesh.uv3 = newUv3;
		}


        private static void ThrowWithoutMesh(SkinnedMeshRenderer smr)
        {
            if (smr == null)
                throw new System.NullReferenceException("SkinnedMeshRenderer not found.");
            if (smr.sharedMesh == null)
                throw new System.NullReferenceException("Mesh not found.");
        }
        public static string GetBlendShapeName(SkinnedMeshRenderer smr, int shapeIndex)
        {
			ThrowWithoutMesh(smr);
			return smr.sharedMesh.GetBlendShapeName(shapeIndex);
		}

		public static Dictionary<string, int> GetBlendShapeDictionary(SkinnedMeshRenderer smr)
        {
			ThrowWithoutMesh(smr);

			int cnt = smr.sharedMesh.blendShapeCount;
			Dictionary<string, int> dict = new Dictionary<string, int>(cnt);
			for (int i = 0; i < cnt; ++i)
            {
				string bsName = smr.sharedMesh.GetBlendShapeName(i);
				if (!dict.ContainsKey(bsName))
					dict.Add(bsName, i);
			}
			return dict;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="dict"></param>
		/// <param name="bsName"></param>
		/// <param name="fuzzy01"></param>
		/// <returns>first int = dictionary index, int = match char</returns>
		public static IEnumerable<KeyValuePair<int, int>> FuzzyMatchBlendShapeBy(Dictionary<string, int> dict, string bsName, float fuzzy01 = 0.5f)
        {
			var bsArr = bsName.ToCharArray();
			fuzzy01 = Mathf.Clamp01(fuzzy01);
			int minMatchChar = Mathf.CeilToInt(fuzzy01 * bsArr.Length);
			foreach(var kvp in dict)
            {
				int distance = Kit.StringExtend.LevenshteinDistance(bsArr, kvp.Key.ToCharArray());
				if (minMatchChar <= distance)
                {
					yield return new KeyValuePair<int, int>(kvp.Value, distance);
                }
            }
        }

		public static int FuzzyMatchBlendShapeHighest(Dictionary<string, int> dict, string bsName, float fuzzy01 = 0.5f)
        {
			int highestMatchIndex = -1;
			int highestMatchScore = -1;
			foreach (var kvp in FuzzyMatchBlendShapeBy(dict, bsName, fuzzy01))
            {
				if (highestMatchScore < kvp.Value)
                {
					highestMatchIndex = kvp.Key;
					highestMatchScore = kvp.Value;
				}
			}
			return highestMatchIndex;
		}

        public static Mesh Clone(this Mesh obj)
        {
            if (obj == null)
                throw new System.NullReferenceException();
            var type = obj.GetType();
            Mesh rst = new Mesh();
            foreach (var property in type.GetProperties())
            {
                if (property.GetSetMethod() != null && property.GetGetMethod() != null)
                {
                    property.SetValue(rst, property.GetValue(obj, null), null);
                }
            }
            return rst;
        }

		/// <summary></summary>
		/// <param name="center"></param>
		/// <param name="rotation">remark : should look at anti-normal direction.</param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns></returns>
        public static Vector3[] CreateQuadVerteies(Vector3 center, Quaternion rotation, float width, float height)
		{
			var halfW = width  / 2f;
			var halfH = height / 2f;
			return new Vector3[]
			{
				center + rotation * new Vector3(-halfW, -halfH, 0f),
                center + rotation * new Vector3( halfW, -halfH, 0f),
                center + rotation * new Vector3( halfW,  halfH, 0f),
                center + rotation * new Vector3(-halfW,  halfH, 0f),
            };
        }

		public static void CreateCube(FEMesh myMesh, Vector3 center, float length)
			=> CreateCube(myMesh, center, Vector3.one * length);

		/// <summary>
		/// to create cube by using 
		/// <see cref="FEMesh.AddQuad(Vector3[], Vector2[], Vector3)"/>
		///    6-------5
		///   /|      /|
		///  / |     / |
		/// 3--7----2  4
		/// | /     | /
		/// |/      |/
		/// 0-------1
		/// </summary>
		/// <param name="feMesh"></param>
		/// <param name="center"></param>
		/// <param name="size"></param>
		public static void CreateCube(FEMesh feMesh, Vector3 center, Vector3 size)
		{
            /*
             * U = X axis
             * V = Y axis
             * 
             * OpenGL - Remark Unity3D
             * 0,1          1,1
             * 
             * 
             * 
             * 0,0          1,0
             * 
             * ===================
             * 
             * DirectX
             * 0,0          1,0
             * 
             * 
             * 
             * 0,1          1,1
             * 
             */
            var uv = new Vector4[]
            {
                new (0, 0, 0, 0),
                new (1, 0, 0, 0),
                new (1, 1, 0, 0),
                new (0, 1, 0, 0),
            };

            var normal = Vector3.back;
			var tangent = new Vector4(1, 0, 0, -1);
			var half = size * .5f;
			
            var p = CreateQuadVerteies(center + normal * half.z, Quaternion.LookRotation(-normal), size.x, size.y);
            feMesh.AddQuad(p, normal, tangent, uv);

            normal = Vector3.forward;
			tangent = new Vector4(-1, 0, 0, -1);
			// half = size.z * .5f;
            p = CreateQuadVerteies(center + normal * half.z, Quaternion.LookRotation(-normal), size.x, size.y);
            feMesh.AddQuad(p, normal, tangent, uv);

            normal = Vector3.left;
            tangent = new Vector4(0, 0, -1, -1);
            p = CreateQuadVerteies(center + normal * half.x, Quaternion.LookRotation(-normal), size.z, size.y);
            feMesh.AddQuad(p, normal, tangent, uv);

            normal = Vector3.right;
            tangent = new Vector4(0, 0, 1, -1);
            p = CreateQuadVerteies(center + normal * half.x, Quaternion.LookRotation(-normal), size.z, size.y);
            feMesh.AddQuad(p, normal, tangent, uv);

            normal = Vector3.up;
            tangent = new Vector4(1, 0, 0, -1);
            p = CreateQuadVerteies(center + normal * half.y, Quaternion.LookRotation(-normal), size.x, size.z);
            feMesh.AddQuad(p, normal, tangent, uv);

            normal = Vector3.down;
            tangent = new Vector4(1, 0, 0, -1);
            // half = size.y * .5f;
            p = CreateQuadVerteies(center + normal * half.y, Quaternion.LookRotation(-normal), size.x, size.z);
            feMesh.AddQuad(p, normal, tangent, uv);
        }

		public static void CreateSphere(FEMesh feMesh, Vector3 center, float radius, int xSegments, int ySegments)
		{
			if (feMesh == null)
                throw new System.NullReferenceException();
            if (xSegments < 2)
                xSegments = 2;
            if (ySegments < 2)
                ySegments = 2;

            // Latitude = y Segments, Longitude = x Segments
            var vertices		= new Vector3[(xSegments + 1) * (ySegments + 1)];
            var uv				= new Vector2[vertices.Length];

            for (int latitude = 0; latitude <= ySegments; latitude++)
            {
                var v			= -(float)latitude / ySegments;
                var theta		= v * Mathf.PI;
                var sinTheta	= Mathf.Sin(theta);
                var cosTheta	= Mathf.Cos(theta);

                for (int longitude = 0; longitude <= xSegments; longitude++)
                {
                    var u		= (float)longitude / xSegments;
                    var phi		= u * Mathf.PI * 2f;
                    var sinPhi	= Mathf.Sin(phi);
                    var cosPhi	= Mathf.Cos(phi);

                    int index = longitude + latitude * (xSegments + 1);
                    Vector3 vertex = new Vector3(
                        sinPhi * sinTheta * radius + center.x,
								 cosTheta * radius + center.y,
                        cosPhi * sinTheta * radius + center.z
                    );
                    vertices[index] = vertex;

                    uv[index] = new Vector4(-u, v, 0, 0);
                }
            }


			feMesh.Clean();
            for (int latitude = 0; latitude < ySegments; latitude++)
            {
                for (int longitude = 0; longitude < xSegments; longitude++)
                {
                    int p0 = (longitude + 0) + (latitude + 0) * (xSegments + 1);
                    int p1 = (longitude + 0) + (latitude + 1) * (xSegments + 1);
					int p2 = (longitude + 1) + (latitude + 0) * (xSegments + 1);
					int p3 = (longitude + 1) + (latitude + 1) * (xSegments + 1);

					{
						var points	= new Vector3[] { vertices[p0],	vertices[p2],	vertices[p1] };
						var uv0		= new Vector4[] { uv[p0],		uv[p2],			uv[p1] };
                        var normal	= (Centroid(points) - center).normalized;
						feMesh.AddTriangle(points, normal, Vector4.zero, uv0);
					}

                    {
                        var points	= new Vector3[] { vertices[p2], vertices[p3],	vertices[p1] };
                        var uv0		= new Vector4[] { uv[p2],		uv[p3],			uv[p1] };
						var normal	= (Centroid(points) - center).normalized;
                        feMesh.AddTriangle(points, normal, Vector4.zero, uv0);
                    }
                }
            }
        }

		public static void CreateCapsule(FEMesh feMesh, Vector3 center, float radius, float height, int direction, int xSegments, int ySegments)
		{
            if (feMesh == null)
                throw new System.NullReferenceException();
			if (direction < 0 || direction > 2)
				throw new System.ArgumentOutOfRangeException("0,1,2");

			// Latitude = y Segments, Longitude = x Segments
			if (xSegments < 2)
				xSegments = 2;
			if (ySegments < 8)
				ySegments = 8;
			if (ySegments % 2 == 0)
				++ySegments;

            var vertices	= new Vector3[(xSegments + 1) * (ySegments + 1)];
            var uv			= new Vector2[vertices.Length];
			var ySegAnchor	= Mathf.CeilToInt(ySegments * 0.5f);
			var offset		= Mathf.Max(0f, (height - (radius * 2f)) * .5f);
			var rot			= direction switch
			{
				0 => Quaternion.AngleAxis(90f, Vector3.forward),
				1 => Quaternion.identity,
				2 => Quaternion.AngleAxis(90f, Vector3.right),
				_ => throw new System.NotImplementedException(),
			};

			// Capsule based from Sphere, with height
            for (int latitude = 0; latitude <= ySegments; latitude++)
            {
                var v			= -(float)latitude / ySegments;
                var theta		= v * Mathf.PI;
                var sinTheta	= Mathf.Sin(theta);
                var cosTheta	= Mathf.Cos(theta);

				bool isTop = latitude < ySegAnchor;

                for (int longitude = 0; longitude <= xSegments; longitude++)
                {
                    var u		= (float)longitude / xSegments;
                    var phi		= u * Mathf.PI * 2f;
                    var sinPhi	= Mathf.Sin(phi);
                    var cosPhi	= Mathf.Cos(phi);

                    int index = longitude + latitude * (xSegments + 1);
                    Vector3 vertex = new Vector3(
                        sinPhi * sinTheta * radius + center.x,
                                 cosTheta * radius + center.y,
                        cosPhi * sinTheta * radius + center.z
                    );

                    // move vertex to top/bottom Hemisphere
                    vertex += new Vector3(0f, isTop ? offset : -offset, 0f);

                    // remap v to capsule's vertex's height
                    if (height < radius * 2f)
                        v = Mathf.Clamp01((vertex.y + radius) / (radius * 2f));
					else 
						v = Mathf.Clamp01((vertex.y + height * .5f) / height);
                    uv[index] = new Vector4(-u, v, 0, 0);

					// transform vertex position base on direction.
                    vertices[index] = rot * vertex;
                }
            }

			feMesh.Clean();
            for (int latitude = 0; latitude < ySegments; latitude++)
            {
                for (int longitude = 0; longitude < xSegments; longitude++)
                {
                    int p0 = (longitude + 0) + (latitude + 0) * (xSegments + 1);
                    int p1 = (longitude + 0) + (latitude + 1) * (xSegments + 1);
					int p2 = (longitude + 1) + (latitude + 0) * (xSegments + 1);
					int p3 = (longitude + 1) + (latitude + 1) * (xSegments + 1);

					{
						var points	= new Vector3[] { vertices[p0], vertices[p2],	vertices[p1] };
						var uv0		= new Vector4[] { uv[p0],		uv[p2],			uv[p1] };
                        var normal	= (Centroid(points) - center).normalized;
						feMesh.AddTriangle(points, normal, Vector4.zero, uv0);
					}

                    {
                        var points	= new Vector3[] { vertices[p2],	vertices[p3],	vertices[p1] };
                        var uv0		= new Vector4[] { uv[p2],		uv[p3],			uv[p1] };
                        var normal = (Centroid(points) - center).normalized;
                        feMesh.AddTriangle(points, normal, Vector4.zero, uv0);
                    }
                }
            }
        }

		private static Vector3 Centroid(params Vector3[] points)
		{
			var sum = Vector3.zero;
			for (int i = 0; i < points.Length; ++i)
			{
				sum += points[i];
			}
			return sum / points.Length;
		}
    }
}
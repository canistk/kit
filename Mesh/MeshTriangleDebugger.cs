using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;
using Kit.Coordinates;

[ExecuteInEditMode]
public class MeshTriangleDebugger : MonoBehaviour
{
    [SerializeField] Mesh m_OrgMesh = null;
    [SerializeField] Mesh m_Mesh = null;
#pragma warning disable 414
    [SerializeField, ContextButton(nameof(RecordMesh))] bool m_RecordMesh = false;
#pragma warning restore 414
    private void RecordMesh()
    {
        MeshFilter mf = GetComponent<MeshFilter>();
        if (m_OrgMesh == null)
            m_OrgMesh = mf.sharedMesh;
        m_Mesh = mf.sharedMesh.Clone();
    }
#pragma warning disable 414
    [SerializeField, ContextButton(nameof(RestoreMesh))] bool m_RestoreMesh = false;
#pragma warning restore 414
    private void RestoreMesh()
    {
        MeshFilter mf = GetComponent<MeshFilter>();
        mf.sharedMesh = m_Mesh = m_OrgMesh.Clone();
    }

    [Space(10)]
#pragma warning disable 414
    [SerializeField, ContextButton(nameof(ShuffleMesh))] bool m_ShuffleMesh = false;
#pragma warning restore 414
    [SerializeField] int m_ShuffleTriangleGroup = 4;
    private void ShuffleMesh()
    {
        if (m_Mesh == null)
            return;
        MeshExtend.SplitMeshTriangles(ref m_Mesh,
                    out Vector2[] newUv2,
                    out Vector2[] newUv3, m_ShuffleTriangleGroup);
        m_Mesh.uv2 = newUv2;
        m_Mesh.uv3 = newUv3;
        m_Mesh.RecalculateNormals();
    }

    private void Reset()
    {
        RecordMesh();
    }

    [Header("Gizmos")]
    [Range(0f, 10f)] public float m_DebugMargin = 0f;
    [Range(-1f, 1f)] public float m_DebugPadding = 0f;
    [MinMaxSlider(0f, 1f)] public Vector2 m_DrawRange = Vector2.up;
    public Color m_TriangleColor00 = new Color(1,0,0,1);
    public Color m_TriangleColor01 = new Color(1f, 0.5f, 0f, 1f);
    public Color m_TriangleColor02 = new Color(1f, 1f, 0f, 1f);
    public Color m_NormalColor = Color.green;
    public bool m_DebugDrawTriangle     = true;
    public bool m_DebugDrawNormal       = false;
    [Header("Label")]
    public bool m_DebugMeshCommonInfo   = false;
    public bool m_DebugDrawTriangleLabel= false;
    public bool m_DebugDrawVertexLabel  = false;
    public bool m_DebugDrawIndexLabel   = false;

    private System.Text.StringBuilder m_sb = new System.Text.StringBuilder(100);

    private void Update()
    {
        DrawGL(false, true);
    }

    private void OnDrawGizmos()
    {
        DrawGL(true, false);
    }

    private void DrawGL(bool drawLabel, bool drawLine)
    {
        if (drawLabel && !(m_DebugMeshCommonInfo || m_DebugDrawIndexLabel || m_DebugDrawVertexLabel || m_DebugDrawTriangleLabel))
            return;

        if (drawLine && !(m_DebugDrawNormal || m_DebugDrawTriangle))
            return;

        if (m_Mesh == null)
            return;


        if (m_DebugMeshCommonInfo)
        {
            string str = $"vertices : {m_Mesh.vertices.Length}\nTriangles : {m_Mesh.triangles.Length}";
            GizmosExtend.DrawLabel(transform.position, str);
        }

        float padding = m_DebugPadding;
        float marign = m_DebugMargin;
        int i0, i1, i2;
        Vector3 v0, v1, v2, n0, n1, n2, nCenter, cc;
        Matrix4x4 matrix;

        int vertexCnt = m_Mesh.triangles.Length;
        int min = Mathf.RoundToInt(m_DrawRange.x * vertexCnt);
        min = min - (min % 3); // fit triangle pattern
        int max = Mathf.RoundToInt(m_DrawRange.y * vertexCnt);
        for (int i = min; i < m_Mesh.triangles.Length && i < max; i += 3)
        {
            matrix = transform.localToWorldMatrix;
            // Vertices
            i0 = m_Mesh.triangles[i];
            v0 = matrix.MultiplyPoint(m_Mesh.vertices[i0]);
            n0 = m_Mesh.normals[i0];
            i1 = m_Mesh.triangles[i + 1];
            v1 = matrix.MultiplyPoint(m_Mesh.vertices[i1]);
            n1 = m_Mesh.normals[i1];
            i2 = m_Mesh.triangles[i + 2];
            v2 = matrix.MultiplyPoint(m_Mesh.vertices[i2]);
            n2 = m_Mesh.normals[i2];

            cc = (v0 + v1 + v2) / 3f;
            nCenter = (n0 + n1 + n2).normalized;
            var nn = nCenter * marign;
            
            v0 += nn + ((cc - v0).normalized * padding);
            v1 += nn + ((cc - v1).normalized * padding);
            v2 += nn + ((cc - v2).normalized * padding);
            cc = (v0 + v1 + v2) / 3f;

            if (drawLabel && (m_DebugDrawVertexLabel || m_DebugDrawIndexLabel))
            {
                m_sb.Clear();
                if (m_DebugDrawVertexLabel) m_sb.Append('v').Append(i0);
                if (m_DebugDrawVertexLabel && m_DebugDrawIndexLabel) m_sb.Append('-');
                if (m_DebugDrawIndexLabel) m_sb.Append('i').Append(i);
                GizmosExtend.DrawLabel(v0, m_sb.ToString());
                m_sb.Clear();
                if (m_DebugDrawVertexLabel) m_sb.Append('v').Append(i1);
                if (m_DebugDrawVertexLabel && m_DebugDrawIndexLabel) m_sb.Append('-');
                if (m_DebugDrawIndexLabel) m_sb.Append('i').Append(i + 1);
                GizmosExtend.DrawLabel(v1, m_sb.ToString());
                m_sb.Clear();
                if (m_DebugDrawVertexLabel) m_sb.Append('v').Append(i2);
                if (m_DebugDrawVertexLabel && m_DebugDrawIndexLabel) m_sb.Append('-');
                if (m_DebugDrawIndexLabel) m_sb.Append('i').Append(i + 2);
                GizmosExtend.DrawLabel(v2, m_sb.ToString());
            }

            // Normal
            if (m_DebugDrawNormal && m_NormalColor.a > float.Epsilon)
            {
                GLExtend.DrawLine(cc, cc + nCenter * 0.05f, m_NormalColor);
            }

            if (m_DebugDrawTriangle)
            {
                //GLExtend.DrawLine(v0, v1, m_TriangleColor00);
                //GLExtend.DrawLine(v1, v2, m_TriangleColor01);
                //GLExtend.DrawLine(v2, v0, m_TriangleColor02);
                GLExtend.DrawLineTriangle(v0, v1, v2, m_TriangleColor00, m_TriangleColor01, m_TriangleColor02);
            }
            if (drawLabel && m_DebugDrawTriangleLabel)
            {
                m_sb.Clear().Append('T').Append(Mathf.CeilToInt(i / 3));
                GizmosExtend.DrawLabel(cc, m_sb.ToString(), 0f, 0f, false);
            }
        }
        GL.End();

    }


}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.MeshLib.Core
{
    /// <summary>
    /// <see cref="https://web.ntnu.edu.tw/~algo/ConvexHull.html"/>
    /// <see cref="https://media.steampowered.com/apps/valve/2014/DirkGregorius_ImplementingQuickHull.pdf"/>
    /// </summary>
    public class QuickHull3d : TaskDebug, IDrawGizmos
    {
        #region Mesh Structure
        private List<Face> m_Faces = new List<Face>();

        private class Point
        {
            public Vector3 position;
            public Point(Vector3 p)
            {
                this.position = p;
            }
            public static implicit operator Vector3(Point p) { return p.position; }
            public static explicit operator Point(Vector3 p) { return new Point(p); }
        }

        private class HalfEdge
        {
            public readonly Point origin;
            public HalfEdge(Point origin)
            {
                this.origin = origin;
            }
            public Face incidentFace;
            public void SetIncidentFace(Face face) { this.incidentFace = face; }

            public bool HasPoints(Point a, Point b)
            {
                if (next != null)
                {
                    return
                        a == origin && b == next.origin ||
                        b == origin && a == next.origin;
                }
                return false;
            }
            public bool TryGetTailPoint(out Point tail)
            {
                if (next == null)
                {
                    tail = default;
                    return false;
                }
                tail = next.origin;
                return tail != origin;
            }

            // public HalfEdge twin;
            public HalfEdge next;
            public HalfEdge prev;
        }

        private class Face
        {
            public HalfEdge m_EdgeHead;
            private readonly int m_PointCnt;
            public readonly Vector3 centroid;
            public Face(params Point[] points)
            {
                if (points.Length < 3)
                    throw new System.ArgumentException("At least 3 point to create face(triangle).");
                this.m_PointCnt = points.Length;
                this.m_Distance_Normal = (-1f, Vector3.zero);

                var c = points[0].position;
                var pt = m_EdgeHead = new HalfEdge(points[0]);
                pt.SetIncidentFace(this);
                for (int i = 1; i < points.Length; ++i)
                {
                    pt.next = new HalfEdge(points[i]);
                    pt.next.prev = pt;
                    pt = pt.next;

                    c += points[i];
                }
                pt.next = m_EdgeHead; // last -> first
                m_EdgeHead.prev = pt;

                this.centroid = c / (float)points.Length;
            }

            public IEnumerable<HalfEdge> GetEdges(bool inverse = false)
            {
                if (m_EdgeHead == null)
                    yield break;
                var edge = m_EdgeHead;
                for (int i = 0; i < m_PointCnt; ++i)
                {
                    yield return edge;
                    edge = inverse ? edge.prev : edge.next;
                }
            }

            public IEnumerable<Point> GetPointsInOrder(bool inverse)
            {
                foreach (var edge in GetEdges(inverse))
                    yield return edge.origin;
            }

            public Face Flip()
            {
                var arr = GetPointsInOrder(true).ToArray();
                return new Face(arr);
            }

            public bool FindEdge(Point a, Point b, out HalfEdge edge)
            {
                foreach (var ele in GetEdges())
                {
                    if (ele.HasPoints(a, b))
                    {
                        edge = ele;
                        return true;
                    }
                }
                edge = default;
                return false;
            }

            /// <summary></summary>
            /// <param name="org"></param>
            /// <param name="twinEdge">The edge in inverse direction</param>
            /// <returns></returns>
            public bool FindTwinEdge(HalfEdge org, out HalfEdge twinEdge)
            {
                if (org == null || !org.TryGetTailPoint(out var from))
                {
                    twinEdge = default;
                    return false;
                }
                var to = org.origin;

                foreach (var ele in GetEdges())
                {
                    if (ele.origin == from &&
                        ele.TryGetTailPoint(out var p) &&
                        p == to)
                    {
                        twinEdge = ele;
                        return true;
                    }
                }
                twinEdge = default;
                return false;
            }

            /// <summary>
            /// <see cref="Plane.Set3Points(Vector3, Vector3, Vector3)"/>
            /// </summary>
            private (float, Vector3) m_Distance_Normal;
            public Vector3 normal
            {
                get
                {
                    if (m_Distance_Normal.Item1 >= 0f)
                        return m_Distance_Normal.Item2;
                    if (m_EdgeHead == null)
                        return default;
                    var edge = m_EdgeHead;
                    {
                        var p0 = edge.origin.position; edge = edge.next;
                        var p1 = edge.origin.position; edge = edge.next;
                        var p2 = edge.origin.position; edge = edge.next;
                        var n = Vector3.Normalize(Vector3.Cross(p1 - p0, p2 - p0));
                        var d = 0f - Vector3.Dot(n, p0);
                        m_Distance_Normal = (d, n);
                    }
                    return m_Distance_Normal.Item2;
                }
            }
            
            public Vector3 ClosestPointOnPlane(Vector3 point)
            {
                if (normal == Vector3.zero)
                    throw new System.Exception("Invalid cases.");
                var distance2origin = m_Distance_Normal.Item1;
                var num = Vector3.Dot(normal, point) + distance2origin;
                return point - normal * num;
            }

            /// <summary>
            /// <see cref="Plane.GetSide(Vector3)"/>
            /// </summary>
            /// <param name="point"></param>
            /// <returns></returns>
            public bool IsPositiveSide(Vector3 point)
            {
                return Vector3.Dot(normal, point) + m_Distance_Normal.Item1 > 0f;
            }

            public void Draw(Color? color, float margin = 0f)
            {
                var c = color.HasValue ? color.Value : Color.white;
                using (new ColorScope(c))
                {
                    foreach (var edge in GetEdges())
                    {
                        var a0 = edge.origin.position;
                        var b0 = edge.next.origin.position;

                        var a1 = margin == 0f ? a0 : a0 + ((a0 - centroid).normalized * margin);
                        var b1 = margin == 0f ? b0 : b0 + ((b0 - centroid).normalized * margin);
                        // Debug.Log($"Draw {a1:F2} {b1:F2}");
                        Gizmos.DrawLine(a1, b1);
                    }
                    Gizmos.DrawLine(centroid, centroid + normal * 0.5f);
                }
            }
        }
        #endregion Mesh Structure

        public enum eState
        {
            Horizon = 0,
            Tetrahedron,
            Hull,

            Completed,
            Error,
        }
        public eState state { get; private set; } = eState.Horizon;

        private List<Point> m_ConflictList = new List<Point>();

        public QuickHull3d(IList<Vector3> points)
        {
            m_ConflictList = new List<Point>(points.Count);
            DefineHorizon(points, m_ConflictList, out m_Horizon);
            m_RequestToNextStep = 1;
        }

        protected override bool InternalStepExecute()
        {
            switch (state)
            {
                case eState.Horizon: ++state; break;
                case eState.Tetrahedron:
                    DefineTetrahedron(m_ConflictList, m_Horizon, m_Faces);
                    ++state;
                    break;
                case eState.Hull:
                    Hull(m_ConflictList, m_Faces, ref m_HullIndex);

                    if (m_ConflictList.Count == 0)
                        ++state;
                    break;
            }

            return state < eState.Completed;
        }

        public void DrawGizmos()
        {
            switch (state)
            {
                case eState.Horizon:        
                case eState.Tetrahedron:
                    DrawGizmosHorizon(); break;
                default:
                    DrawHull();
                    break;
            }
        }

        /// <summary>
        /// 1) Construct conflict list
        /// 2) Find left,right point
        /// 3) Find furthest point
        /// </summary>
        /// <param name="points"></param>
        /// <param name="conflict"></param>
        /// <param name="result"></param>
        private void DefineHorizon(IList<Vector3> points,
            List<Point> conflict,
            out Horizon result)
        {
            var l = (-1, float.MaxValue);
            var r = (-1, float.MinValue);

            // 1) Construct conflict list
            // 2) Find left,right point
            for (int i = 0; i < points.Count; ++i)
            {
                var point = points[i];
                conflict.Add(new Point(point));
                if (l.Item2 > point.x)
                {
                    l = (i, point.x);
                }
                if (r.Item2 < point.x)
                {
                    r = (i, point.x);
                }
            }

            var left    = conflict[l.Item1];
            var right   = conflict[r.Item1];

            // 3) Find furthest point
            (Point, float) f = (null, -1f);
            var k = conflict.Count;
            while (k-- > 0)
            {
                var p = conflict[k];
                if (p == left || p == right)
                {
                    conflict.RemoveAt(k);
                    continue;
                }

                var dis = Vector3Extend.PerpendicularDistance(left.position, right.position, p.position);
                if (f.Item2 < dis)
                {
                    f = (p, dis);
                }
            }
            var furthest = f.Item1;
            conflict.Remove(f.Item1);

            result = new Horizon
            {
                left = left,
                right = right,
                furthest = furthest,
            };
        }

        private Horizon m_Horizon;
        private struct Horizon { public Point left, right, furthest; }
        private void DrawGizmosHorizon()
        {
            var p0 = m_Horizon.left;
            var p1 = m_Horizon.right;
            var p2 = m_Horizon.furthest;
            using (new ColorScope(Color.white))
            {
                GizmosExtend.DrawLine(p0, p1);
                GizmosExtend.DrawLine(p1, p2);
                GizmosExtend.DrawLine(p2, p0);
            }
        }

        /// <summary>
        /// 4) Find 4th point
        /// 5) Calculate centroid
        /// 6) create 4 faces -> Tetrahedron in Hull(m_Faces)
        /// </summary>
        private void DefineTetrahedron(List<Point> conflict, Horizon horizon, List<Face> faces)
        {
            var p0      = horizon.left        ;
            var p1      = horizon.right       ;
            var p2      = horizon.furthest    ;
            var plane   = new Plane(p0, p1, p2);
            
            (int, float) furthest = (-1, -1f);
            for (int i = 0; i < conflict.Count; ++i)
            {
                var p   = conflict[i].position;
                // optimize : cached plane normal, and distance without sqrt.
                var disSqrt = (p - plane.ClosestPointOnPlane(p)).sqrMagnitude;
                if (furthest.Item2 < disSqrt)
                {
                    furthest = (i, disSqrt);
                }
            }

            if (furthest.Item1 == -1)
            {
                Debug.LogError($"Fail to execute during state={state}");
                state = eState.Error;
            }

            // Define tetrahedron
            // We had horizon + far point (4 points)
            var far         = conflict[furthest.Item1];
            var centroid    = ((Vector3)p0 + p1 + p2 + far) / 4f;
            conflict.Remove(far);

            //***
            {
                var c = Color.cyan.CloneAlpha(0.5f);
                var t = 3f;
                Debug.DrawLine(p0, p2, c, t);
                Debug.DrawLine(p0, p1, c, t);
                Debug.DrawLine(p1, p2, c, t);

                Debug.DrawLine(p2, far, c, t);
                Debug.DrawLine(p0, far, c, t);
                Debug.DrawLine(p1, far, c, t);
            
                c = Color.yellow.CloneAlpha(0.5f);
                DebugExtend.DrawPoint(centroid, c, 0.5f, t);
            }
            //***/

            {
                var tmpFace = new Face(p0, p1, p2);
                AddFaceAgainCentroid(faces, tmpFace, centroid);
            }

            foreach (var f in CreateNewFaces(far, p0, p1, p2))
            {
                AddFaceAgainCentroid(faces, f, centroid);
            }

            RemovePointsInsideHull(conflict, faces);
        }

        private IEnumerable<Face> CreateNewFaces(Point point, params Point[] points)
        {
            for (int i = 0; i < points.Length; ++i)
            {
                var p0 = points[i];
                var p1 = points[(i + 1) % points.Length];
                var f = new Face(p0, p1, point);
                // Debug.Log($"Create face = {p0.position:F2}, {p1.position:F2}, {point.position:F2}");
                // f.Draw(Color.red, -0.5f, 3f);
                yield return f;
            }
        }

        private int m_HullIndex = -1;
        /// <summary>
        /// 7) select face in hull,
        /// 8) find furthest positive point depended on selected face,
        /// 9) add furthest point into hull,
        /// continue process 7~9.
        /// </summary>
        /// <param name="conflict"></param>
        /// <param name="faces"></param>
        /// <param name="index"></param>
        private void Hull(List<Point> conflict, List<Face> faces, ref int index)
        {
            if (conflict    == null || conflict.Count   == 0 ||
                faces       == null || faces.Count      == 0)
                return;
            
            ++index;
            if (index >= faces.Count)
                throw new Exception("Invalid logic");

            var furthest    = (-1, -1f); // points index, distance
            var face        = faces[index];
            {
                var i = conflict.Count;
                while (i-- > 0)
                {
                    var p = conflict[i];
                    // optimize : cached plane normal, and distance without sqrt.
                    if (!face.IsPositiveSide(p))
                        continue;
                    var disSqrt = (p.position - face.ClosestPointOnPlane(p)).sqrMagnitude;
                    if (furthest.Item2 < disSqrt)
                    {
                        furthest = (i, disSqrt);
                    }
                }
            }

            if (furthest.Item1 == -1)
            {
                // common cases, no point in front of selected faces.
                return;
            }

            {
                var p           = conflict[furthest.Item1];
                var arr         = face.GetPointsInOrder(false).ToArray();
                conflict.RemoveAt(furthest.Item1);
                faces.Remove(face);
                var centroid    = CalCentroid(p, arr);
                foreach (var f in CreateNewFaces(p, arr))
                {
                    AddFaceAgainCentroid(faces, f, centroid);
                }
            }
            RemovePointsInsideHull(conflict, faces);
        }
        private void DrawHull()
        {
            var c = Color.cyan.CloneAlpha(0.5f);
            foreach (var face in m_Faces)
            {
                face.Draw(c, 0f);
            }
            foreach (var p in m_ConflictList)
            {
                GizmosExtend.DrawSphere(p.position, 1f, Color.red);
            }
        }

        private void RemovePointsInsideHull(List<Point> conflict, List<Face> faces)
        {
            if (conflict.Count == 0)
                return;
            var i = conflict.Count;
            var removed = 0;
            while (i-- > 0)
            {
                var point       = conflict[i];
                var outside     = false;
                for (int k = 0; k < faces.Count && !outside; ++k)
                {
                    if (!faces[k].IsPositiveSide(point))
                        continue;
                    outside = true; // stop search
                }
                
                if (!outside)
                {
                    // point is inside all faces. which mean within convex hull.
                    conflict.RemoveAt(i);
                    ++removed;
                }
            }
            if (removed > 0)
                Debug.Log($"Remove point(s)={removed} within convex hull.");
        }

        private void AddFaceAgainCentroid(List<Face> faces, Face _face, Vector3 _centroid)
        {
            if (_face.IsPositiveSide(_centroid))
                faces.Add(_face.Flip());
            else
                faces.Add(_face);

            //faces.Add(_face.IsPositiveSide(_centroid) ?
            //    _face.Flip() :
            //    _face);
        }

        private Vector3 CalCentroid(Point org, params Point[] points)
        {
            var c   = org.position;
            for (int i = 0; i < points.Length; ++i)
            {
                c += points[i].position;
            }

            int cnt = points.Length + 1; // org +1
            return c / (float)cnt;
        }
    }
}
﻿#if UNITY_STANDALONE && SERIALPORT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.IO.Ports;

namespace Kit
{
	public class SerialPortAgent : MonoBehaviour
	{
		public SerialPortConfig m_SerialPortConfig;
		[Space]
		[SerializeField] bool m_EnableAutoConnection = true;
		[SerializeField] bool m_EnableReadStream = false;
		[SerializeField] bool m_DebugReadStreamBuffer = false;

		[Header("Threading(unsafe)")]
		[SerializeField] int m_ThreadInterval = 100;

		protected SerialPortThreadBase serialPortThread;

		protected virtual void Awake()
		{
			
		}

		private void OnEnable()
		{
			PreInit();
		}

		private void OnDisable()
		{
			Close();
		}

		private void Update()
		{
			if (m_CmdQueue != null)
			{
				int cnt = m_CmdQueue.Count;
				if (cnt > 0)
				{
					for (int i = 0; i < cnt; i++)
					{
						CommandReceived obj = m_CmdQueue.Dequeue();
						obj.Invoke(); // dispatch to mainthread.
					}
				}
			}
		}

		private void PreInit()
		{
			if (!SerialPortManager.TryGetComPort(m_SerialPortConfig.m_PortNumber, out serialPortThread))
			{
				InitSerialPort(m_SerialPortConfig, ref serialPortThread);
			}

			if (serialPortThread == null)
			{
				this.enabled = false;
				throw new System.Exception($"Fail to fetch target serialport {m_SerialPortConfig.PortName}");
			}
			else
			{
				serialPortThread.serialPinChanged += PinChanged;
				serialPortThread.serialDataReceived += DataReceived;
				serialPortThread.serialErrorReceived += ErrorReceived;
				serialPortThread.disposed += OnDisposed;

				if (m_EnableAutoConnection)
					Open();
			}
		}

		protected virtual void InitSerialPort(SerialPortConfig serialPortConfig, ref SerialPortThreadBase serialPortThreadBase)
		{
			SerialPort serialPort = serialPortConfig.ToSerialPort(new SerialPort());
			serialPortThreadBase = new SerialPortThread(serialPort);
			serialPortThreadBase.BeginThread(m_ThreadInterval);
		}

		public void Open()
		{
			if (!serialPortThread.IsOpen)
				serialPortThread.Open();
			if (m_EnableReadStream && !serialPortThread.isReadStreaming)
				BeginStream();
		}

		public void Close()
		{
			if (serialPortThread != null)
			{
				serialPortThread.serialPinChanged -= PinChanged;
				serialPortThread.serialDataReceived -= DataReceived;
				serialPortThread.serialErrorReceived -= ErrorReceived;
				serialPortThread.disposed -= OnDisposed;
				if (serialPortThread.IsOpen)
					serialPortThread.Close();
			}
		}

		#region Raw Event Redirection
		private void ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
		{
			throw new System.Exception("Serial Port Error : "+ e);
		}

		private void DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			// This handler will not work in Unity.
			// https://stackoverflow.com/questions/26591189/c-sharp-serialport-eventhandler-in-unity
			Debug.LogWarning("TODO:Implement - DataReceived " + e);
		}

		private void PinChanged(object sender, SerialPinChangedEventArgs e)
		{
			Debug.LogWarning("TODO:Implement - PinChanged " + e);
		}
		private void OnDisposed()
		{
			Debug.LogWarning("TODO:Implement - OnDisposed");
		}
		#endregion Raw Event Redirection

		#region Read Stream
		protected byte[] m_Buffer;
		protected int m_WriteOffset = 0;
		protected MemoryStream m_MemoryStream = null;
		protected BinaryReader m_BinaryReader = null;
		[SerializeField] protected int m_BufferSize = 256;
		protected int writePointer { get => m_WriteOffset; } // set => m_WriteOffset = value; }
		private void SetWritePoint(int value) { m_WriteOffset = value; }
		protected int readPointer { get => (int)m_MemoryStream.Position; } // set => m_MemoryStream.Position = value; }
#pragma warning disable 0414
		[ContextButton(nameof(BeginStream))]
		[SerializeField] bool m_BeginStream = false;
#pragma warning restore 0414
		public void BeginStream()
		{
			if (isActiveAndEnabled)
			{
				m_CmdQueue = new Queue<CommandReceived>();
				serialPortThread.BeginReadStream(MultiThreadInit, MultiThreadLoop, MultiThreadDispose);
			}
		}
#pragma warning disable 0414
		[ContextButton(nameof(EndStream))]
		[SerializeField] bool m_EndStream = false;
#pragma warning restore 0414
		public void EndStream()
		{
			if (isActiveAndEnabled)
				serialPortThread.EndReadStream();
		}

		private void MultiThreadInit(SerialPort serialPort, Callback<string> log, Callback<string> logWarning, Callback<string> logError)
		{
			m_Buffer = new byte[m_BufferSize];
			m_MemoryStream = new MemoryStream(m_Buffer);
			m_BinaryReader = new BinaryReader(m_MemoryStream);
		}
		private void MultiThreadLoop(SerialPort serialPort, Callback<string> log, Callback<string> logWarning, Callback<string> logError)
		{
			bool changed = false;
			try
			{
				// Error handle for buffern not enough
				int chunk = m_Buffer.Length - writePointer;
				if (chunk <= 0)
				{
					logError($"Not enought buffer({m_Buffer.Length}), force reset pointer({writePointer}) to 0");
					string fullText = System.Text.Encoding.Default.GetString(m_Buffer);
					logError(fullText);
					SetWritePoint(0);
					changed = true;
				}

				int before = writePointer;
				// as same as m_MemoryStream.Write(tempBuffer, writePointer, tempBuffer.Length);
				int x = serialPort.Read(m_Buffer, writePointer, chunk);
				if (x > 0)
				{
					int after = writePointer + x;
					SetWritePoint(after);
					if (m_DebugReadStreamBuffer)
						log("read".ToRichText(Color.red) + " " + Debug_GetPointersInfo(true) +
							Debug_PrintBufferContent(SerialPortUtils.HightLightColor(
								(before, Color.yellow),
								(after, Color.cyan),
								Color.green
							)));
					changed = true;
				}
				if (changed)
				{
					ReadStreamChanged(m_Buffer);
				}
			}
			catch (System.TimeoutException)
			{
				// hidden timeout exception since it's too common
			}
			catch
			{
				throw;
			}
			finally
			{
			}
		}
		private void MultiThreadDispose(SerialPort serialPort, Callback<string> log, Callback<string> logWarning, Callback<string> logError)
		{
			if (m_BinaryReader != null)
			{
				m_BinaryReader.Close();
				m_BinaryReader = null;
			}
			if (m_MemoryStream != null)
			{
				m_MemoryStream.Close();
				// m_MemoryStream.Dispose();
				m_MemoryStream = null;
			}
			if (m_Buffer != null)
			{
				m_Buffer = null;
			}
		}

		protected void ReadStreamChanged(byte[] buffer)
		{
			/// you also can debug multithread via these methods
			/// <see cref="ThreadLog(string)"/>
			/// <see cref="ThreadLogWarning(string)"/>
			/// <see cref="ThreadLogError(string)"/>
			/// use it wisely
			if (readPointer > writePointer)
				throw new System.Exception("Buffer pointer error.");

			// TODO: when the buffer cache too many command, we need to to until all clear.
			int loopCycle = 0;
			int matchCount = 0;
			int cmdCount = 0;
			bool catchup = false;
			do
			{
				matchCount = 0;
				foreach (StreamCommand cmd in m_CmdDict.Values)
				{
					int index = buffer.MatchPattern(cmd.headerHash, readPointer, writePointer);
					if (index >= 0)
					{
						matchCount++;
						int validBufferLength = writePointer - index;
						bool waitUntilBufferWriteIn = validBufferLength < cmd.expectLength;
						if (waitUntilBufferWriteIn)
						{
							// command found, but expect length isn't ready yet.
							// ThreadLog($"Wait command : [{cmd.header}]");
							catchup = true;
							break; // Skip the rest.
						}
						else if (index + cmd.expectLength < m_MemoryStream.Length)
						{
							byte[] data = ReadBuffer(index, cmd.expectLength);

							// main thread queue.
							m_CmdQueue.Enqueue(new CommandReceived(cmd, data));
							cmdCount++;
							break;
						}
						else
						{
							ThreadLogError("FIX THIS : Logic ERROR.");
							break;
						}
					}
				} // foreach
				loopCycle++;

				if (loopCycle > 0 && matchCount == 0)
					break;
				if (matchCount == 0 && cmdCount == 0) // no valid record
					break;
				if (readPointer == writePointer)
					break; // catch up.
			}
			while (matchCount ==0 || !catchup);

			//if (cmdCount > 1) ThreadLogWarning("Read cmd up to " + cmdCount);
			RemoveUnknownCommand(buffer);
		}
		protected struct CommandReceived
		{
			public StreamCommand cmd;
			public byte[] data;
			public CommandReceived(StreamCommand cmd, byte[] data)
			{
				this.cmd = cmd;
				this.data = data;
			}
			public void Invoke()
			{
				cmd.commandReceived?.Invoke(cmd, data);
			}
		}
		private Queue<CommandReceived> m_CmdQueue;

		static readonly byte[] reply_LineEnd = new byte[] { 0x0D, 0x0A }; // it's "\n" line end
		/// <summary>Everytime we find "line end" we check all data we read in buffer,
		/// remove all unknown data, and free the memory.</summary>
		/// <param name="buffer"></param>
		private void RemoveUnknownCommand(byte[] buffer)
		{
			int index = -1;
			while (-1 != (index = buffer.MatchPattern(reply_LineEnd, 0, writePointer)))
			{
				int validLength = index + reply_LineEnd.Length;
				if (validLength <= writePointer)
				{
					if (validLength == reply_LineEnd.Length)
					{
						DelBufferMovePointer(0, validLength); //, "Remove line end.".ToRichText(Color.gray), ThreadLog);
					}
					else
					{
						// Unknown command detected.
						DelBufferMovePointer(0, validLength, "Unknown cmd".ToRichText(Color.red), ThreadLogWarning);
					}
				}
			}
		}

		protected byte[] ReadBuffer(int index, int count)
		{
			if (count <= 0)
				return new byte[0];

			if (m_DebugReadStreamBuffer)
			{
				ThreadLog($"{"Command Readed".ToRichText(Color.yellow)} " + Debug_GetPointersInfo(false) +
					$" from {index.ToString().ToRichText(Color.magenta)} ~ to {(index + count).ToString().ToRichText(Color.yellow)} = Total {count}\n" +
					Debug_PrintBufferContent(
						SerialPortUtils.HightLightColor(
							(index, Color.magenta),
							(index + count, Color.yellow),
							Color.red)));
			}
			// READ
			// Start Read from correct index.
			m_MemoryStream.Position = index;
			// Read and move index
			byte[] rst = m_BinaryReader.ReadBytes(count);
			// End - Updated index position.

			DelBufferMovePointer(index, count);
			return rst;
		}

		/// <summary>To remove the buffer in range :
		/// via optimize the data space within buffer, which mean :
		/// Copy all VALID data from behind/after range to current pointer.</summary>
		/// <param name="index"></param>
		/// <param name="count"></param>
		/// <param name="rst"></param>
		protected void DelBufferMovePointer(int index, int count, string reason = "", System.Action<string> log = null)
		{
			if (m_DebugReadStreamBuffer && log != null)
			{
				log($"Buffer {reason} " + Debug_GetPointersInfo(false) +
					$" from {index} ~ to {index + count} = Total {count}\n" +
					Debug_PrintBufferContent(
						SerialPortUtils.HightLightColor(
							(index, Color.magenta),
							(index + count, Color.yellow),
							Color.red)));
			}

			int anchor = index + count;
			int rest = writePointer - anchor;
			if (rest > 0)
				System.Buffer.BlockCopy(m_Buffer, anchor, m_Buffer, index, rest);
			m_MemoryStream.Position = index;
			SetWritePoint(writePointer - count);
		}


		public class StreamCommand
		{
			public string header;
			public byte[] headerHash;
			public int expectLength;
			public delegate void CommandCallback(StreamCommand cmd, byte[] buffer);
			public CommandCallback commandReceived;
			public StreamCommand(string _header, int _expectLength, CommandCallback _commandCallback = null)
			{
				header = _header;
				headerHash = SerialPortUtils.StringToHex(_header);
				expectLength = _expectLength;
				commandReceived = _commandCallback;
			}
		}
		protected Dictionary<string, StreamCommand> m_CmdDict = new Dictionary<string, StreamCommand>();
		public void RegisterStreamCommand(StreamCommand streamCommand)
		{
			if (m_CmdDict.ContainsKey(streamCommand.header))
				throw new UnityException($"{nameof(RegisterStreamCommand)} double register command : {streamCommand.header}");
			m_CmdDict.Add(streamCommand.header, streamCommand);
		}

		public bool UnregisterStreamCommand(string header)
		{
			return m_CmdDict.Remove(header);
		}
		#endregion Read Stream

		#region Tools
		protected void USBReply(object obj, bool important = false)
		{
			string msg = (string)obj;
			if (!string.IsNullOrEmpty(msg))
			{
				Debug.Log($"Server reply : {msg}");
			}
			else if (important)
			{
				Debug.LogError($"Server didn't response!");
			}
		}
		#endregion Tools

		#region Debug
		protected void ThreadLog(string message) => serialPortThread.Log(message);
		protected void ThreadLogWarning(string message) => serialPortThread.LogWarning(message);
		protected void ThreadLogError(string message) => serialPortThread.LogError(message);

		protected string Debug_GetPointersInfo(bool newLine = true)
		{
			return
			$"Read-pt  = {readPointer.ToString().ToRichText(s_ReadColor)}," +
			$"Write-pt = {writePointer.ToString().ToRichText(s_WriteColor)}," +
			(newLine ? "\n" : "");
		}
		protected string Debug_PrintBufferContent(params (int,Color)[] arg)
		{
			return "Str = " + SerialPortUtils.RemoveNonAscii(m_Buffer.HexToString()) + "\n"+
				"Hex = " + m_Buffer.HexToHexString(arg);
		}
		static Color s_WriteColor = Color.cyan;
		static Color s_ReadColor = Color.green;
		static Color s_HLColor = Color.red;
		private (int, Color) GetWritePtColor() => (writePointer, s_WriteColor);
		private (int, Color) GetReadPtColor() => (readPointer, s_ReadColor);
		#endregion Debug

		#region Warp Read/Write methods
		protected IEnumerator WaitWrite(string text)
		{
			yield return new WaitSerialPortWriteText(serialPortThread, text);
		}

		protected void Write(string text)
		{
			new WaitSerialPortWriteText(serialPortThread, text);
		}

		protected IEnumerator WaitWriteLine(string text)
		{
			yield return new WaitSerialPortWriteLine(serialPortThread, text);
		}

		protected void WriteLine(string text)
		{
			new WaitSerialPortWriteLine(serialPortThread, text);
		}

		protected void WriteBytes(byte[] data)
		{
			new WaitSerialPortWriteByteArray(serialPortThread, data);
		}

		protected IEnumerator WaitReadLine()
		{
			WaitSerialPortReadLine action = new WaitSerialPortReadLine(serialPortThread);
			while (action.keepWaiting)
				yield return null;
			yield return action.m_Result;
		}

		/// <see cref="SerialPort.Read(byte[], int, int)"/>	
		protected IEnumerator WaitRead(byte[] buffer, int offset, int count)
		{
			WaitSerialPortReadByteArray action = new WaitSerialPortReadByteArray(serialPortThread, buffer, offset, count);
			while (action.keepWaiting)
				yield return null;
			yield return action.m_Result;
		}
		#endregion Warp Read/Write methods
	}
}
#endif
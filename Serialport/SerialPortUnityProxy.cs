﻿#if UNITY_STANDALONE && SERIALPORT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    /// <summary>
    /// A unity worker that help <see cref="SerialPortThreadBase"/>
    /// to dispatch the event under Unity main thread.
    /// </summary>
    internal class SerialPortUnityProxy : MonoBehaviour, System.IDisposable
    {
        internal SerialPortThreadBase serialPortThreadBase { get; private set; }
        private bool m_Init = false;
        private bool m_IsDisposed = false;

        private bool ShouldDispose => m_ShouldDispose; // serialPortThreadBase != null && serialPortThreadBase.IsDisposed;
        private bool m_ShouldDispose = false;
        internal void WaitForDispose() => m_ShouldDispose = true;

        internal static SerialPortUnityProxy Create(SerialPortThreadBase sptb)
        {
            GameObject go = new GameObject(nameof(SerialPortUnityProxy));
            
            // go.hideFlags = HideFlags.DontSave | HideFlags.HideInHierarchy;

            SerialPortUnityProxy token = go.AddComponent<SerialPortUnityProxy>();
            token.Init(sptb);
            return token;
        }
        internal void Init(SerialPortThreadBase sptb)
        {
            if (!m_Init)
            {
                serialPortThreadBase = sptb;
                // this proxy should only destroy when sptb dispose.
                DontDestroyOnLoad(gameObject);
                m_Init = true;
            }
        }

		private void OnEnable()
		{
            StartCoroutine(PeriodicUpdate());
		}

		private void OnDisable()
		{
            Dispose();
		}
		private void OnApplicationQuit()
		{
            // Hotfix : to avoid trigger Dispose() during close app.
            m_IsDisposed = true;
        }

		private IEnumerator PeriodicUpdate()
        {
            while (!m_Init)
                yield return null;

            while (!ShouldDispose)
            {
                serialPortThreadBase.DispatchRawEvent_MainThread(10);
                yield return null;
            }

            Dispose();
        }

#region Dispose
		protected virtual void Dispose(bool disposing)
		{
			if (!m_IsDisposed)
			{
				if (disposing)
				{
                    Destroy(gameObject);
                }
				m_IsDisposed = true;
			}
		}

		public void Dispose()
		{
			// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
			Dispose(disposing: true);
			System.GC.SuppressFinalize(this);
		}
#endregion Dispose
    }
}
#endif
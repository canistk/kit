﻿#if UNITY_STANDALONE && SERIALPORT
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Kit
{
    public static class SerialPortUtils
    {
		static Regex nonAsciiFilter = new Regex(@"[^\u0020-\u007E]");
		public static string RemoveNonAscii(string str)
		{
			return nonAsciiFilter.Replace(str, string.Empty);
		}

		public static string HexToString(this byte[] byteArray)
		=> Encoding.ASCII.GetString(byteArray);

		public static byte[] StringToHex(this string str)
		=> Encoding.ASCII.GetBytes(str);

		public static string ToHexString(this string str)
		{
			return str.StringToHex().HexToHexString();
		}

		public static string HexToHexString(this byte[] byteArray, params (int, Color)[] pointers)
		{
			int length = byteArray.Length;
			string rst = "";
			for (int i = 0; i < length; i++)
			{
				Color color = Color.black;
				foreach((int p, Color c) in pointers)
				{
					if (p == i)
						color += c;
				}
				if (!color.Equals(Color.black))
					rst += "<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">";

				if (i == 0)
					rst += byteArray[i].ToString("x2");
				else
					rst += " "+ byteArray[i].ToString("x2");

				if (!color.Equals(Color.black))
					rst += "</color>";
			}
			return rst;
		}

		public static (int, Color)[] HightLightColor((int, Color) pt1, (int, Color) pt2, Color highlight)
		{
			{
				int min = Math.Min(pt1.Item1, pt2.Item1);
				int max = Math.Max(pt1.Item1, pt2.Item1);
				int cnt = max - min;
				(int, Color)[] arr = new (int, Color)[cnt];
				for (int i = 0; i < cnt; i++)
				{
					Color color = highlight;
					if (i == 0)
					{
						if (min == pt1.Item1)
							color += pt1.Item2;
						else
							color += pt2.Item2;
					}
					else if (i == cnt - 1)
					{
						if (max == pt1.Item1)
							color += pt1.Item2;
						else
							color += pt2.Item2;
					}
					arr[i] = (i + min, color);
				}
				return arr;
			}
		}

		/// <summary>Compare byte array</summary>
		/// <param name="byteArray"></param>
		/// <param name="pattern"></param>
		/// <param name="offset">start offset from byteArray</param>
		/// <param name="limit"></param>
		/// <returns>the index of match pattern. -1 = not match.</returns>
		public static int MatchPattern(this byte[] byteArray, byte[] pattern, int offset = 0, int limit = int.MaxValue)
		{
			if (limit > byteArray.Length)
				limit = byteArray.Length;
			int loopCount = (limit - offset) - pattern.Length;
			if (offset < 0)
				return -1;
			if (loopCount < 0 || // not long enough.
				offset > byteArray.Length || // start digit out range.
				limit < 0) // no sample.
				return -1; 
			
			for (int x = offset; x < offset + loopCount + 1; x++)
			{
				bool match = true;
				for (int y = 0; match && y < pattern.Length; y++)
				{
					if (byteArray[x + y] != pattern[y])
						match = false;
				}
				if (match)
				{
					return x;
				}
			}
			return -1;
		}

		public static byte[] BlockClone(this byte[] byteArray, int start, int count)
		{
			int end = start + count;
			if (byteArray.Length < end || count < 0)
				throw new System.IndexOutOfRangeException();
			else if (count == 0)
				return new byte[0];
			byte[] rst = new byte[count];
			Buffer.BlockCopy(byteArray, start, rst, 0, count);
			return rst;
		}

		/// <summary>To combine 2 bits into one value.
		/// e.g. send value > 255, via IO transition.</summary>
		/// <param name="byteArray"></param>
		/// <param name="hightBitIndex"></param>
		/// <param name="lowBitIndex"></param>
		/// <returns>Combie value of 2 bits integer.</returns>
		public static int HighLowBit(this byte[] byteArray, int hightBitIndex, int lowBitIndex, bool sign = true)
		{
			// note, BitConverter require byte[2] {low,high} by order.
			return sign ?
				BitConverter.ToInt16(byteArray, lowBitIndex) :
				byteArray[hightBitIndex] << 8 | byteArray[lowBitIndex];
				// BitConverter.ToUInt16(byteArray, lowBitIndex);
		}
	}
}
#endif
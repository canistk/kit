﻿#if UNITY_STANDALONE && SERIALPORT
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using UnityEngine;

namespace Kit
{
	public class SerialPortThread : SerialPortThreadBase
	{

		public SerialPortThread(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, int threadInterval = 1)
			: base(new SerialPort(portName, baudRate, parity, dataBits, stopBits)) { }
		public SerialPortThread(SerialPortConfig serialPortConfig)
			: base(serialPortConfig.ToSerialPort(new SerialPort())) { }

		public SerialPortThread(SerialPort serialPort)
			: base(serialPort) { }
	}
}
#endif
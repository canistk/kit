﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	public abstract class SerialPortThreadBase : System.IDisposable
	{
		#region Constructor
		public SerialPort rawSerialPort { get; private set; } = null;
		internal SerialPortUnityProxy proxy { get; private set; } = null;
		public int m_ThreadInterval = 1;
		public SerialPortThreadBase(SerialPort serialPort)
		{
			rawSerialPort = serialPort;
			if (rawSerialPort == null)
				throw new System.NullReferenceException("require to defeine SerialPort");

			m_RawEventQueue = new Queue<RawEventPackage>(100);
			m_IORequest = new Queue<SerialPortAction>(100);
			rawSerialPort.DataReceived += RawSerialPort_DataReceived;
			rawSerialPort.ErrorReceived += RawSerialPort_ErrorReceived;
			rawSerialPort.Disposed += RawSerialPort_Disposed;
			rawSerialPort.PinChanged += RawSerialPort_PinChanged;
			proxy = SerialPortUnityProxy.Create(this);
		}
		#endregion Constructor

		#region Threading
		private Thread m_Thread = null;
		public void BeginThread(int threadInterval = 1)
		{
			if (m_Thread == null)
			{
				m_Thread = new Thread(new ThreadStart(ThreadLoop));
				m_Thread.IsBackground = true;
			}
			if (!m_Thread.IsAlive)
			{
				m_ThreadInterval = Mathf.Max(1, threadInterval);
				m_Thread.Start();
			}
			// m_BufferedStream = new BufferedStream(rawSerialPort.BaseStream;
		}

		private void ThreadLoop()
		{
			while (!m_DisposeRequest)
			{
				ProcessIORequest();
				Thread.Sleep(m_ThreadInterval);
			}

			Dispose();
		}
		private void ProcessIORequest()
		{
			try
			{
				if (m_IORequest.Count > 0)
				{
					SerialPortAction action = m_IORequest.Dequeue();
					// Log("IORequest : " + action.GetType().Name);
					if (isReadStreaming && action is SerialPortReadRequest)
					{
						// ignore read request, while we are in read-stream mode.
					}
					else
					{
						action.InternalProcessIORequest();
					}
				}
			}
			catch (System.TimeoutException e)
			{
				LogWarning(e.ToString());
			}
			catch (System.Exception e)
			{
				LogError(e.ToString());
			}
			finally
			{
				
			}
		}
		#endregion Threading

		#region Stream Threading
		/// <summary>
		/// !Important
		/// Since we are using multi thread.
		/// DON'T attempt to use any unity API within threading.
		/// it crash Unity3D engine.
		/// </summary>
		/// <param name="serialPort"></param>
		/// <param name="debugLog"></param>
		/// <param name="debugWarning"></param>
		/// <param name="debugError"></param>
		public delegate void MultiThread(SerialPort serialPort, Callback<string> debugLog, Callback<string> debugWarning, Callback<string> debugError);
		private MultiThread m_MultiThreadLoop = null;
		private MultiThread m_MultiThreadEnd = null;
		private Thread m_ReadStreamThread = null;
		public bool isReadStreaming => m_ReadStreamThread != null && m_ReadStreamThread.IsAlive;
		public void BeginReadStream(MultiThread initMethod, MultiThread loopMethod, MultiThread endMethod)
		{
			if (m_ReadStreamThread == null)
			{
				m_ReadStreamThread = new Thread(StreamThreadLoop);
				m_ReadStreamThread.IsBackground = true;
			}
			if (!m_ReadStreamThread.IsAlive)
			{
				initMethod?.Invoke(rawSerialPort, Log, LogWarning, LogError);
				m_MultiThreadLoop = loopMethod;
				m_MultiThreadEnd = endMethod;
				m_ReadStreamThread.Start();
			}
		}

		public void EndReadStream()
		{
			if (m_ReadStreamThread != null && m_ReadStreamThread.IsAlive)
			{
				m_ReadStreamThread.Abort();
				m_ReadStreamThread = null;
			}
			if (!IsDisposed)
				m_MultiThreadEnd?.Invoke(rawSerialPort, Log, LogWarning, LogError);
		}

		private void StreamThreadLoop()
		{
			while (!m_DisposeRequest)
			{
				ProcessStream();
				Thread.Sleep(m_ThreadInterval);
			}
		}

		private void ProcessStream()
		{
			if (isReadStreaming && m_MultiThreadLoop != null)
			{
				try
				{
					m_MultiThreadLoop?.Invoke(rawSerialPort, Log, LogWarning, LogError);
				}
				catch (System.Exception e)
				{
					LogError(e.ToString());
				}
				finally
				{ }
			}
		}
		#endregion Stream Threading

		#region Unity Thread
		internal void DispatchRawEvent_MainThread(int maxEventPerFrame = int.MaxValue)
		{
			int cnt = m_RawEventQueue.Count < maxEventPerFrame ? m_RawEventQueue.Count : maxEventPerFrame;
			for (int i = 0; i < cnt && rawSerialPort.IsOpen; i++)
			{
				RawEventPackage package = m_RawEventQueue.Dequeue();
				if (package.eventArgs is SerialPinChangedEventArgs pinChanged)
					serialPinChanged?.Invoke(package.sender, pinChanged);
				else if (package.eventArgs is SerialErrorReceivedEventArgs error)
					serialErrorReceived?.Invoke(package.sender, error);
				else if (package.eventArgs is SerialDataReceivedEventArgs received)
					serialDataReceived?.Invoke(package.sender, received);
				else if (package.eventArgs == s_DisposeEvent)
					disposed?.Invoke(); // TODO: make it dispatch high level disconnect event.
				else if (package.sender == s_CustomLogEvent)
					Debug.Log((string)package.eventArgs);
				else if (package.sender == s_WarningLogEvent)
					Debug.LogWarning((string)package.eventArgs);
				else if (package.sender == s_ErrorLogEvent)
					Debug.LogError((string)package.eventArgs);
				else
					throw new System.NotImplementedException();
			}
		}
		#endregion Unity Thread

		#region Read/Write
		private Queue<SerialPortAction> m_IORequest;
		internal void IORequest(SerialPortAction action)
		=> m_IORequest.Enqueue(action);
		#endregion Read/Write

		#region Event Redirection
		private struct RawEventPackage
		{
			public object sender;
			public object eventArgs;
			public RawEventPackage(object sender, object eventArgs)
			{
				this.sender = sender;
				this.eventArgs = eventArgs;
			}
		}
		private Queue<RawEventPackage> m_RawEventQueue;
		
		public event SerialPinChangedEventHandler serialPinChanged;
		private void RawSerialPort_PinChanged(object sender, SerialPinChangedEventArgs e)
			=> m_RawEventQueue.Enqueue(new RawEventPackage(sender, e));

		public event SerialErrorReceivedEventHandler serialErrorReceived;
		private void RawSerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
			=> m_RawEventQueue.Enqueue(new RawEventPackage(sender, e));

		public event SerialDataReceivedEventHandler serialDataReceived;
		private void RawSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
			=> m_RawEventQueue.Enqueue(new RawEventPackage(sender, e));

		private class DisposeEvent { }
		private static readonly DisposeEvent s_DisposeEvent = new DisposeEvent();
		public event System.Action disposed;
		private void RawSerialPort_Disposed(object sender, System.EventArgs ignore)
			=> m_RawEventQueue.Enqueue(new RawEventPackage(sender, s_DisposeEvent));
		#endregion Event Redirection

		#region Method Redirection
		public bool IsOpen => rawSerialPort.IsOpen;
		public void Open()
		{
			if (IsDisposed)
				return;
			rawSerialPort.Open();
		}
		public void Close()
		{
			if (IsDisposed)
				return;
			rawSerialPort.Close();
			m_DisposeRequest = true;
		}

		[System.Obsolete("Unity3D will frozen", true)]
		public void DiscardInBuffer() => rawSerialPort.DiscardInBuffer();

		[System.Obsolete("Unity3D will frozen", true)]
		public void DiscardOutBuffer() => rawSerialPort.DiscardOutBuffer();
		#endregion Method Redirection

		#region Thread Log
		private class CustomLogEvent { }
		private static readonly CustomLogEvent s_CustomLogEvent = new CustomLogEvent();
		internal void Log(string message) => m_RawEventQueue.Enqueue(new RawEventPackage(s_CustomLogEvent, message));
		private static readonly CustomLogEvent s_WarningLogEvent = new CustomLogEvent();
		internal void LogWarning(string message) => m_RawEventQueue.Enqueue(new RawEventPackage(s_WarningLogEvent, message));
		private static readonly CustomLogEvent s_ErrorLogEvent = new CustomLogEvent();
		internal void LogError(string message) => m_RawEventQueue.Enqueue(new RawEventPackage(s_ErrorLogEvent, message));

		#endregion Thread Log

		#region Dispose
		private bool m_DisposeRequest = false;
		internal bool IsDisposed => m_Disposed;
		private bool m_Disposed;
		protected virtual void Dispose(bool disposing)
		{
			if (!m_Disposed)
			{
				if (disposing)
				{
					EndReadStream();
					// dispose managed state (managed objects)
					proxy.WaitForDispose();
					m_DisposeRequest = true;
					m_Thread.Abort();
					if (rawSerialPort != null)
					{
						rawSerialPort.DataReceived -= RawSerialPort_DataReceived;
						rawSerialPort.ErrorReceived -= RawSerialPort_ErrorReceived;
						rawSerialPort.Disposed -= RawSerialPort_Disposed;
						rawSerialPort.PinChanged -= RawSerialPort_PinChanged;
					}
					m_RawEventQueue.Clear();
					m_IORequest.Clear();
				}
				// free unmanaged resources (unmanaged objects) and override finalizer
				// set large fields to null
				if (rawSerialPort != null)
				{
					if (rawSerialPort.IsOpen)
						rawSerialPort.Close();
					rawSerialPort.Dispose();
				}
				m_ReadStreamThread = null;
				m_Thread = null;
				rawSerialPort = null;
				proxy = null;
				m_Disposed = true;
			}
		}

		// 'Dispose(bool disposing)' has code to free unmanaged resources
		~SerialPortThreadBase()
		{
			// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
			Dispose(disposing: false);
		}

		public void Dispose()
		{
			// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
			Dispose(disposing: true);
			System.GC.SuppressFinalize(this);
		}
		#endregion Dispose
	}
}
#endif
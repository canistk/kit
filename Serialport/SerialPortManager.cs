﻿#if UNITY_STANDALONE && SERIALPORT
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public static class SerialPortManager
    {
        private static Dictionary<int, SerialPortThreadBase> m_ComPorts;
        public static Dictionary<int, SerialPortThreadBase> comPorts
        {
            get
            {
                if (m_ComPorts == null)
                    m_ComPorts = new Dictionary<int, SerialPortThreadBase>();
                return m_ComPorts;
			}
		}

        public static bool TryGetComPort(int portNum, out SerialPortThreadBase serialPort)
        {
            return comPorts.TryGetValue(portNum, out serialPort);
		}
    }
}
#endif
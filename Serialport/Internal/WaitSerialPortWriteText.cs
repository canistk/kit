﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO.Ports;

namespace Kit
{
    public class WaitSerialPortWriteText : SerialPortWriteRequest
    {
        private string text;
        public WaitSerialPortWriteText(SerialPortThreadBase sp, string text)
            : base(sp)
        {
            this.text = text;
		}

		protected override void ProcessRequest(SerialPort sp)
		{
            sp.Write(text);
		}
	}
}
#endif
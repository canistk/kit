﻿#if UNITY_STANDALONE && SERIALPORT
namespace Kit
{
	public abstract class SerialPortWriteRequest : SerialPortAction
	{
		public SerialPortWriteRequest(SerialPortThreadBase serialPort) : base(serialPort) { }
	}
}
#endif
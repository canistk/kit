﻿#if UNITY_STANDALONE && SERIALPORT
using UnityEngine;
using System.IO.Ports;

namespace Kit
{
	public abstract class SerialPortAction : CustomYieldInstruction
	{
		protected SerialPortThreadBase m_SerialPortThread;
		public SerialPortAction(SerialPortThreadBase serialPortThread)
		{
			m_SerialPortThread = serialPortThread;
			m_SerialPortThread.IORequest(this); // wait IO process.
		}

		private bool m_ProcessFlag = false;

		public override bool keepWaiting
		{
			get
			{
				if (m_SerialPortThread == null || !m_SerialPortThread.IsOpen || m_SerialPortThread.IsDisposed)
					return false;
				return !m_ProcessFlag;
			}
		}

		internal void InternalProcessIORequest()
		{
			try
			{
				ProcessRequest(m_SerialPortThread.rawSerialPort);
			}
			catch
			{
				throw;
			}
			finally
			{
				m_ProcessFlag = true;
			}
		}

		/// <summary>Do something and 
		/// return via <see cref="m_Result"/></summary>
		protected abstract void ProcessRequest(SerialPort sp);
	}
}
#endif
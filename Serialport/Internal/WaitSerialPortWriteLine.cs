﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO.Ports;

namespace Kit
{
    public class WaitSerialPortWriteLine : SerialPortWriteRequest
    {
        private string text;
        public WaitSerialPortWriteLine(SerialPortThreadBase serialPort, string text)
            : base(serialPort)
        {
            this.text = text;
		}

		protected override void ProcessRequest(SerialPort serialPort)
		{
            serialPort.WriteLine(text);
		}
	}
}
#endif
﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO.Ports;

namespace Kit
{
    public class WaitSerialPortWriteByteArray : SerialPortWriteRequest
    {
        private byte[] data;
        public WaitSerialPortWriteByteArray(SerialPortThreadBase sp, byte[] data)
            : base(sp)
        {
            this.data = data;
		}

		protected override void ProcessRequest(SerialPort sp)
		{
            sp.Write(data, 0, data.Length);
		}
	}
}
#endif
﻿#if UNITY_STANDALONE && SERIALPORT
namespace Kit
{
	public abstract class SerialPortReadRequest : SerialPortAction
	{
		public SerialPortReadRequest(SerialPortThreadBase sp) : base(sp) { }
		
		public object m_Result { get; protected set; }

	}
}
#endif
﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO.Ports;

namespace Kit
{
	public class WaitSerialPortReadLine : SerialPortReadRequest
	{
		public string text => (string)m_Result;
		internal WaitSerialPortReadLine(SerialPortThreadBase sp)
			: base(sp) {}

		protected override void ProcessRequest(SerialPort serialport)
		{
			m_Result = serialport.ReadLine();
		}
	}
}
#endif
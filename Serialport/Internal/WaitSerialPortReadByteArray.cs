﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO.Ports;

namespace Kit
{
	public class WaitSerialPortReadByteArray : SerialPortReadRequest
	{
		private byte[] buffer;
		private int offset, count;

		internal WaitSerialPortReadByteArray(SerialPortThreadBase sp, byte[] buffer, int offset, int count)
			: base(sp)
		{
			this.buffer = buffer;
			this.offset = offset;
			this.count = count;
		}

		protected override void ProcessRequest(SerialPort serialport)
		{
			m_Result = serialport.Read(buffer, offset, count);
		}
	}
}
#endif
﻿#if UNITY_STANDALONE && SERIALPORT
using System.IO.Ports;
using UnityEngine;
namespace Kit
{
	/// <summary>
	/// Note : Unity didn't implement following API.
	/// - ReceivedBytesThreshold Not implement.
	/// - DataReceived will not respone
	/// - BytesToRead process will frozen
	/// </summary>
	[System.Serializable]
    public class SerialPortConfig
    {
		// public string PortName = "";
		public int m_PortNumber = 3;
		public string PortName => GetCorrectedPortName(m_PortNumber);
		public int m_BaudRate = 9600;
		public Parity m_Parity = Parity.None;
		public int m_DataBit = 8;
		public StopBits m_StopBits = StopBits.One;
		public Handshake m_Handshake = Handshake.None;
		
		[Header("Read")]
		public int m_ReadBufferSize = 4096;
		public int m_ReadTimeout = -1;

		[Header("Write")]
		public int m_WriteBufferSize = 2048;
		public int m_WriteTimeout = -1;

		[Header("Others")]
		public bool m_DtrEnable = false;
		public bool m_RtsEnable = false;

		// public byte m_ParityReplace = 63;
		// public bool m_BreakState = false;
		// public string m_NewLine = "\n";

		/// <summary>
		/// Corrects port names for COM port 10 and above. Because .NET can't handle it...
		/// </summary>
		/// <param name="portName"></param>
		/// <returns></returns>
		public static string GetCorrectedPortName(int portNum)//string portName)
		{
			if (portNum < 0)
				throw new System.Exception($"Invalid port name. {portNum}, should be positive numbers");
			else if (portNum <= 9)
				return s_Com + portNum;
			else
				return s_ComExtra + portNum;
		}
		public static int GetPortNumber(string portName)
		{
			if (portName.StartsWith(s_ComExtra))
			{
				portName = portName.Substring(s_ComExtra.Length, portName.Length - s_ComExtra.Length);
			}
			else if (portName.StartsWith(s_Com))
			{
				portName = portName.Substring(s_Com.Length, portName.Length - s_Com.Length);	
			}

			if (int.TryParse(portName, out int rst))
				return rst;
			return -1;
		}

		private const string s_Com = "COM";
		private const string s_ComExtra = @"\\.\COM";
		public void FromSerialPort(SerialPort sp)
		{
			if (sp == null)
				return;
			m_PortNumber = GetPortNumber(sp.PortName);
			m_BaudRate = sp.BaudRate;
			m_Parity = sp.Parity;
			m_DataBit = sp.DataBits;
			m_StopBits = sp.StopBits;
			m_Handshake = sp.Handshake;

			m_ReadBufferSize = sp.ReadBufferSize;
			m_ReadTimeout = sp.ReadTimeout;
			// m_ReceivedBytesThreshold = sp.ReceivedBytesThreshold;

			m_WriteBufferSize = sp.WriteBufferSize;
			m_WriteTimeout = sp.WriteTimeout;

			m_DtrEnable = sp.DtrEnable;
			m_RtsEnable = sp.RtsEnable;
			// m_ParityReplace = sp.ParityReplace;
			// m_BreakState = sp.BreakState;
			// m_NewLine = sp.NewLine;
		}
		
		public SerialPort ToSerialPort(SerialPort sp)
		{
			sp.PortName = PortName;
			sp.BaudRate = m_BaudRate;

			sp.Parity = m_Parity;
			sp.DataBits = m_DataBit;
			sp.StopBits = m_StopBits;
			sp.Handshake = m_Handshake;

			sp.ReadBufferSize = m_ReadBufferSize;
			sp.ReadTimeout = m_ReadTimeout;
			// sp.ReceivedBytesThreshold = m_ReceivedBytesThreshold;

			sp.WriteBufferSize = m_WriteBufferSize;
			sp.WriteTimeout = m_WriteTimeout;

			sp.DtrEnable = m_DtrEnable;
			sp.RtsEnable = m_RtsEnable;
			// sp.ParityReplace = m_ParityReplace;
			// sp.BreakState = m_BreakState;
			// sp.NewLine = m_NewLine;
			return sp;
		}
	}
}
#endif
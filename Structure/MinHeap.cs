//#define DEBUG_LOG
using System.Collections;
using System.Collections.Generic;
namespace Kit
{
    public abstract class Heap<T> where T : System.IComparable<T>
    {
        protected List<T> data;
        public int Count => data.Count;
        public bool IsEmpty => data.Count == 0;
        public Heap(int size = 2)
        {
            data = new List<T>(size);
        }

        protected int GetLeftChild(int idx)     => 2 * idx + 1;
        protected int GetRightChild(int idx)    => 2 * idx + 2;
        protected int GetParent(int idx)        => idx > 0 ? (idx - 1) / 2 : 0;

        protected void Swap(int x, int y)
        {
            T z = data[x];
            data[x] = data[y];
            data[y] = z;
        }

        /// <summary>Depend on compare method, this will become min/max heap.</summary>
        protected abstract int Compare(int bigger, int smaller);

        public bool Contains(T value)
        {
            return data.Contains(value);
        }

        public void Clear()
        {
            data.Clear();
        }

        public T Peek()
        {
            if (data.Count == 0)
                throw new System.IndexOutOfRangeException();
            return data[0];
        }

        public void Add(T item)
        {
            data.Add(item);
            HeapifyUp(data.Count - 1);
        }

        public T Poll()
        {
            if (data.Count == 0)
                throw new System.IndexOutOfRangeException();
            int last = data.Count - 1;
            T output = data[0];
            data[0] = data[last];

            // optimize : remove at last, so the data list will not move forward.
            data.RemoveAt(last);
            // --last; // after remove

            // heapify down to leaf.
            HeapifyDown(0);
            return output;
        }

        public bool Delete(T target)
        {
            if (data.Count == 0)
                return false;
            int idx = data.Count;
            int last = data.Count - 1;
            if (data[last].Equals(target))
            {
                data.RemoveAt(last);
                return true;
            }
            else if (data[0].Equals(target))
            {
                data[0] = data[last];
                data.RemoveAt(last);
                HeapifyDown(0);
                return true;
            }

            // Search index of target
            while (idx-- > 0)
            {
                if (data[idx].Equals(target))
                {
                    Swap(idx, last);
                    data.RemoveAt(last);
                    HeapifyDown(idx);
                    HeapifyUp(idx);
                    return true;
                }
            }
            return false;
        }

        /// <summary>Heapify down to leaf</summary>
        /// <param name="index"></param>
        protected void HeapifyDown(int index)
        {
            int pi = index;
            int last = data.Count - 1;
            while (true)
            {
                int ci = GetLeftChild(pi);
                if (ci > last)
                    break; // no children
                {
                    int rci = ci + 1; // skip function call.
                    if (rci <= last && Compare(ci, rci) >= 0)
                    {
                        // If we got right branch (lci + 1), and it's smaller then left branch
                        // use right branch.
                        ci = rci;
                    }
                }
                if (Compare(ci, pi) >= 0)
                    break; // parent is smaller than (or equal to) smallest child so done
                Swap(ci, pi);
                pi = ci;
            }
        }

        /// <summary>heapify up to root.</summary>
        /// <param name="ci"></param>
        protected void HeapifyUp(int index)
        {
            int ci = index;
            while (ci > 0)
            {
                int pi = GetParent(ci);
                if (Compare(ci, pi) >= 0)
                    break; // child item is larger than (or equal) parent so we're done
                Swap(ci, pi);
                ci = pi;
            }
        }

#if DEBUG_LOG
        /// <summary>Full test, the heap's data obey min-heap's rule.</summary>
        /// <returns></returns>
        public bool IsConsistent()
        {
            if (data.Count == 0)
                return true;
            int li = data.Count - 1;
            for (int pi = 0; pi <= li; ++pi)
            {
                int lci = pi * 2 + 1;
                if (lci <= li && Compare(lci, pi) < 0)
                    return false; // left child should greater than parent.
                int rci = pi * 2 + 2;
                if (rci <= li && Compare(rci, pi) < 0)
                    return false; // right child should greater than parent.
            }
            return true;
        }
        public void Log()
        {
            string str = "";
            for (int i = 0; i < data.Count; ++i)
            {
                str += data[i] + ",";
            }
            UnityEngine.Debug.Log($"Heap : {str}");
        }
#endif
    }

    public class MinHeap<T> : Heap<T> where T : System.IComparable<T>
    {
        public MinHeap(int size = 2)
            : base(size) { }

        protected override int Compare(int bigger, int smaller)
        {
            return data[bigger].CompareTo(data[smaller]);
        }
    }

    public class MaxHeap<T> : Heap<T> where T : System.IComparable<T>
    {
        public MaxHeap(int size = 2)
            : base(size) { }

        protected override int Compare(int bigger, int smaller)
        {
            return data[smaller].CompareTo(data[bigger]);
        }
    }
}
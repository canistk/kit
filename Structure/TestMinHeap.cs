using Kit;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.Test
{
    [ExecuteInEditMode]
    public class TestMinHeap : MonoBehaviour
    {
        public List<int> data;
        public int m_DelNum = 1;
        private void OnEnable()
        {
            int minError = 0;
            for (int i = 0; i < 10000; ++i)
                if (TestMin()) minError++;

            int maxError = 0;
            for (int i = 0; i < 10000; ++i)
                if (TestMax()) maxError++;

            if (minError > 0)
                Debug.LogError($"Min-Heap error {minError}");
            else
                Debug.Log($"Min-Heap no error found.");

            if (maxError > 0)
                Debug.LogError($"Max-Heap error {maxError}");
            else
                Debug.Log($"Max-Heap no error found.");
        }

        private bool TestMin()
        {
            int error = 0;
            MinHeap<int> heap = new MinHeap<int>(data.Count);
            for (int i = 0; i < data.Count; ++i)
            {
                heap.Add(data[i]);
                //heap.Log();
            }

            int del = Random.Range(0, data.Count);
            var delItem = data[del];
            heap.Delete(data[del]);

            //if (!heap.IsConsistent())
            //{
            //    Debug.Log($"Delete index[{del}] = {delItem}");
            //    Debug.LogError("It's not consistent");
            //    heap.Log();
            //    error++;
            //}

            string check = "";
            bool orderMatch = true;
            int last = int.MinValue;
            while (heap.Count > 0)
            {
                var ele = heap.Poll();
                if (last > ele)
                    orderMatch = false;
                else
                    last = ele;
                check += ele;
                if (heap.Count > 0)
                    check += ",";
            }
            if (!orderMatch)
                error++;
            return error > 0;
        }

        private bool TestMax()
        {
            int error = 0;
            MaxHeap<int> heap = new MaxHeap<int>(data.Count);
            for (int i = 0; i < data.Count; ++i)
            {
                heap.Add(data[i]);
                //heap.Log();
            }

            int del = Random.Range(0, data.Count);
            var delItem = data[del];
            heap.Delete(data[del]);

            //if (!heap.IsConsistent())
            //{
            //    Debug.Log($"Delete index[{del}] = {delItem}");
            //    Debug.LogError("It's not consistent");
            //    heap.Log();
            //    error++;
            //}

            string check = "";
            bool orderMatch = true;
            int last = int.MaxValue;
            while (heap.Count > 0)
            {
                var ele = heap.Poll();
                if (last < ele)
                    orderMatch = false;
                else
                    last = ele;
                check += ele;
                if (heap.Count > 0)
                    check += ",";
            }
            if (!orderMatch)
                error++;
            return error > 0;

            //if (orderMatch)
            //    Debug.Log(check);
            //else
            //    Debug.LogError(check);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(
	fileName = "MixamoImportSetting",
	menuName = "Kit/Mixamo Import Setting")]
public class MixamoImportSetting : ScriptableObject
{
	[Header("Setting")]
	public bool EnableMixamoImporter = true;
	public Avatar avatar;
	[SerializeField] string mixamoDirectory = "";
	public string MixamoDirectory => Application.dataPath + "/" + mixamoDirectory;

	[Space]
	[Header("Loop")]
	public bool m_Loop = false;
	public bool m_LoopPose = false;
	public float m_CycleOffset = 0f;

	[Header("Root Transform Rotation")]
	public bool BakeRotationIntoPose = false;
	public enum eRotationBaseUpon { Original = 0, BodyOrientation = 1, }
	public eRotationBaseUpon rotationBaseUpon = eRotationBaseUpon.Original;
	public float rotationOffset = 0f;

	[Header("Root Transform Position (Y)")]
	public bool BakePosYIntoPose = true;
	public enum ePositionYBaseUpon { Original, CenterOfMass, Feet }
	public ePositionYBaseUpon positionYBaseUpon = ePositionYBaseUpon.Original;
	public float positionYOffset = 0f;

	[Header("Root Transform Position (XZ)")]
	public bool BakePosXZIntoPose = false;
	public enum ePositionXZBaseUpon { Original, CenterOfMass }
	public ePositionXZBaseUpon positionXZBaseUpon = ePositionXZBaseUpon.Original;
}



﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class MixamoAssetImporter : AssetPostprocessor
{
	private static MixamoImportSetting m_CacheImportSetting;
	private const double s_AgreePeriod = 10f;
	private static double m_lastTimeAgree = 0f;
	private static int m_WasAgree = -1;


	void OnPreprocessModel()
	{
		if (!assetImporter.importSettingsMissing)
			return;
		if (AssetDatabase.LoadAssetAtPath<Object>(assetImporter.assetPath) != null)
			return;
		MixamoImportSetting importSetting = GetMixAmoImportSetting();
		if (importSetting == null || !importSetting.EnableMixamoImporter)
			return;
		
		if (IsAssetContainedInMixAmoDir(assetImporter.assetPath, importSetting) &&
			assetImporter is ModelImporter modelImporter &&
			AskForDeveloperPermission(importSetting.MixamoDirectory))
		{
			modelImporter.animationType = ModelImporterAnimationType.Human;
			string path = Path.GetDirectoryName(modelImporter.assetPath);
			modelImporter.ExtractTextures(path);
			modelImporter.materialLocation = ModelImporterMaterialLocation.External;
			AssetDatabase.ImportAsset(modelImporter.assetPath);
		}
	}

	void OnPreprocessAnimation()
	{
		if (!assetImporter.importSettingsMissing)
			return;
		if (AssetDatabase.LoadAssetAtPath<Object>(assetImporter.assetPath) != null)
			return; // avoid reimport.
		MixamoImportSetting importSetting = GetMixAmoImportSetting();
		if (importSetting == null || !importSetting.EnableMixamoImporter)
			return;

		if (IsAssetContainedInMixAmoDir(assetImporter.assetPath, importSetting) &&
			assetImporter.importSettingsMissing &&
			assetImporter is ModelImporter modelImporter &&
			AskForDeveloperPermission(importSetting.MixamoDirectory))
		{
            ModelImporterClipAnimation[] animations = modelImporter.defaultClipAnimations;
			if (animations != null && animations.Length > 0)
			{
				foreach(ModelImporterClipAnimation animation in animations)
					DefineAnimationSetting(animation, importSetting, modelImporter);

				modelImporter.clipAnimations = animations;
				if (importSetting.avatar != null)
				{
					modelImporter.sourceAvatar = importSetting.avatar;
				}
			}
			

			modelImporter.animationType = ModelImporterAnimationType.Human;
		}
	}

	private void DefineAnimationSetting(ModelImporterClipAnimation animation, MixamoImportSetting importSetting, AssetImporter modelImporter)
    {
		animation.name = Path.GetFileNameWithoutExtension(modelImporter.assetPath);
		if (importSetting.m_Loop)
		{
			animation.loop = true;
			animation.loopTime = importSetting.m_Loop;
			animation.loopPose = importSetting.m_LoopPose;
			animation.cycleOffset = importSetting.m_CycleOffset;
		}

		// Root Transform Rotation
		animation.lockRootRotation = importSetting.BakeRotationIntoPose;
		animation.keepOriginalOrientation = importSetting.rotationBaseUpon == MixamoImportSetting.eRotationBaseUpon.Original;
		animation.rotationOffset = importSetting.rotationOffset;

		// Root Transform Position (Y)
		animation.lockRootHeightY = importSetting.BakePosYIntoPose;
		switch (importSetting.positionYBaseUpon)
        {
			case MixamoImportSetting.ePositionYBaseUpon.Original:
				animation.keepOriginalPositionY = true;
				animation.heightFromFeet = false;
				break;
			case MixamoImportSetting.ePositionYBaseUpon.CenterOfMass:
				animation.keepOriginalPositionY = false;
				animation.heightFromFeet = false;
				break;
			case MixamoImportSetting.ePositionYBaseUpon.Feet:
				animation.keepOriginalPositionY = false;
				animation.heightFromFeet = true;
				break;
			default: throw new System.NotImplementedException();
        }
		animation.heightOffset = importSetting.positionYOffset;

		// Root Transform Position (XZ)
		animation.lockRootPositionXZ = importSetting.BakePosXZIntoPose;
		animation.keepOriginalPositionXZ = importSetting.positionXZBaseUpon == MixamoImportSetting.ePositionXZBaseUpon.Original;
	}

	private bool AskForDeveloperPermission(string path)
	{
		double duration = EditorApplication.timeSinceStartup - m_lastTimeAgree;
		bool isWithinSession = duration < s_AgreePeriod;
		if (m_WasAgree != -1 && isWithinSession)
			return m_WasAgree == 1;

		m_WasAgree = UnityEditor.EditorUtility.DisplayDialog(
			nameof(MixamoAssetImporter),
			$"Do you want to apply {nameof(MixamoAssetImporter)}\n" +
			$"under folder : {path}",
			"Apply"
			// ,$"Stop asking for {s_AgreePeriod:F0}sec", DialogOptOutDecisionType.ForThisSession, $"Don't ask for session"
			) ? 1 : 0;
		m_lastTimeAgree = EditorApplication.timeSinceStartup;
		return m_WasAgree == 1;
	}

	MixamoImportSetting GetMixAmoImportSetting()
	{
		if (m_CacheImportSetting == null)
		{
			string[] importSettingPaths = AssetDatabase.FindAssets("t:MixAmoImportSetting");
			if (importSettingPaths.Length > 0)
			{
				m_CacheImportSetting = AssetDatabase.LoadAssetAtPath<MixamoImportSetting>(AssetDatabase.GUIDToAssetPath(importSettingPaths[0]));
				return m_CacheImportSetting;
			}
		}

		return m_CacheImportSetting;
	}

	bool IsAssetContainedInMixAmoDir(string assetPath, MixamoImportSetting importSetting)
	{
        List<string> allDirectories = new List<string>();
		allDirectories.AddRange(Directory.GetDirectories(importSetting.MixamoDirectory, "*", SearchOption.AllDirectories));
		allDirectories.Add(importSetting.MixamoDirectory);
		for (int i = 0; i < allDirectories.Count; i++)
		{
			allDirectories[i] = FileUtil.GetProjectRelativePath(allDirectories[i].Replace("\\", "/"));
		}

		string processedAssetPath = Path.GetDirectoryName(assetPath).Replace("\\", "/");

		return allDirectories.Contains(processedAssetPath);
	}
}

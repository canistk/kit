using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit
{
    /// <summary>
    /// A simple SceneGUI depend on <seealso cref="SceneViewHandler.Register(EditorSceneGUI)"/>
    /// </summary>
    public class EditorSceneGUI
    {
        public string title;
        public Rect screenRect;
        public enum eAnchor
        {
            Free = 0,
            LeftTop,
            LeftCenter,
            LeftBottom,
            MiddleTop,
            MiddleCenter,
            MiddleBottom,
            RightTop,
            RightCenter,
            RightBottom,
        }
        public eAnchor anchor;
        public delegate void DrawGUI(int id, Rect rect);
        public DrawGUI drawGUI;
        public EditorSceneGUI(eAnchor anchor, Rect screenRect, DrawGUI drawGUI, string title = "")
        {
            this.anchor = anchor;
            this.screenRect = screenRect;
            this.drawGUI = drawGUI;
            this.title = (title == null || title.Length == 0) ? string.Empty : title;
        }
    }

    /// <summary>
    /// A improved SceneGUI with window like GUI
    /// <seealso cref="SceneViewHandler.Register(EditorSceneGUI)"/>
    /// </summary>
    public class EditorSceneWindow : EditorSceneGUI
    {
        internal bool draggingLeft      = false;
        internal bool draggingRight     = false;
        internal bool draggingUp        = false;
        internal bool draggingDown      = false;
        internal bool canDrag           = true;

        private Vector2 m_Scroll = Vector2.zero;
        public EditorSceneWindow(eAnchor anchor, Rect screenRect, DrawGUI drawGUI, string title = "")
            : base(anchor, screenRect, drawGUI, title)
        { }

        public EditorSceneWindow(eAnchor anchor, Vector2 offset, Vector2 size, DrawGUI drawGUI, string title = "")
            : base(anchor, new Rect(offset, size), drawGUI, title)
        { }

        public EditorSceneWindow(Rect screenRect, DrawGUI drawGUI, string title = "")
            : base(eAnchor.Free, screenRect, drawGUI, title)
        { }

        const float s_DragHeight = 20f;
        public void DragableWindow(int id, Rect rect)
        {
            // A drag able title bar
            GUI.DragWindow(new Rect(0f, 0f, screenRect.width, s_DragHeight));
            // using (new GUILayout.AreaScope(new Rect(0f, s_DragHeight, screenRect.width, screenRect.height - s_DragHeight)))
            {
                using (var scroll = new GUILayout.ScrollViewScope(m_Scroll, false, false, GUILayout.ExpandHeight(true)))
                {
                    drawGUI?.Invoke(id, rect);
                    m_Scroll = scroll.scrollPosition;
                }
            }
        }
    }

    /// <summary>
    /// A helper class to draw window or panel within Unity3D sceneview (Editor mode)
    /// </summary>
    public class SceneViewHandler : System.IDisposable
    {
        public SceneViewHandler()
        {
            SceneView.duringSceneGui += DrawSceneGUIs;
        }
        ~SceneViewHandler()
        {
            Dispose(disposing: false);
        }

        private bool m_IsDisposed;
        private List<EditorSceneGUI> m_List = new List<EditorSceneGUI>(2);

        #region Lock Control
        static HashSet<object> m_LockSceneView      = new HashSet<object>();
        protected bool IsLockedControl              => m_LockSceneView.Count > 0;
        protected void LockControl(object who)      =>  m_LockSceneView.Add(who);
        protected bool ReleaseControl(object who)   =>  m_LockSceneView.Remove(who);
        protected bool IsLockedBy(object who)       =>  m_LockSceneView.Contains(who);
        protected bool IsLocked()                   =>  m_LockSceneView.Count > 0;
        private void InterceptControl(int id)
        {
            if (!IsLockedControl)
                return;
            // https://forum.unity.com/threads/solved-custom-editor-onscenegui-scripting.34137/
            // set current controlId as default control, so we wouldn't lose focus during click in scene view.
            HandleUtility.AddDefaultControl(id);
        }
        #endregion Lock Control

        private void DrawSceneGUIs(SceneView sceneview)
        {
            var controlId       = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
            var oldHotControl   = GUIUtility.hotControl;
            var evt             = Event.current;
            switch(evt.GetTypeForControl(controlId))
            {
                case EventType.Layout: InterceptControl(controlId); break;
            }

            for (int i = 0; i < m_List.Count; ++i)
            {
                var obj         = m_List[i];
			    var rect        = GetGUIRect(obj, sceneview);
                var isLocked    = IsLockedBy(obj);
                var inRange     = rect.Contains(evt.mousePosition);
                if (!isLocked && inRange)
                {
                    LockControl(obj);
                }
                else if (isLocked && !inRange)
                {
                    ReleaseControl(obj);
                }

                var newRect     = GUI.Window(i, rect, (id) => DrawWindow(obj, id, rect), obj.title);
			    SetGUIRect(obj, newRect, sceneview);
            }
        }

        /// <summary><see cref="https://forum.unity.com/threads/is-there-a-resize-equivalent-to-gui-dragwindow.10144/"/></summary>
        /// <param name="nodeWindow"></param>
        /// <param name="dir"></param>
        /// <param name="detectionRange"></param>
        /// <returns></returns>
        private static Rect Resizer(EditorSceneWindow nodeWindow, int dir = 0, float detectionRange = 16f)
        {
            Rect window = nodeWindow.screenRect;
            detectionRange *= .5f;

            Rect resizer = window;
            if (dir == 0)
            {
                // left
                resizer.xMax = resizer.xMin + detectionRange;
                resizer.xMin -= detectionRange;
            }
            else if (dir == 1)
            {
                // right
                resizer.xMin = resizer.xMax - detectionRange;
                resizer.xMax += detectionRange;
            }
            else if (dir == 2)
            {
                // up
                resizer.yMax = resizer.yMin + detectionRange;
                resizer.yMin -= detectionRange;
            }
            else
            {
                // down
                resizer.yMin = resizer.yMax - detectionRange;
                resizer.yMax += detectionRange;
            }
            GUI.Box(resizer, Texture2D.whiteTexture);
            //Debug.Log("Dir: " + dir + "\n(" + resizer.xMin + ", " + resizer.xMax + ")\n(" + resizer.yMin + ", " + resizer.yMax + ")");

            Event current = Event.current;
            if (dir == 0 || dir == 1)
            {
                EditorGUIUtility.AddCursorRect(resizer, MouseCursor.ResizeHorizontal);
            }
            else
            {
                EditorGUIUtility.AddCursorRect(resizer, MouseCursor.ResizeVertical);
            }

            if (current.type == EventType.MouseUp)
            {
                nodeWindow.draggingLeft     = false;
                nodeWindow.draggingRight    = false;
                nodeWindow.draggingUp       = false;
                nodeWindow.draggingDown     = false;
                nodeWindow.canDrag          = false;
            }

            // var scr = sceneview.position;
            bool inXRange = current.mousePosition.x > resizer.xMin && current.mousePosition.x < resizer.xMax;
            bool inYRange = current.mousePosition.y > resizer.yMin && current.mousePosition.y < resizer.yMax;
            
            if (current.type == EventType.MouseDown &&
                (inXRange && inYRange))
            {
                nodeWindow.canDrag = true;
            }


            const float s_MinWidth = 100f;
            const float s_MinHeight = 50f;
            if (nodeWindow.canDrag)
            {

                if (inXRange && inYRange &&
                    current.type == EventType.MouseDrag &&
                    current.button == 0 ||
                    nodeWindow.draggingLeft ||
                    nodeWindow.draggingRight)
                {
                    if ((dir == 1) && !(nodeWindow.draggingRight || nodeWindow.draggingUp || nodeWindow.draggingDown))
                    {
                        window.width += current.delta.x;
                        window.width = Mathf.Max(s_MinWidth, window.width);

                        //sceneview.Repaint();
                        nodeWindow.draggingLeft = true;

                    }

                    if ((dir == 0) && !(nodeWindow.draggingLeft || nodeWindow.draggingUp || nodeWindow.draggingDown))
                    {
                        window.x += current.delta.x;
                        window.width -= current.delta.x;
                        window.width = Mathf.Max(s_MinWidth, window.width);
                        //sceneview.Repaint();
                        nodeWindow.draggingRight = true;
                    }
                }

                if (inXRange && inYRange &&
                    current.type == EventType.MouseDrag &&
                    current.button == 0 ||
                    nodeWindow.draggingUp ||
                    nodeWindow.draggingDown)
                {
                    if ((dir == 2) && !(nodeWindow.draggingDown || nodeWindow.draggingLeft || nodeWindow.draggingRight))
                    {
                        window.y += current.delta.y;
                        window.height -= current.delta.y;
                        window.height = Mathf.Max(s_MinHeight, window.height);
                        //sceneview.Repaint();
                        nodeWindow.draggingUp = true;
                    }

                    if ((dir == 3) && !(nodeWindow.draggingUp || nodeWindow.draggingLeft || nodeWindow.draggingRight))
                    {
                        window.height += current.delta.y;
                        window.height = Mathf.Max(s_MinHeight, window.height);
                        //sceneview.Repaint();
                        nodeWindow.draggingDown = true;
                    }
                }
            }

            return window;
        }

        private static void SetGUIRect(EditorSceneGUI p, Rect v, SceneView view)
        {
            float ox        = p.screenRect.x;
            float oy        = p.screenRect.y;
            float width     = p.screenRect.width;
            float height    = p.screenRect.height;
            float hw        = width * 0.5f;
            float hh        = height * 0.5f;
            float vw        = view.position.width;
            float vh        = view.position.height;
            float hvw       = vw * 0.5f;
            float hvh       = vh * 0.5f;

			float left      = (v.x);
			float middle    = (v.x  - hvw   + hw);
			float right     = (v.x  - vw    + width);
			float top       = (v.y);
			float center    = (v.y  - hvh   + hh);
			float bottom    = (v.y  - vh    + height);
            switch (p.anchor)
			{
                case EditorSceneGUI.eAnchor.Free:			p.screenRect = v; break;
                case EditorSceneGUI.eAnchor.LeftTop:		p.screenRect = new Rect(left,	top,	width, height); break;
                case EditorSceneGUI.eAnchor.LeftCenter:		p.screenRect = new Rect(left,	center,	width, height); break;
				case EditorSceneGUI.eAnchor.LeftBottom:		p.screenRect = new Rect(left,	bottom,	width, height); break;
				case EditorSceneGUI.eAnchor.MiddleTop:		p.screenRect = new Rect(middle,	top,	width, height); break;
				case EditorSceneGUI.eAnchor.MiddleCenter:	p.screenRect = new Rect(middle,	center,	width, height); break;
                case EditorSceneGUI.eAnchor.MiddleBottom:	p.screenRect = new Rect(middle,	bottom,	width, height); break;
				case EditorSceneGUI.eAnchor.RightTop:		p.screenRect = new Rect(right,	top,	width, height); break;
				case EditorSceneGUI.eAnchor.RightCenter:	p.screenRect = new Rect(right,	center,	width, height); break;
                case EditorSceneGUI.eAnchor.RightBottom:	p.screenRect = new Rect(right,	bottom,	width, height); break;
				default: throw new System.NotImplementedException();
            }
		}
        private static Rect GetGUIRect(EditorSceneGUI p, SceneView view)
        {
            float ox        = p.screenRect.x;
            float oy        = p.screenRect.y;
            float width     = p.screenRect.width;
            float height    = p.screenRect.height;
            float hw        = width * 0.5f;
            float hh        = height * 0.5f;
            float vw        = view.position.width;
            float vh        = view.position.height;
            float hvw       = vw * 0.5f;
            float hvh       = vh * 0.5f;

            float left      = ox;
            float middle    = (hvw  - hw        + ox);
            float right     = (vw   - width     + ox);
            float top       = oy;
            float center    = (hvh  - hh        + oy);
            float bottom    = (vh   - height    + oy);
            Rect rst = p.anchor switch
            {
                EditorSceneGUI.eAnchor.Free         => p.screenRect,
                EditorSceneGUI.eAnchor.LeftTop      => new Rect(left,   top,    width, height),
                EditorSceneGUI.eAnchor.LeftCenter   => new Rect(left,   center, width, height),
                EditorSceneGUI.eAnchor.LeftBottom   => new Rect(left,   bottom, width, height),
                EditorSceneGUI.eAnchor.MiddleTop    => new Rect(middle, top,    width, height),
                EditorSceneGUI.eAnchor.MiddleCenter => new Rect(middle, center, width, height),
                EditorSceneGUI.eAnchor.MiddleBottom => new Rect(middle, bottom, width, height),
                EditorSceneGUI.eAnchor.RightTop     => new Rect(right,  top,    width, height),
                EditorSceneGUI.eAnchor.RightCenter  => new Rect(right,  center, width, height),
                EditorSceneGUI.eAnchor.RightBottom  => new Rect(right,  bottom, width, height),
                _ => throw new System.NotImplementedException(),
            };
            return rst;
        }
        private static void DrawWindow(EditorSceneGUI obj, int id, Rect rect)
        {
            if (obj == null)
                throw new System.NullReferenceException();
            if (obj is EditorSceneWindow win)
            {
                //GUILayout.Label($"{win.screenRect:F2}");
                //for (int dir = 0; dir < 4; ++dir)
                //{
                //    win.screenRect = Resizer(win, dir, 16f);
                //    win.screenRect.position = Vector2.zero;
                //}
                win.DragableWindow(id, rect);
                return;
            }
            obj.drawGUI(id, rect);
        }

        #region Register
        public void Register(params EditorSceneGUI[] uis)
        {
            foreach (var ui in uis)
                Register(ui);
        }
        public void Register(EditorSceneGUI ui)
        {
            if (ui != null && !m_List.Contains(ui))
                m_List.Add(ui);
        }
        public void Unregister(params EditorSceneGUI[] uis)
        {
            foreach (var ui in uis)
            {
                Unregister(ui);
            }
        }
        public void Unregister(EditorSceneGUI ui)
        {
            if (ui != null)
                m_List.Remove(ui);
        }
        #endregion Register

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!m_IsDisposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    SceneView.duringSceneGui -= DrawSceneGUIs;
                    m_List.Clear();
                }

                m_List = null;
                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                m_IsDisposed = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }
}

namespace Kit.Test
{
    [CustomEditor(typeof(SceneViewHandlerTest), false)]
    public class SceneViewHandlerTestEditor : EditorBase
    {
        static EditorSceneWindow m_LT, m_LC, m_LB, m_MT, m_MC, m_MB, m_RT, m_RC, m_RB;
        SceneViewHandler m_Handler;
        protected override void OnEnable()
        {
            base.OnEnable();
            const float W = 200f;
            const float H = 100f;
            if (m_LT == null) m_LT = new EditorSceneWindow(EditorSceneWindow.eAnchor.LeftTop,       Vector2.zero, new Vector2(W, H), MySceneGUI, "LT");
            if (m_LC == null) m_LC = new EditorSceneWindow(EditorSceneWindow.eAnchor.LeftCenter,    Vector2.zero, new Vector2(W, H), MySceneGUI, "LC");
            if (m_LB == null) m_LB = new EditorSceneWindow(EditorSceneWindow.eAnchor.LeftBottom,    Vector2.zero, new Vector2(W, H), MySceneGUI, "LB");
            if (m_MT == null) m_MT = new EditorSceneWindow(EditorSceneWindow.eAnchor.MiddleTop,     Vector2.zero, new Vector2(W, H), MySceneGUI, "MB");
            if (m_MC == null) m_MC = new EditorSceneWindow(EditorSceneWindow.eAnchor.MiddleCenter,  Vector2.zero, new Vector2(W, H), MySceneGUI, "MC");
            if (m_MB == null) m_MB = new EditorSceneWindow(EditorSceneWindow.eAnchor.MiddleBottom,  Vector2.zero, new Vector2(W, H), MySceneGUI, "MB");
            if (m_RT == null) m_RT = new EditorSceneWindow(EditorSceneWindow.eAnchor.RightTop,      Vector2.zero, new Vector2(W, H), MySceneGUI, "RT");
            if (m_RC == null) m_RC = new EditorSceneWindow(EditorSceneWindow.eAnchor.RightCenter,   Vector2.zero, new Vector2(W, H), MySceneGUI, "RC");
            if (m_RB == null) m_RB = new EditorSceneWindow(EditorSceneWindow.eAnchor.RightBottom,   Vector2.zero, new Vector2(W, H), MySceneGUI, "RB");
            m_Handler = new SceneViewHandler();
            m_Handler.Register(m_LT, m_LC, m_LB, m_MT, m_MC, m_MB, m_RT, m_RC, m_RB);
        }
        protected override void OnDisable()
        {
            m_Handler.Unregister(m_LT, m_LC, m_LB, m_MT, m_MC, m_MB, m_RT, m_RC, m_RB);
            base.OnDisable();
        }

        private void MySceneGUI(int windowId, Rect rect)
        {
            if (GUILayout.Button($"Dragable area"))
            {
                Debug.Log($"Win {windowId}");
            }
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");
            GUILayout.Label($"Win {windowId}");

        }
    }
}
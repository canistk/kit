namespace Kit
{
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ObjectPool_Bak), editorForChildClasses: true)]
    public class ObjectPoolBakEditor : EditorBase
    {
        private ObjectPool_Bak m_Target;
        private SerializedProperty settingsProperty;
        private FieldInfo templateField;
        private FieldInfo settingsField;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_Target = (ObjectPool_Bak)target;
            settingsProperty = serializedObject.FindProperty("m_PrefabPreloadSettings");
            templateField = typeof(ObjectPool_Bak).GetField("m_PrefabPreloadSettingTemplate", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            settingsField = typeof(ObjectPool_Bak).GetField("m_PrefabPreloadSettings", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        }


        protected override void OnBeforeDrawGUI()
        {
            base.OnBeforeDrawGUI();

            if (!Application.isPlaying)
            {
                GameObject prefab = null;
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    prefab = (GameObject)EditorGUILayout.ObjectField("Drag to add prefab here", null, typeof(GameObject), allowSceneObjects: false);
                    if (check.changed && prefab != null)
                    {
                        ++settingsProperty.arraySize;
                        serializedObject.ApplyModifiedProperties();
                        ObjectPool_Bak.PrefabPreloadSetting originalSetting = (ObjectPool_Bak.PrefabPreloadSetting)templateField.GetValue(m_Target);
                        ObjectPool_Bak.PrefabPreloadSetting settingTemplate = originalSetting?.Clone() ?? new ObjectPool_Bak.PrefabPreloadSetting();
                        settingTemplate.m_Prefab = prefab;
                        settingTemplate.m_Name = prefab.name;
                        ObjectPool_Bak.PrefabPreloadSetting[] settings = (ObjectPool_Bak.PrefabPreloadSetting[])settingsField.GetValue(m_Target);
                        settings[settings.Length - 1] = settingTemplate;
                    }
                }
            }
        }
    }
}

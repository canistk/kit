﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kit
{
	[CustomEditor(typeof(SpawnerProviderBase), true)]
    public class SpawnerProviderEditor : EditorBase
    {
#if PHOTON_UNITY_NETWORKING
		SerializedProperty m_PhotonPoolProp;
#endif
		SerializedProperty m_PoolProp, m_PoolNameProp;

		protected override void OnEnable()
		{
			base.OnEnable();
#if PHOTON_UNITY_NETWORKING
			m_PhotonPoolProp = serializedObject.FindProperty(nameof(SpawnerProviderBase.m_PhotonPool));
#endif
			m_PoolProp = serializedObject.FindProperty("m_Pool"); // nameof(SpawnerProviderBase.m_Pool));
			m_PoolNameProp = serializedObject.FindProperty(nameof(SpawnerProviderBase.m_PoolName));
		}

		protected override void OnDrawProperty(SerializedProperty property)
		{
			string path = property.propertyPath;
			if (path.Equals(m_PoolNameProp.propertyPath))
			{
				if (
#if PHOTON_UNITY_NETWORKING
				!m_PhotonPoolProp.boolValue &&
#endif
				m_PoolProp.objectReferenceValue == null)
					base.OnDrawProperty(property);
			}
			else if (path.Equals(m_PoolProp.propertyPath))
			{
#if PHOTON_UNITY_NETWORKING
				if (!m_PhotonPoolProp.boolValue)
					base.OnDrawProperty(property);
#endif
			}
#if PHOTON_UNITY_NETWORKING
			else if (path.Equals(m_PhotonPoolProp.propertyPath))
			{
				base.OnDrawProperty(property);
			}
#endif
			else
			{
				base.OnDrawProperty(property);
			}
		}
	}
}
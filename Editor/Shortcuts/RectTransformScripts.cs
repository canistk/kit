using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit
{
    public class RectTransformScripts : EditorWindow
    {
        /// <summary>
        /// <see cref="http://forum.unity3d.com/threads/scripts-useful-4-6-scripts-collection.264161/?_ga=2.244486401.583712110.1615051159-1416679152.1573397245"/>
        /// </summary>
        [MenuItem("Kit/UI Kit/Anchors to Corners %t")]
        private static void AnchorsToCorners()
        {
            if (Selection.gameObjects.Length > 0)
            {
                Transform[] transforms = Selection.transforms;
                int cnt = 0;
                int nameCnt = Mathf.Min(cnt, 20);
                string uiNames = "";
                foreach (Transform transform in transforms)
                {
                    if (transform is RectTransform rectTransform &&
                        rectTransform.parent is RectTransform parent)
                    {
                        Vector2 newAnchorsMin = new Vector2(rectTransform.anchorMin.x + rectTransform.offsetMin.x / parent.rect.width,
                                                            rectTransform.anchorMin.y + rectTransform.offsetMin.y / parent.rect.height);
                        Vector2 newAnchorsMax = new Vector2(rectTransform.anchorMax.x + rectTransform.offsetMax.x / parent.rect.width,
                                                            rectTransform.anchorMax.y + rectTransform.offsetMax.y / parent.rect.height);

                        rectTransform.anchorMin = newAnchorsMin;
                        rectTransform.anchorMax = newAnchorsMax;
                        rectTransform.offsetMin = rectTransform.offsetMax = Vector2.zero;
                        cnt++;
                        if (nameCnt > 0)
                        {
                            nameCnt--;
                            uiNames += "," + rectTransform.name;
                        }
                    }
                }
                Debug.Log($"UI: Anchors To Corners : {cnt}\n>>{uiNames}");
            }
        }
    }
}
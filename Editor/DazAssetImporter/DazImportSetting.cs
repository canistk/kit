using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    [CreateAssetMenu(
        fileName = "DazImportSetting",
        menuName = "Kit/Daz3D Import Setting")]
    public class DazImportSetting : CustomAssetImportSetting
    {
    }
}
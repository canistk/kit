using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
namespace Kit
{
    using Helper = Kit.AssetImporterHelper;
    public class DazAssetImporter : AssetPostprocessor
    {
        DazImportSetting m_Setting;
        bool m_WasSearchedSetting = false;

        const string s_DazExportDataExtension = ".dtu";

        void OnPreprocessAsset()
        {
            if (!assetImporter.importSettingsMissing)
                return;

            if (!Helper.TryLocateSetting(ref m_Setting, ref m_WasSearchedSetting))
                return;

            if (!Helper.IsAssetInFolder(assetImporter, m_Setting, s_DazExportDataExtension))
                return;

            string content = File.ReadAllText(assetImporter.assetPath);
            if (string.IsNullOrEmpty(content))
                return;


            Debug.Log(content);
            Debug.Log($"Found Daz3D setting path={assetImporter.assetPath}");
        }
    }

    public class Daz3DExportData
    {

    }
}
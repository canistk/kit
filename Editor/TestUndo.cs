using Kit;
using UnityEditor;
using UnityEngine;

public class TestUndo : MonoBehaviour
{
    private bool startChange = false;
    private int groupId = 0;

    public int iValue1;
    public int iValue2;

    [ContextButton(nameof(ChangeValue1))]
    public bool B_ChangeValue1;
    [ContextButton(nameof(ChangeValue2))]
    public bool B_ChangeValue2;
    [ContextButton(nameof(EndChange))]
    public bool B_EndChange;


    private void TryStartChange()
    {
        if (!startChange)
        {
            startChange = true;
            groupId = Undo.GetCurrentGroup();
            Debug.Log($"New Undo group id {groupId}");
        }
    }

    public void ChangeValue1()
    {
        TryStartChange();
        Undo.RecordObject(this, "Change Value 1");
        iValue1++;
    }

    public void ChangeValue2()
    {
        TryStartChange();
        Undo.RecordObject(this, "Change Value 2");
        iValue2++;
    }

    public void EndChange()
    {
        Undo.CollapseUndoOperations(groupId);
        Debug.Log($"End Change {groupId}");
        startChange = false;
        groupId = -1;
    }
}
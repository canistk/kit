using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
namespace Kit
{
    /// <summary>
    /// <see cref="https://docs.unity3d.com/ScriptReference/AssetPostprocessor.html"/>
    /// </summary>
    /// <typeparam name="SETTING"></typeparam>
    public static class AssetImporterHelper
    {   
        /// <summary>Try locate setting,
        /// when it's not found, early return false on next attempt.
        /// to prevent waste resource in massive import operation.</summary>
        /// <typeparam name="SETTING"></typeparam>
        /// <param name="setting"></param>
        /// <param name="wasSearchSetting"></param>
        /// <returns></returns>
        public static bool TryLocateSetting<SETTING>(ref SETTING setting, ref bool wasSearchSetting)
            where SETTING : CustomAssetImportSetting
        {
            if (setting != null)
                return true;
            if (wasSearchSetting)
                return false;

            wasSearchSetting = true;
            string searchCmd = $"t:{typeof(SETTING)}";
            string[] settingGuids = AssetDatabase.FindAssets(searchCmd);
            if (settingGuids.Length == 0)
                return false;
            string path = AssetDatabase.GUIDToAssetPath(settingGuids[0]);
            setting = (SETTING)AssetDatabase.LoadAssetAtPath(path, typeof(SETTING));
            return setting != null && setting.EnableImporter;
        }

        public static bool IsAssetInFolder(in AssetImporter assetImporter, in CustomAssetImportSetting setting)
        {
            string path0 = Path.GetFullPath(assetImporter.assetPath);
            string path1 = Path.GetDirectoryName(setting.ImportDirectory);
            return path0.StartsWith(path1);
        }

        public static bool IsAssetInFolder(in AssetImporter assetImporter, in CustomAssetImportSetting setting, string fileExtension)
        {
            var ext = Path.GetExtension(assetImporter.assetPath).ToLower();
            ext = ext.TrimStart('.').ToLower();
            fileExtension = fileExtension.TrimStart('.').ToLower();
            if (!fileExtension.Equals(ext))
                return false;
            return IsAssetInFolder(assetImporter, setting);
        }
    }

    /// <summary></summary>
    /// <example>
    /// [CreateAssetMenu(fileName = "__YOUR__ImportSetting", menuName = "Kit/__YOUR__Import Setting")]
    /// </example>
    public class CustomAssetImportSetting : ScriptableObject
    {
        [Header("Setting")]
        public bool EnableImporter = true;
        
        [Help("/Assets/ or content menu > GetDirectory() for auto fill in.", eMessageType.Info)]
        [SerializeField] string _importDirectory = "";

        public string ImportDirectory
        {
            get
            {
                Editor_FetchDirectory();
                return Path.Combine(Path.GetDirectoryName(Application.dataPath), _importDirectory);
            }
        }

        private void OnEnable()
        {
            Editor_FetchDirectory();
        }

        private void Editor_FetchDirectory()
        {
#if UNITY_EDITOR
            if (_importDirectory.Length > 0)
                return;
            _importDirectory = Path.GetDirectoryName(AssetDatabase.GetAssetPath(this));
#endif
        }

        [ContextMenu("Get Directory")]
        private void GetDirectory()
        {
            Editor_FetchDirectory();
            Debug.Log($"Fetch {GetType()} path=" + Path.Combine(Path.GetDirectoryName(Application.dataPath), _importDirectory));
        }
    }
}
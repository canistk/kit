﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Kit
{
	public class SearchForGUID : SearchableEditorWindow
	{
		#region Editor
		static SearchForGUID window;
		[MenuItem("Kit/Project Search/Search GUID reference &#g")]
		private static void Init()
		{
			if (window == null)
			{
				window = (SearchForGUID)SearchableEditorWindow.GetWindow(typeof(SearchForGUID));
				window.Show();
			}
			else
				window.SearchGUID();
		}
		#endregion

		#region Getter Setter

		internal Platform.Feedback feedback;
		private void OnFeedback(Platform.Feedback feedback) => this.feedback = feedback;

		internal class Setting
		{
			public Object obj;
			public bool CheckScene = true;
			public bool CheckMaterial = true;
			public bool CheckPrefab = true;
			public override string ToString()
			{
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
				// *.unity *.prefab *.mat
				return string.Format("{0}{1}{2}",
					(CheckScene ? "*.unity " : ""),
					(CheckPrefab ? "*.prefab " : ""),
					(CheckMaterial ? "*.mat " : "")
				).TrimEnd(' ');
#else
				// --include "*.prefab" --include "*.unity" --include "*.mat"
				return string.Format("{0}{1}{2}",
					(CheckScene ? "--include \"*.unity\" " : ""),
					(CheckPrefab ? "--include \"*.prefab\" " : ""),
					(CheckMaterial ? "--include \"*.mat\" " : "")
				).TrimEnd(' ');
#endif
			}
		}
		internal Setting setting;

		private string selectedGUID = "";
		private string ProjectPath
		{
			get
			{
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
				return Application.dataPath.Replace("/", @"\"); // WTF ? DataPath, you have one job to do.
#else
				return Application.dataPath;
#endif
			}
		}
		#endregion

		#region System
		public override void OnEnable()
		{
			feedback = default;
			setting = new Setting();
		}
		void OnGUI()
		{
			DisplaySelectedObject();
			DisplaySearchResult();
		}

		#endregion

		#region Display panel
		private Vector2 scrollResult = Vector2.zero;


		private void DisplaySearchResult()
		{
			if (feedback.responses == null)
				return;

			GUILayout.Label("Project folder : " + ProjectPath);
			GUILayout.Label("last command :");
			GUILayout.TextArea(feedback.command);

			if (feedback.responses.Length > 0)
			{
				scrollResult = GUILayout.BeginScrollView(scrollResult);
				GUILayout.Label("result :");
				foreach (var line in feedback.responses)
				{
					if (GUILayout.Button(line.msg))
					{
						var fixedPath = Path.Combine("Assets", line.msg);
                        Object obj = AssetDatabase.LoadMainAssetAtPath(fixedPath);
						if (obj != null)
						{
							EditorUtility.FocusProjectWindow();
							EditorGUIUtility.PingObject(obj);
						}
						else
							UnityEngine.Debug.Log("Ping Object fail.\n" + line.msg);
					}
				}
				GUILayout.EndScrollView();
			}
			else if (feedback.responses.Length == 0)
			{
                EditorGUILayout.HelpBox("No matching result.", MessageType.Info);
			}
			else
			{
				EditorGUILayout.HelpBox("Command executed.", MessageType.Warning);
			}
		}
		private void DisplaySelectedObject()
		{
			Object obj = Selection.activeObject;
			if (obj == null)
			{
				GUILayout.Label(string.Format("Selected Object : {0}", "None"));
			}
			else
			{
				GUILayout.BeginHorizontal();

				GUILayout.BeginVertical();
				GUILayout.Label(string.Format("Selected Object : {0}", obj.name));
				selectedGUID = string.Join(string.Empty, Selection.assetGUIDs);
				GUILayout.Label(string.Format("GUID : {0}\nInstance ID : {1}\nType : {2}", selectedGUID, obj.GetInstanceID(), obj.GetType()));
				GUILayout.EndVertical();

				GUILayout.BeginVertical(GUILayout.MaxWidth(300f));
				if (GUILayout.Button("Ping Selected", GUILayout.MaxHeight(80f)))
				{
					Selection.activeObject = (setting.obj == null) ? obj : setting.obj;
					EditorGUIUtility.PingObject(Selection.activeObject);
				}
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();

				DisplaySearchSetting();

				GUILayout.Space(20f);

			}
		}
		private void DisplaySearchSetting()
		{
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Search name code", GUILayout.Height(80f), GUILayout.Width(130f)))
			{
				SearchInCode();
				setting.obj = Selection.activeObject;
			}
			if (Selection.activeObject.GetType() == typeof(Texture2D))
			{
				if (GUILayout.Button("Search NGUI sprite", GUILayout.Height(80f), GUILayout.Width(130f)))
				{
					SearchSpriteReference();
					setting.obj = Selection.activeObject;
				}
			}
			GUILayout.Space(30f);
			if (GUILayout.Button("Search for asset", GUILayout.Height(80f)))
			{
				SearchGUID();
				setting.obj = Selection.activeObject;
			}
			GUILayout.Space(20f);
			GUILayout.BeginVertical(GUILayout.Width(100f));
			setting.CheckScene = GUILayout.Toggle(setting.CheckScene, "Check Scene");
			setting.CheckMaterial = GUILayout.Toggle(setting.CheckMaterial, "Check Material");
			setting.CheckPrefab = GUILayout.Toggle(setting.CheckPrefab, "Check Prefab");
			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
		}
		#endregion

		#region Core
		public void SearchGUID()
		{
			if (Selection.activeObject == null)
			{
				UnityEngine.Debug.LogWarning("Please select object to search");
				return;
			}
			string guid = string.Join(string.Empty, Selection.assetGUIDs);

#if UNITY_EDITOR_WIN
			Platform.Command(@"findstr.exe", string.Format("/S /M /O /L /C:\"{0}\" {1}", guid, setting.ToString()), OnFeedback);
#else
			Platform.Command(@"grep", string.Format("-l -R {0} \"{1}\" .", setting.ToString(), guid), OnFeedback);
#endif
        }
        public void SearchInCode()
		{
			if (Selection.activeObject == null)
			{
				UnityEngine.Debug.LogWarning("Please select object to search");
				return;
			}
			string fileName = Selection.activeObject.name;
			UnityEngine.Debug.Log(fileName);

#if UNITY_EDITOR_WIN
			Platform.Command(@"findstr.exe", string.Format("/S /M /O /L /C:\"{0}\" {1}", fileName, "*.cs *.js"), OnFeedback);

#else
			Platform.Command(@"grep", string.Format("-l -R {0} \"{1}\"", "--include \"*.cs\" --include \"*.js\"", fileName), OnFeedback);
#endif
        }
        public void SearchSpriteReference()
		{
			if (Selection.activeObject == null)
			{
				UnityEngine.Debug.LogWarning("Please select object to search");
				return;
			}
			string assetName = Selection.activeObject.name;

			bool tmp = setting.CheckMaterial;
			setting.CheckMaterial = false;

#if UNITY_EDITOR
			Platform.Command(@"findstr.exe", string.Format("/S /M /O /L /C:\"{0}\" {1}", assetName, setting.ToString()), OnFeedback);
#else
			Platform.Command(@"grep", string.Format("-l -R {0} \"{1}\"", setting.ToString(), assetName), OnFeedback);
#endif
            setting.CheckMaterial = tmp;
		}
		#endregion
	}
}
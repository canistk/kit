#define EVENT_MODE
#define DEBUG_MODE
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;
namespace Kit
{
    public static class Platform
    {
        private static string ProjectPath
        {
            get
            {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                return Application.dataPath.Replace("/", @"\"); // WTF ? DataPath, you have one job to do.
#else
                return Application.dataPath;
#endif
            }
        }

        public struct Response
        {
            public bool isError;
            public string msg;
            public Response(string msg, bool isErr)
            {
                this.msg = msg;
                this.isError = isErr;
            }
        }

        public struct Feedback
        {
            public string command;
            public Response[] responses;
        }

        private class Task : IDisposable
        {
            public enum eState
            {
                Idle,
                Run,
                End,
            }
            public eState state { get; private set; } = eState.Idle;
            private Process process;
            
            private List<Response> result;

            public readonly string command;
            private System.Action<Feedback> callback;
            public Task(string shell, string args = "", System.Action<Feedback> completed = null)
            {
                this.command = $"{shell} {args}";
                this.callback = completed;
                var p = new Process()
                {
                    EnableRaisingEvents = true,
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = shell,
                        Arguments = args,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        // WorkingDirectory = System.Environment.CurrentDirectory, //ProjectPath,
                        WorkingDirectory = ProjectPath,
                        // ErrorDialog = true,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        // RedirectStandardInput = true,
                    }
                };
                
                this.result = new List<Response>();
                this.process = p;
#if EVENT_MODE
                this.process.OutputDataReceived += OutputDataReceived;
                this.process.ErrorDataReceived += ErrorDataReceived;
#endif
            }
#if EVENT_MODE
            private void OutputDataReceived(object sender, DataReceivedEventArgs e)
            {
                if (e.Data == null)
                    return;
                result.Add(new Response(e.Data, false));
            }

            private void ErrorDataReceived(object sender, DataReceivedEventArgs e)
            {
                if (e.Data == null)
                    return;
                result.Add(new Response(e.Data, true));
            }
#endif

            public bool Run()
            {
                if (isDisposed)
                    return false;
                if (state == eState.Idle)
                {
                    state = eState.Run;
#if EVENT_MODE
                    this.process.Start();
                    this.process.BeginOutputReadLine();
                    this.process.BeginErrorReadLine();
#else
                    this.process.Start();
                    using (var stdout = process.StandardOutput)
                    {
                        string line;
                        while ((line = stdout.ReadLine()) != null)
                            result.Add(new Response(line, false));
                    }
                    using (var stderr = process.StandardError)
                    {
                        string line;
                        while ((line = stderr.ReadLine()) != null)
                            result.Add(new Response(line, true));
                    }
#endif
                }
                if (state == eState.Run)
                {
                    if (this.process.HasExited)
                    {
                        state = eState.End;
                    }
                }
                if (state == eState.End)
                {
                }
                return state != eState.End;
            }

            #region Dispose
            private bool isDisposed;

            private void Dispose(bool disposing)
            {
                if (!isDisposed)
                {
                    if (disposing)
                    {
                        if (this.callback != null)
                        {
                            var rst = new Feedback
                            {
                                command = this.command,
                                responses = result.ToArray(),
                            };
                            this.callback?.Invoke(rst);
                        }
                        state = eState.End;
                        if (process != null)
                        {
                            try
                            {
#if EVENT_MODE
                                this.process.OutputDataReceived -= OutputDataReceived;
                                this.process.ErrorDataReceived -= ErrorDataReceived;
#endif
                                if (!process.HasExited)
                                {
                                    process.Dispose();
                                    process.CloseMainWindow();
                                    process.Kill();
                                    UnityEngine.Debug.Log("Process suspend.");
                                }
                            }
                            catch (System.Exception ex)
                            {
                                UnityEngine.Debug.LogError("Fail to suspend OS process "+ ex.Message);
                            }
                            finally
                            {
                                process = null;
                                callback = null;
                                result.Clear();
                            }
                        }
                    }

                    // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                    // TODO: set large fields to null
                    isDisposed = true;
                }
            }

            // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
             ~Task()
             {
                 // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
                 Dispose(disposing: false);
             }

            public void Dispose()
            {
                // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
                Dispose(disposing: true);
                GC.SuppressFinalize(this);
            }
            #endregion Dispose
        }
        private static Queue<Task> s_Process = new Queue<Task>();

        /// <summary>Run OS Command</summary>
        /// <param name="shell"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        /// <see cref="http://forum.unity3d.com/threads/start-a-external-package.process.17488/"/>
        /// <seealso cref="http://ss64.com/nt/findstr.html"/>
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void Command(string shell, string args = "", System.Action<Feedback> completed = null)
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                UnityEngine.Debug.LogError($"Block OS command on play mode. {shell}");
                return;
            }

#if DEBUG_MODE
            var t = new Task(shell, args, (b) => { InternalDebugFeedBack(b); completed?.Invoke(b); });
#else
            var t = new Task(shell, args, completed);
#endif
            EditorApplication.update -= OnEditorUpdated;
            EditorApplication.update += OnEditorUpdated;
            s_Process.Enqueue(t);
        }

        private static void InternalDebugFeedBack(Feedback feedBack)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine(feedBack.command);
            foreach (var line in feedBack.responses)
            {
                if (line.isError)
                    sb.Append("Err:").AppendLine(line.msg);
                else
                    sb.AppendLine(line.msg);
            }
            UnityEngine.Debug.Log(sb.ToString());
        }

        private static void OnEditorUpdated()
        {
            if (s_Process.Count == 0)
            {
                UnityEngine.Debug.Log("All platform request completed.");
                EditorApplication.update -= OnEditorUpdated;
                return;
            }

            if (!s_Process.Peek().Run())
            {
                s_Process.Dequeue()?.Dispose();
            }
        }
    }
}
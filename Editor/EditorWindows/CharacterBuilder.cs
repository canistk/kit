using UnityEditor;
using UnityEngine;
namespace Kit
{
    public class CharacterBuilder : EditorWindow
    {
        private static CharacterBuilder window;
        [MenuItem("Kit/Generate/Character Builder %#F1")]
        private static void Init()
        {
            if (window == null)
            {
                window = EditorWindow.GetWindow<CharacterBuilder>();
                window.Show();
            }
        }

        public string s_SourceFolder
        {
            get => EditorPrefs.GetString($"{nameof(CharacterBuilder)}_{nameof(s_SourceFolder)}", Application.dataPath);
            set => EditorPrefs.SetString($"{nameof(CharacterBuilder)}_{nameof(s_SourceFolder)}", value);
        }

        public string s_TargetFolder
        {
            get => EditorPrefs.GetString($"{nameof(CharacterBuilder)}_{nameof(s_TargetFolder)}", Application.dataPath);
            set => EditorPrefs.SetString($"{nameof(CharacterBuilder)}_{nameof(s_TargetFolder)}", value);
        }

        private void OnGUI()
        {
            using (new GUILayout.VerticalScope())
            {
                DrawSourceSession();
            }
        }

        private static GUIContent s_SearchIcon => EditorGUIUtility.IconContent("Search Icon");
        private void DrawSourceSession()
        {
            string folder = s_SourceFolder;
            if (EditorExtend.AssetFolderField("Raw Model folder", ref folder, "Character model folder"))
            {
                s_SourceFolder = folder;
                Debug.Log(folder.ToRichText(Color.cyan));
            }

            folder = s_TargetFolder;
            if (EditorExtend.AssetFolderField("Export Model folder", ref folder, "Character generate folder"))
            {
                s_TargetFolder = folder;
                Debug.Log(folder.ToRichText(Color.green));
            }
        }
    }
}
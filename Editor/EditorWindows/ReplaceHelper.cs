using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit
{
    public class ReplaceHelper : SearchableEditorWindow
    {
        #region Editor
        static ReplaceHelper window;
        [MenuItem("Kit/Scene/Model Replace &%#r")]
        private static void Init()
        {
            if (window == null)
            {
                window = SearchableEditorWindow.GetWindow<ReplaceHelper>();
                window.Show();
            }
        }
        #endregion Editor

        GameObject m_Prefab;
        string m_FilterFactor;
        bool m_OnlySelectedChildren = true;
        bool m_CaseSensitive = true;
        private void OnGUI()
        {
            using (new EditorGUILayout.VerticalScope())
            {
                EditorGUILayout.LabelField("Replace target object in scene :");
                using (new EditorGUILayout.HorizontalScope())
                {
                    m_FilterFactor = EditorGUILayout.TextField("Target name to replace :", m_FilterFactor);
                }
                GUILayout.Space(15f);
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.LabelField("Prefab to replace :");
                    m_Prefab = (GameObject)EditorGUILayout.ObjectField(m_Prefab, typeof(GameObject), allowSceneObjects: false); ;
                }
                m_OnlySelectedChildren  = EditorGUILayout.Toggle("Only replace the selected object(s)", m_OnlySelectedChildren);
                m_CaseSensitive         = EditorGUILayout.Toggle("Case Sensitive", m_CaseSensitive);
                GUILayout.Space(15f);
                if (GUILayout.Button("Replace"))
                {
                    GameObject[] objs = m_OnlySelectedChildren ?
                        Selection.gameObjects :
                        Object.FindObjectsOfType<GameObject>();
                    string trimed = m_FilterFactor.Trim();
                    string lowerCase = trimed.ToLower();
                    bool isNullOrEmpty = m_FilterFactor == null || m_FilterFactor.Length == 0;
                    int affectCnt = 0;
                    int undoIndex = Undo.GetCurrentGroup();
                    Undo.SetCurrentGroupName("ReplaceObjInScene");
                    for (int i = 0; i < objs.Length; ++i)
                    {
                        if (isNullOrEmpty || FilterCondition(objs[i], trimed, lowerCase))
                        {
                            ReplaceObjsInScene(objs[i]);
                            affectCnt++;
                        }
                    }
                    Debug.Log($"Search object(s) : Total {objs.Length}, replaced {affectCnt}");
                    Undo.CollapseUndoOperations(undoIndex);
                }
                GUILayout.FlexibleSpace();
            }
        }

        private bool FilterCondition(GameObject obj, string trimed, string lowerCase)
        {
            string n = obj.name.Trim();
            if (m_CaseSensitive && n.Contains(trimed))
                return true;

            if (n.Contains(lowerCase))
                return true;

            return false;
        }

        private void ReplaceObjsInScene(GameObject target)
        {
            Vector3 pos         = target.transform.position;
            Quaternion rot      = target.transform.rotation;
            Vector3 scale       = target.transform.localScale;
            Transform parent    = target.transform.parent;
            GameObject prefabToken = (GameObject)PrefabUtility.InstantiatePrefab(m_Prefab);
            Undo.RegisterCreatedObjectUndo(prefabToken, "ReplaceObjInScene");
            Undo.DestroyObjectImmediate(target);

            prefabToken.transform.SetParent(parent, false);
            prefabToken.transform.localScale = scale;
            prefabToken.transform.SetPositionAndRotation(pos, rot);
        }
    }
}
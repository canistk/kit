using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace Kit
{
    public class ScreenCaptureHelper : SearchableEditorWindow
    {
        #region Editor
        static ScreenCaptureHelper window;
        [MenuItem("Kit/Generate/Model Preview &%#c")]
        private static void Init()
        {
            if (window == null)
            {
                window = SearchableEditorWindow.GetWindow<ScreenCaptureHelper>();
                window.Show();
            }
        }
        #endregion Editor

        #region System
        private void OnGUI()
        {
            using (new GUILayout.VerticalScope())
            {
                EditorGUILayout.LabelField("Step 1");
                EditorGUILayout.LabelField("Setup Capture in Scene :");
                m_Camera = EditorGUILayout.ObjectField("Camera", m_Camera, typeof(Camera), allowSceneObjects: true) as Camera;
                m_TargetParent = EditorGUILayout.ObjectField("Model Position", m_TargetParent, typeof(Transform), allowSceneObjects: true) as Transform;

                EditorGUILayout.LabelField("Step 2");
                EditorGUILayout.LabelField("Batch Model's preview in folder :");
                EditorGUILayout.LabelField("Folder", s_RecentPath);
                using (new GUILayout.HorizontalScope())
                {
                    m_Filter.width = EditorGUILayout.IntField(nameof(FileFilter.width), m_Filter.width);
                    m_Filter.height = EditorGUILayout.IntField(nameof(FileFilter.height), m_Filter.height);
                }
                m_Filter.searchOption = (SearchOption)EditorGUILayout.EnumPopup(nameof(SearchOption), m_Filter.searchOption);
                m_Filter.existPreview = (eExistPreview)EditorGUILayout.EnumPopup(nameof(eExistPreview), m_Filter.existPreview);
                m_Filter.prefix = EditorGUILayout.TextField(nameof(FileFilter.prefix), m_Filter.prefix);
                m_Filter.suffix = EditorGUILayout.TextField(nameof(FileFilter.suffix), m_Filter.suffix);

                if (GUILayout.Button("Select Folder"))
                {
                    string tmp = EditorUtility.OpenFolderPanel("Choose Folder to start capture process", s_RecentPath, "");
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        s_RecentPath = tmp;
                        GenerateFolderModelPreviews(GetAssetPath(tmp));
                    }
                }

                EditorGUILayout.Space();

                if (GUILayout.Button("Capture Camera Preview"))
                {
                    if (m_Camera == null)
                    {
                        Debug.LogError("Required to assign Camera");
                        return;
                    }

                    string rawPath = EditorUtility.SaveFilePanelInProject(
                        "save test screen", "preview", "png",
                        "save image into", "Assets");
                    if (string.IsNullOrEmpty(rawPath))
                        return;

                    Texture2D preview = Capture(m_Camera, m_Filter.width, m_Filter.height);
                    byte[] bytes = preview.EncodeToPNG();

                    string path = Application.dataPath.Replace("Assets", "") + rawPath;
                    SaveBytesToPath(bytes, path);
                    AssetDatabase.Refresh(ImportAssetOptions.Default);
                }
            }
        }
        #endregion System

        public Camera m_Camera = null;
        public Transform m_TargetParent = null;
        public enum eExistPreview
        {
            Ignore = 0,
            Replace = 1,
        }

        public class FileFilter
        {
            public int width = 512;
            public int height = 512;
            public SearchOption searchOption = SearchOption.TopDirectoryOnly;

            public eExistPreview existPreview = eExistPreview.Ignore;
            public string prefix = "";
            public string suffix = "_preview";
        }
        public FileFilter m_Filter = new FileFilter();
        private const string s_AllPrefab = "*.prefab";
        private const string s_Png = ".png";

        public string s_RecentPath
        {
            get => EditorPrefs.GetString($"{nameof(ScreenCaptureHelper)}_RecentPath", Application.dataPath);
            set => EditorPrefs.SetString($"{nameof(ScreenCaptureHelper)}_RecentPath", value);
        }

        private void GenerateFolderModelPreviews(string rawPath)
        {
            if (string.IsNullOrEmpty(rawPath))
                return;
            else if (m_Camera == null)
            {
                Debug.LogError($"Please assign {nameof(m_Camera)} on step 1");
                return;
            }
            else if (m_TargetParent == null)
            {
                Debug.LogError($"Please assign {nameof(m_TargetParent)} on step 1");
                return;
            }
            else
            {
                DirectoryInfo dirInfo = new DirectoryInfo(rawPath);
                FileInfo[] fileInfos = dirInfo.GetFiles(s_AllPrefab, m_Filter.searchOption);
                Texture2D texture = null;
                foreach (var info in fileInfos)
                {
                    string dirPath = Path.GetDirectoryName(info.FullName);
                    string fileName = Path.GetFileNameWithoutExtension(info.FullName);
                    string previewFileName = $"{m_Filter.prefix}{fileName}{m_Filter.suffix}{s_Png}";
                    string previewFullPath = Path.Combine(dirPath, previewFileName);
                    // Debug.Log(previewFullPath);

                    string assetPath = GetAssetPath(info);
                    if (string.IsNullOrEmpty(assetPath))
                        throw new UnityException($"Fail to fetch asset path from {info.FullName}");
                    GameObject prefab = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
                    if (prefab == null)
                        throw new UnityException($"Fail to load asset from {assetPath}");
                    GameObject token = Instantiate(prefab, m_TargetParent);
                    if (token == null)
                        throw new UnityException($"Fail to clone asset from {prefab}");
                    token.transform.localPosition = Vector3.zero;
                    token.transform.localRotation = Quaternion.identity;

                    texture = Capture(m_Camera, m_Filter.width, m_Filter.height);
                    DestroyImmediate(token);
                    byte[] bytes = texture.EncodeToPNG();
                    SaveBytesToPath(bytes, previewFullPath);
                }
                if (texture != null)
                    DestroyImmediate(texture);
                AssetDatabase.Refresh(ImportAssetOptions.Default);
            }
        }

        private string GetAssetPath(FileInfo fileInfo)
        {
            return GetAssetPath(fileInfo.FullName);
        }
        private string GetAssetPath(string rawPath)
        {
            if (string.IsNullOrEmpty(rawPath))
                return string.Empty;
            string fullPath = rawPath.Replace(@"\", "/").TrimEnd('/');
            string assetPath = "Assets" + fullPath.Replace(Application.dataPath, "");
            return assetPath;
        }

        /// <summary>Capture screen shot via giving camera</summary>
        /// <param name="_camera"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="depth"> buffer in bits (0, 16, 24/32 are supported).
        /// <see cref="https://docs.unity3d.com/ScriptReference/RenderTexture-depth.html"/>
        /// </param>
        /// <param name="renderTextureFormat"></param>
        /// <returns></returns>
        private static Texture2D Capture(in Camera camera,
            int width, int height,
            int depth = 24,
            RenderTextureFormat renderTextureFormat = RenderTextureFormat.ARGB32,
            TextureFormat textureFormat = TextureFormat.ARGB32)
        {
            if (camera == null)
                throw new System.NullReferenceException();
            // Cache old stuff
            RenderTexture oldTT = camera.targetTexture;
            RenderTexture oldRT = RenderTexture.active;

            RenderTexture tmp = new RenderTexture(width, height, depth, renderTextureFormat);
            Texture2D preview = new Texture2D(width, height, textureFormat, false);
            try
            {
                // Prepare screen shot.
                camera.targetTexture = tmp;
                camera.Render();

                // Read Texture from RenderTexture.
                RenderTexture.active = tmp;
                preview.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0, false);
                preview.Apply();
            }
            catch
            {
                throw;
            }
            finally
            {
                // Clean up
                RenderTexture.active = oldRT;
                camera.targetTexture = oldTT;
                if (!Application.isPlaying)
                {
                    DestroyImmediate(tmp);
                }
            }
            return preview;
        }

        private bool SaveBytesToPath(byte[] bytes, string path)
        {
            // byte[] bytes = preview.EncodeToPNG();
            if (bytes == null || bytes.Length == 0)
            {
                Debug.LogError($"Unknown error, read nothing from texture.");
                return false;
            }

            if (File.Exists(path) && m_Filter.existPreview == eExistPreview.Ignore)
            {
                Debug.Log($"{path} found, ignore");
                return false;
            }

            Debug.Log($"Try save asset into {path}");
            File.WriteAllBytes(path, bytes);
            return true;
            // AssetDatabase.Refresh(ImportAssetOptions.Default);
        }
    }
}
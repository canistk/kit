using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    [CreateAssetMenu(fileName = "CharacterBuildConfig", menuName = "Kit/CharacterBuildConfig")]
    public class CharacterBuildConfig : ScriptableObject
    {
        [Header("Setting")]
        [SerializeField, AssetFolder("Model Import Folder", "", "Included sub-folder.")]
        string m_ImportFolder;
    }
}
// --------------------------------------------------------------------------------------------------------------------
// <author>
//   HiddenMonk
//   http://answers.unity3d.com/users/496850/hiddenmonk.html
//
//   Johannes Deml
//   send@johannesdeml.com
// </author>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Kit
{
	/// <summary>
	/// Extension class for SerializedProperties
	/// See also: http://answers.unity3d.com/questions/627090/convert-serializedproperty-to-custom-class.html
	/// </summary>
	public static class SerializedPropertyExtensions
	{
		const BindingFlags m_DefaultBindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

		/// <summary>
		/// Get the object the serialized property holds by using reflection
		/// </summary>
		/// <typeparam name="T">The object type that the property contains</typeparam>
		/// <param name="property"></param>
		/// <returns>Returns the object type T if it is the type the property actually contains</returns>
		public static T GetValue<T>(this SerializedProperty property)
		{
			MemberInfo ignored;
			return GetNestedObject<T>(property.propertyPath, GetSerializedPropertyRootComponent(property), false, out ignored);
		}

		public static T GetValue<T>(this SerializedProperty property, out MemberInfo memberInfo)
		{
			return GetNestedObject<T>(property.propertyPath, GetSerializedPropertyRootComponent(property), false, out memberInfo);
		}

		/// <summary>
		/// Set the value of a field of the property with the type T
		/// </summary>
		/// <typeparam name="T">The type of the field that is set</typeparam>
		/// <param name="property">The serialized property that should be set</param>
		/// <param name="value">The new value for the specified property</param>
		/// <returns>Returns if the operation was successful or failed</returns>
		public static bool SetValue<T>(this SerializedProperty property, T value)
		{
			MemberInfo ignored;

			object obj = GetSerializedPropertyRootComponent(property);
			//Iterate to parent object of the value, necessary if it is a nested object
			string[] fieldStructure = property.propertyPath.Split('.');
			for (int i = 0; i < fieldStructure.Length - 1; i++)
			{
				obj = GetFieldOrPropertyValue<object>(fieldStructure[i], obj, false, m_DefaultBindingFlags, out ignored);
			}
			string fieldName = fieldStructure.Last();

			return SetFieldOrPropertyValue(fieldName, obj, value);

		}

		/// <summary>
		/// Get the component of a serialized property
		/// </summary>
		/// <param name="property">The property that is part of the component</param>
		/// <returns>The root component of the property</returns>
		public static UnityEngine.Object GetSerializedPropertyRootComponent(SerializedProperty property)
		{
			return property.serializedObject.targetObject;
		}

		/// <summary>
		/// Iterates through objects to handle objects that are nested in the root object
		/// </summary>
		/// <typeparam name="T">The type of the nested object</typeparam>
		/// <param name="path">Path to the object through other properties e.g. PlayerInformation.Health</param>
		/// <param name="obj">The root object from which this path leads to the property</param>
		/// <param name="includeAllBases">Include base classes and interfaces as well</param>
		/// <returns>Returns the nested object casted to the type T</returns>
		public static T GetNestedObject<T>(string path, object obj, bool includeAllBases, out MemberInfo memberInfo)
		{
			memberInfo = null;
			var pathParts = path.Split('.');
			for (int i = 0; i < pathParts.Length; ++i)
			{
				Match regexMatch;
				if (
					i + 2 < pathParts.Length &&
					pathParts[i + 1] == "Array" &&
					(regexMatch = Regex.Match(pathParts[i + 2], @"data\[([0-9]+)\]")).Success)
				{
					int serializedIndex = int.Parse(regexMatch.Groups[1].Value);
					obj = GetFieldOrPropertyValue<object>(pathParts[i], obj, includeAllBases, serializedIndex, out memberInfo);
					i += 2;
				}
				else
				{
					obj = GetFieldOrPropertyValue<object>(pathParts[i], obj, includeAllBases, m_DefaultBindingFlags, out memberInfo);
				}
			}
			return (T)obj;
		}

		public static T GetFieldOrPropertyValue<T>(string fieldName, object obj, bool includeAllBases, BindingFlags bindings, out MemberInfo memberInfo)
		{
			memberInfo = null;
			FieldInfo field = obj.GetType().GetField(fieldName, bindings);
			if (field != null)
			{
				memberInfo = field;
				return (T)field.GetValue(obj);
			}

			PropertyInfo property = obj.GetType().GetProperty(fieldName, bindings);
			if (property != null)
			{
				memberInfo = property;
				return (T)property.GetValue(obj, null);
			}

			if (includeAllBases)
			{

				foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType()))
				{
					field = type.GetField(fieldName, bindings);
					if (field != null)
					{
						memberInfo = field;
						return (T)field.GetValue(obj);
					}

					property = type.GetProperty(fieldName, bindings);
					if (property != null)
					{
						memberInfo = property;
						return (T)property.GetValue(obj, null);
					}
				}
			}

			return default(T);
		}

		public static T GetFieldOrPropertyValue<T>(string fieldName, object obj, bool includeAllBases, int arrayIndex, out MemberInfo memberInfo)
		{
			memberInfo = null;

			FieldInfo field = obj.GetType().GetField(fieldName, m_DefaultBindingFlags);
			if (field != null)
			{
				memberInfo = field;
				return (T)((IList)field.GetValue(obj))[arrayIndex];
			}

			PropertyInfo property = obj.GetType().GetProperty(fieldName, m_DefaultBindingFlags);
			if (property != null)
			{
				memberInfo = property;
				return (T)property.GetValue(obj, new object[] { arrayIndex });
			}

			if (includeAllBases)
			{
				foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType()))
				{
					field = type.GetField(fieldName, m_DefaultBindingFlags);
					if (field != null)
					{
						memberInfo = field;
						return (T)((IList)field.GetValue(obj))[arrayIndex];
					}

					property = type.GetProperty(fieldName, m_DefaultBindingFlags);
					if (property != null)
					{
						memberInfo = property;
						return (T)property.GetValue(obj, new object[] { arrayIndex });
					}
				}
			}

			return default(T);
		}

		public static bool SetFieldOrPropertyValue(string fieldName, object obj, object value, bool includeAllBases = false, BindingFlags bindings = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
		{
			FieldInfo field = obj.GetType().GetField(fieldName, bindings);
			if (field != null)
			{
				field.SetValue(obj, value);
				return true;
			}

			PropertyInfo property = obj.GetType().GetProperty(fieldName, bindings);
			if (property != null)
			{
				property.SetValue(obj, value, null);
				return true;
			}

			if (includeAllBases)
			{
				foreach (Type type in GetBaseClassesAndInterfaces(obj.GetType()))
				{
					field = type.GetField(fieldName, bindings);
					if (field != null)
					{
						field.SetValue(obj, value);
						return true;
					}

					property = type.GetProperty(fieldName, bindings);
					if (property != null)
					{
						property.SetValue(obj, value, null);
						return true;
					}
				}
			}
			return false;
		}

		public static IEnumerable<Type> GetBaseClassesAndInterfaces(this Type type, bool includeSelf = false)
		{
			List<Type> allTypes = new List<Type>();

			if (includeSelf) allTypes.Add(type);

			if (type.BaseType == typeof(object))
			{
				allTypes.AddRange(type.GetInterfaces());
			}
			else
			{
				allTypes.AddRange(
						Enumerable
						.Repeat(type.BaseType, 1)
						.Concat(type.GetInterfaces())
						.Concat(type.BaseType.GetBaseClassesAndInterfaces())
						.Distinct());
			}

			return allTypes;
		}

		public static T SerializedPropertyToObject<T>(this SerializedProperty property)
		{
			return GetNestedObject<T>(property.propertyPath, GetSerializedPropertyRootComponent(property), true, out MemberInfo memberInfo); //The "true" means we will also check all base classes
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit
{
	public abstract class EditorWithSceneGUI : EditorBase
	{
		SceneViewHandler m_SceneGUI; 
		protected override void OnEnable()
		{
			base.OnEnable();
			SceneView.duringSceneGui += this._OnSceneGUI;
			m_SceneGUI = new SceneViewHandler();
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			SceneView.duringSceneGui -= this._OnSceneGUI;
			m_SceneGUI.Dispose();
		}

		private void _OnSceneGUI(SceneView view)
		{
			if (InternalEventIntercept(view))
				return;

			OnSceneGUI(view);
		}

		protected abstract void OnSceneGUI(SceneView view);

		protected virtual bool InternalEventIntercept(SceneView view)
		{
			Event evt = Event.current;
			int controlId = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
			int cmtCount = 0;
			if (evt.control) cmtCount++;
			if (evt.alt) cmtCount++;
			if (evt.shift) cmtCount++;
			int oldHotControl = GUIUtility.hotControl;

			bool locked = m_LockSceneView.Contains(this);
			if (evt.control && !locked)
			{
				m_LockSceneView.Add(this);
			}
			else if (!evt.control && locked)
			{
				m_LockSceneView.Remove(this);
			}

			KeyCode key = evt.keyCode;
			switch (evt.GetTypeForControl(controlId))
			{
				case EventType.KeyDown: FunctionKey(key, true); break;
				case EventType.KeyUp: FunctionKey(key, false); break;
				case EventType.MouseDown: MouseClick(evt.button, true, evt.mousePosition); break;
				case EventType.MouseUp: MouseClick(evt.button, false, evt.mousePosition); break;
				case EventType.Layout: LockControl(controlId); break;
			}

			return false;

			void LockControl(int id)
			{
				if (!IsLockedControl)
					return;
				// https://forum.unity.com/threads/solved-custom-editor-onscenegui-scripting.34137/
				// set current controlId as default control, so we wouldn't lose focus during click in scene view.
				HandleUtility.AddDefaultControl(id);
			}
		}

		protected virtual void FunctionKey(KeyCode key, bool isDown)
		{
			// Debug.Log($"{key}, isDown = {isDown}");
		}

		protected virtual void MouseClick(int mouseBtn, bool isDown, Vector2 mousePos)
		{
			// Debug.Log($"Mouse click {mouseBtn}, pos={mousePos:F2}, {(isDown?"Down":"Up")}.");
		}

		#region Lock Control
		static HashSet<object> m_LockSceneView = new HashSet<object>();
		protected bool IsLockedControl => m_LockSceneView.Count > 0;
		protected void LockControl(object who)
		{
			m_LockSceneView.Add(who);
		}
		protected bool ReleaseControl(object who)
		{
			return m_LockSceneView.Remove(who);
		}
		protected bool IsLockedBy(object who)
		{
			return m_LockSceneView.Contains(who);
		}
		protected bool IsLocked()
		{
			return m_LockSceneView.Count > 0;
		}
		#endregion Lock Control

		#region Draw Window
		[System.Obsolete("replace by EditorSceneGUI", true)]
		/// <summary>A panel can anchor based on U3D scene view.</summary>
		protected class Panel
		{
			public string title;
			public Rect screenRect;
			public enum eAnchor
			{
				Free = 0,
				LeftTop,
				LeftCenter,
				LeftBottom,
				MiddleTop,
				MiddleCenter,
				MiddleBottom,
				RightTop,
				RightCenter,
				RightBottom,
			}
			public eAnchor anchor;
			public delegate void DrawGUI(int id, Rect rect);
			public DrawGUI drawGUI;
			public Panel(eAnchor anchor, Rect screenRect, DrawGUI drawGUI, string title = "")
			{
				this.anchor = anchor;
				this.screenRect = screenRect;
				this.drawGUI = drawGUI;
				this.title = (title == null || title.Length == 0) ? string.Empty : title;
			}
		}

		[System.Obsolete("replace by EditorSceneGUI", true)]


		/// <summary>A drag able window can anchor based on U3D scene view.</summary>
		protected class Window : Panel
		{
			public Window(eAnchor anchor, Rect screenRect, DrawGUI drawGUI, string title = "")
				: base(anchor, screenRect, drawGUI, title)
			{ }

			public Window(eAnchor anchor, Vector2 offset, Vector2 size, DrawGUI drawGUI, string title = "")
				: base(anchor, new Rect(offset, size), drawGUI, title)
			{ }

			public Window(Rect screenRect, DrawGUI drawGUI, string title = "")
				: base(eAnchor.Free, screenRect, drawGUI, title)
			{ }

			public void DragableWindow(int id, Rect rect)
			{
				// A drag able title bar
				Rect dragArea = new Rect(0f, 0f, screenRect.width, 20f);
				GUI.DragWindow(dragArea);
				drawGUI?.Invoke(id, rect);
			}
		}

		/*
		private List<Panel> m_WindowList = new List<Panel>(2);
		/// <summary>Register a dragable window on sceneview.</summary>
		/// <param name="panels"></param>
		protected void Register(params Panel[] panels)
		{
			foreach (var w in panels)
			{
				if (w != null && !m_WindowList.Contains(w))
					m_WindowList.Add(w);
			}
		}
		protected void UnRegister(params Panel[] panels)
		{
			foreach (var w in panels)
				m_WindowList.Remove(w);
		}
		protected void DrawWindows(SceneView view)
		{
			for (int i = 0; i < m_WindowList.Count; ++i)
			{
				Panel w = m_WindowList[i];
				var rect = GetRect(w, view);
				var newRect = GUI.Window(i,
					rect,
					(id) => Draw(w, id, rect),
					w.title);
				SetRect(w, newRect, view);
			}
			Rect GetRect(Panel p, SceneView view)
			{
				float ox = p.screenRect.x;
				float oy = p.screenRect.y;
				float width = p.screenRect.width;
				float height = p.screenRect.height;
				float hw = width * 0.5f;
				float hh = height * 0.5f;
				float vw = view.position.width;
				float vh = view.position.height;
				float hvw = vw * 0.5f;
				float hvh = vh * 0.5f;

				float left = ox;
				float middle = (hvw - hw + ox);
				float right = (vw - width + ox);
				float top = oy;
				float center = (hvh - hh + oy);
				float bottom = (vh - height + oy);
				Rect rst = p.anchor switch
				{
					Panel.eAnchor.Free			=> p.screenRect,
					Panel.eAnchor.LeftTop		=> new Rect(left,	top,	width, height),
					Panel.eAnchor.LeftCenter	=> new Rect(left,	center,	width, height),
					Panel.eAnchor.LeftBottom	=> new Rect(left,	bottom,	width, height),
					Panel.eAnchor.MiddleTop	=> new Rect(middle,	top,	width, height),
					Panel.eAnchor.MiddleCenter	=> new Rect(middle,	center,	width, height),
					Panel.eAnchor.MiddleBottom => new Rect(middle,	bottom,	width, height),
					Panel.eAnchor.RightTop		=> new Rect(right,	top,	width, height),
					Panel.eAnchor.RightCenter	=> new Rect(right,	center,	width, height),
					Panel.eAnchor.RightBottom	=> new Rect(right,	bottom,	width, height),
					_ => throw new System.NotImplementedException(),
				};
				return rst;
			}
			void SetRect(Panel p, Rect v, SceneView view)
			{
				float ox = p.screenRect.x;
				float oy = p.screenRect.y;
				float width = p.screenRect.width;
				float height = p.screenRect.height;
				float hw = width * 0.5f;
				float hh = height * 0.5f;
				float vw = view.position.width;
				float vh = view.position.height;
				float hvw = vw * 0.5f;
				float hvh = vh * 0.5f;

				float left = ox;
				float middle = (v.x - hvw + hw);
				float right = (v.x - vw + width);
				float top = v.y;
				float center = (v.y - hvh + hh);
				float bottom = (v.y - vh + height);
				switch (p.anchor)
				{
					case Panel.eAnchor.Free:			p.screenRect = v; break;
					case Panel.eAnchor.LeftTop:			p.screenRect = new Rect(left,	top,	width, height); break;
					case Panel.eAnchor.LeftCenter:		p.screenRect = new Rect(left,	center,	width, height); break;
					case Panel.eAnchor.LeftBottom:		p.screenRect = new Rect(left,	bottom,	width, height); break;
					case Panel.eAnchor.MiddleTop:		p.screenRect = new Rect(middle,	top,	width, height); break;
					case Panel.eAnchor.MiddleCenter:	p.screenRect = new Rect(middle,	center,	width, height); break;
					case Panel.eAnchor.MiddleBottom:	p.screenRect = new Rect(middle,	bottom,	width, height); break;
					case Panel.eAnchor.RightTop:		p.screenRect = new Rect(right,	top,	width, height); break;
					case Panel.eAnchor.RightCenter:		p.screenRect = new Rect(right,	center,	width, height); break;
					case Panel.eAnchor.RightBottom:		p.screenRect = new Rect(right,	bottom,	width, height); break;
					default: throw new System.NotImplementedException();
				}
			}
			void Draw(Panel obj, int id, Rect rect)
			{
				if (obj == null)
					throw new System.Exception();
				if (obj is Window w)
				{
					w.DragableWindow(id, rect);
					return;
				}

				obj.drawGUI(id, rect);
			}
		}
		*/
		#endregion Draw Window
	}
}
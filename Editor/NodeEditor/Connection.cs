using System;
using UnityEditor;
using UnityEngine;
namespace Kit.Node
{
    public class Connection
    {
        public ConnectionPoint inPoint;
        public ConnectionPoint outPoint;

        public delegate void RemoveConnection(Connection removed);
        public static event RemoveConnection EVENT_Remove;
        
        public static Color s_lineColor = Color.white.CloneAlpha(0.3f);

        public Connection(ConnectionPoint inPoint, ConnectionPoint outPoint)
        {
            this.inPoint = inPoint;
            this.outPoint = outPoint;
        }

        public void Draw()
        {
            Handles.DrawBezier(
                inPoint.rect.center,
                outPoint.rect.center,
                inPoint.rect.center + Vector2.left * 50f,
                outPoint.rect.center - Vector2.left * 50f,
                s_lineColor,
                null,
                2f
            );

            if (Handles.Button((inPoint.rect.center + outPoint.rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
            {
                // OnClickRemoveConnection?.Invoke(this);
                EVENT_Remove?.Invoke(this);
            }
        }
    }
}
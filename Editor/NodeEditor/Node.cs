using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit.Node
{
    /// <summary>
    /// <see cref="https://gram.gs/gramlog/creating-node-based-editor-unity/"/>
    /// </summary>
    public class Node
    {
        public  Rect                rect;

        public  bool                isDragged { get; private set; }
        public  bool                isSelected { get; private set; }
        
        private NodeSkin            m_Skin;
        private GUIStyle            m_Style;

        public  ConnectionPoint     inPoint;
        public  ConnectionPoint     outPoint;

        public static event System.Action<Node> EVENT_NodeRemove;
        
        
        public Node(Vector2 position, float width, float height,
            NodeSkin skin)
        {
            rect        = new Rect(position.x, position.y, width, height);
            m_Skin      = skin;
            m_Style     = skin.defaultSkin;

            inPoint     = new ConnectionPoint(this, ConnectionPointType.In,     skin.inPointStyle);
            outPoint    = new ConnectionPoint(this, ConnectionPointType.Out,    skin.outPointStyle);
        }

        public void Drag(Vector2 delta)
        {
            rect.position += delta;
        }

        public void Draw()
        {
            inPoint.Draw();
            outPoint.Draw();
            GUI.Box(rect, string.Empty, m_Style);
        }

        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (rect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                            isSelected = true;
                            m_Style = m_Skin.selectedNode;
                        }
                        else
                        {
                            GUI.changed = true;
                            isSelected = false;
                            m_Style = m_Skin.defaultSkin;
                        }
                    }
                    if (e.button == 1 && isSelected && rect.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }
            return false;
        }

        private void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
            genericMenu.ShowAsContext();
        }

        private void OnClickRemoveNode()
        {
            EVENT_NodeRemove?.Invoke(this);
        }
    }
}
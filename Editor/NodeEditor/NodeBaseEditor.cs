using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kit.Node
{
    public class NodeSkin
    {
        public GUIStyle
            defaultSkin, btn, selectedNode,
            inPointStyle, outPointStyle;

        public NodeSkin()
        {
            defaultSkin = new GUIStyle();
            defaultSkin.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            defaultSkin.border = new RectOffset(12, 12, 12, 12);

            selectedNode = new GUIStyle();
            selectedNode.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
            selectedNode.border = new RectOffset(12, 12, 12, 12);

            btn = new GUIStyle();
            btn.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
            btn.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
            btn.border = new RectOffset(4, 4, 12, 12);

            inPointStyle = new GUIStyle();
            inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
            inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
            inPointStyle.border = new RectOffset(4, 4, 12, 12);

            outPointStyle = new GUIStyle();
            outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
            outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
            outPointStyle.border = new RectOffset(4, 4, 12, 12);
        }
    }

    /// <summary>
    /// <see cref="https://gram.gs/gramlog/creating-editor-windows-in-unity/"/>
    /// <see cref="https://gram.gs/gramlog/creating-clone-unitys-console-window/"/>
    /// <see cref="https://gram.gs/gramlog/creating-node-based-editor-unity/"/>
    /// </summary>
    public class NodeBaseEditor : EditorWindowBase
    {
        [MenuItem("Kit/Test/Node Base Editor")]
        private static void Init()
        {
            NodeBaseEditor window = GetWindow<NodeBaseEditor>();
            window.titleContent = new GUIContent("Node Base Editor");
        }

        private List<Node> m_Nodes = null;
        private List<Node> nodes
        {
            get
            {
                if (m_Nodes == null)
                {
                    m_Nodes = new List<Node>(8);
                }
                return m_Nodes;
            }
        }
        private List<Connection> m_Connections = null;
        private List<Connection> connections
        {
            get
            {
                if (m_Connections == null)
                {
                    m_Connections = new List<Connection>(8);
                }
                return m_Connections;
            }
        }

        private ConnectionPoint selectedInPoint, selectedOutPoint;
        private Vector2 offset, drag;

        private static Color s_SmallGridColor   = Color.gray.CloneAlpha(0.1f);
        private static Color s_BigGridColor     = Color.gray.CloneAlpha(0.4f);

        private void OnEnable()
        {
            Node.EVENT_NodeRemove           += RemoveNode;
            ConnectionPoint.EVENT_ClickedConnectionPoint += OnClickConnectionPoint;
            Connection.EVENT_Remove         += OnConnectionRemoved;
        }

        private void OnDisable()
        {
            Node.EVENT_NodeRemove           -= RemoveNode;
            ConnectionPoint.EVENT_ClickedConnectionPoint -= OnClickConnectionPoint;
            Connection.EVENT_Remove         -= OnConnectionRemoved;
        }

        private void OnGUI()
        {
            DrawGrid(20, 0.2f, s_SmallGridColor);
            DrawGrid(100, 0.4f, s_BigGridColor);

            DrawNodes();
            DrawConnections();

            DrawConnectionLine(Event.current);

            ProcessNodeEvents(Event.current);
            ProcessInputEvents(Event.current);

            if (GUI.changed) Repaint();
        }

        #region Grid
        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs   = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs  = Mathf.CeilToInt(position.height / gridSpacing);
            offset          += drag * 0.5f;

            Handles.BeginGUI();
            var old = Handles.color;
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; ++i)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; ++j)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = old;
            Handles.EndGUI();
        }

        private void DragGrid(Vector2 delta)
        {
            drag = delta;

            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; ++i)
                {
                    nodes[i].Drag(delta);
                }
            }

            GUI.changed = true;
        }
        #endregion Grid

        private void DrawNodes()
        {
            if (nodes == null)
                return;
            for (int i = 0; i < nodes.Count; ++i)
            {
                nodes[i].Draw();
            }
        }

        private void DrawConnections()
        {
            if (connections == null)
                return;

            for (int i = 0; i < connections.Count; ++i)
            {
                connections[i].Draw();
            }
        }

        private void DrawConnectionLine(Event e)
        {
            if (selectedInPoint != null && selectedOutPoint == null)
            {
                Handles.DrawBezier(
                    selectedInPoint.rect.center,
                    e.mousePosition,
                    selectedInPoint.rect.center + Vector2.left * 50f,
                    e.mousePosition - Vector2.left * 50f,
                    Connection.s_lineColor,
                    null,
                    2f
                );

                GUI.changed = true;
            }

            if (selectedOutPoint != null && selectedInPoint == null)
            {
                Handles.DrawBezier(
                    selectedOutPoint.rect.center,
                    e.mousePosition,
                    selectedOutPoint.rect.center - Vector2.left * 50f,
                    e.mousePosition + Vector2.left * 50f,
                    Connection.s_lineColor,
                    null,
                    2f
                );

                GUI.changed = true;
            }
        }

        private void ProcessInputEvents(Event e)
        {
            drag = Vector2.zero;
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        ClearConnectionSelection();
                    }
                    if (e.button == 1)
                    {
                        ProcessContextMenu(e.mousePosition);
                    }
                    break;
                case EventType.MouseDrag:
                    if (e.button == 0)
                    {
                        DragGrid(e.delta);
                    }
                    break;
            }
        }

        private void ProcessNodeEvents(Event e)
        {
            if (nodes == null)
                return;

            for (int i = nodes.Count - 1; i >= 0; --i)
            {
                bool guiChanged = nodes[i].ProcessEvents(e);
                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }

        private void ProcessContextMenu(Vector2 mousePosition)
        {
            var genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Add node"), false, () => OnClickAddNode(mousePosition));
            genericMenu.ShowAsContext();
        }


        private void OnClickAddNode(Vector2 mousePosition)
        {
            nodes.Add(new Node(mousePosition, 200, 50, new NodeSkin()));
        }

        private void OnClickConnectionPoint(ConnectionPoint cp)
        {
            switch (cp.type)
            {
                case ConnectionPointType.In: OnClickInPoint(cp); break;
                case ConnectionPointType.Out: OnClickOutPoint(cp); break;
                default: throw new System.NotImplementedException();
            }
        }

        private void OnClickInPoint(ConnectionPoint inPoint)
        {
            selectedInPoint = inPoint;
            if (selectedOutPoint == null)
                return;

            if (selectedOutPoint.node != selectedInPoint.node)
            {
                CreateConnection();
            }
            ClearConnectionSelection();
        }

        private void OnClickOutPoint(ConnectionPoint outPoint)
        {
            selectedOutPoint = outPoint;
            if (selectedInPoint == null)
                return;

            if (selectedOutPoint.node != selectedInPoint.node)
            {
                CreateConnection();
            }
            ClearConnectionSelection();
        }

        private void RemoveNode(Node node)
        {
            if (connections == null)
                return;

            int i = connections.Count;
            while (i --> 0)
            {
                if (connections[i].inPoint == node.inPoint ||
                    connections[i].outPoint == node.outPoint)
                {
                    connections.RemoveAt(i);
                }
            }

            nodes.Remove(node);
        }

        private void OnConnectionRemoved(Connection connection)
        {
            connections.Remove(connection);
        }

        private void CreateConnection()
        {
            connections.Add(new Connection(selectedInPoint, selectedOutPoint));
        }

        private void ClearConnectionSelection()
        {
            selectedInPoint = null;
            selectedOutPoint = null;
        }
    }
}
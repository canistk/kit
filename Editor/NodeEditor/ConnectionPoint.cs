using System;
using UnityEngine;
namespace Kit.Node
{
    public enum ConnectionPointType { In, Out }

    public class ConnectionPoint
    {
        public Rect rect;

        public readonly ConnectionPointType type;

        public readonly Node node;

        public GUIStyle style;

        public static event Action<ConnectionPoint> EVENT_ClickedConnectionPoint;

        public ConnectionPoint(Node node, ConnectionPointType type, GUIStyle style)
        {
            this.node = node;
            this.type = type;
            this.style = style;
            rect = new Rect(0, 0, 10f, 20f);
        }

        public void Draw()
        {
            rect.y = node.rect.y + (node.rect.height * 0.5f) - rect.height * 0.5f;

            switch (type)
            {
                case ConnectionPointType.In:
                    rect.x = node.rect.x - rect.width + 8f;
                    break;

                case ConnectionPointType.Out:
                    rect.x = node.rect.x + node.rect.width - 8f;
                    break;
            }

            if (GUI.Button(rect, "", style))
            {
                EVENT_ClickedConnectionPoint?.Invoke(this);
            }
        }
    }
}
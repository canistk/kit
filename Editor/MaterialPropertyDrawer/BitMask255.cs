using UnityEngine;
using UnityEditor;
using System;
using Kit;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Use with "[BitMask255]" before a float shader property.
public class BitMask255Drawer : MaterialPropertyDrawer
{
    // Draw the property inside the given rect
    public override void OnGUI(Rect position, MaterialProperty prop, String label, MaterialEditor editor)
    {
        const float w = 17f;
        const float intField = 50f;
        const float gap = 3f;
        // EditorGUI.showMixedValue = prop.hasMixedValue;
        using (var checker = new EditorGUI.ChangeCheckScope())
        {
            // int mask = prop.intValue
            int mask        = (editor.target as Material).GetInt(prop.name);
            var bodyWidth   = w * 10f + gap + intField;
            var rect        = position.SplitRight(bodyWidth);

            EditorGUI.LabelField(rect[0], label);
            var ch = rect[1].SplitHorizontalUnclamp(true, w, w, w, w, w, w, w, w, w, w, gap, intField);

            if (GUI.Button(ch[0], "-"))
                mask = 0;

            if (GUI.Button(ch[9], "A"))
                mask = 255;

            int value = 0;
            for (int i = 0; i < 8; ++i)
            {
                var offset  = 1 << i;
                var bit     = (mask & offset) != 0;
                if (GUI.Button(ch[i+1], (i+1).ToString(), bit ? GUI.skin.button : GUI.skin.label))
                {
                    bit = !bit;
                }
                if (bit)
                {
                    value |= offset;
                }
            }

            var finalValue = EditorGUI.IntField(ch[ch.Length - 1], value);
            value = Mathf.Clamp(finalValue, 0, 255);

            if (checker.changed)
            {
                // prop.intValue = value; // why it didn't work ?
                // Debug.Log($"BitMask255 change value = {mask} -> {value}");
                foreach (var obj in editor.targets)
                {
                    if (obj is not Material mat)
                        continue;

                    mat.SetInt(prop.name, value);
                    EditorUtility.SetDirty(mat);
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using System.Reflection;

namespace Kit
{
	[CustomPropertyDrawer(typeof(AnimatorStateSelector))]
	public class AnimatorStateSelectorDrawer : PropertyDrawer
	{
		static readonly float m_LineH = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		private RuntimeAnimatorController m_CacheController = null;
		private List<AnimatorStateCacheLite> m_AnimStateList = null;
		// private AnimatorStateSelector m_Self = null;
		private string[] pathsCache;
		private int[] str2hashs;

		SerializedProperty animProp = null;
		SerializedProperty selectProp = null;
		SerializedProperty layerIndexProp = null;
		SerializedProperty displayPathProp = null;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (animProp == null) animProp = property.FindPropertyRelative(nameof(AnimatorStateSelector.m_Animator));
			if (selectProp == null) selectProp = property.FindPropertyRelative(nameof(AnimatorStateSelector.m_SelectedHash));
			if (layerIndexProp == null) layerIndexProp = property.FindPropertyRelative(nameof(AnimatorStateSelector.m_LayerIndex));
			if (displayPathProp == null) displayPathProp = property.FindPropertyRelative(nameof(AnimatorStateSelector.m_DisplayPath));
			Animator animator = (Animator)animProp.objectReferenceValue;
			RuntimeAnimatorController runtimeController = animator ? animator.runtimeAnimatorController : null;

			Rect line = position.Clone(height: m_LineH);
			EditorGUI.PropertyField(line, animProp, true);
			line = line.GetRectBottom();

			if (animator != null && runtimeController != null)
			{
				UpdateController(animator.runtimeAnimatorController, property);
				if (m_CacheController is AnimatorController)
				{
					int val = selectProp.intValue;
					EditorGUI.BeginChangeCheck();
					val = EditorGUI.IntPopup(line, val, pathsCache, str2hashs);
					if (EditorGUI.EndChangeCheck())
					{
						selectProp.intValue = val;
						int index = -1, cnt = m_AnimStateList.Count;
						for (int i = 0; index == -1 && i < cnt; i++)
						{
							if (m_AnimStateList[i].fullNameHash == val)
							{
								index = i;
								layerIndexProp.intValue = m_AnimStateList[index].layer;
								displayPathProp.stringValue = m_AnimStateList[index].displayPath;
							}
						}
						// don't cache index, since animator will change afterward.
						// use hash result directly.
						Debug.Log($"Change to : Element[{index}], hash = {val}, Path={m_AnimStateList[index].displayPath}");//, Motion={m_AnimStateList[index].motion}", m_AnimStateList[index].motion);
						property.serializedObject.ApplyModifiedProperties();
					}
				}
				else
				{
					EditorGUI.HelpBox(line, "it's not a valid AnimatorController", MessageType.Error);
				}
			}
			else
			{
				EditorGUI.HelpBox(line, "Invalid Component", MessageType.Warning);
			}
			// 	base.OnGUI(position, property, label);
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return m_LineH * 2f; // base.GetPropertyHeight(property, label);
		}

		private void UpdateController(RuntimeAnimatorController controller, SerializedProperty property)
		{
			if (m_CacheController != controller || (pathsCache == null && str2hashs == null))
			{
				m_CacheController = controller;
				UpdateState(property);
			}
		}

		private void UpdateState(SerializedProperty property)
		{
			if (m_CacheController == null || !(m_CacheController is AnimatorController))
			{
				m_AnimStateList = null;
				return;
			}

			AnimatorController ac = (AnimatorController)m_CacheController;
			AnimatorExtend.Editor_BuildStateInfoList(ac, out m_AnimStateList);

			// reflection hack : put reference back into variables
			int cnt = m_AnimStateList.Count;
			pathsCache = new string[cnt];
			str2hashs = new int[cnt];
			for (int i = 0; i < cnt; i++)
			{
				pathsCache[i] = m_AnimStateList[i].displayPath;
				str2hashs[i] = m_AnimStateList[i].fullNameHash;
			}
		}
	}
}
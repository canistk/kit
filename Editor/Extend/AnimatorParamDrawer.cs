﻿using UnityEngine;
using UnityEditor;

namespace Kit
{
	[CustomPropertyDrawer(typeof(AnimatorParam), true)]
	public class AnimatorParamDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			EditorGUI.BeginChangeCheck();
			SerializedProperty paramProp = property.FindPropertyRelative(nameof(AnimatorParam.m_ParamString));
			SerializedProperty hashProp = property.FindPropertyRelative("m_ParamId");
			EditorGUI.DelayedTextField(position, paramProp, label); // new GUIContent(hashProp.intValue.ToString()));
			if (EditorGUI.EndChangeCheck())
			{
				hashProp.intValue = Animator.StringToHash(paramProp.stringValue);
			}
			EditorGUI.EndProperty();
		}
	}
}
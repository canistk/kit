﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace Kit
{
	[CustomPropertyDrawer(typeof(OnChangeAttribute))]
	public sealed class OnChangeDrawer : PropertyDrawer
	{
		OnChangeAttribute onChangeAttribute => (OnChangeAttribute)attribute;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			EditorGUI.BeginChangeCheck();
			EditorGUI.PropertyField(position, property, label, true);
			if (EditorGUI.EndChangeCheck())
			{
				try
				{
					Type type = property.serializedObject.targetObject.GetType();
					MethodInfo methodInfo = type.GetMethod(onChangeAttribute.callbackMethodName);
					if (methodInfo != null)
						methodInfo.Invoke(property.serializedObject.targetObject, null);
					else if (property.isArray)
					{
						Debug.LogError($"Type = {type.Name}, Arrary detected, fail to locate {onChangeAttribute.callbackMethodName} are those the right path ?");
					}
					else
						Debug.LogError($"Type = {type.Name} ~ MethodInfo {onChangeAttribute.callbackMethodName} not found.");
				}
				catch
				{
					throw new NullReferenceException($"That method {onChangeAttribute.callbackMethodName}() not exist.");
				}
			}
			EditorGUI.EndProperty();
		}
	}
}
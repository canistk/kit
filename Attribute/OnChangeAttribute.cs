﻿using UnityEngine;

namespace Kit
{
	public class OnChangeAttribute : PropertyAttribute
	{
		public readonly string callbackMethodName;
		public OnChangeAttribute(string callbackMethodName)
		{
			this.callbackMethodName = callbackMethodName;
		}
	}
}
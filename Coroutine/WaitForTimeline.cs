using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Kit
{
	public class WaitForTimeline : CustomYieldInstruction
	{
		public override bool keepWaiting => m_Waiting;

		private bool m_Waiting = true;

		private PlayableDirector m_Director = null;
		public WaitForTimeline(PlayableDirector director, bool autoPlay)
		{
			if (director == null)
				throw new System.NullReferenceException();
			m_Director = director;
			director.stopped += OnTimelineCompleted;
			m_Waiting = true;

			// auto play when it's not.
			if (autoPlay)
			{
				switch (m_Director.state)
				{
					// case PlayState.Delayed:
					case PlayState.Paused:
						m_Director.Play();
						break;
					case PlayState.Playing:
						break;
					default: throw new System.NotImplementedException();
				}
			}
		}

		private void OnTimelineCompleted(PlayableDirector obj)
		{
			m_Waiting = false;
		}
	}
}
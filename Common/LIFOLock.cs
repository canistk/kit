﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	// TODO: this is weight mapping instead of "LIFOLock"
	// TODO: remove and reimplement
    [System.Obsolete("This didn't service purpose, plan to remove.")]
	public class LIFOLock<T, RST>
	{
		private readonly bool strict;

		/// <summary>To define the default value of this locker</summary>
		public RST defaultValue { get; private set; }

		/// <summary>the value current in used.</summary>
		public RST value;

		/// <summary>To cache the register order of <see cref="T"/></summary>
		private List<T> queue = new List<T>();

		/// <summary>The binding of the <see cref="T"/> & <see cref="RST"/></summary>
		private Dictionary<T, RST> mapping = new Dictionary<T, RST>();

		/// <summary>Trigger eveny time when value was changed</summary>
		public event System.Action<RST> Changed;

		public bool IsModified => queue.Count > 0;

		public LIFOLock(RST value, bool strict = true)
		{
			defaultValue = value;
			this.value = value;
			this.strict = strict;
		}

		public bool AquireLock(T caller, RST value)
		{
			if (mapping.ContainsKey(caller)) // already exist, update instead of new lock
			{
				if (!mapping[caller].Equals(value)) // is value changed ?
				{
					mapping[caller] = value;
					if (caller.Equals(queue[queue.Count - 1])) // only dispatch when the value was changed.
					{
						// modify the one in used.
						this.value = value;
						Changed?.Invoke(this.value);
						return true;
					}
				}
			}
			else
			{
				queue.Add(caller);
				mapping.Add(caller, value);
				this.value = value;
				Changed?.Invoke(this.value);
				return true;
			}
			return false;
		}
		public bool ReleaseLock(T caller)
		{
			if (mapping.ContainsKey(caller))
			{
				queue.Remove(caller);
				mapping.Remove(caller);

				if (queue.Count > 0)
				{
					T pt = queue[queue.Count - 1];
					this.value = mapping[pt];
				}
				else
				{
					this.value = defaultValue;
				}
				Changed?.Invoke(this.value);
				return true;
			}
			else if (strict)
			{
				throw new UnityException($"{GetType().Name} : not exist target, attempt to remove {caller.ToString()}");
			}
			return false;
		}
		public void SetDefaultValue(RST value)
		{
			defaultValue = value;
			if (queue.Count == 0)
			{
				// Only trigger change event, when it's no other locker.
				this.value = value;
				Changed?.Invoke(value);
			}
		}
		public void Clear()
		{
			if (queue.Count > 0)
			{
				queue.Clear();
				mapping.Clear();
				this.value = defaultValue;
				Changed?.Invoke(value);
			}
		}
		public override string ToString()
		{
			return queue.Count > 0 ?
				$"[{GetType().Name} : Modified, Locked by {queue[queue.Count - 1]} with value = {value.ToString()}, lock count = {queue.Count}]" :
				$"[{GetType().Name} : Unchanged, default value = {value.ToString()}";
		}
	}
}
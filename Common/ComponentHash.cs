using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    /// <summary>
    /// Register a group of Component, and provide API to search them based on condition.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ComponentHash<T> where T : Component
    {
        private HashSet<T> m_Entities       = new HashSet<T>();
        public IEnumerable<T> entities      => m_Entities;
        public void Register(T entity)      => m_Entities.Add(entity);
        public bool Unregister(T entity)    => m_Entities.Remove(entity);

        public bool TryGetEntity<COMP>(out COMP target, System.Func<COMP, bool> condition = null)
            where COMP : T
        {
            foreach (var entity in m_Entities)
            {
                if (entity is COMP obj &&
                    (condition == null || condition.Invoke(obj)))
                {
                    target = obj;
                    return obj != null;
                }
            }
            target = default(COMP);
            return false;
        }
        public IEnumerable<COMP> TryGetEntities<COMP>(System.Func<COMP, bool> condition = null)
            where COMP : T
        {
            foreach (var entity in m_Entities)
            {
                if (entity is COMP obj &&
                    (condition == null || condition.Invoke(obj)))
                    yield return obj;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Kit
{
	[System.Serializable]
	public class TagParam
	{
		const string Untagged = "Untagged";
		[TagField] public string m_Tag = Untagged;
		[Tooltip("None = All")] public LayerMask m_Layer = ~Physics.AllLayers;
		[Tooltip("Empty = Any")] public string m_NameContain = "";
		public bool m_DisableAsLost = true;
		public TagParam(string tag, LayerMask layer, string nameContain = "", bool disableAsLost = true)
		{
			m_Tag = tag;
			m_Layer = layer;
			m_NameContain = nameContain;
			m_DisableAsLost = disableAsLost;
		}

		public override bool Equals(object obj)
		{
			var param = obj as TagParam;
			return param != null &&
				   m_Tag == param.m_Tag &&
				   EqualityComparer<LayerMask>.Default.Equals(m_Layer, param.m_Layer) &&
				   m_NameContain == param.m_NameContain &&
				   m_DisableAsLost == param.m_DisableAsLost;
		}

		public override int GetHashCode()
		{
			var hashCode = -968944216;
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(m_Tag);
			hashCode = hashCode * -1521134295 + EqualityComparer<LayerMask>.Default.GetHashCode(m_Layer);
			hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(m_NameContain);
			hashCode = hashCode * -1521134295 + m_DisableAsLost.GetHashCode();
			return hashCode;
		}

		public override string ToString()
		{
			if (!string.IsNullOrEmpty(m_NameContain))
				return m_NameContain;
			else if (m_Tag != Untagged && !string.IsNullOrEmpty(m_Tag))
				return "Tab:" + m_Tag;
			else
				return "Unknow";
			//return base.ToString();
		}
	}

	public class TagLocator : MonoBehaviour
	{
		[SerializeField] float m_SearchInterval = 0.3f;

		[Header("Condition")]
		[SerializeField, TagField] string m_Tag = "Untagged";
		[SerializeField, Tooltip("None = All")] LayerMask m_Layer = ~Physics.AllLayers;
		[SerializeField, Tooltip("Empty = Any")] string m_NameContain = "";

		[Header("Disable Condition (Optional)")]
		[SerializeField] bool m_DisableAsLost = true;

		[System.Serializable] public class LocatedEvent : UnityEvent<Transform> { }
		public LocatedEvent EVENT_Located = new LocatedEvent();
		public UnityEvent EVENT_Lost = new UnityEvent();
		private Transform m_Target = null;
		public Transform target { get { return m_Target; }
			private set {
				if (value == null)
					TargetLost();
				m_Target = value;
			} }
		public System.Func<GameObject, bool> extraCondition { get; set; } = null;

		private void OnEnable()
		{
			StartCoroutine(LocateTarget());
		}

		public void Set(TagParam param)
		{
			if (target != null)
				target = null;
			m_Tag = param.m_Tag;
			m_Layer = param.m_Layer;
			m_NameContain = param.m_NameContain;
			m_DisableAsLost = param.m_DisableAsLost;
		}

		private IEnumerator LocateTarget()
		{
			while (gameObject.activeInHierarchy) // we still active in scene.
			{
				if (target == null) // and target lost.
				{
					GameObject[] arr = GameObject.FindGameObjectsWithTag(m_Tag);
					int cnt = arr.Length;
					for (int i = 0; i < cnt; i++)
					{
						if (CheckCondition(arr[i]))
						{
							target = arr[i].transform;
							OneShotEvent handler = OneShotEvent.GetOrAdd(target);
							if (m_DisableAsLost)
								handler.EVENT_OnDisable += TargetLost;
							else
								handler.EVENT_OnDestroy += TargetLost;
							EVENT_Located.Invoke(target);
							yield break;
						}
					}
				}
				yield return new WaitForSeconds(m_SearchInterval);
			}
		}

		private void TargetLost()
		{
			// when this being trigger, the gameobject actually still on the scene.
			// we MUST remove current target, and try again on next frame.
			if (target != null)
				m_Target = null; // directly remove target, avoid dead loop

			EVENT_Lost.Invoke();

			// Importart for disable, to skip this frame.
			// to not locate the same object if it's disabling this frame.
			// - because still exist in GameObject.Find() result.
			// also ensure to resume the searching.
			if (gameObject.activeInHierarchy)
				Invoke("LocateTarget", 0f); // an async fucntion called on next frame.
		}

		private bool CheckCondition(GameObject go)
		{
			// assume go is active (that's how can it be found)
			bool containString = string.IsNullOrWhiteSpace(m_NameContain) || go.name.Contains(m_NameContain);
			bool layer = m_Layer == ~Physics.AllLayers || IsInLayerMask(go.layer, m_Layer);
			bool extra = extraCondition == null ? true : extraCondition(go);
			return containString && layer && extra;
		}

		private bool IsInLayerMask(int layer, LayerMask layermask)
		{
			return layermask == (layermask | (1 << layer));
		}

		private bool IsTargetEnable()
		{
			return target != null && m_DisableAsLost && target.gameObject.activeInHierarchy;
		}
	}
}
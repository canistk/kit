﻿using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	public class OneShotEvent : MonoBehaviour
	{
		public static OneShotEvent GetOrAdd(Transform _transform)
		{
			return _transform.GetOrAddComponent<OneShotEvent>();
		}

		private List<Callback> m_EnableList;
		public event Callback EVENT_OnEnable
		{
			add { m_EnableList.Add(value); }
			remove { m_EnableList.Remove(value); }
		}

		private List<Callback> m_DisableList;
		public event Callback EVENT_OnDisable
		{
			add { m_DisableList.Add(value); }
			remove { m_DisableList.Remove(value); }
		}

		private List<Callback> m_DestroyList;
		public event Callback EVENT_OnDestroy
		{
			add { m_DestroyList.Add(value); }
			remove { m_DestroyList.Remove(value); }
		}

		OneShotEvent()
		{
			m_EnableList = new List<Callback>();
			m_DisableList = new List<Callback>();
			m_DestroyList = new List<Callback>();
		}

		~OneShotEvent()
		{
			m_EnableList.Clear();
			m_DisableList.Clear();
			m_DestroyList.Clear();
		}

		private void OnEnable()
		{
			Dispatch(m_EnableList);
		}

		private void OnDisable()
		{
			Dispatch(m_DisableList);
		}

		private void OnDestroy()
		{
			Dispatch(m_DestroyList);
		}

		private void Dispatch(List<Callback> list)
		{
			int i = list.Count;
			while (i-- > 0)
			{
				// to ignore the Object Destroyed or Disposing
				if (!list[i].Equals(null) &&
					list[i].Target is UnityEngine.Object &&
					!list[i].Target.Equals(null))
					list[i]();
				list.RemoveAt(i);
			}
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;

public class DebugTimeScale : MonoBehaviour
{
    private static float[] timeScaleArrary = { 10f, 5f, 2f, 1f, 0.5f, 0.25f, 0.1f, 0f };
    private const int normalPt = 3;
    private int pt = normalPt;
    [SerializeField, ReadOnly] float m_CurrentTimeScale = 1f;
    [SerializeField] int m_TargetFrameRate = 60;

    private void Awake()
    {
        float timeScale = Time.timeScale;
        for (int i = 0; i < timeScaleArrary.Length; i++)
        {
            if (timeScale.EqualRoughly(timeScaleArrary[i], 0.1f))
            {
                SetTimeScale(i);
                return;
            }
        }
        SetTimeScale(normalPt);
        Application.targetFrameRate = m_TargetFrameRate;
    }
    void Update()
    {
        bool alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
        if (alt)
        {
            bool minus = Input.GetKeyDown(KeyCode.Minus) || Input.GetKeyDown(KeyCode.KeypadMinus);
            bool plus = Input.GetKeyDown(KeyCode.Equals) || Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Plus);
            bool zero = Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0);
            if (zero)
            {
                SetTimeScale(normalPt);
            }
            else if (minus == plus)
            {
                return;
            }
            else if (minus)
            {
                SetTimeScale(pt + 1);
            }
            else if (plus)
            {
                SetTimeScale(pt - 1);
            }
        }
    }

    private void SetTimeScale(int nextPt)
    {
        pt = Mathf.Clamp(nextPt, 0, timeScaleArrary.Length - 1);
        m_CurrentTimeScale = Time.timeScale = timeScaleArrary[pt];
        Debug.Log($"Change TimeScale : {Time.timeScale}", this);
    }
}

﻿using UnityEngine;

namespace Kit
{
	public static class KitBox
	{
		/// <summary>
		/// Since U3D team keep adding state between play mode & non-play mode.
		/// this mode are used for doing/prevent things try to execute between "play" & "edit" mode.
		/// </summary>
		[System.Obsolete("hidden editor stage, due to U3D update API.")]
		public static bool IsEditorMode => !isPlayingOrWillChangePlaymode && !Application.isPlaying;

		/// <summary>
		/// A fucking over kill to prevent Unity fall into the gap
		/// between playmode & editormode.
		/// </summary>
		public static bool IsEditorModeNonCompiling =>
			Application.isEditor && // only open project in unity editor.
			!Application.isPlaying && // not in play mode
			!isPlayingOrWillChangePlaymode && // not changing play mode. (the moment right after press "play" button in editor)
			!isUpdating && // not updating asset database.
			!isCompiling; // not re-compiling script (after detect assets update)
						  // ...to be continue, after U3D updated more API

		/// <summary>
		/// There is a gap between playMode & not play mode.
		/// </summary>
		[System.Obsolete("Wrong implement, can't detect gap, after U3D add more stage.")]
		public static bool IsGapOrPlayMode => isPlayingOrWillChangePlaymode || Application.isPlaying;

		public static bool IsEditorModeValueChanged(this MonoBehaviour monoBehaviour)
		{
			return monoBehaviour.IsSceneObject() && IsEditorModeNonCompiling && IsDirty(monoBehaviour);
		}

		public static bool IsSceneObject(this MonoBehaviour monoBehaviour) =>
#if UNITY_EDITOR
			monoBehaviour?.gameObject?.scene.rootCount != 0;
			// monoBehaviour.transform.root != null;
#else
			true;
#endif

		public static bool IsDirty(Object obj) =>
#if UNITY_EDITOR
			UnityEditor.EditorUtility.IsDirty(obj);
#else
			false;
#endif

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public static void Editor_SetDirty(Object obj)
        {
#if UNITY_EDITOR
			if (!Application.isPlaying)
				UnityEditor.EditorUtility.SetDirty(obj);
#endif
		}

		private static bool isPlayingOrWillChangePlaymode =>
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode;
#else
			Application.isPlaying;
#endif

		private static bool isCompiling =>
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isCompiling;
#else
		false;
#endif

		private static bool isUpdating =>
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isUpdating;
#else
		false;
#endif
	}
}
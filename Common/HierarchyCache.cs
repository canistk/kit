﻿using UnityEngine;

namespace Kit
{
	public class HierarchyCache
	{
		public readonly Transform node;
		public readonly Transform parent;
		public readonly Vector3 localPosition;
		public readonly Quaternion localRotation;
		public HierarchyCache(Transform node)
		{
			this.node = node;
			parent = node.parent;
			localPosition = node.localPosition;
			localRotation = node.localRotation;
		}
		public void Reset()
		{
			if (node.parent != parent)
				node.SetParent(parent, false);
			node.localPosition = localPosition;
			node.localRotation = localRotation;
		}
	}
}
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Kit.Test
{
    public class TimerLockTest : MonoBehaviour
    {
        TimeKeeper m_Keeper = new TimeKeeper();
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                string result = RandomExtend.String(8);
                m_Keeper.CreateOrExtend(1f, true, () => {
                    Debug.Log($"Keeper expired : {result}");
                });
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                new GameTimeLock(1f, () => { Debug.Log("GameTime 1"); });
                new GameTimeLock(2f, () => { Debug.Log("GameTime 2"); });
                new GameTimeLock(3f, () => { Debug.Log("GameTime 3"); });
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                new RealTimeLock(1f, () => { Debug.Log("RealTime 1"); });
                new RealTimeLock(2f, () => { Debug.Log("RealTime 2"); });
                new RealTimeLock(3f, () => { Debug.Log("RealTime 3"); });
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Time.timeScale = Mathf.Clamp(Time.timeScale * 2f, 0f, 10f);
                Debug.Log($"TimeScale = {Time.timeScale:F2}");
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Time.timeScale = Mathf.Clamp01(Time.timeScale * 0.5f);
                Debug.Log($"TimeScale = {Time.timeScale:F2}");
            }

            if (Input.GetKeyDown(KeyCode.Keypad0))
            {
                Time.timeScale = 1f;
                Debug.Log($"TimeScale = {Time.timeScale:F2}");
            }
        }
    }
}
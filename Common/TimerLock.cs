using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class TimeKeeper : IDisposable
    {
        public TimeKeeper()
        {
            m_Callbacks = new List<Callback>(8);
            m_Dict = new Dictionary<System.Type, TimerLock>(8);
            UpdateManager.instance.Register(Update);
        }

        #region Public API
        public event System.Action EVENT_Expired;
        private Dictionary<System.Type, TimerLock> m_Dict;
        public List<Callback> m_Callbacks;

        /// <summary>Create or extend current timer</summary>
        /// <param name="duration"></param>
        /// <param name="ignoreTimeScale"></param>
        /// <param name="expireCallback"></param>
        public void CreateOrExtend(float duration, bool ignoreTimeScale, Callback expireCallback = null)
        {
            if (ignoreTimeScale)
            {
                TryExtend<RealTimeLock>(duration);
            }
            else
            {
                TryExtend<GameTimeLock>(duration);
            }
            if (expireCallback != null)
                m_Callbacks.Add(expireCallback);
        }
        #endregion Public API

        #region Core
        private void Update()
        {
            if (!IsAllExpired())
                return;

            EVENT_Expired.TryCatchDispatchEventError(evt => evt.Invoke());
            TriggerCallbackOnce();
            m_Dict.Clear();
        }

        private bool IsAllExpired()
        {
            foreach (var timer in m_Dict.Values)
            {
                if (timer == null)
                    continue;
                if (!timer.IsExpired())
                    return false;
            }
            return true;
        }

        private void TriggerCallbackOnce()
        {
            if (m_Callbacks.Count == 0)
                return;

            foreach (var callback in m_Callbacks)
            {
                if (callback == null)
                    continue;
                try
                {
                    callback.Invoke();
                }
                catch (System.Exception ex)
                {
                    ex.DeepLogInvocationException($"{nameof(TimerLock)}.{nameof(TriggerCallbackOnce)}");
                }
            }
            m_Callbacks.Clear();
        }

        private void TryExtend<T>(float duration) where T : TimerLock
        {
            bool lockExistAndDurationInRange =
                    m_Dict.TryGetValue(typeof(T), out var priorLock) &&
                    !priorLock.IsExpired() &&
                    priorLock.TryExtend(duration);
            if (!lockExistAndDurationInRange)
            {
                m_Dict[typeof(T)] = TimerLock.Create<T>(duration);
            }
        }
        #endregion Core

        #region Dispose
        private bool IsDisposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    m_Dict.Clear();
                }

                UpdateManager.instance.Deregister(Update);
                m_Dict = null;
                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                IsDisposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        ~TimeKeeper()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }

    public abstract class TimerLock : IDisposable
    {
        private         bool    isDisposed = false;
        public          double  endTime { get; protected set; }
        public abstract double  now { get; }
        private System.Action m_ExpireCallback;

        #region Construct
        public TimerLock(float duration, System.Action expireCallback)
        {
            UpdateManager.instance.Register(Update);
            endTime = duration + now;
            m_ExpireCallback = expireCallback;
        }
        ~TimerLock()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }
        #endregion Construct

        private void Update()
        {
            if (isDisposed)
                return;

            if (!IsExpired())
                return;

            if (m_ExpireCallback != null)
                m_ExpireCallback.Invoke();
            Dispose();
        }

        public bool IsExpired()     => now >= endTime;

        public void ForceExpire()   => endTime = 0f;
        
        public bool TryExtend(float duration)
        {
            var rst = now + duration;
            if (rst > endTime)
            {
                endTime = rst;
                return true;
            }
            return false;
        }

        public static T Create<T>(float duration) where T : TimerLock
        {
            return (T)Activator.CreateInstance(typeof(T), new object[] { duration, null });
        }

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                UpdateManager.instance.Deregister(Update);
                isDisposed = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }

    public class GameTimeLock : TimerLock
    {
        public override double now => Time.timeSinceLevelLoadAsDouble;
        public GameTimeLock(float duration, System.Action expireCallback = null) : base(duration, expireCallback) { }
    }

    public class RealTimeLock : TimerLock
    {
        public override double now => Time.realtimeSinceStartupAsDouble;
        public RealTimeLock(float duration, System.Action expireCallback = null) : base(duration, expireCallback) { }
    }
}
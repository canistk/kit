using System;
using System.Collections;
using System.Collections.Generic;
namespace Kit
{
    public static class Sorting
    {
        /*  Example :
         *  int[] arr = { 6, 5, 3, 1, 8, 7, 2, 4 };
         *  Sorting.QuickSort(ref arr, 0, arr.Length - 1);
         *  Debug.Log(string.Join(", ", arr));
         */


        /// <summary>Quick Sort, without exchange giving arr,
        /// but return a index array that based on comparer methods.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="comparer"></param>
        public static void QuickSort<T>(T[] array, IComparer<T> comparer, out int[] resultIndex)
        {
            resultIndex = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                resultIndex[i] = i;
            }

            _Sort(array, ref resultIndex, 0, array.Length - 1, comparer);
            return;

            void _Sort(T[] array, ref int[] resultIndex, int low, int high, IComparer<T> comparer)
            {
                if (low >= high)
                    return;

                int partitionIndex = _Partition(array, ref resultIndex, low, high, comparer);
                
                _Sort(array, ref resultIndex, low, partitionIndex - 1, comparer);
                _Sort(array, ref resultIndex, partitionIndex + 1, high, comparer);
            }

            int _Partition(T[] array, ref int[] resultIndex, int low, int high, IComparer<T> comparer)
            {
                T pivot = array[resultIndex[high]];
                int i = low - 1;

                for (int j = low; j < high; ++j)
                {
                    if (comparer.Compare(array[resultIndex[j]], pivot) <= 0)
                    {
                        i++;
                        Swap(ref resultIndex[i], ref resultIndex[j]);
                    }
                }

                Swap(ref resultIndex[i + 1], ref resultIndex[high]);
                return i + 1;
            }
        }

        /// <summary>Quick Sort, without exchange giving arr
        /// but return a index array based on there default comparable methods.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="resultIndex"></param>
        public static void QuickSort<T>(T[] array, out int[] resultIndex)
            where T : IComparable<T>
        {
            resultIndex = new int[array.Length];
            for (int i = 0; i < array.Length; ++i)
            {
                resultIndex[i] = i;
            }

            _Sort(array, ref resultIndex, 0, array.Length - 1);

            return;

            void _Sort(T[] array, ref int[] resultIndex, int low, int high)
            {
                if (low >= high)
                    return;
                
                int partitionIndex = _Partition(array, ref resultIndex, low, high);

                _Sort(array, ref resultIndex, low, partitionIndex - 1);
                _Sort(array, ref resultIndex, partitionIndex + 1, high);
            }

            int _Partition(T[] array, ref int[] resultIndex, int low, int high)
            {
                T pivot = array[resultIndex[high]];
                int i = low - 1;

                for (int j = low; j < high; j++)
                {
                    if (array[resultIndex[j]].CompareTo(pivot) <= 0)
                    {
                        i++;
                        Swap(ref resultIndex[i], ref resultIndex[j]);
                    }
                }

                Swap(ref resultIndex[i + 1], ref resultIndex[high]);
                return i + 1;
            }
        }

        public static void QuickSort<T>(ref T[] array, int low, int high, in IComparer<T> comparer)
        {
            if (comparer == null)
                throw new System.NullReferenceException();
            if (low >= high)
                return;

            int partitionIndex = Partition(ref array, low, high, comparer);

            QuickSort(ref array, low, partitionIndex - 1, comparer);
            QuickSort(ref array, partitionIndex + 1, high, comparer);
            int Partition<TT>(ref TT[] array, int low, int high, in IComparer<TT> comparer)
            {
                TT pivot = array[high];
                int i = low - 1;

                for (int j = low; j < high; ++j)
                {
                    if (comparer.Compare(array[j], pivot) < 0)
                    {
                        ++i;
                        Swap(ref array[i], ref array[j]);
                    }
                }

                Swap(ref array[i + 1], ref array[high]);
                return i + 1;
            }
        }

        public static void QuickSort<T>(ref T[] array, int low, int high)
            where T : IComparable<T>
        {
            if (low >= high || low < 0)
                return;

            // Partition array and get the pivot index
            int partitionIndex = Partition(ref array, low, high);

            // Sort the two partitions
            QuickSort(ref array, low, partitionIndex - 1);  // Left side of pivot
            QuickSort(ref array, partitionIndex + 1, high); // Right side of pivot
            int Partition<TT>(ref TT[] array, int low, int high)
                where TT : IComparable<TT>
            {
                TT pivot = array[high];
                int i = low - 1;

                for (int j = low; j < high; ++j)
                {
                    if (array[j].CompareTo(pivot) <= 0)
                    {
                        ++i;
                        Swap(ref array[i], ref array[j]);
                    }
                }

                Swap(ref array[i + 1], ref array[high]);
                return i + 1;
            }
        }

        private static void Swap<T>(ref T x, ref T y)
        {
            T temp = x;
            x = y;
            y = temp;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kit.BehaviorTree.Core
{
    /// <summary>
    /// <see cref="https://gram.gs/gramlog/creating-editor-windows-in-unity/"/>
    /// <see cref="https://gram.gs/gramlog/creating-clone-unitys-console-window/"/>
    /// <see cref="https://gram.gs/gramlog/creating-node-based-editor-unity/"/>
    /// </summary>
    public class BehaviorTreeEditor : EditorWindowBase
    {
        [MenuItem("Kit/Behavior Tree/Editor")]
        private static void Init()
        {
            instance.Focus();
        }

        public static BehaviorTreeEditor m_Instance = null;
        public static BehaviorTreeEditor instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = GetWindow<BehaviorTreeEditor>();
                    m_Instance.titleContent = new GUIContent("Behavior Tree Editor");
                    m_Instance.minSize = new Vector2(420f, 320f);
                    
                }
                return m_Instance;
            }
        }

        public BTBrain currentBrain { get; private set; } = null;
        public BTDecisionGraph decisionGraph => currentBrain?.decisionGraph;
        public void Observe(BTBrain brain)
        {
            this.currentBrain = brain;
        }
        void OnInspectorUpdate()
        {
            Repaint();
        }
        private List<BTNodeBase> nodes
        {
            get
            {
                if (decisionGraph == null)
                    return null;
                return decisionGraph.nodes;
            }
        }

        private Vector2 offset, drag;

        private static Color s_SmallGridColor = Color.gray.CloneAlpha(0.1f);
        private static Color s_BigGridColor = Color.gray.CloneAlpha(0.4f);

        private void OnEnable()
        {
            BTNodeBase.EVENT_NodeRemove     += OnBeforeRemoveNode;
            BTNodeBase.EVENT_AfterRemoved   += OnAfterRemoveNode;
        }

        private void OnDisable()
        {
            BTNodeBase.EVENT_NodeRemove     -= OnBeforeRemoveNode;
            BTNodeBase.EVENT_AfterRemoved   -= OnAfterRemoveNode;
        }

        private void OnGUI()
        {
            DrawGrid(20, 0.2f, s_SmallGridColor);
            DrawGrid(100, 0.4f, s_BigGridColor);

            DrawNodes(Event.current);
            DrawTempConnectionLine(Event.current);

            DrawTopMenu(Event.current);

            ProcessNodeEvents(Event.current);
            ProcessInputEvents(Event.current);
            
            DrawNoDB();

            if (GUI.changed) Repaint();
        }

        #region Grid
        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);
            offset += drag * 0.5f;

            Handles.BeginGUI();
            var old = Handles.color;
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; ++i)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; ++j)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = old;
            Handles.EndGUI();
        }

        private void DragGrid(Vector2 delta)
        {
            drag = delta;

            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; ++i)
                {
                    nodes[i].Drag(delta);
                }
            }

            GUI.changed = true;
        }
        #endregion Grid

        private void DrawNodes(Event e)
        {
            if (nodes == null)
                return;
            for (int i = 0; i < nodes.Count; ++i)
            {
                if (nodes[i] == null)
                    continue;
                nodes[i].Draw(e);
            }
        }

        private void DrawNoDB()
        {
            if (currentBrain == null ||
                currentBrain.decisionGraph == null)
            {
                var w = 200f;
                var h = 80f;
                var cx = position.width * 0.5f;
                var cy = position.height * 0.5f;
                var rect = new Rect(cx - w * 0.5f, cy - h * 0.5f, w, h);
                using (new GUILayout.AreaScope(rect))
                {
                    GUILayout.Box("Database not found.", EditorStyles.helpBox);
                    if (GUILayout.Button("Create Database"))
                    {
                        var path = EditorUtility.SaveFilePanelInProject("Create Database", "Decision Graph", "asset", "Create new decision graph in project");
                        if (!string.IsNullOrEmpty(path))
                            _CreateDB(path);

				    }
                }
                // GUI.Box(rect, "Database not found.", EditorStyles.helpBox);
            }
            void _CreateDB(in string path)
            {
                if (string.IsNullOrEmpty(path))
                    return;
                Undo.IncrementCurrentGroup();
				var asset = ScriptableObject.CreateInstance<BTDecisionGraph>();
				Undo.RegisterCreatedObjectUndo(asset, "Create DecisionGraph");
				AssetDatabase.CreateAsset(asset, path);
				AssetDatabase.SaveAssets();
				EditorUtility.FocusProjectWindow();
				Selection.activeObject = asset;
				Undo.SetCurrentGroupName("Create DecisionGraph");
				currentBrain.decisionGraph = asset;
			}
        }
        #region Draw Connection Line (temp)
        private ConnectionBase selectedInPoint
            => ConnectionBase.Get(ConnectionType.In);
        private ConnectionBase selectedOutPoint
            => ConnectionBase.Get(ConnectionType.Out);
        public static Color s_lineColor = Color.yellow.CloneAlpha(0.3f);
        private void DrawTempConnectionLine(Event e)
        {
            if (selectedInPoint != null && selectedOutPoint == null)
            {
                Handles.DrawBezier(
                    selectedInPoint.rect.center,
                    e.mousePosition,
                    selectedInPoint.rect.center + Vector2.left * 50f,
                    e.mousePosition - Vector2.left * 50f,
                    s_lineColor,
                    null,
                    2f
                );

                GUI.changed = true;
            }

            if (selectedOutPoint != null && selectedInPoint == null)
            {
                Handles.DrawBezier(
                    selectedOutPoint.rect.center,
                    e.mousePosition,
                    selectedOutPoint.rect.center - Vector2.left * 50f,
                    e.mousePosition + Vector2.left * 50f,
                    s_lineColor,
                    null,
                    2f
                );

                GUI.changed = true;
            }
        }
        #endregion Draw Connection Line (temp)

        #region Input Event
        private void ProcessInputEvents(Event e)
        {
            drag = Vector2.zero;
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        ConnectionBase.ClearConnectionSelection();
                    }
                    if (e.button == 1)
                    {
                        ProcessContextMenu(e.mousePosition);
                    }
                    break;
                case EventType.MouseDrag:
                    if (e.button == 0)
                    {
                        DragGrid(e.delta);
                    }
                    break;
            }
        }

        private void ProcessNodeEvents(Event e)
        {
            if (nodes == null)
                return;

            for (int i = nodes.Count - 1; i >= 0; --i)
            {
                bool guiChanged = nodes[i].ProcessEvents(e);
                if (guiChanged)
                {
                    GUI.changed = true;
                }
            }
        }
        #endregion Input Event

        #region Menu
        static readonly GUIContent s_SaveLabel = new GUIContent("Save");
        private void DrawTopMenu(Event e)
        {
            var menuHeight = 20f;
            var rect = new Rect(0f, 0f, position.width, menuHeight);
            using (new GUILayout.AreaScope(rect, GUIContent.none, EditorStyles.toolbar))
            {
                using (new GUILayout.HorizontalScope(GUILayout.Height(480f)))
                {
                    if (GUILayout.Button(s_SaveLabel, EditorStyles.toolbarButton, GUILayout.Width(70f)))
                    {
                        InternalDatabaseReimport();
                    }
                    GUILayout.Space(10f);
                }
            }
        }

        private void ProcessContextMenu(Vector2 mousePosition)
        {
            List<KeyValuePair<string, System.Action>> funcs = new List<KeyValuePair<string, System.Action>>();
            funcs.Add(new KeyValuePair<string, System.Action>("Add node", () => OnClickAddNode(mousePosition)));
            BTUtils.ContextMenu(funcs);
        }

        private void OnClickAddNode(Vector2 pos)
        {
            if (currentBrain == null || currentBrain.decisionGraph == null)
                throw new System.Exception("Database not found.");
            var db = currentBrain.decisionGraph;
            int gid = BTUtils.GetCurrentGroup();
            Undo.SetCurrentGroupName($"Create node completed");
            var node = BTUtils.Create<BTNode>(db, (n) =>
            {
                n.Init(pos);
            });
            nodes.Add(node);

            BTUtils.RecordObject(db, "Snapshot of db.");
            AssetDatabase.SaveAssets();
            BTUtils.CollapseUndoOperations(gid);
            InternalDatabaseReimport();
        }

        private void OnBeforeRemoveNode(BTNodeBase node)
        {
            if (currentBrain == null || currentBrain.decisionGraph == null)
                throw new System.Exception("Database not found.");
            if (nodes == null)
                return;
            
            if (!nodes.Remove(node))
            {
                Debug.LogError("target node not found, fail to remove.");
            }
        }
        private void OnAfterRemoveNode(int gid, Object obj)
        {
            var db = currentBrain.decisionGraph;
            BTUtils.Remove(db, obj);
            BTUtils.CollapseUndoOperations(gid, "Remove node");
            InternalDatabaseReimport();
        }
        #endregion Menu

        #region Utils
        private void InternalDatabaseReimport()
        {
            if (currentBrain == null || currentBrain.decisionGraph == null)
                return;
            var path = AssetDatabase.GetAssetPath(currentBrain.decisionGraph);
            if (!string.IsNullOrEmpty(path))
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        }
        #endregion Utils
    }
}
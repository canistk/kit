using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Kit.BehaviorTree.Core;

namespace Kit.BehaviorTree
{
    [CustomEditor(typeof(BTBrain), true)]
    public class BTBrainEditor : EditorBase
    {
        BTBrain brain => (BTBrain)target;

        protected override void OnBeforeDrawGUI()
        {
            base.OnBeforeDrawGUI();
            if (GUILayout.Button("Open Decision Graph"))
            {
                var helper = BehaviorTreeEditor.instance;
                helper.Observe(brain);
                helper.Focus();
            }
        }
    }
}
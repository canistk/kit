using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.BehaviorTree.Core
{
    /// <summary>
    /// A component to read & process <see cref="BTDecisionGraph"/>
    /// </summary>
    public class BTBrain : MonoBehaviour
    {
        [SerializeField] private BTDecisionGraph m_DecisionGraph = null;
        public BTDecisionGraph decisionGraph
        {
            get => m_DecisionGraph;
            set { m_DecisionGraph = value; }
        }

    }
}
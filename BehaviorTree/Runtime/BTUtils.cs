using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Kit.BehaviorTree.Core
{
    public static class BTUtils
    {
        public static T Create<T>(ScriptableObject parent, System.Action<T> onCreate = null)
            where T : ScriptableObject
        {
#if UNITY_EDITOR
            
            if (parent == null)
                throw new System.Exception("target scriptableObject not found.");

            Undo.RecordObject(parent, "Cache parent");
            var child = ScriptableObject.CreateInstance<T>();
            AssetDatabase.AddObjectToAsset(child, parent);
            try
            {
                onCreate?.Invoke(child);
            }
            catch (System.Exception ex)
            {
                Debug.LogException(ex);
            }
            Undo.RecordObject(child, "Snapshot create + init");
            EditorUtility.SetDirty(parent);
            return child;
#else
            throw new System.NotImplementedException();
#endif
        }

        public static void Remove(ScriptableObject parent, Object child)
        {
#if UNITY_EDITOR
            Undo.RecordObject(parent, "Cache parent");
            AssetDatabase.RemoveObjectFromAsset(child);
            AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(parent);
            // BTUtils.DestroyObject(node);
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void DestroyObject(Object obj)
        {
#if UNITY_EDITOR
            if (obj == null)
                return;
            
            Undo.DestroyObjectImmediate(obj);
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void RecordObject(Object obj, string msg)
        {
#if UNITY_EDITOR
            Undo.RecordObject(obj, msg);
            EditorUtility.SetDirty(obj);
#endif
        }

        public static void SetDirty(Object obj)
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(obj);
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void RegisterCompleteObjectUndo(Object obj, string msg)
        {
#if UNITY_EDITOR
            Undo.RegisterCompleteObjectUndo(obj, msg);
#endif
        }

        public static int GetCurrentGroup(string groupName = null)
        {
#if UNITY_EDITOR
            var groupId = Undo.GetCurrentGroup();
            if (groupName != null && groupName.Length > 0)
                Undo.SetCurrentGroupName(groupName);
            return groupId;
#else
            throw new System.NotImplementedException();
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void CollapseUndoOperations(int groupId, string groupName = null)
        {
#if UNITY_EDITOR
            Undo.CollapseUndoOperations(groupId);
#endif
        }

        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void ContextMenu(IList<KeyValuePair<string, System.Action>> options)
        {
#if UNITY_EDITOR
            int cnt = options.Count;
            GenericMenu genericMenu = new GenericMenu();
            for(int i = 0; i < cnt; ++i)
            {
                (string name, System.Action func) = options[i];
                genericMenu.AddItem(
                    new GUIContent(name),
                    false,
                    () => { func?.Invoke(); }
                );
            }
            genericMenu.ShowAsContext();
#endif
        }
    }
}
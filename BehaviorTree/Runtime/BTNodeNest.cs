using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.BehaviorTree
{
    /// <summary>The <see cref="BTTask"/> that will be execute only once (and removed)</summary>
    /// <remarks><see cref="BTNodeNest"/></remarks>
    public interface IExecuteOnce { }

    public abstract class BTNodeNest : BTTask
    {
        private List<BTTask> defaultTasks;
        protected BTTask[] tasks;
        public BTNodeNest(params BTTask[] tasks)
        {
            this.defaultTasks = new List<BTTask>(tasks);
            this.tasks = null;
        }
        public BTNodeNest(BTTask task) : this(new[] { task })
        { }

        protected override void OnEnter()
        {
            base.OnEnter();
            tasks = defaultTasks.ToArray();
            int i = defaultTasks.Count;
            while (i-- > 0)
            {
                if (defaultTasks[i] is IExecuteOnce)
                    defaultTasks.RemoveAt(i);
            }
        }

        /// <summary>
        /// Task that will execute on next cycle.
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public BTNodeNest AddFirst(BTTask task)
        {
            if (task == null)
                return this;
            if (state != eTaskState.Idle && task is not IExecuteOnce)
            {
                Debug.LogWarning($"The task {task}, will be append to task list on next execution");
            }

            if (defaultTasks.Count != 0)
                defaultTasks.Insert(0, task);
            else
                defaultTasks.Add(task);
            return this;
        }

        public BTNodeNest Add(BTTask task)
        {
            if (task == null)
                return this;
            if (state != eTaskState.Idle && task is not IExecuteOnce)
            {
                Debug.LogWarning($"The task {task}, will be append to task list on next execution");
            }
            defaultTasks.Add(task);
            return this;
        }

        public void Clear()
        {
            if (state != eTaskState.Idle)
            {
                Debug.LogWarning($"The tasklist {defaultTasks.Count}, will be elase on next execution");
            }
            defaultTasks.Clear();
        }
    }
}

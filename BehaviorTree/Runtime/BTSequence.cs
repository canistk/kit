using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.BehaviorTree
{
    using EX = BTSequence.eExecuteMethod;
    using RM = BTSequence.eReturnMethod;
    public abstract class BTSequence : BTNodeNest
    {
        public enum eReturnMethod
        {
            FailStop, // default
            SkipFail,
        }
        protected readonly eReturnMethod m_ReturnMethod;

        public enum eExecuteMethod
        {
            StateMachine,
            Reevaluate,
        }
        protected readonly eExecuteMethod m_ExecuteMethod;

        protected int m_Index = 0;

        public BTSequence(eExecuteMethod executeMethod, eReturnMethod returnMethod, params BTTask[] tasks)
            : base(tasks)
        {
            this.m_ExecuteMethod = executeMethod;
            this.m_ReturnMethod = returnMethod;
        }
        public BTSequence(eExecuteMethod executeMethod, eReturnMethod returnMethod, BTTask task)
            : base(task)
        {
            this.m_ExecuteMethod = executeMethod;
            this.m_ReturnMethod = returnMethod;
        }

        protected override void OnEnter()
        {
            base.OnEnter();
            m_Index = 0;
        }
        protected override void OnLeave(eTaskState reason)
        {
            base.OnLeave(reason);
            m_Index = 0;
        }
        protected override eTaskState Update()
        {
            int cnt = tasks.Length;
            if (m_ExecuteMethod == EX.Reevaluate)
            {
                m_Index = 0;
            }

            for (; m_Index < cnt; ++m_Index)
            {
                var rst = tasks[m_Index].Execute();
                if (rst == eTaskState.Running)
                {
                    return eTaskState.Running;
                }
                else
                {
                    if (rst == eTaskState.Failure)
                    {
                        InternalAbortChildren();
                        switch (m_ReturnMethod)
                        {
                            default: throw new System.NotImplementedException();
                            case eReturnMethod.FailStop: return eTaskState.Failure;
                            case eReturnMethod.SkipFail: continue;
                        }
                    }
                }
            }
            return eTaskState.Success;
        }

        public override void Abort()
        {
            InternalAbortChildren();
            base.Abort();
        }

        private void InternalAbortChildren()
        {
            if (state != eTaskState.Running)
                return; // ignore

            if (m_ExecuteMethod == EX.Reevaluate)
            {
                int stopIdx = m_Index;
                int i = tasks.Length;
                while (i-- > stopIdx)
                {
                    if (tasks[i].state == eTaskState.Running)
                        tasks[i].Abort();
                }
            }
        }
    }

    public class BTSequenceFailStop : BTSequence
    {
        public BTSequenceFailStop(EX execute, params BTTask[] tasks) : base(execute, RM.FailStop, tasks) { }
        public BTSequenceFailStop(EX execute, BTTask task) : base(execute, RM.FailStop, task) { }
        public BTSequenceFailStop(params BTTask[] tasks) : base(EX.StateMachine, RM.FailStop, tasks) { }
        public BTSequenceFailStop(BTTask task) : base(EX.StateMachine, RM.FailStop, task) { }
    }

    public class BTSequenceSkipFail : BTSequence
    {
        public BTSequenceSkipFail(EX execute, params BTTask[] tasks) : base(execute, RM.SkipFail, tasks) { }
        public BTSequenceSkipFail(EX execute, BTTask task) : base(execute, RM.SkipFail, task) { }
        public BTSequenceSkipFail(params BTTask[] tasks) : base(EX.StateMachine, RM.SkipFail, tasks) { }
        public BTSequenceSkipFail(BTTask task) : base(EX.StateMachine, RM.SkipFail, task) { }
    }

    public class BTReturnSuccess : BTSequenceFailStop
    {
        public BTReturnSuccess(params BTTask[] tasks) : base(tasks) { }
        public BTReturnSuccess(BTTask task) : base(task) { }
        protected override eTaskState Update()
        {
            base.Update();
            return eTaskState.Success;
        }
    }

    public class BTReturnFail : BTSequenceFailStop
    {
        public BTReturnFail(params BTTask[] tasks) : base(tasks) { }
        public BTReturnFail(BTTask task) : base(task) { }
        protected override eTaskState Update()
        {
            base.Update();
            return eTaskState.Failure;
        }
    }

    public class BTReturnInvert : BTSequenceFailStop
    {
        public BTReturnInvert(params BTTask[] tasks) : base(tasks) { }
        public BTReturnInvert(BTTask task) : base(task) { }
        protected override eTaskState Update()
        {
            var tmp = base.Update();
            if (tmp == eTaskState.Success)
                return eTaskState.Failure;
            else if (tmp == eTaskState.Failure)
                return eTaskState.Success;
            return tmp;
        }
    }

    public static class BTTaskUtils
    {
        public static BTTask Invert(this BTTask task)
        {
            return new BTReturnInvert(task);
        }

        public static BTTask ReturnSuccess(this BTTask task)
        {
            return new BTReturnSuccess(task);
        }

        public static BTTask ReturnFail(this BTTask task)
        {
            return new BTReturnFail(task);
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Kit.BehaviorTree.Core
{
    public static class NodeSkin
    {
        private static GUIStyle m_defaultSkin;
        public static GUIStyle defaultSkin
        {
            get
            {
                if (m_defaultSkin == null)
                {
                    m_defaultSkin = new GUIStyle();
					m_defaultSkin.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
                    m_defaultSkin.border = new RectOffset(12, 12, 12, 12);
                }
                return m_defaultSkin;
			}
        }

		private static GUIStyle m_selectedNode;
		public static GUIStyle selectedNode
		{
			get
			{
				if (m_selectedNode == null)
				{
					m_selectedNode = new GUIStyle();
					m_selectedNode.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
					m_selectedNode.border = new RectOffset(12, 12, 12, 12);

				}
				return m_selectedNode;
			}
		}

		private static GUIStyle m_btn;
        public static GUIStyle btn
        {
            get
            {
                if (m_btn == null)
				{
					m_btn = new GUIStyle();
					m_btn.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
					m_btn.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
					m_btn.border = new RectOffset(4, 4, 12, 12);
				}
                return m_btn;
			}
        }

		private static GUIStyle m_inPointStyle;
		public static GUIStyle inPointStyle
		{
			get
			{
				if (m_inPointStyle == null)
				{
					m_inPointStyle = new GUIStyle();
					m_inPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
					m_inPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
					m_inPointStyle.border = new RectOffset(4, 4, 12, 12);
				}
				return m_inPointStyle;
			}
		}

		private static GUIStyle m_outPointStyle;
		public static GUIStyle outPointStyle
		{
			get
			{
				if (m_outPointStyle == null)
				{
					m_outPointStyle = new GUIStyle();
					m_outPointStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
					m_outPointStyle.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
					m_outPointStyle.border = new RectOffset(4, 4, 12, 12);
				}
				return m_outPointStyle;
			}
		}

    }
}
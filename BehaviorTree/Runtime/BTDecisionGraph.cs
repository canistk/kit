using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.BehaviorTree.Core
{
    [CreateAssetMenu(
        fileName = "Decision Graph",
        menuName = "Kit/BehaviorTree/Decision Graph")]
    public class BTDecisionGraph : ScriptableObject
    {
        [SerializeField] List<BTNodeBase> m_Nodes = new List<BTNodeBase>();
        public List<BTNodeBase> nodes => m_Nodes;
    }
}
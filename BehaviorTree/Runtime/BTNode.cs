using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.BehaviorTree.Core
{
    /// <summary>
    /// <see cref="https://gram.gs/gramlog/creating-node-based-editor-unity/"/>
    /// </summary>
    [System.Serializable]
    public class BTNode : BTNodeBase
    {
		private GUIStyle m_Style;

		#region serialize data
		[SerializeField] private    ConnectionBase     m_InPoint;
        public                      ConnectionBase     inPoint => m_InPoint;

        [SerializeField] private    ConnectionBase     m_OutPoint;
        public                      ConnectionBase     outPoint => m_OutPoint;
        #endregion serialize data

        #region System
        public BTNode() { }
        ~BTNode() { }
        #endregion System

        #region Creation
        protected override void OnInit()
        {
            name        = nameof(BTNode);
            m_InPoint   = BTUtils.Create<ConnectionBase>(this, (cp) =>
            {
                cp.Init(this, ConnectionType.In);
            });
            m_OutPoint  = BTUtils.Create<ConnectionBase>(this, (cp) =>
            {
                cp.Init(this, ConnectionType.Out);
            });
            m_Style = NodeSkin.defaultSkin;
        }
        protected override Vector2 DefaultSize()
        {
            return new Vector2(125f, 120f);
        }

        protected override void OnDestroy()
        {
            //Debug.LogWarning($"{name} OnDestroy", this);

        }
        #endregion Creation

        #region Draw GUI
        public override void Draw(Event e)
        {
            inPoint.Draw(e);
            outPoint.Draw(e);
            GUI.Box(rect, string.Empty, m_Style);
        }

        protected override void ProcessContextMenu()
        {
            List<KeyValuePair<string, System.Action>> funcs = new List<KeyValuePair<string, System.Action>>();
            funcs.Add(new KeyValuePair<string, System.Action>("Remove Node", () => RemoveNode(this)));
            BTUtils.ContextMenu(funcs);
        }

        protected override void OnSelected()
        {
            m_Style = NodeSkin.selectedNode;
        }

        protected override void OnDeSelected()
        {
			m_Style = NodeSkin.defaultSkin;
        }
        #endregion Draw GUI

        protected override int BeforeRemove()
        {
            var gid = base.BeforeRemove();
            if (m_InPoint != null)
            {
                BTUtils.Remove(this, m_InPoint);
            }

            if (m_OutPoint != null)
            {
                BTUtils.Remove(this, m_OutPoint);
            }
            return gid;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.BehaviorTree.Core
{
    [System.Serializable]
    public abstract class BTNodeBase : ScriptableObject
    {
        #region Editor Graph
        public bool     isDragged   { get; private set; }
        public bool     isSelected  { get; private set; }
        #endregion Editor Graph

        #region position + size
        public Rect             rect        => new Rect(position, size);
        public Vector2          position    ;
        public Vector2          size        ;
        protected abstract Vector2 DefaultSize();
        
        public virtual void Drag(Vector2 delta)
        {
            position += delta;
        }
        #endregion position + size

        #region Creation / Destroy
        public void Init(Vector2 position)
        {
            this.position = position;
            this.size = DefaultSize();
            OnInit();
        }

        protected abstract void OnInit();

        /// <summary>Call by u3d</summary>
        protected abstract void OnDestroy();
        #endregion Creation / Destroy

        #region Draw GUI
        public virtual bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (rect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                            isSelected = true;
                            OnSelected();
                        }
                        else
                        {
                            GUI.changed = true;
                            isSelected = false;
                            OnDeSelected();
                        }
                    }
                    if (e.button == 1 && isSelected && rect.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }
            return false;
        }

        public abstract void Draw(Event e);

        protected abstract void ProcessContextMenu();
        protected virtual void OnSelected() { }
        protected virtual void OnDeSelected() { }
        public static event System.Action<BTNodeBase> EVENT_NodeRemove;
        public static event System.Action<int, Object> EVENT_AfterRemoved;
        protected static bool RemoveNode(BTNodeBase node)
        {
            if (node == null)
                return false;

            int gid = node.BeforeRemove();
            var ev = EVENT_NodeRemove.GetInvocationList();
            foreach(var e in ev )
            {
                try
                {
                    e.DynamicInvoke(node);
                }
                catch (System.Exception ex)
                {
                    Debug.LogException(ex);
                }
            }
            EVENT_AfterRemoved?.Invoke(gid, node);
            return true;
        }
        protected virtual int BeforeRemove()
        {
            return BTUtils.GetCurrentGroup("Remove Node");
        }
        #endregion Draw GUI
    }
}
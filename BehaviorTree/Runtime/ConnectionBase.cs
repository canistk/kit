using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Kit.BehaviorTree.Core
{
    public enum ConnectionType
    {
        In, Out,
        Previous, Next,
        Parent, Children,
    }
    [System.Serializable]
    public class ConnectionBase : ScriptableObject
    {
        #region serialize data
        [SerializeField] public  Rect rect;
        private GUIStyle style
        {
            get
            {
                return type switch
                {
                    ConnectionType.In  => NodeSkin.inPointStyle,
                    ConnectionType.Out => NodeSkin.outPointStyle,
                    _ => throw new System.NotImplementedException(),
                };
            }
        }

        [SerializeField] ConnectionType    m_Type;
        public ConnectionType              type        => m_Type;

        /// <summary>The owner of this connection</summary>
        [SerializeField] BTNodeBase             m_Owner;
        public BTNodeBase                       Owner        => m_Owner;

        /// <summary>Target node linked toward</summary>
        [SerializeField] ConnectionBase        m_Link;
        public ConnectionBase                  link        => m_Link;
        #endregion serialize data

        #region System
        public ConnectionBase()
        {
            BTNodeBase.EVENT_NodeRemove += OnNodeRemoved;
        }
        ~ConnectionBase()
        {
            BTNodeBase.EVENT_NodeRemove -= OnNodeRemoved;
        }
        #endregion System

        #region Draw GUI
        public void Draw(Event e)
        {
            if (Owner == null)
                return;

            rect.y = Owner.rect.y + (Owner.rect.height * 0.5f) - rect.height * 0.5f;

            switch (type)
            {
                case ConnectionType.In:
                    rect.x = Owner.rect.x - rect.width + 8f;
                    break;

                case ConnectionType.Out:
                    rect.x = Owner.rect.x + Owner.rect.width - 8f;
                    break;
            }

            if (GUI.Button(rect, string.Empty, style))
            {
                if (IsSelected(this))
                    Deselect(this);
                else
                    Select(this);
            }

            if (type == ConnectionType.Out &&
                link != null && link.Owner != null)
            {
                DrawConnection(this);
            }
        }
        #endregion Draw GUI

        #region Link
        public void Link(ConnectionBase other)
        {
            if (other == this)
                throw new System.Exception($"Invalid programing flow, {nameof(Link)} to itself.");
            if (other != null && Owner == other.Owner)
                return; // avoid self connection.
            if (link == other)
                return; // already connected.

            // Remove existing connection
            InternalRemoveConnection();

            // build two way connection.
            m_Link = other;
            if (other != null && other != this)
                other.Link(this);
        }

        private void InternalRemoveConnection()
        {
            if (link != null)
            {
                var exist = link;
                m_Link = null;      // remove my connection.
                exist.Link(null);   // remove other connection.
            }
        }

        private void OnNodeRemoved(BTNodeBase other)
        {
            if (link == null)
                return;
            if (link.Owner != other)
                return;
            InternalRemoveConnection();
        }
        #endregion Link

        #region Selected Connection Point
        private static Dictionary<ConnectionType, ConnectionBase> s_SelectedPoints = null;
        private static Dictionary<ConnectionType, ConnectionBase> selectedPoints
        {
            get
            {
                if (s_SelectedPoints == null)
                {
                    s_SelectedPoints = new Dictionary<ConnectionType, ConnectionBase>(8);
                }
                return s_SelectedPoints;
            }
        }
        public static void Select(ConnectionBase cp)
        {
            if (cp == null)
                throw new System.NullReferenceException();
            
            // check records, attempt to search for matching connection.
            var pairable = GetPairNodes(cp.type);
            foreach(var obj in pairable)
            {
                if (!selectedPoints.TryGetValue(obj, out var other) ||
                    other == null ||
                    !IsMatchingConnection(cp, other))
                    continue;
                // found possible connection.
                cp.Link(other);
                ClearConnectionSelection();
                return;
            }

            // when no possible connection found.
            ClearConnectionSelection();
            // clean up and select this.
            selectedPoints[cp.type] = cp;
        }

        public static void Deselect(ConnectionBase cp)
        {
            if (cp == null) throw new System.NullReferenceException();
            if (!selectedPoints.TryGetValue(cp.type, out var pt) || // created
                pt != cp) // not the same
                return;
            selectedPoints[cp.type] = null;
        }

        public static void ClearConnectionSelection()
        {
            selectedPoints.Clear();
        }

        public static bool IsSelected(ConnectionBase cp)
        {
            return cp != null &&
                selectedPoints.TryGetValue(cp.type, out var pt) &&
                pt == cp;
        }

        public static ConnectionBase Get(ConnectionType type)
        {
            if (!selectedPoints.TryGetValue(type, out var rst))
                return null;
            return rst;
        }

        public static ConnectionType[] GetPairNodes(ConnectionType cpt)
        {
            switch (cpt)
            {
                case ConnectionType.In: return new[] { ConnectionType.Out };
                case ConnectionType.Out: return new[] { ConnectionType.In };
                default:
                    throw new System.NotImplementedException();
            }
        }

        private static bool IsMatchingConnection(ConnectionBase lhs, ConnectionBase rhs)
        {
            if (lhs == null || rhs == null)
                return false;
            return lhs.type != rhs.type;
        }

        #endregion Selected Connection Point

        #region Creation / Destroy
        public void Init(BTNodeBase owner, ConnectionType type)
        {
            this.m_Owner     = owner;
            this.m_Type     = type;
            this.rect       = new Rect(0, 0, 10f, 20f);
            this.name       = nameof(ConnectionBase) +"_"+ type.ToString();
        }

        /// <summary>Call by u3d</summary>
        private void OnDestroy()
        {
            // Debug.LogWarning($"{name} OnDestroy", this);
        }
        #endregion Creation / Destroy

        #region Editor Draw
        private static readonly Color s_lineColor = Color.white.CloneAlpha(0.3f);
        private static void DrawConnection(ConnectionBase lhs)
        {
            if (lhs == null ||
                lhs.type == ConnectionType.In) // only draw on output
                return;
            var rhs     = lhs.link;
            var from    = lhs.rect.center;
            var to      = rhs.rect.center;
#if UNITY_EDITOR
            Handles.DrawBezier(from, to, 
                from    - Vector2.left * 50f,
                to      + Vector2.left * 50f,
                s_lineColor,
                null,
                2f
            );

            if (Handles.Button((from + to) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
            {
                // OnClickRemoveConnection?.Invoke(this);
                if (lhs.link == rhs)
                    lhs.Link(null);
                if (rhs.link == lhs)
                    rhs.Link(null);
            }
#endif
        }
        private static void DrawBezier(Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent, Color color, Texture2D texture, float width)
        {
#if UNITY_EDITOR
            Handles.DrawBezier(startPosition, endPosition, startTangent, endTangent, color, texture, width);
#endif
        }
        #endregion Editor Draw
    }
}
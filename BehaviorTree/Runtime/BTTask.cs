using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.BehaviorTree
{
    public enum eTaskState
    {
        Idle = 0,
        Running,
        Success,
        Failure,
        Abort,
    }
    public abstract class BTTask
    {
        public eTaskState state { get; private set; } = eTaskState.Idle;
        public eTaskState Execute()
        {
            if (state == eTaskState.Idle)
            {
                OnEnter();
                state = eTaskState.Running;
            }

            if (state == eTaskState.Running)
            {
                var tmp = Update();
                if (tmp < eTaskState.Running)
                {
                    throw new System.InvalidOperationException($"state can only return : Success, Failure, Abort");
                }
                else if (tmp > eTaskState.Running)
                {
                    state = tmp;
                    InternalAbort();
                }
            }
            return state;
        }

        protected virtual void OnEnter() { }
        protected virtual void OnLeave(eTaskState reason) { }
        protected abstract eTaskState Update();
        public virtual void Abort()
        {
            if (state != eTaskState.Running)
                return;
            state = eTaskState.Abort;
            InternalAbort();
        }
        private void InternalAbort()
        {
            if (state <= eTaskState.Running)
                throw new System.InvalidOperationException();
            OnLeave(state);
            state = eTaskState.Idle;
        }
    }

    // public abstract class BTTaskTemplate : BTTask {}
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Kit
{
    /// <summary>
    /// for linking character to timeline,
    /// assume timeline provide separate track names, correctly.
    /// </summary>
    public class TimelineActor : MonoBehaviour
    {
        [SerializeField] Transform m_MoverRaw = null;
        [SerializeField] Animator m_Animator = null;

        [Space]
        [SerializeField]
        PlayableInfo m_PlayableInfo = new PlayableInfo()
        {
            skinTrackName   = "skin",
            moverTrackName  = "mover",
        };

        [System.Serializable]
        public class PlayableInfo
        {
            /// <summary>The timeline director that we reference on.</summary>
            public PlayableDirector director;
            /// <summary>an animator for character's animation clips override.</summary>
            public string skinTrackName = "skin";
            /// <summary>an animator for reference position, for character to follow refernce position and follow it.</summary>
            public string moverTrackName = "mover";
            public PlayableInfo(PlayableDirector director, string skinTrackName, string moverTrackName)
            {
                this.director = director;
                this.skinTrackName = skinTrackName;
                this.moverTrackName = moverTrackName;
            }

            internal PlayableInfo() { }

            #region Binding
            private Dictionary<string, List<TrackAsset>> m_TrackDict = null;
            private Dictionary<string, List<TrackAsset>> trackDict
            {
                get
                {
                    if (m_TrackDict == null)
                    {
                        m_TrackDict = new Dictionary<string, List<TrackAsset>>();
                        var timeline = (TimelineAsset)director.playableAsset;
                        foreach (TrackAsset track in timeline.GetOutputTracks())
                        {
                            if (!m_TrackDict.TryGetValue(track.name, out var list))
                            {
                                m_TrackDict.Add(track.name, list = new List<TrackAsset>());
                            }
                            list.Add(track);
                        }
                    }
                    return m_TrackDict;
                }
            }

            public bool TryGetTrackAsset(string trackName, out TrackAsset trackAsset)
            {
                if (trackDict.TryGetValue(trackName, out var list))
                    trackAsset = list[0];
                else
                    trackAsset = default;

                return trackAsset != null;
            }

            public bool TryGetTrackAssets(string trackName, out IReadOnlyList<TrackAsset> trackAsset)
            {
                if (trackDict.TryGetValue(trackName, out var tmp))
                    trackAsset = tmp;
                else
                    trackAsset = null;
                return trackAsset != null;
            }

            public void SetBindings(string trackName, Animator actor)
            {
                // director.SetReferenceValue(new PropertyName(trackName), actor);
                if (TryGetTrackAssets(trackName, out var tracks))
                {
                    foreach (var track in tracks)
                        director.SetGenericBinding(track, actor);
                }
                else
                    Debug.LogError($"Fail to locate track [{trackName}]");
            }

            public UnityEngine.Object GetBinding(string trackName)
            {
                if (TryGetTrackAsset(trackName, out var track))
                    return director.GetGenericBinding(track);
                return null;
            }
            public IEnumerable<UnityEngine.Object> GetBindings(string trackName)
            {
                if (TryGetTrackAssets(trackName, out var tracks))
                    foreach (var track in tracks)
                        yield return director.GetGenericBinding(track);
            }

            public float GetTrackNormalizeTime(string trackName)
            {
                if (TryGetTrackAsset(trackName, out var track))
                    return GetTrackNormalizeTime(track);
                return 0f;
            }

            private float GetTrackNormalizeTime(TrackAsset track)
            {
                var time    = director.time;
                var start   = track.start;
                var end     = track.end;
                if (start   <= time && time <= end)
                {
                    // already playing.
                    var pass        = time - start;
                    var duration    = track.duration;
                    var pt          = (float)(pass / duration);
                    return pt;
                }
                else if (time > end)
                {
                    return 1f;
                }
                else if (time < start)
                {
                    return 0f;
                }
                return 0f;
            }

            public IEnumerable<AnimationPlayableAsset> GetAnimationPlayableAssets(TrackAsset track)
            {
                foreach (var clip in track.GetClips())
                {
                    var ani = (AnimationPlayableAsset)clip.asset;
                    if (ani != null)
                        yield return ani;
                }
            }
            #endregion Binding
        }

        public void SetAnimator(Animator animator, Transform mover = null)
        {
            this.m_Animator = animator;
            this.m_MoverRaw = mover != null ? mover : animator?.transform;
        }

        public void SetPlayableInfo(PlayableDirector director, string skinTrackName, string moverTrackName)
        {
            m_PlayableInfo = new PlayableInfo(director, skinTrackName, moverTrackName);
        }

        private void LateUpdate()
        {
            if (!IsConnected)
                return;
            m_Linker.UpdateMover();
        }

        public bool IsConnected => m_Linker != null;

        public void Connect()
        {
            if (m_Linker != null)
                Disconnect();
            if (m_Animator != null && m_MoverRaw != null)
            {
                m_Linker = new Connect2Actor(m_PlayableInfo, m_MoverRaw, m_Animator);
            }
            else
            {
                Debug.LogError("Invalid setup.");
            }
        }

        public void Disconnect()
        {
            if (m_Linker != null)
                m_Linker.Dispose();
            m_Linker = null;
        }

        public Connect2Actor GetConnectInfo() => m_Linker;
        private Connect2Actor m_Linker = null;
        public class Connect2Actor : IDisposable
        {
            public PlayableInfo director;
            public Transform moverRoot;     // reference transform on actor
            public Transform moverTLRef;    // reference transform in timeline.
            public Animator animator;

            private bool isDisposed;

            public Connect2Actor(PlayableInfo director, Transform moverRoot, Animator animator)
                : this(director, animator)
            {
                this.director = director;
                this.moverRoot = moverRoot;
                this.animator = animator;
            }

            private Connect2Actor(PlayableInfo director, Animator animator)
            {
                this.director = director;
                if (!string.IsNullOrEmpty(director.moverTrackName))
                {
                    var obj = director.GetBinding(director.moverTrackName);
                    if (obj is Animator timelineTransformRef)
                    {
                        this.moverTLRef = timelineTransformRef.transform;
                    }
                    else
                    {
                        Debug.LogError($"Track name : {director.moverTrackName} not found.");
                    }
                }

                this.animator = animator;
                if (!string.IsNullOrEmpty(director.skinTrackName) && this.animator)
                {
                    director.SetBindings(director.skinTrackName, this.animator);
                }
            }

            public void UpdateMover()
            {
                if (moverTLRef == null)
                    return;
                if (moverRoot == null)
                    return;

                var lhs         = moverRoot;
                var rhs         = moverTLRef.transform;
                lhs.position    = rhs.position;
                lhs.rotation    = rhs.rotation;
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!isDisposed)
                {
                    if (disposing)
                    {
                        // TODO: dispose managed state (managed objects)
                        if (director != null)
                        {
                            if (!string.IsNullOrEmpty(director.skinTrackName))
                                director.SetBindings(director.skinTrackName, null);
                        }
                    }

                    // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                    // TODO: set large fields to null
                    director = null;
                    animator = null;
                    moverTLRef = null;
                    isDisposed = true;
                }
            }

            // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
            ~Connect2Actor()
            {
                // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
                Dispose(disposing: false);
            }

            public void Dispose()
            {
                // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
                Dispose(disposing: true);
                GC.SuppressFinalize(this);
            }

        }
    }
}
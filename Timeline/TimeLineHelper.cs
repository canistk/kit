﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace Kit
{
	public class TimeLineHelper : MonoBehaviour
	{
		[SerializeField] PlayableDirector m_Director = null;
		public PlayableDirector director => m_Director;
#pragma warning disable 0414
		[SerializeField, ContextButton(nameof(Play))] private bool m_Play = false;
		[SerializeField, ContextButton(nameof(Pause))] private bool m_Pause = false;
		[SerializeField, ContextButton(nameof(Stop))] private bool m_Stop = false;
#pragma warning restore 0414
		
		protected virtual void Reset()
		{
			m_Director = GetComponentInChildren<PlayableDirector>();
		}
		protected virtual void OnValidate()
		{
			if (Application.isEditor &&
				// !Application.isPlaying &&
				m_NormalizeTimeCache != m_NormalizeTime)
			{
				m_NormalizeTimeCache = m_NormalizeTime;
				Goto(m_NormalizeTime);
			}
			if (m_Director == null && !Application.isPlaying)
				m_Director = GetComponentInChildren<PlayableDirector>();
		}

		protected virtual void Awake() { }

		#region Frame control
		public void Play() => m_Director.Play();
		public void Pause() => m_Director.Pause();
		public void Stop()
		{
			if (m_Director.state == PlayState.Playing)
				m_Director.Stop();
			else
			{
				m_NormalizeTime = 0f;
				Goto(m_NormalizeTime);
			}
		}

		[Range(0f, 1f)] public float m_NormalizeTime = 0f;
		private float m_NormalizeTimeCache = 0f;
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		private void Goto(float time)
		{
			m_NormalizeTimeCache = time;
			m_Director.time = time * m_Director.duration;
			m_Director.Evaluate();
		}
		#endregion Frame control

		#region Binding
		private Dictionary<string, TrackAsset> m_TrackDict = null;
		private Dictionary<string, TrackAsset> trackDict
		{ 
			get
			{
				if (m_TrackDict == null)
				{
					m_TrackDict = new Dictionary<string, TrackAsset>();
					foreach (TrackAsset track in timeline.GetOutputTracks())
						m_TrackDict.Add(track.name, track);
				}
				return m_TrackDict;
			}
		}

		public bool TryGetTrackAsset(string trackName, out TrackAsset trackAsset)
		=> trackDict.TryGetValue(trackName, out trackAsset);
		public TimelineAsset timeline => (TimelineAsset)m_Director.playableAsset;
		public void SetBinding(string trackName, Animator actor)
		{
			if (trackDict.TryGetValue(trackName, out TrackAsset track))
				m_Director.SetGenericBinding(track, actor);
			else
				Debug.LogError($"Fail to locate track [{trackName}]", this);
		}
		public Object GetBinding(string trackName)
		{
			if (trackDict.TryGetValue(trackName, out TrackAsset track))
				return m_Director.GetGenericBinding(track);
			return null;
		}

		public float GetTrackNormalizeTime(string trackName)
		{
			if (trackDict.TryGetValue(trackName, out TrackAsset track))
				return GetTrackNormalizeTime(track);
			return 0f;
		}

		public float GetTrackNormalizeTime(TrackAsset track)
		{
			if (!trackDict.ContainsValue(track))
				throw new System.Exception();

			double time = m_Director.time;
			double start = track.start;
			double end = track.end;
			if (start <= time && time <= end)
			{
				// already playing.
				double pass = time - start;
				double duration = track.duration;
				float pt = (float)(pass / duration);
				return pt;
			}
			else if (time > end)
			{
				return 1f;
			}
			else if (time < start)
			{
				return 0f;
			}
			return 0f;
		}

		/*
		[ContextButton(nameof(foo))]
		public bool m_Foo = false;
		public void foo()
		{
			if (trackDict.TryGetValue("avatar", out TrackAsset track))
			{
				foreach (var pa in GetAnimationPlayableAssets(track))
					Debug.Log($"We got {pa} - {pa.position}");
			}
		}
		*/

		public IEnumerable<AnimationPlayableAsset> GetAnimationPlayableAssets(TrackAsset track)
		{
			foreach (var clip in track.GetClips())
			{
				AnimationPlayableAsset ani = (AnimationPlayableAsset)clip.asset;
				if (ani != null)
					yield return ani;
			}
		}
		#endregion Binding
	}
}
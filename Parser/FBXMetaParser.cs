using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using UnityEditor;
using UnityEngine;
using Kit.Parser;

namespace Kit
{
    public class FBXMetaParser
    {
        public static bool TryGetMetaPath(Object obj, out string metaPath)
        {
            metaPath = null;
#if UNITY_EDITOR
            var assetPath = AssetDatabase.GetAssetPath(obj);
            var ext = Path.GetExtension(assetPath);
            if (ext.ToLower() != ".fbx")
                return false;
            var fullPath = Path.GetFullPath(assetPath);
            metaPath = $"{fullPath}.meta";
#endif
            return File.Exists(metaPath);
        }

        public static bool TryParser(Object obj, out FBXInfo info)
        {
            info = default;
            if (!TryGetMetaPath(obj, out var metaPath))
                return false;
            if (!File.Exists(metaPath))
                return false;
            var content = File.ReadAllText(metaPath);
            return TryParser(content, out info);
        }

        public static bool TryParser(string content, out FBXInfo info)
        {
            content = Regex.Unescape(content);
            content = content.Replace('\"', '"');

            return AnalysisContent(new Lexer(content), out info);
        }

        /// <summary>
        /// The information we needed for fbx meta.
        /// depend on U3D's fileFormatVersion: 2
        /// </summary>
        public class FBXInfo
        {
            public int fileFormatVersion;
            public string guid;
            public Clip[] clips;
            public class Clip
            {
                public string fileId; // - first:\n\t74:
                public string name; // second:
            }

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine($"fileFormatVersion: {fileFormatVersion}");
                sb.AppendLine($"guid: {guid}");
                sb.AppendLine($"clips: {clips.Length}");
                foreach (var clip in clips)
                {
                    sb.Append($"\tname: {clip.name}");
                    sb.AppendLine($"\tfileId: {clip.fileId}");
                }
                return sb.ToString();
            }
        }

        private static bool AnalysisContent(Lexer l, out FBXInfo info)
        {
            info = new FBXInfo();
            bool guidFound = false, clipsFound = false;

            l.SkipNewlineToken();
            while (l.NextToken())
            {
                if (l.token.IsIdentifier("fileFormatVersion") && l.NextToken())
                {
                    if (

                        l.token.IsOperator(":") && l.NextToken() &&
                        l.token.IsNumber() &&
                        int.TryParse(l.token.value, out int fileFormatVersion))
                    {
                        info.fileFormatVersion = fileFormatVersion;
                    }
                    else
                    {
                        throw new LexerException(l, "Fail to parse fileFormatVersion");
                    }
                }

                if (l.token.IsIdentifier("guid") && l.NextToken())
                {
                    if (l.token.IsOperator(":") && l.NextToken())
                    {
                        var guid = string.Empty;
                        while (!(l.token.IsSpace() || l.token.IsNewLine()))
                        {
                            guid += l.token.value;
                            l.NextToken(eSkipMethods.None);
                        }

                        info.guid = guid;
                        guidFound = true;
                    }
                    else
                    {
                        throw new LexerException(l, "Fail to parse guid");
                    }
                }

                if (l.token.IsIdentifier("ModelImporter"))
                {
                    info.clips = ModelImporterWalker(l);
                    clipsFound = true;
                    break; // for now that's all information we need.
                }
            }
            return guidFound && clipsFound;
        }

        private static FBXInfo.Clip[] ModelImporterWalker(Lexer l)
        {
            while (!l.token.IsIdentifier("internalIDToNameTable") && l.NextToken())
            { }

            if (l.token.IsIdentifier("internalIDToNameTable") &&
                l.NextToken() && l.token.IsOperator(":"))
            {
                // Start to read clip
            }

            List<FBXInfo.Clip> rst = new List<FBXInfo.Clip>(4);

            while (l.NextToken())
            {
                if (l.token.IsOperator("-") && l.NextToken())
                {
                    string fileId = "";
                    string clipName = string.Empty;
                    int cnt = 0;
                    if (
                        l.token.IsIdentifier("first") && l.NextToken() &&
                        l.token.IsOperator(":")       && l.NextToken() &&
                        l.token.IsNumber()            && l.NextToken() && // 74:
                        l.token.IsOperator(":")       && l.NextToken() &&
                        l.token.IsNumber())
                    {
                        fileId = l.token.value;
                        ++cnt;
                        l.NextToken();
                    }
                    if (l.token.IsIdentifier("second")    && l.NextToken() &&
                        l.token.IsOperator(":")           && l.NextToken() &&
                        l.token.IsIdentifier())
                    {
                        clipName = l.token.value;
                        ++cnt;
                    }

                    if (cnt != 2)
                    {
                        Debug.LogError($"Fail to parse clip, {l.token}");
                        break;
                    }
                    var clip = new FBXMetaParser.FBXInfo.Clip
                    {
                        fileId = fileId,
                        name = clipName,
                    };
                    rst.Add(clip);
                }
                else
                {
                    break;
                }
            }

            return rst.ToArray();
        }
    }
}
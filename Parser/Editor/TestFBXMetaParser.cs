using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
namespace Kit
{
    public class TestFBXMetaParser : EditorWindow
    {
        [MenuItem("Kit/Test/FBXMetaParser")]
        public static void Open()
        {
            TestFBXMetaParser window = GetWindow<TestFBXMetaParser>();
            window.Show();
        }
        public Object m_Obj = null;
        private void OnGUI()
        {
            m_Obj = EditorGUILayout.ObjectField(m_Obj, typeof(Object), false);

            var rawPath = AssetDatabase.GetAssetPath(m_Obj);
            var ext = Path.GetExtension(rawPath);
            if (ext.ToLower() != ".fbx")
            {
                EditorGUILayout.HelpBox("only support *.FBX file.", MessageType.Error);
                return;
            }

            var metaFound = FBXMetaParser.TryGetMetaPath(m_Obj, out string metaPath);
            if (!metaFound)
            {
                EditorGUILayout.HelpBox($"Meta not found :\n{metaPath}", MessageType.Error);
            }
            else
            {
                EditorGUILayout.HelpBox("Meta found", MessageType.Info);
            }

            if (GUILayout.Button("Search"))
            {
                if (!metaFound)
                {
                    EditorUtility.DisplayDialog("Error", "Meta data not found.", "Cancel");
                    return;
                }


                var content = File.ReadAllText(metaPath);
                FBXMetaParser.TryParser(content, out var fbxInfo);
                Debug.Log(fbxInfo);
            }
        }
    }
}
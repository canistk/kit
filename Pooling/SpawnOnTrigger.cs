﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    [RequireComponent(typeof(Collider))]
    public class SpawnOnTrigger : SpawnHandlerBase
    {
        private enum eTriggerMethod
        {
            Enter = 0,
            Stay,
            Leave,
        }
        [SerializeField] private eTriggerMethod m_TriggerMethod = eTriggerMethod.Enter;

        [Header("Stay - Extra")]
        [SerializeField, Kit.DrawIf(nameof(m_TriggerMethod), eTriggerMethod.Stay, ComparisonType.Equals, DisablingType.ReadOnly)]
        private float m_StayPeriod = 1f;

        [Header("Rules")]
        [SerializeField] private string m_CompareTag = "";
        [SerializeField] bool m_DisableSpawner = false;
        [SerializeField] float m_Cooldown = 0f;

        private Dictionary<GameObject, float> m_StayDict = new Dictionary<GameObject, float>(10);
        private float m_LastSpawn = 0f;


        private void OnEnable()
        {
            m_StayDict.Clear();
            m_LastSpawn = 0f;
        }

        private void FixedUpdate()
        {
            if (m_TriggerMethod != eTriggerMethod.Stay ||
                m_StayDict.Count == 0)
                return;
            DetectTriggerStay();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (m_StayDict.ContainsKey(other.gameObject))
                return;
            m_StayDict.Add(other.gameObject, Time.timeSinceLevelLoad);

            if (m_TriggerMethod == eTriggerMethod.Enter)
                InternalTrigger(other.gameObject);
            if (m_TriggerMethod == eTriggerMethod.Stay && m_StayPeriod <= 0f)
                InternalTrigger(other.gameObject);
        }

        private void OnTriggerExit(Collider other)
        {
            m_StayDict.Remove(other.gameObject);

            if (m_TriggerMethod == eTriggerMethod.Leave)
                InternalTrigger(other.gameObject);
        }

        private void DetectTriggerStay()
        {
            if (m_TriggerMethod != eTriggerMethod.Stay)
                throw new System.Exception();
            List<KeyValuePair<GameObject, float>> kvp = new List<KeyValuePair<GameObject, float>>(m_StayDict);
            float time = Time.timeSinceLevelLoad;
            int i = kvp.Count;
            while (i-- > 0)
            {
                if (!kvp[i].Key.activeInHierarchy)
                {
                    m_StayDict.Remove(kvp[i].Key);
                }
                else if (time - kvp[i].Value > m_Cooldown)
                {
                    InternalTrigger(kvp[i].Key);
                    m_StayDict[kvp[i].Key] = time; // reset time.
                }
            }
        }

        private void InternalTrigger(GameObject go)
        {
            if (string.IsNullOrEmpty(m_CompareTag) || // No tag
                go.CompareTag(m_CompareTag)) // Match tag
            {
                if (m_Spawner.AllowToSpawn(null) &&
                    Time.timeSinceLevelLoad - m_LastSpawn > m_Cooldown)
                {
                    m_LastSpawn = Time.timeSinceLevelLoad;
                    InternalSpawnToken();

                    if (m_DisableSpawner)
                        m_Spawner.gameObject.SetActive(false);
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class SpawnPeriodic : SpawnHandlerBase
    {
		[SerializeField] float m_Internal = 0.25f;

		private bool m_Started = false;
        private void Start()
        {
			m_Started = true;
			StartCoroutine(PeriodicUpdate());
		}

        private void OnEnable()
		{
			if (m_Started)
				StartCoroutine(PeriodicUpdate());
		}

		private IEnumerator PeriodicUpdate()
		{
			while (true)
			{
				while (m_Spawner.AllowToSpawn(null))
				{
					InternalSpawnToken();
					yield return new WaitForSeconds(m_Internal);
				}
				yield return null;
			}
		}
	}
}
namespace Kit
{
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ObjectPool), editorForChildClasses: true)]
    public class ObjectPoolEditor : EditorBase
    {
        private ObjectPool m_Target;
        private SerializedProperty settingsProperty;
        private FieldInfo templateField;
        private FieldInfo settingsField;

        protected override void OnEnable()
        {
            base.OnEnable();
            m_Target = (ObjectPool)target;
            settingsProperty = serializedObject.FindProperty("m_PrefabPreloadSettings");
            templateField = typeof(ObjectPool).GetField("m_PrefabPreloadSettingTemplate", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            settingsField = typeof(ObjectPool).GetField("m_PrefabPreloadSettings", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        }


        protected override void OnBeforeDrawGUI()
        {
            base.OnBeforeDrawGUI();

            if (!Application.isPlaying)
            {
                GameObject prefab = null;
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    prefab = (GameObject)EditorGUILayout.ObjectField("Drag to add prefab here", null, typeof(GameObject), allowSceneObjects: false);
                    if (check.changed && prefab != null)
                    {
                        ++settingsProperty.arraySize;
                        serializedObject.ApplyModifiedProperties();
                        ObjectPool.PrefabPreloadSetting originalSetting = (ObjectPool.PrefabPreloadSetting)templateField.GetValue(m_Target);
                        ObjectPool.PrefabPreloadSetting settingTemplate = originalSetting?.Clone() ?? new ObjectPool.PrefabPreloadSetting();
                        settingTemplate.m_Prefab = prefab;
                        settingTemplate.m_Name = prefab.name;
                        ObjectPool.PrefabPreloadSetting[] settings = (ObjectPool.PrefabPreloadSetting[])settingsField.GetValue(m_Target);
                        settings[settings.Length - 1] = settingTemplate;
                    }
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
using Kit.Photon;
#endif

namespace Kit
{
    [RequireComponent(typeof(Spawner))]
    public abstract class SpawnHandlerBase : MonoBehaviour
    {
        [SerializeField] protected Spawner m_Spawner = null;
#if PHOTON_UNITY_NETWORKING
        public bool m_IsRoomToken = false;
#endif

        private void Reset()
        {
            m_Spawner = GetComponent<Spawner>();
            if (m_Spawner != null)
                m_Spawner.Fetch();
        }

        protected void InternalSpawnToken()
        {
#if PHOTON_UNITY_NETWORKING
            m_Spawner.TrySpawn(m_IsRoomToken);
#else
			m_Spawner.TrySpawn();
#endif
        }
    }
}
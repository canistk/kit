﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
using Photon.Realtime;
using Kit.Avatar;
using Kit.Photon;
#endif
namespace Kit
{
    public class PhotonToken : PhotonTokenBase
		, ISpawnObject
    {
		protected ObjectPool_Bak pool = null;
		public void OnSpawned(ObjectPool_Bak pool)
		{
			this.pool = pool;
		}

		public void OnDespawned(ObjectPool_Bak pool)
		{

		}

		private void InternalDespawn()
		{
#if PHOTON_UNITY_NETWORKING
			// PhotonView is owner, despawn this.
			if (pool &&
				PhotonSpawnPool.hadInstance &&
				PhotonSpawnPool.Instance.pool == pool)
			{
				PhotonSpawnPool.Instance.Despawn(gameObject);
			}
#else
			if (pool && pool.IsSpawned(gameObject))
            {
				pool.Despawn(gameObject);
            }
			else
            {
				gameObject.SetActive(false);
            }
#endif
		}

#if PHOTON_UNITY_NETWORKING
		public override void OnPhotonInstantiate(PhotonMessageInfo info)
		{
			// base.OnPhotonInstantiate(info);
			if (info.photonView != null &&
				info.photonView.InstantiationData != null &&
				info.photonView.InstantiationData.Length > 0 &&
				info.photonView.InstantiationData[0] is Dictionary<string, object> dict)
			{
				PhotonSpawnInit(dict);
			}
		}

		private Dictionary<GameObject, HitBoxBase[]> s_HitBoxDict = new Dictionary<GameObject, HitBoxBase[]>();
		public override void PhotonSpawnInit(Dictionary<string, object> dict)
		{
			base.PhotonSpawnInit(dict);
			object obj;
			if (dict.TryGetValue(PhotonSpawnPool.DamageLayer, out obj) && obj is int maskNum)
			{
				var boxs = gameObject.GetComponentsInChildCache<HitBoxBase>(s_HitBoxDict, true);
				foreach(var box in boxs)
					box.SetDamageLayer((eDamageLayer)maskNum);
			}
			if (dict.TryGetValue(PhotonSpawnPool.Parent, out obj) && obj is int viewId)
            {
                PhotonView view = PhotonView.Find(viewId);
				gameObject.transform.SetParent(view.transform);
			}
		}
#endif
	}
}
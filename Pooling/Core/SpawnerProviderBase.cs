﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
using Kit.Photon;
#endif

namespace Kit
{
    /// <summary>To switch between 
    /// - <see cref="PhotonSpawnPool"/>
    /// - <see cref="ObjectPool_Bak"/></summary>
    public abstract class SpawnerProviderBase : MonoBehaviour
#if PHOTON_UNITY_NETWORKING
        , IPhotonTokenCallback
#endif
    {
#pragma warning disable 0414
        [ContextButton(nameof(TrySpawnOnPivot))]
        [SerializeField] bool m_DebugTrySpawn = false;
#pragma warning restore 0414

        [Header("Provider")]
#if PHOTON_UNITY_NETWORKING
        public bool m_PhotonPool = false;
        PhotonView m_ParentView;
        private NetworkTokenPath m_NetworkTokenPath;
#endif
        [SerializeField] private ObjectPool_Bak m_Pool = null;
        public string m_PoolName = "";
        public ObjectPool_Bak pool
        {
            get
            {
                if (m_IsAppQuit)
                {
                    Debug.LogWarning($"Try to access {name} on app quit.");
                    return null;
                }
#if PHOTON_UNITY_NETWORKING
                if (m_PhotonPool)
                    return PhotonSpawnPool.hadInstance ? PhotonSpawnPool.Instance.pool : null;
#endif
                // when it's not Photon spawn pool. normal object pool it is.
                if (m_Pool == null)
                {
                    m_Pool = PoolManager.GetOrCreatePool(m_PoolName);
                    if (m_Pool == null)
                        throw new System.Exception($"{name} fail to fetch/create pool {m_PoolName}.");
                }
                return m_Pool;
            }
		}

        [System.Serializable] public class SpawnLocationEvent : UnityEvent<GameObject, Vector3, Quaternion> { }
        [Header("Events")] public SpawnLocationEvent Event_SpawnLocation = new SpawnLocationEvent();

        protected void Awake()
        {
#if PHOTON_UNITY_NETWORKING
            m_ParentView = GetComponentInParent<PhotonView>();
            if (m_ParentView != null)
            {
                if (!NetworkTokenPath.TryGetTokenPath(transform, m_ParentView, out m_NetworkTokenPath))
                {
                    Debug.LogError($"Fail to create {nameof(NetworkTokenPath)}");
                }
            }
#endif
        }
    protected bool m_IsAppQuit { get; private set; } = false;
		private void OnApplicationQuit()
		{
            m_IsAppQuit = true;
        }

		/// <summary>Define pivot point to spawn at</summary>
		/// <param name="pos"></param>
		/// <param name="rotate"></param>
		protected abstract void GetPivot(out Vector3 pos, out Quaternion rotate, out object parent);
        public Vector3 GetPivot()
        {
            GetPivot(out Vector3 rst, out Quaternion ignore, out object obj);
            return rst;
		}
        /// <summary>Provide prefab to spawn</summary>
        /// <returns></returns>
        public abstract GameObject GetPrefab();

        protected abstract Dictionary<string, object> GetExtraData();

        /// <summary>Photontoken callback,
        /// sync between different client(s) so we can handle the spawn effect on local,
        /// instead of another network package.
        /// <see cref="IPhotonTokenCallback"/></summary>
        /// <param name="token"></param>
        /// <param name="extraData"></param>
        public virtual void OnTokenSpawned(GameObject token, Dictionary<string, object> extraData)
        {
            //if (extraData != null &&
            //    extraData.TryGetValue(PhotonSpawnPool.NetworkTokenCallback, out object task) &&
            //    task is string ccbJson)
            //{
            //    Debug.Log($"{nameof(OnTokenSpawned)} received from server.");
            //}
            if (token != null)
            {
                // Debug.Log($"{nameof(OnTokenSpawned)}, {token.name}");
                Event_SpawnLocation?.Invoke(token, token.transform.position, token.transform.rotation);
            }
        }

        public abstract bool AllowToSpawn(GameObject prefab = null);

        public virtual GameObject TrySpawn(
#if PHOTON_UNITY_NETWORKING
            bool isRoomToken = false,
            byte group = 0
#endif
            )
        {
            // Prefab
            GameObject prefab = GetPrefab();
            // Pos & rotate
            GetPivot(out Vector3 _pos, out Quaternion _rot, out object parent);

#if PHOTON_UNITY_NETWORKING
            return TrySpawn(prefab, _pos, _rot, isRoomToken, group, parent);
#else
            GameObject token = TrySpawn(prefab, _pos, _rot);
            if (token && parent is Transform p)
                token.transform.SetParent(p, true);
            return token;
#endif
        }

        public void TrySpawnOnPivot()
        {
            if (Application.isPlaying)
                TrySpawn();
        }

        public virtual GameObject TrySpawn(GameObject prefab, Vector3 _pos, Quaternion _rot
#if PHOTON_UNITY_NETWORKING
            ,
            bool isRoomToken = false,
            byte group = 0,
            object parent = null
#endif
            )
        {
            if (!AllowToSpawn(prefab))
                return null;

            Dictionary<string, object> data = GetExtraData();

#if PHOTON_UNITY_NETWORKING
            if (parent != null)
            {
                if (parent is PhotonView pViewId)
                {
                    if (!data.ContainsKey(PhotonSpawnPool.Parent))
                        data.Add(PhotonSpawnPool.Parent, pViewId);
                }
                else if (parent is Transform pTransform)
                {
                    Debug.LogWarning("Only PhotonView can sync via network.");
                }
                else
                {
                    throw new System.NotImplementedException("Only support Transfrom/PhotonViewId data type.");
                }
            }

            if (m_PhotonPool &&
                PhotonSpawnPool.hadInstance &&
                PhotonSpawnPool.Instance.pool != null)
            {
                var localPlayer = NetworkManager.instance.GetLocalPlayer();
                m_NetworkTokenPath.fromActor = NetworkManager.IsMultiplay && localPlayer != null ?
                        localPlayer.player.ActorNumber : -1;

                if (m_ParentView != null && m_NetworkTokenPath.hierarchyPath != null)
                    NetworkTokenPath.TryEmbedTokenPath(ref data, m_NetworkTokenPath); // this will call OnTokenSpawned
                object[] extraData = data != null && data.Count > 0 ? new object[] { data } : null;
                GameObject photonToken = PhotonSpawnPool.Instance.Spawn(prefab, _pos, _rot, isRoomToken, group, extraData);
                return photonToken;
            }
#endif
            
            /// when it's Photon, but didn't really using <see cref="PhotonSpawnPool"/>
            /// we manally go though all callback on local <see cref="IPhotonSpawnInitData"/>
            GameObject token = pool.Spawn(prefab, _pos, _rot);
#if PHOTON_UNITY_NETWORKING
            IPhotonSpawnInitData[] arr = token.GetComponentsCache(s_SpawnInitDict);
            if (arr != null)
            {
                foreach (IPhotonSpawnInitData s in arr)
                    if (s != null) s.PhotonSpawnInit(data);
            }
#endif
            /// Trigger <see cref="OnTokenSpawned(GameObject, Dictionary{string, object})"/>
            /// maintain the same flow as <see cref="IPhotonTokenCallback"/>
            OnTokenSpawned(token, data);
            return token;
        }

#if PHOTON_UNITY_NETWORKING
        private static Dictionary<GameObject, IPhotonSpawnInitData[]> s_SpawnInitDict = new Dictionary<GameObject, IPhotonSpawnInitData[]>();
#endif
    }
}
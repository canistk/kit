﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Kit
{
    public class PoolManager
    {
        #region APIs
        private static Dictionary<string, ObjectPool_Bak> poolDict = new Dictionary<string, ObjectPool_Bak>(10);
        public static IEnumerable<ObjectPool_Bak> GetObjectPools() => poolDict.Values;

        public static ObjectPool_Bak GetOrCreatePool(string poolId)
        {
            return GetOrCreatePool<ObjectPool_Bak>(poolId);
        }

        public static poolType GetOrCreatePool<poolType>(string poolId = "")
            where poolType : ObjectPool_Bak
        {
            if (string.IsNullOrEmpty(poolId))
                poolId = typeof(poolType).Name;
            ObjectPool_Bak pool = null;
            if (!poolDict.TryGetValue(poolId, out pool))
            {
                pool = new GameObject(poolId).AddComponent<poolType>();
            }

            return pool as poolType;
		}
        #endregion // APIs

        #region register
        internal static void Register(ObjectPool_Bak pool, out string poolId)
        {
            poolId = pool.name;
            // Giving the unique Id, if duplicate Id was found in memory.
            if (poolDict.TryGetValue(poolId, out ObjectPool_Bak existPool))
            {
                if (existPool == pool)
                    return; // same name & same referecne.
                else
                    poolId = LocateUniqueId(poolId);
            }
            poolDict.Add(poolId, pool);
        }

        internal static void Unregister(ObjectPool_Bak pool)
        {
            if (poolDict.ContainsValue(pool))
            {
                if (!poolDict.Remove(pool.name))
                {
                    foreach (var pair in poolDict)
                    {
                        if (pair.Value.Equals(pool))
                        {
                            poolDict.Remove(pair.Key);
                            break;
                        }
                    }
				}
			}
		}
		#endregion // register

		#region Unique Id
		private static Regex m_Regex = new Regex(@"^(.*\ )\(([0-9]+)\)$", RegexOptions.Singleline);
        private static string LocateUniqueId(string poolId)
        {
            if (poolDict.ContainsKey(poolId))
            {
                Match match = m_Regex.Match(poolId);
                if (match.Success)
                {
                    string numStr = match.Groups[2].Value;
                    if (!int.TryParse(numStr, out int num))
                        throw new UnityException("Regex logic error");
                    num++;
                    poolId = match.Groups[1] + $"({num.ToString()})";
                    return LocateUniqueId(poolId);
                }
                else
                {
                    return LocateUniqueId(poolId + " (0)");
				}
			}
            return poolId;
		}
        #endregion // Unique Id
    }
    [System.Serializable]
    public class ObjectPoolSetting
    {
        [SerializeField] public string m_PoolName = "";
        [SerializeField] private ObjectPool_Bak m_Pool = null;
        [SerializeField] private bool m_IsPhotonSpawnPool = false;
        public bool IsPhotonSpawnPool => m_IsPhotonSpawnPool;
        public ObjectPool_Bak pool
        {
            get
            {
                if (m_Pool == null)
                {
                    if (m_IsPhotonSpawnPool)
                    {
#if PHOTON_UNITY_NETWORKING
                        return Kit.Photon.PhotonSpawnPool.Instance.pool;
#endif
                    }
                    else
                    {
                        m_Pool = PoolManager.GetOrCreatePool(m_PoolName);
                    }
                }
                return m_Pool;
            }
        }
    }
}
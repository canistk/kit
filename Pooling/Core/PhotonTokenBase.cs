﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
using Kit.Photon;
#endif
namespace Kit
{

    public abstract class PhotonTokenBase : MonoBehaviour
#if PHOTON_UNITY_NETWORKING
        ,
        IPunInstantiateMagicCallback,
        IPhotonSpawnInitData
#endif
    {

#if PHOTON_UNITY_NETWORKING
        public abstract void OnPhotonInstantiate(PhotonMessageInfo info);

        /// <summary>call by <see cref="IPhotonSpawnInitData"/></summary>
        public virtual void PhotonSpawnInit(Dictionary<string, object> dict)
        {
            object task;
            if (dict.TryGetValue(PhotonSpawnPool.NetworkTokenCallback, out task) &&
                task is string ccbJson)
            {
                NetworkTokenPath networkTokenPath = JsonUtility.FromJson<NetworkTokenPath>(ccbJson);
                // Debug.Log($"{GetType().Name} : {networkTokenPath}");
                NetworkTokenPath.SpawnCallback(gameObject, networkTokenPath, dict);
            }
        }
#endif


        /// <summary>
        /// A common function to check if it's 
        /// </summary>
        /// <param name="pool"></param>
        /// <param name="token"></param>
        public static void Despawn(ObjectPool_Bak pool, GameObject token)
        {
            if (pool == null || token == null)
                return;
#if PHOTON_UNITY_NETWORKING
			// PhotonView is owner, despawn this.
			if (pool &&
				PhotonSpawnPool.hadInstance &&
				PhotonSpawnPool.Instance.pool == pool)
			{
				PhotonSpawnPool.Instance.Despawn(token);
			}
            else if (pool != null && pool.IsSpawned(token))
            {
                pool.Despawn(token);
            }
#else
            if (pool && pool.IsSpawned(token))
            {
                pool.Despawn(token);
            }
            else if (token.activeSelf)
            {
                Debug.LogWarning($"{token.name} didn't had spawn pool reference, disable instead.", token);
                token.SetActive(false);
            }
#endif
        }
    }

}
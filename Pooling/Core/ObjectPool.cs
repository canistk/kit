using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public interface ISpawnToken
    {
        public void OnSpawned(IDespawner pool);
        public void OnDespawn();
    }

    public interface IDespawner
    {
        public bool Despawn(GameObject token);
    }

    public class ObjectPool : MonoBehaviour, IDespawner
    {
        [SerializeField] private PrefabPreloadSetting m_PrefabPreloadSettingTemplate = new PrefabPreloadSetting();
        [SerializeField] protected PrefabSetting m_RuntimePrefabSettingTemplate = new PrefabSetting();
        [SerializeField] protected PrefabPreloadSetting[] m_PrefabPreloadSettings = { };

        public delegate void SpawnStatus(GameObject token, ObjectPool pool);
        public event SpawnStatus Event_Spawned;
        public event SpawnStatus Event_Despawned;
        public static event SpawnStatus Event_G_Spawned;
        public static event SpawnStatus Event_G_Despawned;

        #region System
        protected virtual void Awake()
        {
            InitPreload();

        }
        public bool IsDestroy { get; private set; } = false;
        protected virtual void OnDestroy()
        {
            IsDestroy = true;
        }
        protected static bool IsAppQuit { get; private set; } = false;
        protected void OnApplicationQuit()
        {
            IsAppQuit = true;
        }
        private void Update()
        {
            PreloadTaskUpdate();
        }
        #endregion System

        #region Prefab Preload Setting
        public enum eAwakeTiming
        {
            /// <summary>
            /// Awake but set to inactive on same cycle of preload.
            /// PS: Coroutine won't work.
            /// </summary>
            AwakeOnPreload = 0,
            /// <summary>
            /// Preload token inactively,
            /// the Awake will delay until it's first Active timing.
            /// </summary>
            DelayAwakeOnFirstSpawn
        }

        /// <summary>
        /// Behaviour when a spawn is requested but current pool has reached its maximum limit.
        /// </summary>
        public enum eCulling
        {
            /// <summary>
            /// Will spawn more as if there is no limit.
            /// </summary>
            NoPoolLimit,
            /// <summary>
            /// Do not spawn and returns null.
            /// </summary>
            ReturnsNull,
            /// <summary>
            /// Despawn the oldest instance and spawn it for current request.
            /// </summary>
            CycleOldest
        }

        /// <summary>
        /// Prefab active state when spawned on demand.
        /// </summary>
        public enum eRuntimeActiveStateOnPrefab
        {
            /// <summary>
            /// Keep original active state.
            /// </summary>
            Original,
            /// <summary>
            /// Set prefab to active when spawned on demand.
            /// </summary>
            ForceActive,
            /// <summary>
            /// Set prefab to inactive when spawned on demand.
            /// </summary>
            ForceInactive
        }

        public enum eSpawnMode
        {
            WorldCenter,
            OnlyParent,
            PosRot,
            PosRotParent
        }

        /// <summary>Preload Setting</summary>
        [System.Serializable]
        public class PrefabPreloadSetting : PrefabSetting
        {
            [Header("Preload")]
            [Tooltip("After Awake(), trigger auto preload in ? second.")]
            public float m_PreloadDelay = 0f;
            [Tooltip("The interval between each preload elements, distribute the performace overhead during GameObject.Instantiate")]
            public int m_PreloadFramePeriod = 0;
            [Tooltip("Auto preload prefab(s) base on giving amount")]
            public int m_PreloadAmount = 1;
            [Tooltip("To control the moment of token's active/inactive timing.")]
            public eAwakeTiming m_AwakeTiming = eAwakeTiming.AwakeOnPreload;
            // public bool m_IsPreloaded { get; set; }

            public new PrefabPreloadSetting Clone() => (PrefabPreloadSetting)MemberwiseClone();
        }
        /// <summary>Setting for spawn/despawn prefab behavior</summary>
        [System.Serializable]
        public class PrefabSetting
        {
            [Header("Reference")]
            [Tooltip("Name used for spawning the preloaded prefab(s), e.g. across network")]
            public string m_Name = string.Empty;
            public GameObject m_Prefab;
            [Header("Life cycle")]
            [Tooltip("Token active state after spawn. PS: Photon require inactive.")]
            public bool m_SpawnActive = true;
            [Tooltip("Ensure the token(s) will re-parent under current pool on hierachy level.")]
            public bool m_ReturnToPoolAfterDespawn = true;
            [Header("Overflow control")]
            [Tooltip("The maximum amount of the current pool.")]
            public int m_PoolLimit = 100;
            [Tooltip("Will block any further spawn request, if current pool was reach maximum limit.")]
            public eCulling m_CullOverLimit = eCulling.ReturnsNull;
            [Tooltip("Prefab active state when spawned on demand.")]
            public eRuntimeActiveStateOnPrefab m_RuntimeActiveStateOnPrefab = eRuntimeActiveStateOnPrefab.Original;
            internal void Validate()
            {
                if (m_Name.Length == 0 && m_Prefab != null)
                {
                    m_Name = m_Prefab.name;
                }
            }

            public PrefabSetting Clone() => (PrefabSetting)MemberwiseClone();
        }
        #endregion Prefab Preload Setting

        #region Prefab Cache
        protected struct TimedToken : IComparable<TimedToken>
        {
            public readonly float spawnTime;
            public readonly GameObject gameObject;
            public readonly PrefabCache prefabCache;
            public GameObject prefab => prefabCache.setting.m_Prefab;

            public TimedToken(in GameObject gameObject, in PrefabCache prefabCache)
            {
                this.spawnTime = Time.realtimeSinceStartup;
                this.gameObject = gameObject;
                this.prefabCache = prefabCache;
            }

            public int CompareTo(TimedToken other)
            {
                return spawnTime.CompareTo(other.spawnTime);
            }
            public void Despawn()
            {
                prefabCache.ReturnToken(this);
            }
        }

        /// <summary>Internal memory cache to handle spawn flow and keep instance reference</summary>
        protected class PrefabCache : IEquatable<GameObject>
        {
            public readonly PrefabSetting setting;
            private readonly Callback<TimedToken> returnTokenCallback;
            private MinHeap<TimedToken> activeObjs; // sort on spawned time
            private Queue<GameObject> deactiveObjs;
            private Transform transform;

            public int activeCount      => activeObjs.Count;
            public int deactiveCount    => deactiveObjs.Count;
            public int totalCount       => deactiveObjs.Count + activeObjs.Count;
            public GameObject prefab    => setting.m_Prefab;

            public PrefabCache(PrefabSetting setting, Transform transform, Callback<TimedToken> returnTokenCallback)
            {
                this.setting = setting;
                this.transform = transform;
                this.activeObjs = new MinHeap<TimedToken>(Mathf.Max(0, setting.m_PoolLimit));
                this.deactiveObjs = new Queue<GameObject>(Mathf.Max(0, setting.m_PoolLimit));
                this.returnTokenCallback = returnTokenCallback;
            }

            internal bool GetOrCreateToken(eSpawnMode mode, Vector3 position, Quaternion rotation, Transform parent, out TimedToken result)
            {
                result = default;

                if (deactiveObjs.Count > 0)
                {
                    // had deactive token
                    var _token = deactiveObjs.Dequeue();
                    result = RenewTokenStatus(_token, mode, position, rotation, parent);
                    return _token != null;
                }

                // handle reach pool limit

                if (setting.m_CullOverLimit != eCulling.NoPoolLimit && setting.m_PoolLimit == 0)
                {
                    Debug.LogError($"Setting Error, in {nameof(eCulling.CycleOldest)} mode, pool limit = 0, will not able to spawn anything.\nOverride pool limit to 1.");
                    setting.m_PoolLimit = 1;
                }
                bool overLimit = totalCount >= setting.m_PoolLimit;
                if (overLimit)
                {
                    // over limit
                    switch (setting.m_CullOverLimit)
                    {
                        case eCulling.ReturnsNull:
                            Debug.LogWarning($"Not enough token and culled over limit, consider increase pool limit.\n" +
                                $"Prefab = {setting.m_Name}, {setting.m_Prefab}, poolLimit={setting.m_PoolLimit}");
                            return false;
                        case eCulling.CycleOldest:
                            {
                                TimedToken oldest = default;
                                while (activeObjs.Count > 0)
                                {
                                    oldest = activeObjs.Poll();
                                    if (oldest.gameObject != null)
                                        break;
                                }
                                if (oldest.gameObject == null)
                                    throw new Exception("Fail to spawn while over limit & no token exist.");

                                var token = oldest.gameObject;
                                /// <see cref="AfterTokenReturn(TimedToken)"/>
                                returnTokenCallback?.Invoke(oldest);
                                result = RenewTokenStatus(token, mode, position, rotation, parent);
                                return token != null;
                            }
                        case eCulling.NoPoolLimit:
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }

                // New token required
                var _newToken = CreateRawToken(false);
                result = RenewTokenStatus(_newToken, mode, position, rotation, parent);
                return _newToken != null;

                TimedToken RenewTokenStatus(GameObject token, eSpawnMode mode, Vector3 position, Quaternion rotation, Transform parent)
                {
                    switch (mode)
                    {
                        case eSpawnMode.WorldCenter:
                            token.transform.SetParent(null);
                            token.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
                            break;
                        case eSpawnMode.OnlyParent:
                            token.transform.SetParent(parent, false);
#if UNITY_2023_1_OR_NEWER
                            token.transform.SetLocalPositionAndRotation(Vector3.zero, Quaternion.identity);
#else
                            token.transform.localPosition = Vector3.zero;
                            token.transform.localRotation = Quaternion.identity;
#endif
                            break;
                        case eSpawnMode.PosRot:
                            token.transform.SetPositionAndRotation(position, rotation);
                            break;
                        case eSpawnMode.PosRotParent:
                            token.transform.SetParent(parent, false);
                            token.transform.SetPositionAndRotation(position, rotation);
                            break;
                        default:
                            throw new NotImplementedException();
                    }

                    // handle token active state on spawn
                    bool orgActiveStateOnPrefab = setting.m_Prefab.activeSelf;
                    bool runtimeActiveState = setting.m_RuntimeActiveStateOnPrefab switch
                    {
                        eRuntimeActiveStateOnPrefab.Original => orgActiveStateOnPrefab,
                        eRuntimeActiveStateOnPrefab.ForceActive => true,
                        eRuntimeActiveStateOnPrefab.ForceInactive => false,
                        _ => throw new System.NotImplementedException()
                    };
                    token.SetActive(runtimeActiveState);
                    var timedToken = new TimedToken(token, this);
                    activeObjs.Add(timedToken);
                    return timedToken;
                }
            }

            internal GameObject CreateRawToken(bool preloadDeactive)
            {
                if (setting.m_Prefab == null)
                    throw new NullReferenceException();

                // Handle prefab/token init active state.
                var prefab = setting.m_Prefab;
                prefab.hideFlags = HideFlags.DontSave;
                bool orgState = prefab.activeSelf;
                prefab.SetActive(false);

                GameObject token = Instantiate(prefab, transform) as GameObject;
                prefab.SetActive(orgState);
                
                token.name = token.name.Replace("(Clone)", $"[{totalCount + 1:0000}]");
                if (preloadDeactive)
                    deactiveObjs.Enqueue(token);
                return token;
            }

            public void ReturnToken(TimedToken timedToken)
            {
                if (timedToken.gameObject == null)
                    throw new Exception("PrefabCache:Error token is null.");
                if (timedToken.prefabCache != this)
                    throw new Exception("PrefabCache:Error token isn't spawn by this.");
                var token = timedToken.gameObject;
                if (!activeObjs.Delete(timedToken))
                    throw new Exception($"PrefabCache:Error fail to {nameof(ReturnToken)}, since {token} didn't belong this pool.");

                token.SetActive(false);
                if (setting.m_ReturnToPoolAfterDespawn)
                {
                    token.transform.SetParent(transform);
                }
                deactiveObjs.Enqueue(token);

                returnTokenCallback?.Invoke(timedToken);
                timedToken = default;
            }

            internal bool IsDespawnedObject(GameObject go)
            {
                return deactiveObjs.Contains(go);
            }

            public bool Equals(GameObject other)
            {
                return this.prefab == other;
            }
        }

        private List<PrefabCache> m_PrefabCaches = new List<PrefabCache>();
        private bool TryGetCacheByPrefab(GameObject prefab, out PrefabCache prefabCache)
        {
            prefabCache = default;
            if (prefab == null)
                return false;
            for (int i=0; i<m_PrefabCaches.Count; ++i)
            {
                if (!m_PrefabCaches[i].Equals(prefab))
                    continue;
                prefabCache = m_PrefabCaches[i];
                return true;
            }
            return false;
        }

        private Dictionary<GameObject /* token */, TimedToken> m_ActiveTokens = new Dictionary<GameObject, TimedToken>();
        private bool TryGetCacheByActiveToken(GameObject token, out PrefabCache prefabCache)
        {
            prefabCache = default;
            if (token == null)
                return false;
            if (!m_ActiveTokens.TryGetValue(token, out var timedToken))
                return false;

            prefabCache = timedToken.prefabCache;
            return prefabCache.prefab != null;
        }

        private PrefabCache CreatePrefabCacheByTemplate(GameObject prefab)
        {
            if (TryGetCacheByPrefab(prefab, out var exist))
                return exist;

            var template = m_PrefabPreloadSettingTemplate.Clone();
            template.m_Prefab = prefab;
            template.m_Name = prefab.name;

            PrefabCache cache = new PrefabCache(template, transform, AfterTokenReturn);
            return cache;
        }
        private PrefabCache CreatePrefabCache(PrefabPreloadSetting setting)
        {
            var prefab = setting.m_Prefab;
            if (prefab == null)
            {
                Debug.LogError("Prefab cannot be found. skip preload.", this);
                return default;
            }
            if (TryGetCacheByPrefab(setting.m_Prefab, out var exist))
            {
                Debug.LogError($"Duplicate preload setting on prefab {setting.m_Prefab}, keep previous setting.", this);
                return exist;
            }
            Debug.LogWarning($"New PrefabCache created {setting.m_Name}, {prefab}");
            PrefabCache newPrefabCache = new PrefabCache(setting, transform, AfterTokenReturn);
            m_PrefabCaches.Add(newPrefabCache);
            return newPrefabCache;
        }
        #endregion Prefab Cache

        #region Preload Task
        private List<Kit.kTask> m_PreloadTasks = new List<Kit.kTask>(8);
        private bool m_Init = false;
        private void InitPreload()
        {
            if (m_Init)
                return;
            m_Init = true;
            foreach (var setting in m_PrefabPreloadSettings)
            {
                if (TryGetCacheByPrefab(setting.m_Prefab, out var cache))
                    continue;
                cache = CreatePrefabCache(setting);
                m_PreloadTasks.Add(new PreloadTask(setting, cache));
            }
        }
        private void PreloadTaskUpdate()
        {
            int i = m_PreloadTasks.Count;
            while (i-- > 0)
            {
                if (m_PreloadTasks[i] == null)
                    m_PreloadTasks.RemoveAt(i);

                if (!m_PreloadTasks[i].Execute())
                    m_PreloadTasks.RemoveAt(i);
            }
        }
        private class PreloadTask : Kit.TaskWithState
        {
            private readonly PrefabPreloadSetting m_Setting;
            private readonly PrefabCache m_Cache;
            private float   m_StartTime;
            private int     m_Tick;
            public PreloadTask(PrefabPreloadSetting setting, PrefabCache cache)
            {
                this.m_Setting = setting;
                this.m_Cache = cache;
            }

            protected override void OnEnter()
            {
                m_StartTime = Time.timeSinceLevelLoad;
                m_Tick = 0;
            }

            protected override bool ContinueOnNextCycle()
            {
                if (m_Cache.totalCount >= m_Setting.m_PreloadAmount)
                    return false; // enough - exit

                var diff = Time.timeSinceLevelLoad - m_StartTime;
                if (diff < m_Setting.m_PreloadDelay)
                    return true; // wait

                if (m_Tick++ < m_Setting.m_PreloadFramePeriod)
                    return true; // skip this frame.

                m_Cache.CreateRawToken(preloadDeactive: true);
                m_Tick = 0;
                return true;
            }

            protected override void OnComplete() { }
        }
        #endregion Preload Task

        #region Spawn Despawn
        /// <summary>Check if the giving gameobject was spawned by this spawn pool</summary>
        /// <param name="go">giving gameobject</param>
        /// <returns></returns>
        public bool IsSpawned(GameObject go) => m_ActiveTokens.ContainsKey(go);

        public IEnumerable<GameObject> GetSpawnedTokens() => m_ActiveTokens.Keys;

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
            => InternalSpawn(prefab, position, rotation, parent);

        private GameObject InternalSpawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        {
            if (!TryGetCacheByPrefab(prefab, out var prefabCache))
                prefabCache = CreatePrefabCacheByTemplate(prefab);

            if (!prefabCache.GetOrCreateToken(eSpawnMode.PosRotParent, position, rotation, parent, out var timedToken))
                Debug.LogError($"Unknown reason, fail to spawn {prefab}");
            m_ActiveTokens.Add(timedToken.gameObject, timedToken);

            ISpawnToken[] comps = GetComponentsInChildren<ISpawnToken>();
            foreach (var comp in comps)
            {
                if (comp == null)
                    continue;
                comp.OnSpawned(this);
            }

            if (Event_Spawned != null)
            {
                var handles = Event_Spawned.GetInvocationList();
                foreach (var handle in handles)
                {
                    try
                    {
                        var dispatcher = (SpawnStatus)handle;
                        dispatcher.Invoke(timedToken.gameObject, this);
                    }
                    catch(System.Exception ex)
                    {
                        ex.DeepLogInvocationException($"{nameof(ObjectPool)}", handle);
                    }
                }
            }
            if (Event_G_Spawned != null)
            {
                var handles = Event_G_Spawned.GetInvocationList();
                foreach (var handle in handles)
                {
                    try
                    {
                        var dispatcher = (SpawnStatus)handle;
                        dispatcher.Invoke(timedToken.gameObject, this);
                    }
                    catch (System.Exception ex)
                    {
                        ex.DeepLogInvocationException($"{nameof(ObjectPool)}", handle);
                    }
                }
            }
            return timedToken.gameObject;
        }

        public bool Despawn(GameObject token)
            => InternalDespawn(token);

        private bool InternalDespawn(GameObject token)
        {
            if (token == null)
            {
                Debug.LogError("Token is null, cannot be despawn");
                return false;
            }
            if (!m_ActiveTokens.TryGetValue(token, out TimedToken timedToken))
            {
                foreach(var pc in m_PrefabCaches)
                {
                    if (pc.IsDespawnedObject(token))
                    {
                        Debug.LogError($"Token {token} already despawned!");
                        return false;
                    }
                }

                Debug.LogError($"Token {token} is not spawned by this pool.");
                return false;
            }

            var comps = token.GetComponentsInChildren<ISpawnToken>();
            foreach (var comp in comps)
            {
                if (comp == null)
                    continue;
                comp.OnDespawn();
            }
            timedToken.Despawn();
            return true;
        }

        private void AfterTokenReturn(TimedToken timedToken)
        {
            if (!m_ActiveTokens.Remove(timedToken.gameObject))
            {
                Debug.LogError($"Token handle error, try to return a non-exist token.");
                return;
            }
            if (Event_Despawned != null)
            {
                var handles = Event_Despawned.GetInvocationList();
                foreach (var handle in handles)
                {
                    try
                    {
                        var dispatcher = (SpawnStatus)handle;
                        dispatcher.Invoke(timedToken.gameObject, this);
                    }
                    catch (System.Exception ex)
                    {
                        ex.DeepLogInvocationException($"{nameof(ObjectPool)}", handle);
                    }
                }
            }
            if (Event_G_Despawned != null)
            {
                var handles = Event_G_Despawned.GetInvocationList();
                foreach (var handle in handles)
                {
                    try
                    {
                        var dispatcher = (SpawnStatus)handle;
                        dispatcher.Invoke(timedToken.gameObject, this);
                    }
                    catch (System.Exception ex)
                    {
                        ex.DeepLogInvocationException($"{nameof(ObjectPool)}", handle);
                    }
                }
            }
        }

        #endregion Spawn Despawn

    }
}
﻿#if PHOTON_UNITY_NETWORKING
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Kit;
using System.Collections;

namespace Kit.Photon
{
	public class PhotonSpawnPool : MonoBehaviour, IPunPrefabPool
	{
		public const string Tag = "tag";
		public const string Parent = "parent";
		public const string NetworkTokenCallback = "NetworkTokenCallback";
		public const string DamageLayer = "eDamageLayer"; //nameof(Kit.Avatar.eDamageLayer);

		[SerializeField] ObjectPool m_Pool = null;
		public ObjectPool pool => m_Pool;
		private static PhotonSpawnPool m_Instance;
		public static PhotonSpawnPool Instance
		{
			get
			{
				if (m_Instance == null)
				{
					m_Instance = GameObject.FindObjectOfType<PhotonSpawnPool>();
					if (m_Instance != null)
						PhotonNetwork.PrefabPool = m_Instance; // register
					else
						Debug.LogWarning($"Fail to locate {nameof(PhotonSpawnPool)} on scene.");
				}
				return m_Instance;
			}
		}
		public static bool hadInstance => m_Instance != null;

		public GameObject[] m_SpawnInRoomPrefab = { };
		private HashSet<GameObject> m_RoomPrefabs = new HashSet<GameObject>();
		protected void Awake()
		{
			if (m_Instance == null)
			{
				m_Instance = this;
				// DontDestroyOnLoad(m_Instance.gameObject); // avoid destroy on change scene.
				PhotonNetwork.PrefabPool = m_Instance; // register
			}
			m_RoomPrefabs = new HashSet<GameObject>(m_SpawnInRoomPrefab);
		}



		#region PhotonNetwork Implement
		/// <summary>
		/// An API called by <see cref="PhotonNetwork.Instantiate(string, Vector3, Quaternion, byte, object[])"/>
		/// since we are register as <see cref="IPunPrefabPool"/>
		/// </summary>
		public GameObject Instantiate(string prefabId, Vector3 position, Quaternion rotation)
		{
			GameObject go = pool.Spawn(prefabId, position, rotation);
			// PhotonView pv = go.GetComponentCache(s_TokenSpawnByThisClientPV);
			return go;
		}

		/// <summary>
		/// An API called by <see cref="PhotonNetwork.Destroy(GameObject)"/>
		/// </summary>
		public void Destroy(GameObject go)
		{
			PhotonView pv = go.GetComponentCache(s_TokenSpawnByThisClientPV);
			if (pv && pv.IsMine)
				PhotonNetwork.RemoveRPCs(pv);

			if (pool.IsSpawned(go))
				pool.Despawn(go);
			// Debug.LogError($"Fail to despawn via network, {go} was not managed by {GetType().Name}", this);
		}
		#endregion PhotonNetwork Implement


		private static Dictionary<GameObject, PhotonView> s_TokenSpawnByThisClientPV = new Dictionary<GameObject, PhotonView>();
		public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, bool isRoomToken = false, byte group = 0, object[] extraData = null)
		{
			if (pool.TryGetPrefabName(prefab, out string prefabId))
			{
				GameObject go;
				if (PhotonNetwork.InRoom && (isRoomToken || m_RoomPrefabs.Contains(prefab)))
					go = PhotonNetwork.InstantiateRoomObject(prefabId, position, rotation, group, extraData);
				else
					go = PhotonNetwork.Instantiate(prefabId, position, rotation, group, extraData);
				return go;
			}
			else
			{
				Debug.LogError($"{nameof(PhotonSpawnPool)} spawn fail, [{prefab}] - config not found, please ensure the prefab are setup correctly.");
			}
			return null;
		}

		public bool IsSpawnByThisClient(GameObject go)
		{
			return pool.IsSpawned(go);
		}

		public void Despawn(GameObject go)
		{
			if (go == null)
			{
				Debug.LogError($"Trying to despawn {go}");
			}
			else if (pool.IsSpawned(go))
			{
				PhotonNetwork.Destroy(go);
				/// sync server & clients <see cref="Destroy(GameObject)"/>
			}
			// else if (pool.IsSpawned(go)), can be spawn by others.
			// else, Ignore that request, since Photon not allow us to despawn it.
		}
	}

	public interface IPhotonSpawnInitData
	{
		void PhotonSpawnInit(Dictionary<string, object> dict);
	}

	public interface IPhotonTokenCallback
	{
		void OnTokenSpawned(GameObject token, Dictionary<string, object> extraData);
	}

	public struct NetworkTokenPath
    {
		public int photonViewId;
		public string[] hierarchyPath;
		public int fromActor;

		/// <summary><see cref="IPhotonSpawnInitData"/></summary>
		/// <param name="receiver"></param>
		/// <param name="parentPV"></param>
		/// <param name="spawnInitData"></param>
		/// <returns></returns>
		public static bool TryEmbedTokenPath(Transform receiver, PhotonView parentPV, ref Dictionary<string, object> spawnInitData)
        {
			if (!receiver.IsChildOf(parentPV.transform))
			{
				Debug.LogError($"Can only embed {nameof(NetworkTokenPath)} when it's direct parent.", receiver);
				return false;
			}
			if (spawnInitData != null)
			{
				if (!spawnInitData.TryGetValue(PhotonSpawnPool.NetworkTokenCallback, out object task))
				{
					if (TryGetTokenPath(receiver, parentPV, out NetworkTokenPath networkTokenPath) &&
						TryEmbedTokenPath(ref spawnInitData, networkTokenPath))
					{
						return true;
					}
					else
					{
						Debug.LogError($"Fail to {nameof(TryGetTokenPath)}");
					}
				}
				else
				{
					Debug.LogError($"Giving dictionary already contain {nameof(PhotonSpawnPool.NetworkTokenCallback)}");
				}
			}
			else
			{
				spawnInitData = new Dictionary<string, object>();
			}
			return false;
        }

		public static bool TryEmbedTokenPath(ref Dictionary<string, object> spawnInitData, in NetworkTokenPath networkTokenPath)
		{
			if (spawnInitData != null)
			{
				if (spawnInitData.ContainsKey(PhotonSpawnPool.NetworkTokenCallback))
				{
					Debug.LogError($"Giving dictionary already contain {nameof(PhotonSpawnPool.NetworkTokenCallback)}");
					return false;
				}
				else if (networkTokenPath.hierarchyPath == null)
				{
					Debug.LogError($"invalid {nameof(NetworkTokenPath)}");
					return false;
				}
			}
			else
            {
				spawnInitData = new Dictionary<string, object>();
			}
			string json = JsonUtility.ToJson(networkTokenPath);
			spawnInitData.Add(PhotonSpawnPool.NetworkTokenCallback, json);
			return true;
		}

		public static bool TryGetTokenPath(Transform receiver, PhotonView parentPV, out NetworkTokenPath networkTokenPath)
		{
			if (!receiver.IsChildOf(parentPV.transform))
			{
				Debug.LogError($"Can only embed {nameof(NetworkTokenPath)} when it's direct parent.", receiver);
				networkTokenPath = default;
				return false;
			}

			//int actorId = NetworkManager.instance.GetLocalPlayer().player.ActorNumber;
			networkTokenPath = new NetworkTokenPath
			{
				//fromActor = actorId,
				photonViewId = parentPV.ViewID,
				hierarchyPath = receiver.GetPathToParent(parentPV.transform),
			};
			return true;
		}

		/// <summary><see cref="IPhotonTokenCallback"/></summary>
		/// <param name="gameObject"></param>
		/// <param name="ccb"></param>
		/// <param name="dict"></param>
		public static void SpawnCallback(GameObject gameObject, in NetworkTokenPath ccb, in Dictionary<string, object> dict)
		{
			var photonView = PhotonView.Find(ccb.photonViewId);
			if (photonView == null)
			{
				Debug.LogError($"{nameof(NetworkTokenPath)} Unknown vid={ccb.photonViewId}");
			}
			else
			{
				var callbacks = photonView.transform.GetComponentsInChildren<IPhotonTokenCallback>(true);
				if (callbacks.Length == 0)
				{
					Debug.LogError($"{nameof(NetworkTokenPath)} Fail to locate vid={ccb.photonViewId}, {string.Join(",", ccb.hierarchyPath)}");
				}
				else if (callbacks.Length == 1)
				{
					// common case that only had one callback.
					// Server & client both receive this callback.
					Debug.Log($"Attempt trigger OnTokenSpawned on 0-{callbacks[0]}");
					callbacks[0].OnTokenSpawned(gameObject, dict);
				}
				else
				{
					Transform target = photonView.transform.FindChildByPath(ccb.hierarchyPath, false);
					if (target == null)
						Debug.LogError($"{nameof(NetworkTokenPath)} Fail to locate vid={ccb.photonViewId}, {string.Join(",", ccb.hierarchyPath)}");
					else
					{
						bool found = false;
						for (int i = 0; i < callbacks.Length && !found; i++)
						{
							if (callbacks[i] is Component comp &&
								comp.transform == target)
							{
								// Debug.Log($"Attempt trigger OnTokenSpawned on {i}-{callbacks[i]}");
								callbacks[i].OnTokenSpawned(gameObject, dict);
								found = true;
							}
						}
						if (!found)
							Debug.LogError($"{nameof(NetworkTokenPath)} Logic error, no callback target.", gameObject);
					}
				}
			}
		}
        public override string ToString()
        {
			return $"[{nameof(NetworkTokenPath)} actor {fromActor}, PV = {photonViewId}, {string.Join(",", hierarchyPath)}]";
        }
    }

}
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;

[RequireComponent(typeof(ObjectPool))]
public class TestObjectPoolV2 : MonoBehaviour
{
    [SerializeField] ObjectPool m_Pool;
    private ObjectPool pool
    {
        get
        {
            if (m_Pool == null)
            {
                m_Pool = GetComponent<ObjectPool>();
            }
            return m_Pool;
        }
    }

    [SerializeField] GameObject[] m_Prefabs = null;
    [SerializeField] Transform m_SpawnPoint = null;
    [SerializeField] Transform m_SpawnParent = null;

    List<GameObject> m_Spawned = new List<GameObject>();
    void Update()
    {
        if (m_Prefabs == null)
            return;

        if (Input.GetKeyUp(KeyCode.Space))
        {
            var s = m_SpawnPoint;
            var prefab = m_Prefabs[Random.Range(0, m_Prefabs.Length)];
            if (prefab != null)
            {
                var token = pool.Spawn(prefab, s.position, s.rotation, m_SpawnParent);
                m_Spawned.Add(token);
            }
        }

        if (Input.GetKey(KeyCode.Delete) && m_Spawned.Count > 0)
        {
            int rnd = Random.Range(0, m_Spawned.Count);
            var toDespawn = m_Spawned[rnd];
            m_Spawned.Remove(toDespawn);
            pool.Despawn(toDespawn);
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    [System.Obsolete("Use ObjectPool")]
    public class ObjectPool_Bak : MonoBehaviour
    {
        [SerializeField] private PrefabPreloadSetting m_PrefabPreloadSettingTemplate = new PrefabPreloadSetting();
        [SerializeField] protected PrefabSetting m_RuntimePrefabSettingTemplate = new PrefabSetting();
        [SerializeField] protected PrefabPreloadSetting[] m_PrefabPreloadSettings = { };

        #region Data structure
        /// <summary>Preload Setting</summary>
        [System.Serializable]
        public class PrefabPreloadSetting : PrefabSetting
        {
            [Header("Preload")]
            [Tooltip("After Awake(), trigger auto preload in ? second.")]
            public float m_PreloadDelay = 0f;
            [Tooltip("The interval between each preload elements, distribute the performace overhead during GameObject.Instantiate")]
            public int m_PreloadFramePeriod = 0;
            [Tooltip("Auto preload prefab(s) base on giving amount")]
            public int m_PreloadAmount = 1;
            [Tooltip("To control the moment of token's active/inactive timing.")]
            public eAwakeTiming m_AwakeTiming = eAwakeTiming.AwakeOnPreload;
            public bool m_IsPreloaded { get; set; }

            public new PrefabPreloadSetting Clone() => (PrefabPreloadSetting)MemberwiseClone();
        }

        public enum eAwakeTiming
        {
            /// <summary>
            /// Awake but set to inactive on same cycle of preload.
            /// PS: Coroutine won't work.
            /// </summary>
            AwakeOnPreload = 0,
            /// <summary>
            /// Preload token inactively,
            /// the Awake will delay until it's first Active timing.
            /// </summary>
            DelayAwakeOnFirstSpawn
        }

        /// <summary>
        /// Behaviour when a spawn is requested but current pool has reached its maximum limit.
        /// </summary>
        public enum eCulling
        {
            /// <summary>
            /// Will spawn more as if there is no limit.
            /// </summary>
            NoPoolLimit,
            /// <summary>
            /// Do not spawn and returns null.
            /// </summary>
            ReturnsNull,
            /// <summary>
            /// Despawn the oldest instance and spawn it for current request.
            /// </summary>
            CycleOldest
        }

        /// <summary>
        /// Prefab active state when spawned on demand.
        /// </summary>
        public enum eRuntimeActiveStateOnPrefab
        {
            /// <summary>
            /// Keep original active state.
            /// </summary>
            Original,
            /// <summary>
            /// Set prefab to active when spawned on demand.
            /// </summary>
            ForceActive,
            /// <summary>
            /// Set prefab to inactive when spawned on demand.
            /// </summary>
            ForceInactive
        }

        /// <summary>Setting for spawn/despawn prefab behavior</summary>
        [System.Serializable]
        public class PrefabSetting
        {
            [Header("Reference")]
            [Tooltip("Name used for spawning the preloaded prefab(s), e.g. across network")]
            public string m_Name = string.Empty;
            public GameObject m_Prefab;
            [Header("Life cycle")]
            [Tooltip("Token active state after spawn. PS: Photon require inactive.")]
            public bool m_SpawnActive = true;
            [Tooltip("Ensure the token(s) will re-parent under current pool on hierachy level.")]
            public bool m_ReturnToPoolAfterDespawn = true;
            [Header("Overflow conktrol")]
            [Tooltip("The maximum amount of the current pool.")]
            public int m_PoolLimit = 100;
            [Tooltip("Will block any further spawn request, if current pool was reach maximum limit.")]
            public eCulling m_CullOverLimit = eCulling.ReturnsNull;
            [Tooltip("Prefab active state when spawned on demand.")]
            public eRuntimeActiveStateOnPrefab m_RuntimeActiveStateOnPrefab = eRuntimeActiveStateOnPrefab.Original;
            internal void Validate()
            {
                if (m_Name.Length == 0 && m_Prefab != null)
                {
                    m_Name = m_Prefab.name;
                }
            }

            public PrefabSetting Clone() => (PrefabSetting)MemberwiseClone();
        }

        protected class TimedGameObject : IComparable<TimedGameObject>
        {
            public float time { get; private set; }
            public GameObject gameObject { get; private set; }
            public GameObject prefab { get; private set; }

            public TimedGameObject(float time, GameObject gameObject, GameObject prefab)
            {
                this.time = time;
                this.gameObject = gameObject;
                this.prefab = prefab;
            }

            public int CompareTo(TimedGameObject other)
            {
                return time.CompareTo(other.time);
            }
        }
        /// <summary>Internal memory cache to handle spawn flow and keep instance reference</summary>
        protected class TokenCache
        {
            public PrefabSetting setting;

            public MinHeap<TimedGameObject> activeObjs; // sort on spawned time
            public Queue<GameObject> deactiveObjs;
            public int totalCount => deactiveObjs.Count + activeObjs.Count;
            public TokenCache(PrefabSetting setting)
            {
                this.setting = setting;
                activeObjs = new MinHeap<TimedGameObject>(Mathf.Max(0, setting.m_PoolLimit));
                deactiveObjs = new Queue<GameObject>(Mathf.Max(0, setting.m_PoolLimit));
            }
            internal void Clear()
            {
                activeObjs.Clear();
                deactiveObjs.Clear();
            }
        }

        private Dictionary<GameObject, TokenCache> _cacheDict = null;
        protected Dictionary<GameObject /* prefab */, TokenCache> m_CacheDict
        {
            get
            {
                if (_cacheDict == null)
                {
                    _cacheDict = new Dictionary<GameObject, TokenCache>(10);
                }

                return _cacheDict;
            }
        }

        /// <summary>The table to increase the speed to tracking the token and it's prefab group.</summary>
        private Dictionary<GameObject /* token */, TimedGameObject /* prefab */> m_SpawnedObjs = new Dictionary<GameObject, TimedGameObject>();
        private Dictionary<GameObject /* token */ , ISpawnObject[]> m_TokenSpawnDict = new Dictionary<GameObject, ISpawnObject[]>();

        public delegate void SpawnStatus(GameObject token, GameObject prefab);
        public static event SpawnStatus Event_G_Spawned;
        public event SpawnStatus Event_Spawned;
        public static event SpawnStatus Event_G_Despawned;
        public event SpawnStatus Event_Despawned;
        #endregion // Data structure

        #region U3D Cycle
        private void OnValidate()
        {
            for (int i = 0; i < m_PrefabPreloadSettings.Length; i++)
            {
                m_PrefabPreloadSettings[i].Validate();
            }
        }

        protected virtual void Awake()
        {
            Initialize();
            TriggerPreloadHandler();
        }

        private void OnDestroy()
        {
            PoolManager.Unregister(this);
            m_SpawnedObjs.Clear();
            foreach (TokenCache cache in m_CacheDict.Values)
            {
                cache.Clear();
            }

            m_CacheDict.Clear();
        }
        #endregion // U3D Cycle

        #region Preload Token
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public void Editor_AddPreloadPrefab(GameObject _prefab)
        {
            if (_prefab == null)
                return;
            if (_prefab.transform.parent == null)
            {
                bool found = false;
                foreach(var setting in m_PrefabPreloadSettings)
                {
                    if (setting.m_Prefab == _prefab)
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                {
                    Debug.Log($"{name} already contain prefab {_prefab}");
                }
                else
                {
                    Debug.Log($"{name} add preload prefab = {_prefab.name}");
                    int end = m_PrefabPreloadSettings.Length + 1;
                    Array.Resize(ref m_PrefabPreloadSettings, end);
                    var template = m_PrefabPreloadSettingTemplate.Clone();
                    template.m_Prefab = _prefab;
                    template.m_Name = _prefab.name;
                    m_PrefabPreloadSettings[end - 1] = template;
                    KitBox.Editor_SetDirty(this);
                }
            }
            // m_PrefabPreloadSettings
        }
        public void PreloadPrefab(PrefabPreloadSetting setting)
        {
            if (setting.m_Prefab == null)
            {
                Debug.LogError($"Fail to preload {setting}, missing prefab", this);
            }
            else
            {
                StartCoroutine(PreloadHandler(setting));
            }
        }

        private void TriggerPreloadHandler()
        {
            for (int i = 0; i < m_PrefabPreloadSettings.Length; i++)
            {
                if (m_PrefabPreloadSettings[i].m_Prefab == null)
                {
                    Debug.LogError($"Fail to preload index {i}, missing prefab", this);
                }
                else
                {
                    StartCoroutine(PreloadHandler(m_PrefabPreloadSettings[i]));
                }
            }
        }

        private IEnumerator PreloadHandler(PrefabPreloadSetting setting)
        {
            if (setting.m_IsPreloaded)
            {
                yield break;
            }

            setting.m_IsPreloaded = true;
            LocateOrCreateCache(setting.m_Prefab, setting, out TokenCache cache);
            if (setting.m_PreloadDelay > 0f)
            {
                yield return new WaitForSecondsRealtime(setting.m_PreloadDelay);
            }

            bool orgActiveStateOnPrefab = setting.m_Prefab.activeSelf;
            bool needToSpawnActiveState;
            switch (setting.m_AwakeTiming)
            {
                case eAwakeTiming.AwakeOnPreload: needToSpawnActiveState = true; break;
                case eAwakeTiming.DelayAwakeOnFirstSpawn: needToSpawnActiveState = false; break;
                default: throw new System.NotImplementedException();
            }

            if (needToSpawnActiveState != orgActiveStateOnPrefab)
            {
                setting.m_Prefab.SetActive(needToSpawnActiveState);
            }

            int cnt = Mathf.Min(setting.m_PreloadAmount, setting.m_PoolLimit);
            while (cache.totalCount < cnt) // Async spawning parallel maintain preload amount 
            {
                // Create instance for prefab.
                GameObject go = CreateToken(cache, transform.position, transform.rotation, transform, false);
                if (go.activeSelf)
                    go.SetActive(false);
                cache.deactiveObjs.Enqueue(go);

                int countFrame = setting.m_PreloadFramePeriod;
                while (countFrame-- > 0)
                {
                    yield return null;
                }
            }

            if (orgActiveStateOnPrefab != needToSpawnActiveState)
            {
                setting.m_Prefab.SetActive(orgActiveStateOnPrefab);
            }
        }

        private GameObject CreateToken(TokenCache cache, Vector3 position, Quaternion rotation, Transform parent, bool useParentParam)
        {
            GameObject go = useParentParam ?
                Instantiate(cache.setting.m_Prefab, position, rotation, parent) :
                Instantiate(cache.setting.m_Prefab, position, rotation, transform); // follow current pool as parent, if not using parent param
            go.name += $" #{cache.totalCount + 1:0000}";
            return go;
        }

        protected void LocateOrCreateCache(GameObject prefab, PrefabSetting setting, out TokenCache cache)
        {
            if (prefab == null)
            {
                throw CreateException("Fail to spawn Null prefab.");
            }
            if (setting != null && prefab != setting.m_Prefab)
            {
                throw CreateException($"Invalid prefab setting for this asset {prefab}");
            }

            if (!m_CacheDict.TryGetValue(prefab, out cache))
            {
                // cache not found, create one.
                if (setting == null)
                {
                    // spawn on demend, but setting not found.
                    Debug.Log($"load default {nameof(PrefabSetting)} for [{prefab}].", this);
                    setting = m_RuntimePrefabSettingTemplate.Clone();
                    setting.m_Prefab = prefab;
                    setting.m_Name = prefab.name;
                }
                // spawn on demend, without using preload setting.
                cache = new TokenCache(setting);
                m_CacheDict.Add(prefab, cache);
            }
            else if (setting != null)
            {
                // override setting
                // Case : execution order issue. Spawn() call early then Awake();
                cache.setting = setting;
            }
        }
        #endregion // Preload Token

        #region Pooling Core
        private bool m_Inited = false;
        public bool IsInited => m_Inited;

        protected void Initialize()
        {
            if (!m_Inited)
            {
                PoolManager.Register(this, out string poolId);
                if (!gameObject.name.Equals(poolId))
                {
                    gameObject.name = poolId;
                }

                // Force put setting into memory
                foreach (var setting in m_PrefabPreloadSettings)
                {
                    LocateOrCreateCache(setting.m_Prefab, setting, out TokenCache cache);
                }

                m_Inited = true;
            }
        }

        protected virtual bool GetTokenCacheByName(string prefabName, out TokenCache tokenCache)
        {
            // By default throw error.
            if (!TryGetTokenCacheByName(prefabName, out tokenCache))
                throw CreateException($"Cannot spawn {prefabName} by name. Set up its prefab preload setting or use one of the Spawn(GameObject prefab, ...) variant.");
            return true;
        }

        protected bool TryGetTokenCacheByName(string prefabName, out TokenCache tokenCache)
        {
            foreach (var cache in m_CacheDict.Values)
            {
                if (cache.setting.m_Name.Equals(prefabName))
                {
                    tokenCache = cache;
                    return true;
                }
            }
            tokenCache = null;
            return false;
        }

        public bool IsValidPrefabName(string prefabName)
        {
            foreach (var cache in m_CacheDict.Values)
            {
                if (cache.setting.m_Name.Equals(prefabName))
                {
                    return true;
                }
            }

            return false;
        }

        public bool TryGetPrefabName(GameObject prefab, out string prefabName)
        {
            if (m_CacheDict.TryGetValue(prefab, out TokenCache tokenCache))
            {
                prefabName = tokenCache.setting.m_Name;
                return true;
            }
            prefabName = string.Empty;
            return false;
        }

        public GameObject Spawn(string prefabName, Transform parent)
        {
            Initialize();
            if (GetTokenCacheByName(prefabName, out TokenCache cache))
            {
                GameObject go = InternalSpawn(cache, Vector3.zero, Quaternion.identity, parent, true, null);
                if (go != null)
                {
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localRotation = Quaternion.identity;
                    go.transform.localScale = Vector3.one;
                }
                return go;
            }
            return null;
        }

        public GameObject Spawn(string prefabName, Vector3 position, Quaternion rotation)
        {
            Initialize();
            if (GetTokenCacheByName(prefabName, out TokenCache cache))
            {
                return InternalSpawn(cache, position, rotation, null, false);
            }

            return null;
        }

        public GameObject Spawn(string prefabName, Vector3 position, Quaternion rotation, Transform parent)
        {
            Initialize();
            if (GetTokenCacheByName(prefabName, out TokenCache cache))
            {
                return InternalSpawn(cache, position, rotation, parent, true);
            }

            return null;
        }

        public GameObject Spawn(GameObject prefab, Transform parent, PrefabSetting setting = null)
        {
            if (prefab == null)
            {
                throw CreateException("Fail to spawn Null prefab.");
            }
            Initialize();
            LocateOrCreateCache(prefab, setting, out TokenCache cache);
            GameObject go = InternalSpawn(cache, Vector3.zero, Quaternion.identity, parent, true, setting);
            if (go != null)
            {
                go.transform.localPosition = Vector3.zero;
                go.transform.localRotation = Quaternion.identity;
                go.transform.localScale = Vector3.one;
            }
            return go;
        }

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, PrefabSetting setting = null)
        {
            if (prefab == null)
            {
                throw CreateException("Fail to spawn Null prefab.");
            }
            Initialize();
            
            LocateOrCreateCache(prefab, setting, out TokenCache cache);
            return InternalSpawn(cache, position, rotation, null, false, setting);
        }

        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent, PrefabSetting setting = null)
        {
            if (prefab == null)
            {
                throw CreateException("Fail to spawn Null prefab.");
            }
            Initialize();
            
            LocateOrCreateCache(prefab, setting, out TokenCache cache);
            return InternalSpawn(cache, position, rotation, parent, true, setting);
        }

        /// <summary>the spawn flow, Allow to override in child class. e.g. network implementation.</summary>
        /// <param name="cache"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <param name="parent"></param>
        /// <param name="useParentParam"></param>
        /// <param name="setting"></param>
        /// <returns></returns>
        protected virtual GameObject InternalSpawn(TokenCache cache, Vector3 position, Quaternion rotation,
            Transform parent, bool useParentParam,
            PrefabSetting setting = null)
        {
            /// <remarks>For network implementation, redirect this function to network protocol
            /// instead of calling internal local spawn flow.
            return InternalLocalSpawn(cache, position, rotation, parent, useParentParam, setting);
        }

        protected GameObject InternalLocalSpawn(
            TokenCache cache, Vector3 position, Quaternion rotation,
            Transform parent, bool useParentParam,
            PrefabSetting setting)
        {
            GameObject go = null;
            bool notEnoughToken = cache.deactiveObjs.Count == 0;
            if (notEnoughToken)
            {
                bool overLimit = cache.totalCount >= cache.setting.m_PoolLimit;
                if (!overLimit || cache.setting.m_CullOverLimit == eCulling.NoPoolLimit)
                {
                    // Spawn on demend
                    bool orgActiveStateOnPrefab = cache.setting.m_Prefab.activeSelf;
                    bool runtimeActiveStateOnPrefab;
                    switch (cache.setting.m_RuntimeActiveStateOnPrefab)
                    {
                        case eRuntimeActiveStateOnPrefab.Original: runtimeActiveStateOnPrefab = orgActiveStateOnPrefab; break;
                        case eRuntimeActiveStateOnPrefab.ForceActive: runtimeActiveStateOnPrefab = true; break;
                        case eRuntimeActiveStateOnPrefab.ForceInactive: runtimeActiveStateOnPrefab = false; break;
                        default: throw new System.NotImplementedException();
                    }
                    if (runtimeActiveStateOnPrefab != orgActiveStateOnPrefab)
                    {
                        cache.setting.m_Prefab.SetActive(runtimeActiveStateOnPrefab);
                    }
                    go = CreateToken(cache, position, rotation, parent, useParentParam);
                    if (orgActiveStateOnPrefab != runtimeActiveStateOnPrefab)
                    {
                        cache.setting.m_Prefab.SetActive(orgActiveStateOnPrefab);
                    }
                }
                else if (cache.setting.m_CullOverLimit == eCulling.ReturnsNull)
                {
                    Debug.LogWarning($"Not enough token and culled over limit, consider increase pool limit.\nPrefab = {cache.setting.m_Name}, {cache.setting.m_Prefab}, poolLimit={cache.setting.m_PoolLimit}", this);
                }
                else if (cache.setting.m_CullOverLimit == eCulling.CycleOldest)
                {
                    GameObject token = cache.activeObjs.Peek().gameObject;
                    //Debug.Log($"Cycle old token{token}.\nPrefab = {cache.setting.m_Name}, {cache.setting.m_Prefab}, poolLimit={cache.setting.m_PoolLimit}", this);
                    Despawn(token);
                    InternalSpawn();
                }
            }
            else
            {
                InternalSpawn();
            }
            void InternalSpawn()
            {
                go = cache.deactiveObjs.Dequeue();
                if (go == null)
                {
                    // Try to fetch the info of target token by setting.
                    throw CreateException($"Token not found, Please ensure the token not being destroy by other function.\nPrefab = {cache.setting.m_Name}, {cache.setting.m_Prefab}");
                }
                if (go.transform.parent != parent && (parent != null || useParentParam))
                {
                    go.transform.SetParent(parent);
                }
                go.transform.SetPositionAndRotation(position, rotation);
            }

            if (go != null)
            {
                TimedGameObject item = new TimedGameObject(Time.timeSinceLevelLoad, go, cache.setting.m_Prefab);
                cache.activeObjs.Add(item);
                m_SpawnedObjs.Add(go, item);
                if (cache.setting.m_SpawnActive)
                {
                    go.SetActive(true);
                }

                Event_Spawned?.Invoke(go, cache.setting.m_Prefab);
                Event_G_Spawned?.Invoke(go, cache.setting.m_Prefab);
                ISpawnObject[] listeners = go.GetComponentsCache(m_TokenSpawnDict);
                foreach (ISpawnObject obj in listeners)
                {
                    obj.OnSpawned(this);
                }
                // case : when gameobject is "inactive"
                // Following broadcast won't receive callback
                // go.BroadcastMessage(nameof(ISpawnObject.OnSpawned), this, SendMessageOptions.DontRequireReceiver);
            }
            return go;
        }

        /// <summary>Check if the giving gameobject was spawned by this spawn pool</summary>
        /// <param name="go">giving gameobject</param>
        /// <returns></returns>
        public bool IsSpawned(GameObject go) => m_SpawnedObjs.ContainsKey(go);

        protected bool TryGetPrefabByToken(GameObject token, out GameObject prefab)
        {
            if (m_SpawnedObjs.TryGetValue(token, out var timedGameObject))
            {
                prefab = timedGameObject.prefab;
                return true;
            }
            prefab = null;
            return false;
        }

        /// <summary>Assume giving token are spawned by this pool.</summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool TryGetPrefabFromActiveToken(GameObject token, out GameObject prefab)
        {
            if (token == null)
            {
                prefab = null;
                return false;
            }

            foreach (var pool in PoolManager.GetObjectPools())
            {
                if (pool.IsSpawned(token) &&
                    pool.TryGetPrefabByToken(token, out prefab))
                {
                    return true;
                }
            }
            prefab = null;
            return false;
        }

        public virtual void Despawn(GameObject go)
        {
            InternalLocalDespawn(go);
        }

        protected void InternalLocalDespawn(GameObject go)
        {
            if (go == null)
            {
                throw CreateException("Try to despawn null reference unit.");
            }
            if (m_SpawnedObjs.TryGetValue(go, out TimedGameObject t))
            {
                var prefabKey = t.prefab;
                DeactiveToken(go, m_CacheDict[prefabKey], t);
                Event_Despawned?.Invoke(go, prefabKey);
                Event_G_Despawned?.Invoke(go, prefabKey);
                ISpawnObject[] listeners = go.GetComponentsCache(m_TokenSpawnDict);
                foreach (ISpawnObject obj in listeners)
                {
                    obj.OnDespawned(this);
                }
            }
        }

        private void DeactiveToken(GameObject token, TokenCache cache, TimedGameObject t)
        {
            if (m_SpawnedObjs.Remove(token))
            {
                cache.deactiveObjs.Enqueue(token);
                if (!cache.activeObjs.Delete(t))
                {
                    throw CreateException($"Unable to locate {t.gameObject} in min-heap, fail to despawn {t.gameObject}");
                }
            }
            if (cache.setting.m_ReturnToPoolAfterDespawn && token.transform.parent != transform)
            {
                token.transform.SetParent(transform);
            }
            token.SetActive(false);
        }
        #endregion // Pooling Core

        #region Exception
        protected UnityException CreateException(string message)
        {
            UnityException ex = new UnityException(message);
            ex.Data["instanceId"] = GetInstanceID();
            return ex;
        }
        #endregion
    }

    [System.Obsolete("Use ISpawnToken instead")]
    /// <summary>An interface to allow spawned object to receive the following callback during Spawn/Despawn flow.</summary>
    public interface ISpawnObject
    {
        /// <summary>Will boardcast to token(s) after spawn flow.</summary>
        /// <param name="pool">Handling pool reference</param>
        void OnSpawned(ObjectPool_Bak pool);

        /// <summary>Will boardcast to token(s) after despawn flow.</summary>
        /// <param name="pool">Handling pool reference</param>
        void OnDespawned(ObjectPool_Bak pool);
    }
}
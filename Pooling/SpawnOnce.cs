﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class SpawnOnce : SpawnHandlerBase
    {
        private enum eSpawnOn
        {
            Awake = 0,
            Start,
            Enable,
            Disable,
            Destroy,
		}

        [SerializeField] eSpawnOn m_SpawnOn = eSpawnOn.Enable;


        private void Awake() { if (m_SpawnOn == eSpawnOn.Awake) TriggerSpawn(); }
        private void Start() { if (m_SpawnOn == eSpawnOn.Start) TriggerSpawn(); }
        private void OnEnable() { if (m_SpawnOn == eSpawnOn.Enable) TriggerSpawn(); }
        private void OnDisable() { if (m_SpawnOn == eSpawnOn.Disable) TriggerSpawn(); }
        private void OnDestroy() { if (m_SpawnOn == eSpawnOn.Destroy) TriggerSpawn(); }

        private void TriggerSpawn()
        {
            if (m_Spawner.AllowToSpawn(null))
            {
                InternalSpawnToken();
            }
		}
    }
}
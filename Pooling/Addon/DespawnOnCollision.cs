﻿using UnityEngine;

namespace Kit
{
	[RequireComponent(typeof(Collider))]
	public class DespawnOnCollision : MonoBehaviour, ISpawnObject
	{
		[SerializeField] string m_Tag = string.Empty;
		[SerializeField] LayerMask m_Layer = 0;

		private enum eMatching
		{
			any = 0,
			tag = 1,
			layer = 2,
			all = 3,
		}
		[SerializeField] eMatching m_Matching = eMatching.all;

		[Header("Trigger")]
		[SerializeField] bool m_TriggerEnter = true;
		[SerializeField] bool m_TriggerExit = true;
		[Header("Collider")]
		[SerializeField] bool m_CollisionEnter = false;
		[SerializeField] bool m_CollisionExit = false;

		#region pooling
		protected ObjectPool_Bak pool { get; private set; }
		public void OnSpawned(ObjectPool_Bak pool) { this.pool = pool; }
		public void OnDespawned(ObjectPool_Bak pool) { }
		#endregion // pooling

		private void OnTriggerEnter(Collider other)
		{
			if (m_TriggerEnter && Condition(other))
				SelfDespawn();
		}
		private void OnTriggerExit(Collider other)
		{
			if (m_TriggerExit && Condition(other))
				SelfDespawn();
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (m_CollisionEnter && Condition(collision.collider))
				SelfDespawn();
		}
		private void OnCollisionExit(Collision collision)
		{
			if (m_CollisionExit && Condition(collision.collider))
				SelfDespawn();
		}

		private bool Condition(Collider other)
		{
			switch(m_Matching)
			{
				case eMatching.any: return IsTag(other) || IsLayer(other);
				case eMatching.all: return IsTag(other) && IsLayer(other);
				case eMatching.tag: return IsTag(other);
				case eMatching.layer: return IsTag(other);
				default: throw new System.NotImplementedException();
			}
		}

		private bool IsTag(Collider other)
		{
			if (string.IsNullOrEmpty(m_Tag))
				return m_Matching == eMatching.all ? false : true;
			return other.CompareTag(m_Tag);
		}

		private bool IsLayer(Collider other)
		{
			return m_Layer.Contain(other.gameObject.layer);
		}

		protected virtual void SelfDespawn()
		{
			PhotonTokenBase.Despawn(pool, gameObject);
		}
	}
}
﻿using UnityEngine;

namespace Kit
{
	public class DespawnDisable : MonoBehaviour, ISpawnObject
	{
		protected ObjectPool_Bak pool { get; private set; }
		public virtual void OnSpawned(ObjectPool_Bak pool)
		{
			this.pool = pool;
		}
		public virtual void OnDespawned(ObjectPool_Bak pool) { }

        private void OnEnable()
        {
			if (IsInvoking(nameof(Hotfix_DisableChangeParent)))
			{
				Debug.LogError("Hotfix had risk check required : Despawn & Spawn at same frame.");
				CancelInvoke(nameof(Hotfix_DisableChangeParent));
			}
		}
        private void OnDisable()
		{
			// either it's being despawn by other,
			// or we need to despawn from here.
			// however, Unity3D can not change parent during disable.
			// and it didn't throw error, but Debug.LogError,
			// so no way to detect when the error throw.
			Hotfix_DisableChangeParent();
		}

		private void Hotfix_DisableChangeParent()
        {
			//if (pool && pool.IsSpawned(gameObject))
			//	pool.Despawn(gameObject);
			PhotonTokenBase.Despawn(pool, gameObject);
		}
	}
}
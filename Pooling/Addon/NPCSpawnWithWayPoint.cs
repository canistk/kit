﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Kit.Avatar;

namespace Kit
{
    public class NPCSpawnWithWayPoint : Spawner
    {
        [Header("Waypoint")]
        [SerializeField] Vector3 m_Offset = Vector3.zero;
        [SerializeField] float m_MinRadius = 1f;
        [SerializeField] float m_ExtraRadius = 1f;
        [SerializeField] bool m_NavMeshOnly = false;

        private static Dictionary<GameObject, NPCFirstWaypointMovement> s_NPCDict = new Dictionary<GameObject, NPCFirstWaypointMovement>(10);
        public override void OnTokenSpawned(GameObject token, Dictionary<string, object> extraData)
        {
            base.OnTokenSpawned(token, extraData);
            NPCFirstWaypointMovement npc = token.GetComponentInChildCache(s_NPCDict);
            if (npc != null)
            {
                // handler.SetTargetTransfrom
                Vector3 targetPos = GetRandomPos();
                npc.SetTargetPosition(targetPos);
            }
        }

        private Vector3 GetRandomPos()
        {
            Vector2 xz = Random.insideUnitCircle;
            Vector2 dir = xz.normalized;
            xz = (dir * m_MinRadius) + (xz * m_ExtraRadius);
            GetPivot(out Vector3 center, out Quaternion rot, out object parent);
            Vector3 altOffset = rot * new Vector3(xz.x, 0f, xz.y);
            Vector3 pos = center + (rot * m_Offset) + altOffset;
            if (m_NavMeshOnly &&
                NavMesh.SamplePosition(pos, out NavMeshHit hit, float.MaxValue, NavMesh.AllAreas))
            {
                pos = hit.position;
            }
            return pos;
        }

        private void OnDrawGizmosSelected()
        {
            GetPivot(out Vector3 center, out Quaternion rot, out object parent);
            GizmosExtend.DrawCircle(center, rot.GetUp(), Color.green, m_MinRadius);
            GizmosExtend.DrawCircle(center, rot.GetUp(), Color.cyan, m_MinRadius + m_ExtraRadius);

            if (!Application.isPlaying)
            {
                Vector2 xz = Random.insideUnitCircle;
                Vector2 dir = xz.normalized;
                xz = (dir * m_MinRadius) + (xz * m_ExtraRadius);
                GizmosExtend.DrawRay(center, new Vector3(xz.x, 0f, xz.y), Color.red);
            }
        }
    }
}
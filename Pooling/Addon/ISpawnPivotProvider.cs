﻿using UnityEngine;
namespace Kit
{
	public interface ISpawnPivotProvider
	{
		void GetPivot(out Vector3 pos, out Quaternion rotate, out object parent);
	}
}
﻿using UnityEngine;
namespace Kit
{
	public interface ISpawnRule
	{
		bool AllowToSpawn(GameObject prefab);
	}
}
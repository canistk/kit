﻿using System.Collections;
using UnityEngine;
#if PHOTON_UNITY_NETWORKING
using Kit.Photon;
#endif

namespace Kit
{
	public class DespawnTimer : MonoBehaviour, ISpawnObject
	{
		[Header("Despawn")]
		public float m_AutoDespawnPeriod = 3f;
		private float m_EndTime = -1f;
		private bool m_WasEnd = false;
		protected ObjectPool_Bak pool { get; private set; }
		public virtual void OnSpawned(ObjectPool_Bak pool)
		{
			this.pool = pool;
			m_WasEnd = false;
			m_EndTime = Time.timeSinceLevelLoad + m_AutoDespawnPeriod;
		}
		public virtual void OnDespawned(ObjectPool_Bak pool) { }

		protected virtual void LateUpdate()
		{
			if (!m_WasEnd && m_EndTime > 0f && Time.timeSinceLevelLoad > m_EndTime)
			{
				m_WasEnd = true;
				InternalDespawn();
			}
		}

		protected void InternalDespawn()
		{
			PhotonTokenBase.Despawn(pool, gameObject);
		}
	}
}
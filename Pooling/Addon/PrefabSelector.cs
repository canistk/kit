﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	public class PrefabSelector : MonoBehaviour, ISpawnPrefabProvider
	{
		[System.Serializable]
		private enum eMethod
		{
			Manually = 0,
			Order = 1,
			Reverse = 2,
			Random = 3,
		}

		[SerializeField] public GameObject[] m_Prefabs = { };
		[SerializeField] private eMethod m_Method = eMethod.Manually;
		public int index = 0;

		public GameObject GetPrefab()
		{
			if (m_Prefabs.Length == 0)
				return null;
			switch (m_Method)
			{
				default:
				case eMethod.Manually:
					index = Mathf.Clamp(index, 0, m_Prefabs.Length);
					break;
				case eMethod.Order:
					index++;
					if (index >= m_Prefabs.Length)
						index = 0;
					break;
				case eMethod.Reverse:
					index--;
					if (index < 0)
						index = m_Prefabs.Length - 1;
					break;
				case eMethod.Random:
					index = Random.Range(0, m_Prefabs.Length);
					break;
			}
			return m_Prefabs[index];
		}
	}
}
﻿using UnityEngine;
namespace Kit
{
	public interface ISpawnPrefabProvider
	{
		GameObject GetPrefab();
	}
}
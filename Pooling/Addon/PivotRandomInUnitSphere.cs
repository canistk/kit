﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
#endif
namespace Kit
{
	public class PivotRandomInUnitSphere : MonoBehaviour, ISpawnPivotProvider
	{
		[SerializeField] Vector3 m_Offset = Vector3.zero;
		[SerializeField] float m_Radius = 1f;
		[SerializeField] bool m_Sphere = false;
		[SerializeField] bool m_NavMeshOnly = false;
		[Header("Parent")]
#if PHOTON_UNITY_NETWORKING
		[SerializeField] bool m_UsePhotonParent = false;
		[SerializeField] PhotonView parentPV = null;
#endif
		[SerializeField] Transform parentTransform = null;

		public void GetPivot(out Vector3 pos, out Quaternion rotate, out object parent)
		{
			Vector3 altOffset;
			if (m_Sphere)
			{
				altOffset = Random.insideUnitSphere * m_Radius;
			}
			else
			{
				Vector2 xz = Random.insideUnitCircle * m_Radius;
				altOffset = transform.TransformDirection(new Vector3(xz.x, 0f, xz.y));
			}
			pos = transform.TransformPoint(m_Offset + altOffset);
			if (m_NavMeshOnly &&
				NavMesh.SamplePosition(pos, out NavMeshHit hit, float.MaxValue, NavMesh.AllAreas))
            {
				pos = hit.position;
            }
			rotate = transform.rotation;

			
#if PHOTON_UNITY_NETWORKING
			if (m_UsePhotonParent)
				parent = parentPV;
			else
#endif
			parent = parentTransform;

		}

		private void OnDrawGizmosSelected()
		{
			Vector3 center = transform.TransformPoint(m_Offset);
			if (m_Sphere)
				GizmosExtend.DrawWireSphere(center, m_Radius, Color.blue);
			else
				GizmosExtend.DrawCircle(center, transform.up, Color.blue, m_Radius);
		}
	}
}

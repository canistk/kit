﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class PivotSelector : MonoBehaviour, ISpawnPivotProvider
    {
		[System.Serializable]
		private enum eMethod
		{
			Manually = 0,
			Order = 1,
			Reverse = 2,
			Random = 3,
		}

		[SerializeField] public Transform[] m_Pivots = { };
		[SerializeField] private eMethod m_Method = eMethod.Manually;
		public int index = 0;
		public void GetPivot(out Vector3 pos, out Quaternion rotate, out object parent)
		{
			parent = null; // TODO: implement this.
			if (m_Pivots.Length == 0)
			{
				Debug.LogError($"{name}, pivot reference not found.", this);
				pos = transform.position;
				rotate = transform.rotation;
				return;
			}
			switch (m_Method)
			{
				default:
				case eMethod.Manually:
					index = Mathf.Clamp(index, 0, m_Pivots.Length);
					break;
				case eMethod.Order:
					index++;
					if (index >= m_Pivots.Length)
						index = 0;
					break;
				case eMethod.Reverse:
					index--;
					if (index < 0)
						index = m_Pivots.Length - 1;
					break;
				case eMethod.Random:
					index = Random.Range(0, m_Pivots.Length);
					break;
			}

			if (m_Pivots[index] == null)
			{
				Debug.LogError($"{name}, pivot reference on index[{index}] is null.", this);
				pos = transform.position;
				rotate = transform.rotation;
				return;
			}

			pos = m_Pivots[index].position;
			rotate = m_Pivots[index].rotation;
		}
    }
}
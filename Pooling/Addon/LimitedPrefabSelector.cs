﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
	[RequireComponent(typeof(Spawner))]
	public class LimitedPrefabSelector : MonoBehaviour, ISpawnPrefabProvider, ISpawnRule
	{
		[SerializeField] Spawner m_Spawner = null;
		[System.Serializable]
		public class PrefabConfig
		{
			public GameObject prefab;
			public int maxAmount = 1;
		}
		[SerializeField] List<PrefabConfig> m_PrefabConfig = new List<PrefabConfig>();
		private Dictionary<GameObject /* prefab */, PrefabConfig> _config = null;
		private Dictionary<GameObject /* prefab */, PrefabConfig> m_Config
		{
			get
			{
				if (_config == null)
				{
					_config = new Dictionary<GameObject, PrefabConfig>(m_PrefabConfig.Count);
					foreach (PrefabConfig config in m_PrefabConfig)
						_config.Add(config.prefab, config);
				}
				return _config;
			}
		}
		private static Dictionary<GameObject /* prefab */, HashSet<GameObject> /* Token */> s_SpawnedTokenDict = new Dictionary<GameObject, HashSet<GameObject>>();

		public bool AllowToSpawn(GameObject prefab)
		{
			if (!m_Inited)
				return false;
			if (prefab == null)
				return true; // when no prefab giving, just okay to spawn.
			
			if (m_Config.TryGetValue(prefab, out PrefabConfig config))
			{
				int spawned = GetSpawnedCount(prefab);
				if (config.maxAmount <= 0)
					Debug.LogWarning($"The max spawnable amount of {prefab} is {config.maxAmount}.");
				// if we still had quota to spawn.
				return config.maxAmount > spawned;
			}
			return false;
		}

		public GameObject GetPrefab()
		{
			if (m_PrefabConfig.Count == 0)
				return null;
			else if (m_PrefabConfig.Count == 1)
				return m_PrefabConfig[0].prefab;
			int index = Random.Range(0, m_PrefabConfig.Count);
			return m_PrefabConfig[index].prefab;
		}

		#region System
		private void Reset()
		{
			m_Spawner = GetComponent<Spawner>();
		}

		private bool m_Inited = false;
		private void Awake()
		{
			if (m_Spawner == null)
				m_Spawner = GetComponent<Spawner>();
			foreach (var config in m_PrefabConfig)
			{
				if (!s_SpawnedTokenDict.TryGetValue(config.prefab, out HashSet<GameObject> hash))
				{
					hash = new HashSet<GameObject>();
					s_SpawnedTokenDict.Add(config.prefab, hash);
				}
			}
			m_Spawner.Event_SpawnLocation.AddListener(OnTokenSpawned);
			ObjectPool_Bak.Event_G_Despawned += OnAnythingDespawn;
			m_Inited = true;
		}

		private void OnDestroy()
		{
			if (m_Spawner != null)
			{
				m_Spawner.Event_SpawnLocation.RemoveListener(OnTokenSpawned);
			}
			ObjectPool_Bak.Event_G_Despawned -= OnAnythingDespawn;
		}
		#endregion System

		#region Tracking Spawned
		private void OnTokenSpawned(GameObject token, Vector3 pos, Quaternion rot)
		{
			// Notes: this method should only Success,
			// otherwise, bug.
			if (ObjectPool_Bak.TryGetPrefabFromActiveToken(token, out GameObject prefab))
			{
				if (s_SpawnedTokenDict.TryGetValue(prefab, out HashSet<GameObject> hash))
				{
					hash.Add(token); // so now we keep counting.
				}
				else
				{
					throw new System.Exception("Logic error, this method should only listen from this spawner.");
				}
			}
			else
			{
				throw new UnityException($"Fail to trace the prefab object from {token}");
			}
		}

		private void OnAnythingDespawn(GameObject token, GameObject prefab)
		{
			if (s_SpawnedTokenDict.TryGetValue(prefab, out HashSet<GameObject> hash))
			{
				hash.Remove(token);
			}
		}

		private int GetSpawnedCount(GameObject prefab)
		{
			if (prefab == null)
				throw new UnityException($"Fix this : giving prefab cannot be Null");
			if (!m_Config.ContainsKey(prefab))
				throw new UnityException($"Fix this : Assume giving prefab are record in {nameof(m_PrefabConfig)}");

			// we had that config.
			if (!s_SpawnedTokenDict.TryGetValue(prefab, out HashSet<GameObject> hash))
			{
				// spawn amount = 0
				return 0;
			}
			else
			{
				// when we still had quota to spawn.
				return hash.Count;
			}
		}
		#endregion Tracking Spawned
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
using Kit.Photon;
#endif

namespace Kit
{
	public class SpawnRulePhoton : MonoBehaviour, ISpawnRule
	{
#if PHOTON_UNITY_NETWORKING
		[Header("Active condition")]
		[SerializeField] bool m_OnlySpawnOnMaster = false;
		[SerializeField] int m_MinActivePlayer = 0;
		[SerializeField] int m_MaxActivePlayer = 10;
#endif
		public bool AllowToSpawn(GameObject prefab)
		{
#if PHOTON_UNITY_NETWORKING
			if (m_OnlySpawnOnMaster && !PhotonNetwork.IsMasterClient)
				return false;

			int pNum = PhotonNetwork.CountOfPlayersInRooms;
			if (pNum < m_MinActivePlayer || m_MaxActivePlayer < pNum)
				return false;
#endif
			return true;
		}
	}
}
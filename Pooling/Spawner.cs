﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	public class Spawner : SpawnerProviderBase
	{
		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		public void Fetch()
		{
			if (!Application.isPlaying)
			{
				OnValidate();
			}
		}

		private void OnValidate()
		{
			if (m_PivotProvider != null &&
				!(m_PivotProvider is ISpawnPivotProvider))
			{
				m_PivotProvider = null;
			}

			if (m_PrefabProvide != null &&
				!(m_PrefabProvide is ISpawnPrefabProvider))
			{
				m_PrefabProvide = null;
			}

			if (!Application.isPlaying)
			{
				if (m_PivotProvider == null)
					m_PivotProvider = (Component)GetComponent<ISpawnPivotProvider>();
				if (m_PrefabProvide == null)
					m_PrefabProvide = (Component)GetComponent<ISpawnPrefabProvider>();
				if (m_BirthControls.Count == 0)
				{
					FetchSpawnRule();
				}
			}
		}

		private void FetchSpawnRule()
		{
			ISpawnRule[] ctrls = GetComponents<ISpawnRule>();
			if (ctrls.Length > 0)
			{
				foreach (Component c in ctrls)
					if (!m_BirthControls.Contains(c))
						m_BirthControls.Add(c);
			}
		}

		#region Birth Control
		[Header("Birth Control")]
		[SerializeField] List<Component> m_BirthControls = new List<Component>();
		public override bool AllowToSpawn(GameObject prefab = null)
		{
			bool allow = true;
			int cnt = m_BirthControls.Count;
			for (int i = 0; i < cnt && allow; i++)
			{
				if (m_BirthControls[i] is ISpawnRule ctrl)
				{
					// "prefab" just a reference, can be null.
					allow &= ctrl.AllowToSpawn(prefab);
				}
			}
			return allow;
		}
		#endregion Birth Control
		
		#region Prefab
		[Header("Prefab")]
		[SerializeField] Component m_PrefabProvide = null;
		[SerializeField, DrawIf(nameof(m_PrefabProvide), null, ComparisonType.Equals, DisablingType.DontDraw)]
		private GameObject m_Prefab = null;
		public override GameObject GetPrefab()
		{
			if (m_PrefabProvide is ISpawnPrefabProvider proxy)
				return proxy.GetPrefab();
			return m_Prefab;
		}
		#endregion Prefab

		#region Pivot
		[Header("Pivot")]
		[SerializeField] Component m_PivotProvider = null;
		[SerializeField, DrawIf(nameof(m_PivotProvider), null, ComparisonType.Equals, DisablingType.DontDraw)]
		private Vector3 m_PivotOffset = Vector3.zero;
		[SerializeField, DrawIf(nameof(m_PivotProvider), null, ComparisonType.Equals, DisablingType.DontDraw)]
		private Vector3 m_PivotRotate = Vector3.zero;
		protected override void GetPivot(out Vector3 pos, out Quaternion rotate, out object parent)
		{
			if (m_PivotProvider is ISpawnPivotProvider proxy)
			{
				proxy.GetPivot(out pos, out rotate, out parent);
				return;
			}
			pos = transform.TransformPoint(m_PivotOffset);
			rotate = Quaternion.Euler(m_PivotRotate) * transform.rotation;
			parent = transform;
		}
		#endregion Pivot

		protected override Dictionary<string, object> GetExtraData() => null;
	}
}
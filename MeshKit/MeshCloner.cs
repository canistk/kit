using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit.MeshKit.FaceEdge;
using Kit.MeshKit.HalfEdge;

namespace Kit.MeshKit
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class MeshCloner : MonoBehaviour
    {
        public MeshFilter m_Target;
        public bool m_KeepPosition = false;
        public bool m_KeepRotation = false;
        public bool m_KeepScale = false;

        [Space]
        public MeshCloner m_Other;

        private MeshFilter m_MeshFilter;
        public MeshFilter meshFilter
        {
            get
            {
                if (m_MeshFilter == null)
                    m_MeshFilter = GetComponent<MeshFilter>();
                return m_MeshFilter;
            }
        }

        private struct CloneInfo
        {
            public Mesh targetMesh, clonedMesh;
            public HEMesh heMesh;
            //public FEMesh feMesh;
            public BSP_Tree bsp;
            public int clonedMeshHashCode;
            public Matrix4x4 matrix;
        }
        private CloneInfo m_CloneInfo;

        public void CloneTarget(MeshFilter target, bool keepPosition, bool keepRotation, bool keepScale)
        {
            if (target == null)
            {
                Debug.LogError("Invalid target = null, abort clone process.");
                return;
            }

            var localSpace = !keepPosition && !keepRotation && !keepScale;
            var matrix = localSpace ? Matrix4x4.identity :
            Matrix4x4.TRS(
                keepPosition ? target.transform.position : Vector3.zero,
                keepRotation ? target.transform.rotation : Quaternion.identity,
                keepScale ? target.transform.lossyScale : Vector3.one);

            if (
                m_CloneInfo.heMesh == null ||
                (m_CloneInfo.targetMesh != null && m_CloneInfo.targetMesh.GetHashCode() == m_CloneInfo.clonedMeshHashCode) // modified
                )
            {
                // mesh matrix setup are same
                if ((localSpace && m_CloneInfo.matrix.isIdentity) ||
                    (m_CloneInfo.matrix == matrix))
                {
                    Debug.Log($"Target {target.name}, clone already, abort process.");
                    return;
                }
            }

            m_Target = target; // keep editor reference.

            if (m_CloneInfo.heMesh == null)
                m_CloneInfo.heMesh = new HEMesh();

            var heMesh = m_CloneInfo.heMesh;
            heMesh.Clean();

            m_CloneInfo.targetMesh  = target.sharedMesh;
            m_CloneInfo.matrix      = matrix;

            heMesh.LoadFrom(m_CloneInfo.targetMesh, m_CloneInfo.matrix);
            ApplyHeMesh(heMesh);
            m_CloneInfo.clonedMeshHashCode = meshFilter.sharedMesh.GetHashCode();
        }

        public void Clean()
        {
            if (meshFilter != null)
            {
                meshFilter.sharedMesh = null;
            }
            if (m_CloneInfo.clonedMesh != null)
            {
                if (Application.isPlaying)
                    Destroy(m_CloneInfo.clonedMesh);
                else
                    DestroyImmediate(m_CloneInfo.clonedMesh);
                m_CloneInfo.clonedMesh = null;
            }
            m_CloneInfo = default(CloneInfo);
        }

        public void ApplyHeMesh(HEMesh heMesh)
        {
            var mesh = heMesh.ToMesh();
            mesh.name = $"Mesh-{mesh.GetHashCode()}";
            mesh.hideFlags = HideFlags.DontSaveInEditor;
            meshFilter.sharedMesh = m_CloneInfo.clonedMesh = mesh;
        }

        public bool TryGetHEMesh(out HEMesh heMesh)
        {
            heMesh = m_CloneInfo.heMesh;
            if (m_Target != null && heMesh == null)
            {
                CloneTarget(m_Target, m_KeepPosition, m_KeepRotation, m_KeepScale);
                heMesh = m_CloneInfo.heMesh;
            }
            return heMesh != null;
        }

        public void TryIntersectTarget(MeshCloner other, 
            System.Action<BSP_Tree> success = null, 
            System.Action<System.Exception> fail = null)
        {
            if (m_CloneInfo.heMesh == null)
                m_CloneInfo.heMesh = new HEMesh();
            void _DispatchSuccess(BSP_Tree rst)
            {
                m_CloneInfo.heMesh.LoadFrom(rst);
                ApplyHeMesh(m_CloneInfo.heMesh);
                success?.TryCatchDispatchEventError(o => o?.Invoke(rst));
            }
            void _DispatchFail(string msg)
            {
                var ex = new System.Exception(msg);
                fail?.TryCatchDispatchEventError(o => o?.Invoke(ex));
            }

            HEMesh otherMesh = null;
            
            if (!TryGetHEMesh(out var feMesh))
            {
                _DispatchFail($"Fail to intersect this ({nameof(MeshCloner)}) without any data yet. PS: {nameof(CloneTarget)}() first.");
                return;
            }
            else if (other == null)
            {
                _DispatchFail("Fail to intersect empty reference.");
                return;
            }
            else if (other == this)
            {
                _DispatchFail($"Fail to self intersect, other == this({nameof(MeshCloner)}).");
                return;
            }
            else if (!other.TryGetHEMesh(out otherMesh))
            {
                _DispatchFail($"Fail to intersect, target without any {nameof(FEMesh)} data.");
                return;
            }
            if (otherMesh == null)
                throw new System.Exception("Logic Error");

            BSP_Thread.ConvertMeshs(this.meshFilter, other.meshFilter,
                _OnBSPTreeReady, fail);
            void _OnBSPTreeReady(BSP_Thread.MatrixBSPData data)
            {
                var share = new ThreadShare<BSP_Tree>(_IntersectCallback);
                var thread = BSP_Thread.Intersect(data.main, data.slave, share);
                thread.IsBackground = true;
                thread.Start();
                thread.Join();
                share.WaitResult(out var rst, 3000);
                void _IntersectCallback(BSP_Tree o)
                {
                    m_CloneInfo.bsp = o;
                }
                if (share.IsError)
                {
                    fail?.TryCatchDispatchEventError(o => o?.Invoke(share.exception));
                    return;
                }
                var bsp = share.result;
                _DispatchSuccess(bsp);
            }
        }

        public void TrySubtractTarget(MeshCloner other,
            System.Action<BSP_Tree> success = null,
            System.Action<System.Exception> fail = null)
        {
            void _DispatchSuccess(BSP_Tree rst)
            {
                if (m_CloneInfo.heMesh == null)
                    m_CloneInfo.heMesh = new HEMesh();
                m_CloneInfo.heMesh.LoadFrom(rst);
                ApplyHeMesh(m_CloneInfo.heMesh);
                success?.TryCatchDispatchEventError(o => o?.Invoke(rst));
            }
            void _DispatchFail(string msg)
            {
                var ex = new System.Exception(msg);
                fail?.TryCatchDispatchEventError(o => o?.Invoke(ex));
            }

            HEMesh otherMesh = null;

            if (!TryGetHEMesh(out var feMesh))
            {
                _DispatchFail($"Fail to intersect this ({nameof(MeshCloner)}) without any data yet. PS: {nameof(CloneTarget)}() first.");
                return;
            }
            else if (other == null)
            {
                _DispatchFail("Fail to intersect empty reference.");
                return;
            }
            else if (other == this)
            {
                _DispatchFail($"Fail to self intersect, other == this({nameof(MeshCloner)}).");
                return;
            }
            else if (!other.TryGetHEMesh(out otherMesh))
            {
                _DispatchFail($"Fail to intersect, target without any {nameof(FEMesh)} data.");
                return;
            }
            if (otherMesh == null)
                throw new System.Exception("Logic Error");

            BSP_Thread.ConvertMeshs(this.meshFilter, other.meshFilter,
                _OnBSPTreeReady, fail);
            void _OnBSPTreeReady(BSP_Thread.MatrixBSPData data)
            {
                var share = new ThreadShare<BSP_Tree>(_IntersectCallback);
                var thread = BSP_Thread.Subtract(data.main, data.slave, share);
                thread.IsBackground = true;
                thread.Start();
                thread.Join();
                //share.WaitResult(out var rst, 3000);
                void _IntersectCallback(BSP_Tree o)
                {
                    m_CloneInfo.bsp = o;
                }
                if (share.IsError)
                {
                    fail.TryCatchDispatchEventError(o => o?.Invoke(share.exception));
                    return;
                }
                var bsp = share.result;
                _DispatchSuccess(bsp);
            }
        }

        public void TryUnionTarget(MeshCloner other,
            System.Action<BSP_Tree> success = null,
            System.Action<System.Exception> fail = null)
        {
            void _DispatchSuccess(BSP_Tree rst)
            {
                if (m_CloneInfo.heMesh == null)
                    m_CloneInfo.heMesh = new HEMesh();
                m_CloneInfo.heMesh.LoadFrom(rst);
                ApplyHeMesh(m_CloneInfo.heMesh);
                success?.TryCatchDispatchEventError(o => o?.Invoke(rst));
            }
            void _DispatchFail(string msg)
            {
                var ex = new System.Exception(msg);
                fail?.TryCatchDispatchEventError(o => o?.Invoke(ex));
            }

            HEMesh otherMesh = null;

            if (!TryGetHEMesh(out var feMesh))
            {
                _DispatchFail($"Fail to intersect this ({nameof(MeshCloner)}) without any data yet. PS: {nameof(CloneTarget)}() first.");
                return;
            }
            else if (other == null)
            {
                _DispatchFail("Fail to intersect empty reference.");
                return;
            }
            else if (other == this)
            {
                _DispatchFail($"Fail to self intersect, other == this({nameof(MeshCloner)}).");
                return;
            }
            else if (!other.TryGetHEMesh(out otherMesh))
            {
                _DispatchFail($"Fail to intersect, target without any {nameof(FEMesh)} data.");
                return;
            }
            if (otherMesh == null)
                throw new System.Exception("Logic Error");

            BSP_Thread.ConvertMeshs(this.meshFilter, other.meshFilter,
                _OnBSPTreeReady, fail);
            void _OnBSPTreeReady(BSP_Thread.MatrixBSPData data)
            {
                var share = new ThreadShare<BSP_Tree>(_IntersectCallback);
                var thread = BSP_Thread.Union(data.main, data.slave, share);
                thread.IsBackground = true;
                thread.Start();
                thread.Join();
                //share.WaitResult(out var rst, 3000);
                void _IntersectCallback(BSP_Tree o)
                {
                    m_CloneInfo.bsp = o;
                }
                if (share.IsError)
                {
                    fail.TryCatchDispatchEventError(o => o?.Invoke(share.exception));
                    return;
                }
                var bsp = share.result;
                _DispatchSuccess(bsp);
            }
        }

        private void Awake()
        {
            if (m_Target != null && m_CloneInfo.heMesh == null)
            {
                CloneTarget(m_Target, m_KeepPosition, m_KeepRotation, m_KeepScale);
            }
        }
    }
}
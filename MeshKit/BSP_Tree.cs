using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Kit.MeshKit
{
    /// <summary>
    /// Constructive Solid Geometry (CSG)
    /// via Binary space partitioning (BSP)
    /// <see cref="https://en.wikipedia.org/wiki/Constructive_solid_geometry"/>
    /// </summary>
    public class BSP_Tree
    {
        public Polygon[] root;
        public Plane plane;
        public BSP_Tree front, back;
        public int GetPolygonCount()
        {
            var rst = root.Length;
            if (front != null)
                rst += front.GetPolygonCount();
            if (back != null)
                rst += back.GetPolygonCount();
            return rst;
        }

        /// <summary><see cref="Clone"/></summary>
        /// <param name="root"></param>
        /// <param name="plane"></param>
        /// <param name="front"></param>
        /// <param name="back"></param>
        internal BSP_Tree(Polygon[] root, Plane plane, BSP_Tree front = null, BSP_Tree back = null)
        {
            this.root = root;
            this.plane = plane;
            if (plane.normal == Vector3.zero)
            {
                throw new System.Exception($"Invalid BSP_Tree created. root.length={root.Length}," +
                    $" p0={root[0].vertices[0].position:F2}" +
                    $" p1={root[0].vertices[1].position:F2}" +
                    $" p2={root[0].vertices[2].position:F2}");
            }
            this.front = front;
            this.back = back;
        }

        /// <summary>Return a list of all polygons in this BSP tree.</summary>
        /// <returns></returns>
        public IEnumerable<Polygon> Polygons()
        {
            foreach (Polygon r in this.root)
                yield return r;

            if (this.front != null)
            {
                foreach (Polygon o in this.front.Polygons())
                    yield return o;
            }

            if (this.back != null)
            {
                foreach (Polygon o in this.back.Polygons())
                    yield return o;
            }
        }

        /// <summary>Deep copy</summary>
        /// <returns></returns>
        public BSP_Tree Clone()
        {
            var f = front == null ? null : front.Clone();
            var b = back == null ? null : back.Clone();
            return new BSP_Tree(root, plane, f, b);
        }

        /// <summary>Convert solid space to empty space and empty space to solid space.</summary>
        public void Invert()
        {
            {
                for (int i = 0; i < this.root.Length; i++)
                    this.root[i].Flip();

                this.plane.Flip();

                if (this.front != null)
                    this.front.Invert();

                if (this.back != null)
                    this.back.Invert();

                var tmp = this.front;
                this.front = this.back;
                this.back = tmp;
            }
        }

        public void Apply(Matrix4x4 matrix)
        {
            for (int i = 0; i < this.root.Length; ++i)
                this.root[i] = this.root[i] * matrix;

            var vs = this.root[0].vertices;
            this.plane = new Plane(vs[0].position, vs[1].position, vs[2].position);

            if (this.front != null)
                this.front.Apply(matrix);

            if (this.back != null)
                this.back.Apply(matrix);
        }
    }
}
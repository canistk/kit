using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit.MeshKit
{
    [CustomEditor(typeof(MeshCloner), true)]
    public class MeshClonerEditor : EditorBase
    {
        MeshCloner meshCloner => (MeshCloner)target;
        protected override void OnAfterDrawGUI()
        {
            base.OnAfterDrawGUI();
            if (GUILayout.Button("Clone Target"))
            {
                meshCloner.CloneTarget(meshCloner.m_Target,
                    meshCloner.m_KeepPosition,
                    meshCloner.m_KeepRotation,
                    meshCloner.m_KeepScale);
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("Intersect Target"))
            {
                meshCloner.TryIntersectTarget(meshCloner.m_Other, null, Debug.LogError);
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("Subtract Target"))
            {
                meshCloner.TrySubtractTarget(meshCloner.m_Other, null, Debug.LogError);
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("Union Target"))
            {
                meshCloner.TryUnionTarget(meshCloner.m_Other, null, Debug.LogError);
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Clean"))
            {
                meshCloner.Clean();
            }
        }
    }
}
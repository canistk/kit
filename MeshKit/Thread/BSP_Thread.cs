#define BACKGROUND_THREAD
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
namespace Kit.MeshKit
{
    public class BSP_Thread
    {
        /// <summary>
        /// Constructive Solid Geometry (CSG)
        /// via Binary space partitioning (BSP)
        /// Create <see cref="BSP_Tree"/> from <see cref="Polygon"/>
        /// </summary>
        /// <param name="polygons"></param>
        /// <param name="share"></param>
        /// <returns></returns>
        public static Thread Create_BSP_Tree(Polygon[] polygons,
            ThreadShare<BSP_Tree> share)
        {
            var job     = new _Create_BSP_Tree(polygons, share);
            var thread  = new Thread(new ThreadStart(job.Process));
#if BACKGROUND_THREAD
            thread.IsBackground = true;
#endif
            thread.Name = nameof(Create_BSP_Tree);
            return thread;
        }
        private class _Create_BSP_Tree
        {
            private Polygon[] polygons;
            private List<Thread> threads;
            private ThreadShare<BSP_Tree> share, frontShare, backShare;
            private List<AutoResetEvent> shares;
            public _Create_BSP_Tree(Polygon[] polygons,
                ThreadShare<BSP_Tree> request)
            {
                this.polygons   = polygons;
                this.share      = request;
                this.threads    = new List<Thread>(2);
                this.shares     = new List<AutoResetEvent>(2);
            }

            public void Process()
            {
                try
                {
                    lock (share)
                    {
                        var plane = default(Plane);
                        for (int i = 0; i < polygons.Length && plane.normal == Vector3.zero; ++i)
                        {
                            if (!polygons[i].IsValid())
                                continue;
                            plane = polygons[i].plane;
                        }
                        if (plane.normal == Vector3.zero)
                            throw new System.Exception($"Fail to create {nameof(BSP_Tree)} due to invalid plane normal.");
                        var cnt     = polygons.Length;
                        var roots   = new List<Polygon>(cnt);
                        var front   = new List<Polygon>(cnt);
                        var back    = new List<Polygon>(cnt);
                        for (int i = 0; i < polygons.Length; ++i)
                        {
                            BSP_Utils.SplitPolygon(polygons[i], plane,
                                roots, roots, front, back);
                        }
                        FilterInvalid(front);
                        FilterInvalid(back);
                        if (front.Count > 0)
                        {
                            frontShare = new ThreadShare<BSP_Tree>();
                            shares.Add(frontShare.waithandle);
                            threads.Add(Create_BSP_Tree(front.ToArray(), frontShare));
                        }
                        if (back.Count > 0)
                        {
                            backShare = new ThreadShare<BSP_Tree>();
                            shares.Add(backShare.waithandle);
                            threads.Add(Create_BSP_Tree(back.ToArray(), backShare));
                        }

                        foreach (var t in threads)
                            t.Start();

                        foreach (var t in threads)
                            t.Join();

                        if (shares.Count > 0)
                            WaitHandle.WaitAll(shares.ToArray(), 10000);

                        var f = frontShare == null ? null
                            : frontShare.IsError ? throw new Exception($"{GetHashCode()}-Front:\n" + frontShare.exception.Message, frontShare.exception)
                            : frontShare.result;

                        var b = backShare == null ? null
                            : backShare.IsError ? throw new Exception($"{GetHashCode()}-Back:\n" + backShare.exception.Message, backShare.exception)
                            : backShare.result;

                        if (frontShare != null && !frontShare.IsError && f == null)
                            throw new Exception("Front tree read fail.");

                        if (backShare != null && !backShare.IsError && b == null)
                            throw new Exception("back tree read fail.");

                        share.Dispatch(new BSP_Tree(roots.ToArray(), plane, f, b));
                    }
                }
                catch(Exception ex)
                {
                    share.Throw(ex);
                    foreach (var t in threads)
                    {
                        if (t != null && t.IsAlive)
                            t.Abort();
                    }
                }
            }
        }

        #region Intersect
        public static Thread Intersect(BSP_Tree a, BSP_Tree b,
            ThreadShare<BSP_Tree> share)
        {
            var job = new _Intersect(a, b, share);
            var thread   = new Thread(new ThreadStart(job.Process));
#if BACKGROUND_THREAD
            thread.IsBackground = true;
#endif
            return thread;
        }

        private class _Intersect : BSP_Base
        {
            public _Intersect(BSP_Tree a, BSP_Tree b,
                ThreadShare<BSP_Tree> share)
                : base(a, b, share)
            { }

            Thread t;
            public override void Process()
            {
                try
                {
                    var clipped = new List<Polygon>();
                    _ = BSP_Utils.ClipPolygons(a, b, ref clipped);
                    _ = BSP_Utils.ClipPolygons(b, a, ref clipped);

                    t = Create_BSP_Tree(clipped.ToArray(), share);
                    t.Start();
                    t.Join();
                }
                catch (System.Exception ex)
                {
                    if (t != null && t.IsAlive)
                        t.Abort();
                    share.Throw(ex);
                }
            }
        }
        #endregion Intersect

        #region Subtract
        public static Thread Subtract(BSP_Tree a, BSP_Tree b,
            ThreadShare<BSP_Tree> share)
        {
            var job = new _Subtract(a, b, share);
            var thread = new Thread(new ThreadStart(job.Process));
#if BACKGROUND_THREAD
            thread.IsBackground = true;
#endif
            return thread;
        }

        private class _Subtract : BSP_Base
        {
            public _Subtract(BSP_Tree a, BSP_Tree b,
                ThreadShare<BSP_Tree> share)
                : base(a, b, share)
            { }

            Thread t = null;
            public override void Process()
            {
                try
                {
                    List<Polygon> discard = new List<Polygon>();
                    var clipped = new List<Polygon>(b.GetPolygonCount());
                    var remainA = BSP_Utils.ClipPolygons(a, b, ref discard);
                    _ = BSP_Utils.ClipPolygons(b, a, ref clipped);
                    
                    for (int i=0; i<clipped.Count; ++i)
                        clipped[i].Flip();

                    var join = new List<Polygon>(remainA.Count + clipped.Count);
                    join.AddRange(remainA);
                    join.AddRange(clipped);
                    FilterInvalid(join);
                    t = Create_BSP_Tree(join.ToArray(), share);
                    t.Start();
                    t.Join();
                }
                catch (System.Exception ex)
                {
                    if (t != null && t.IsAlive)
                        t.Abort();
                    share.Throw(ex);
                }
            }
        }
        #endregion Subtract

        #region Union
        public static Thread Union(BSP_Tree a, BSP_Tree b,
            ThreadShare<BSP_Tree> share)
        {
            var job = new _Union(a, b, share);
            var thread = new Thread(new ThreadStart(job.Process));
#if BACKGROUND_THREAD
            thread.IsBackground = true;
#endif
            return thread;
        }
        private class _Union: BSP_Base
        {
            public _Union(BSP_Tree a, BSP_Tree b,
                ThreadShare<BSP_Tree> share)
                : base(a, b, share)
            { }

            Thread t = null;
            public override void Process()
            {
                try
                {
                    List<Polygon> discard = new List<Polygon>(a.GetPolygonCount() + b.GetPolygonCount());
                    var remainA = BSP_Utils.ClipPolygons(a, b, ref discard);
                    var remainB = BSP_Utils.ClipPolygons(b, a, ref discard);

                    var join = new List<Polygon>(remainA.Count + remainB.Count);
                    join.AddRange(remainA);
                    join.AddRange(remainB);
                    FilterInvalid(join);
                    t = Create_BSP_Tree(join.ToArray(), share);
                    t.Start();
                    t.Join();
                }
                catch (System.Exception ex)
                {
                    if (t != null && t.IsAlive)
                        t.Abort();
                    share.Throw(ex);
                }
            }
        }
        #endregion Union

        #region BSP_Tree convertion
        public class MatrixBSPData
        {
            public Matrix4x4 mainL2W, mainW2L, slaveL2W, slaveW2L;
            /// <summary>BSP In local space </summary>
            public BSP_Tree main;
            /// <summary>BSP in 'main's local space</summary>
            public BSP_Tree slave;
        }
        public static void ConvertMeshs(MeshFilter main, MeshFilter slave, 
            System.Action<MatrixBSPData> success, System.Action<Exception> fail)
        {
            var job = new Convert2MeshIntoSameSpace(main, slave, out var share);
            var thread = new Thread(new ThreadStart(job.Process));
#if BACKGROUND_THREAD
            thread.IsBackground = true;
#endif
            thread.Start();
            thread.Join();
            share.WaitResult(out var rst); // wait until back to main-thread

            // Convert Thread result into main-thread callback method.
            if (share.IsError)
            {
                fail?.Invoke(share.exception);
            }
            else
            {
                success?.Invoke(share.result);
            }
        }

        private class Convert2MeshIntoSameSpace
        {
            private Matrix4x4 mainL2W, mainW2L, slaveL2W, slaveW2L;
            private HalfEdge.HEMesh main, slave;
            private ThreadShare<MatrixBSPData> share;
            public Convert2MeshIntoSameSpace(MeshFilter mainRef, MeshFilter slaveRef, out ThreadShare<MatrixBSPData> share)
            {
                this.share = share = new ThreadShare<MatrixBSPData>();
                this.mainL2W = mainRef.transform.localToWorldMatrix;
                this.mainW2L = mainRef.transform.worldToLocalMatrix;
                this.slaveL2W = slaveRef.transform.localToWorldMatrix;
                this.slaveW2L = slaveRef.transform.worldToLocalMatrix;
                this.main = new HalfEdge.HEMesh();
                this.main.LoadFrom(mainRef.sharedMesh, Matrix4x4.identity);
                
                // calculate in main's local space.
                this.slave = new HalfEdge.HEMesh();
                this.slave.LoadFrom(slaveRef.sharedMesh, slaveL2W * mainW2L);
            }

            internal void Process()
            {
                List<Thread> threads = new List<Thread>(2);
                var mainShare = new ThreadShare<BSP_Tree>();
                var slaveShare = new ThreadShare<BSP_Tree>();
                var sharesHandle = new List<AutoResetEvent>(2);
                try
                {
                    threads.Add(Create_BSP_Tree(main.ToPolygons().ToArray(), mainShare));
                    threads.Add(Create_BSP_Tree(slave.ToPolygons().ToArray(), slaveShare));
                    sharesHandle.Add(mainShare.waithandle);
                    sharesHandle.Add(slaveShare.waithandle);

                    foreach (var t in threads)
                        t.Start();
                    foreach (var t in threads)
                        t.Join();

                    //mainShare.WaitResult(out var mTree);
                    //slaveShare.WaitResult(out var sTree);
                    WaitHandle.WaitAll(sharesHandle.ToArray(), 3000); // 3sec

                    if (mainShare.IsError)
                        throw mainShare.exception;
                    if (slaveShare.IsError)
                        throw slaveShare.exception;

                    // mainShare.result.Apply(mainW2L);
                    // slaveShare.result.Apply(mainL2W * slaveW2L);

                    var rst = new MatrixBSPData
                    {
                        mainL2W     = mainL2W,
                        mainW2L     = mainW2L,
                        slaveL2W    = slaveL2W,
                        slaveW2L    = slaveW2L,
                        main        = mainShare.result,

                        // Remark : slave was inside main's local space,
                        // need to apply matrix after all process.
                        slave       = slaveShare.result,
                    };
                    share.Dispatch(rst);
                }
                catch (System.Exception ex)
                {
                    share.Throw(ex);
                }
            }
        }
        #endregion BSP_Tree convertion

        private static void FilterInvalid(List<Polygon> polygons)
        {
            var i = polygons.Count;
            while (i-- > 0)
            {
                if (!polygons[i].IsValid())
                {
                    polygons.RemoveAt(i);
                    continue;
                }
#if false
                if (i > 1 &&
                    polygons[i - 1].IsValid() &&
                    PolygonHelper.TryUnionPolygons(polygons[i - 1], polygons[i],
                    out var report, 1e-6f))
                {
                    polygons[i - 1] = report.result;
                    polygons.RemoveAt(i);
                }
#endif
            }
        }

        public abstract class BSP_Base
        {
            protected BSP_Tree a, b;
            protected ThreadShare<BSP_Tree> share;
            public BSP_Base(BSP_Tree a, BSP_Tree b,
                ThreadShare<BSP_Tree> share)
            {
                this.a = a.Clone();
                this.b = b.Clone();
                this.share = share;
            }

            public abstract void Process();
        }
    }

}
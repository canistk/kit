using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
namespace Kit.MeshKit
{
    /// <summary>
    /// Share memory "result" between thread
    /// with <see cref="WaitHandle"/> to ensure sub-thread join align
    /// and hold the <see cref="exception"/> from sub-thread.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ThreadShare<T> : IDisposable
    {
        public T result { get; private set; } = default(T);
        public AutoResetEvent waithandle;

        public bool IsError => exception != null;
        public Exception exception;

        public bool isDisposed { get; private set; }
        private Action<T> callback;

        public ThreadShare()
        {
            this.waithandle = new AutoResetEvent(false);
        }
        public ThreadShare(Action<T> callback)
            : this()
        {
            this.callback = callback;
        }

        /// <summary>Signal for completed thread process.</summary>
        /// <param name="rst">return object value.</param>
        public void Dispatch(T rst)
        {
            this.result = rst;
            if (callback != null)
            {
                callback?.Invoke(rst);
            }
            this.waithandle.Set();
        }

        /// <summary>To throw exception from sub-thread.</summary>
        /// <param name="ex"></param>
        public void Throw(Exception ex)
        {
            if (exception == null)
            {
                exception = ex;
            }
            this.waithandle.Set();
        }
        /// <summary>For main-thread to listen and wait for sub-thread process return.
        /// <see cref="result"/></summary>
        /// <param name="rst"></param>
        /// <param name="timeoutMs"></param>
        public void WaitResult(out T rst, int timeoutMs = 10000)
        {
            WaitHandle.WaitAll(new[] { waithandle }, timeoutMs);
            rst = result;
        }

        #region Dispose
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    waithandle.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                isDisposed = true;
            }
        }

        ~ThreadShare()
        {
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }
}
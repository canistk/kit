using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.MeshKit
{
    public struct Polygon2d
    {
        public readonly Polygon source;

        public Polygon2d(Polygon polygon)
        {
            this.source = polygon;
        }

        public ref Vertex this[int key]
        {
            get => ref source.vertices[key];
        }

        public int Count => source.Count;

        /// <summary>Assume polygon is projected, ignore Z-axis calculation</summary>
        /// <param name="polygonXY"></param>
        /// <returns></returns>
        public (int, float) FindFarestIndex(Vector2 anchor)
        {
            // A.K.A the longest vector is bottom-left
            (int, float) farest = (-1, float.NegativeInfinity);
            for (int i = 0; i < Count; ++i)
            {
                var p = source.vertices[i].position.XY() - anchor;
                var sqrDis = p.sqrMagnitude;
                if (sqrDis < farest.Item2)
                    continue;

                farest = (i, sqrDis);
            }
            return farest;
        }

        public Vector3 Centroid()
        {
            return source.Centroid();
        }

        /// <summary>
        /// Assume polygon vertex within local space, which ignore Y-axis
        /// only use XY to calculate the bound box which is present by rect in U3D
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        public Rect GetPolygonRect()
        {
            var rst = new Rect(source[0].position.XY(), Vector2.zero);
            for (int i = 0; i < source.Count; ++i)
            {
                var p = source[i].position.XY();
                rst = rst.Encapsulate(p);
            }
            return rst;
        }

        /// <summary>Check if the giving point is near by edge,
        /// near by = include both inside AND outside.</summary>
        /// <param name="point"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public bool IsCloseEnough(Vector2 point, float threshold = float.Epsilon)
        {
            var ppp = source.ClosestPointInPolygon(point);
            var rst =
                ppp.x.EqualRoughly(point.x, threshold) &&
                ppp.y.EqualRoughly(point.y, threshold);
            return rst;
        }

        /// <summary>check if point within or align edge(s)</summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsWithin(Vector3 point)
        {
            for (int i = 0; i < source.vertices.Length; ++i)
            {
                var p0 = source.vertices[i].position;
                var p1 = source.vertices[(i + 1) % source.vertices.Length].position;
                var v0 = p1 - p0;
                var v1 = point - p0;
                var n0 = Vector3.Cross(v1, v0);
                if (n0.z > 0)
                    return false;
            }
            return true;
        }

        public bool TryGetVertexById(int id, out Vertex vertex)
        {
            for (int i = 0; i < source.vertices.Length; ++i)
            {
                if (source.vertices[i].id == id)
                {
                    vertex = source.vertices[i];
                    return true;
                }
            }
            vertex = default;
            return false;
        }

        public static Polygon operator *(Polygon2d p, Matrix4x4 matrix)
        {
            var v = new Vertex[p.source.vertices.Length];
            for (int i = 0; i < p.source.vertices.Length; ++i)
            {
                v[i].position   = matrix.MultiplyPoint(p.source.vertices[i].position);
                v[i].normal     = matrix.MultiplyVector(p.source.vertices[i].normal);
                v[i].tangent    = matrix.MultiplyVector(p.source.vertices[i].tangent);
                v[i].uv         = p.source.vertices[i].uv;
            }
            var plane = new Plane(
                v[0].position,
                v[1].position,
                v[2].position);
            return new Polygon
            {
                plane = plane,
                vertices = v
            };
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using Kit;
using Kit.MeshKit;
using Kit.MeshKit.FaceEdge;

using UnityEngine;
[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class PrimitiveMesh : MonoBehaviour
{
    private FEMesh feMesh;
    private Mesh m_Mesh;
    [SerializeField] MeshFilter m_MeshFilter;
    private MeshFilter meshFilter
    {
        get
        {
            if (m_MeshFilter == null)
                m_MeshFilter = GetComponent<MeshFilter>();
            return m_MeshFilter;
        }
    }

    private enum eShape
    {
        None = 0,
        Cube,
        Sphere,
        Capsule,
    }
    [SerializeField] private eShape m_Shape = eShape.None;

    #region Mono
    private void Reset()
    {
        _ = ReferenceEquals(meshFilter,null);
    }

    private void OnEnable()
    {
        GenerateMesh();
    }

    private void OnDisable()
    {
        CleanMesh();
    }

    private void OnValidate()
    {
        // hack to avoid send message during OnValidate.
        Invoke(nameof(GenerateMesh), 0f);
    }
    #endregion Mono

    #region Generate Mesh
    private void GenerateMesh()
    {
        switch (m_Shape)
        {
            default: CleanMesh(); break;
            case eShape.Cube: GenCube(); break;
            case eShape.Sphere: GenSphere(); break;
            case eShape.Capsule: GenCapsule(); break;
        }
    }

    // [Header("Cube")]
    [DrawIf(nameof(m_Shape), eShape.Cube, disablingType: DisablingType.DontDraw)]
    [DrawIf(nameof(m_Shape), eShape.Cube, disablingType: DisablingType.DontDraw)] public Vector3 m_Center0 = Vector3.zero;
    [DrawIf(nameof(m_Shape), eShape.Cube, disablingType: DisablingType.DontDraw)][Min(0.1f)] public Vector3 m_Size = Vector3.one;
    private void GenCube()
    {
        if (m_Mesh != null)
            CleanMesh();
        if (feMesh == null)
            feMesh = new FEMesh();
        MeshExtend.CreateCube(feMesh, m_Center0, m_Size);
        AssignMesh(feMesh);
    }


    // [Header("Sphere")]
    [DrawIf(nameof(m_Shape), eShape.Sphere, disablingType: DisablingType.DontDraw)]
    [DrawIf(nameof(m_Shape), eShape.Sphere, disablingType: DisablingType.DontDraw)] public Vector3 m_Center1 = Vector3.zero;
    [DrawIf(nameof(m_Shape), eShape.Sphere, disablingType: DisablingType.DontDraw)] [Min(0.1f)] public float m_Radius1 = .5f;
    [DrawIf(nameof(m_Shape), eShape.Sphere, disablingType: DisablingType.DontDraw)] [Min(2)] public int m_xSegment1 = 10;
    [DrawIf(nameof(m_Shape), eShape.Sphere, disablingType: DisablingType.DontDraw)] [Min(2)] public int m_ySegment1 = 10;
    private void GenSphere()
    {
        if (m_Mesh != null)
            CleanMesh();
        if (feMesh == null)
            feMesh = new FEMesh();
        MeshExtend.CreateSphere(feMesh, m_Center1, m_Radius1, m_xSegment1, m_ySegment1);
        AssignMesh(feMesh);
    }

    // [Header("Capsule")]
    [DrawIf(nameof(m_Shape), eShape.Capsule, disablingType: DisablingType.DontDraw)] public Vector3 m_Center2 = Vector3.zero;
    [DrawIf(nameof(m_Shape), eShape.Capsule, disablingType: DisablingType.DontDraw)] [Range(0,2)] public int m_Direction2 = 1; // x=0, y=1, z=2
    [DrawIf(nameof(m_Shape), eShape.Capsule, disablingType: DisablingType.DontDraw)] [Min(0.1f)] public float m_Height2 = 2f;
    [DrawIf(nameof(m_Shape), eShape.Capsule, disablingType: DisablingType.DontDraw)] [Min(0.1f)] public float m_Radius2 = 0.5f;
    [DrawIf(nameof(m_Shape), eShape.Capsule, disablingType: DisablingType.DontDraw)] [Min(2)] public int m_xSegment2 = 10;
    [DrawIf(nameof(m_Shape), eShape.Capsule, disablingType: DisablingType.DontDraw)] [Min(2)] public int m_ySegment2 = 10;
    private void GenCapsule()
    {
        if (m_Mesh != null)
            CleanMesh();
        if (feMesh == null)
            feMesh = new FEMesh();
        MeshExtend.CreateCapsule(feMesh, m_Center2, m_Radius2, m_Height2, m_Direction2, m_xSegment2, m_ySegment2);
        AssignMesh(feMesh);
    }
    #endregion Generate Mesh

    private void AssignMesh(FEMesh mesh)
        => AssignMesh(mesh.ToMesh());

    private void AssignMesh(Mesh mesh)
    {
        m_Mesh = mesh;
        m_Mesh.name = $"Mesh-{m_Mesh.GetHashCode()}";
        meshFilter.sharedMesh = m_Mesh;
    }

    private void CleanMesh()
    {
        if (m_Mesh != null)
        {
            m_Mesh.Clear();
            if (Application.isPlaying)
                Destroy(m_Mesh);
            else
                DestroyImmediate(m_Mesh);
        }
        if (feMesh != null)
        {
            feMesh.Clean();
        }
    }
}

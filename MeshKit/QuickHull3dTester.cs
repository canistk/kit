using Kit.MeshKit.FaceEdge;
using Kit.MeshLib.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.MeshKit
{
    /// <summary>
    /// <see cref="https://media.steampowered.com/apps/valve/2014/DirkGregorius_ImplementingQuickHull.pdf"/>
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class QuickHull3dTester : MonoBehaviour
    {
        private MeshFilter m_MeshFilter;
        public MeshFilter meshFilter
        {
            get
            {
                if (m_MeshFilter == null)
                    m_MeshFilter = GetComponent<MeshFilter>();
                return m_MeshFilter;
            }
        }

        [SerializeField] MeshFilter m_Target = null;

        [SerializeField] bool m_GreenFlag = false;
        [SerializeField] bool m_Reboot = false;

        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                m_GreenFlag = true;
            }

            if (m_Reboot)
            {
                m_Reboot = false;
                m_Points.Clear();
                m_Conflict.Clear();
                state = eState.Idle;
            }

            if (m_GreenFlag)
            {
                m_GreenFlag = false;
                switch (state)
                {
                    default: break;
                    case eState.Idle:   ++state; break;
                    case eState.RawPoint:   CaptureVertexFromMesh(m_Target, out m_Points); ++state; break;
                    case eState.Horizon:    DefineHorizon(m_Points, m_Conflict); ++state; break;
                    case eState.Tetrahedron:DefineTetrahedron(m_Points, m_Conflict); ++state; break;
                    case eState.Hulling:

                        break;
                    case eState.Completed:break;
                    case eState.Error:break;
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (m_Points == null || m_Points.Count == 0)
                return;

            _DrawRawPoints();
            _DrawConflict();

            void _DrawRawPoints()
            {
                for (int i = 0; i < m_Points.Count; ++i)
                {
                    var p0 = m_Points[i].position;
                    GizmosExtend.DrawSphere(p0, 0.01f, Color.red);
                }
            }

            void _DrawConflict()
            {
                using (new ColorScope(Color.yellow.CloneAlpha(0.3f)))
                {
                    foreach (var poly in m_Conflict)
                    {
                        foreach ((var p0, var p1) in poly.GetEdges())
                        {
                            Gizmos.DrawLine(p0.position, p1.position);
                        }
                    }
                }
            }
        }


        public enum eState
        {
            Idle = 0,
            RawPoint,
            Horizon,
            Tetrahedron,
            Hulling, // N step.

            Completed,
            Error,
        }
        [ReadOnly]
        public eState state = eState.Idle;

        /// <summary>contain all point(s) that didn't process</summary>
        public List<Vertex> m_Points;

        /// <summary>build convex hull from faces.</summary>
        public List<Polygon> m_Conflict = new List<Polygon>();
        public FEMesh m_Hull = new FEMesh();

        /// <summary>
        /// 1) Construct conflict list
        /// </summary>
        private void CaptureVertexFromMesh(MeshFilter target, out List<Vertex> points)
        {
            if (target == null)
            {
                points = null;
                return;
            }

            var l2w     = target.transform.localToWorldMatrix;
            // var w2l = target.transform.worldToLocalMatrix;
            // points from local space.
            var p = new List<Vector3>();
            target.sharedMesh.GetVertices(p);
            points = new List<Vertex>(p.Count);
            for (int i = 0; i < p.Count; ++i)
            {
                // calculate point in world position.
                var vertex = new Vertex(i, l2w.MultiplyPoint(p[i]));
                points.Add(vertex);
            }
        }

        /// <summary>
        /// 2) Find left,right point
        /// 3) Find furthest point
        /// </summary>
        /// <param name="points">non-sort vertex</param>
        /// <param name="conflict">output horizon to openList</param>
        private void DefineHorizon(List<Vertex> points, List<Polygon> conflict)
        {
            var l = (-1, float.MaxValue);
            var r = (-1, float.MinValue);

            // 1) Construct conflict list
            // 2) Find left,right point
            var i = points.Count;
            while(i --> 0)
            {
                var point = points[i];
                if (l.Item2 > point.position.x)
                {
                    l = (i, point.position.x);
                }
                if (r.Item2 < point.position.x)
                {
                    r = (i, point.position.x);
                }
            }

            var left    = points[l.Item1];
            var right   = points[r.Item1];

            // 2) define vector from left,right 
            /// <see cref="Vector3Extend.PerpendicularDistance(Vector3, Vector3, Vector3)"/>
            var baseVector = right.position - left.position;
            var baseSqrDis = Vector3.Dot(baseVector, baseVector);
            var anchor = left.position;

            // 3) Find furthest point by Perpendicular distance (optimize use sqrt)
            (Vertex, float) f = (Vertex.Invalid, -1f);
            var k = points.Count;
            while (k-- > 0)
            {
                var p = points[k];
                if (p.id == left.id || p.id == right.id)
                {
                    // remove left,right from conflict list.
                    points.RemoveAt(k);
                    continue;
                }

                // project point on base vector
                var fromLineToPoint = p.position - anchor;
                var t = Vector3.Dot(fromLineToPoint, baseVector) / baseSqrDis;
                var projectedPoint = anchor + baseVector * t;

                // find sqr distance from point projected to line.
                var sqrDis = Vector3.SqrMagnitude(p.position - projectedPoint);
                if (f.Item2 < sqrDis)
                {
                    f = (p, sqrDis);
                }
            }
            
            // 4) located furthest point
            var furthest = f.Item1;
            points.Remove(f.Item1); // remove from list.

            // 5) define Horizon.
            var horizon = new Polygon(left, right, furthest);
            conflict.Add(horizon);
        }

        /// <summary>
        /// 4) Find 4th point
        /// 5) Calculate centroid
        /// 6) create 4 faces -> Tetrahedron in Hull(m_Faces)
        /// </summary>
        private void DefineTetrahedron(List<Vertex> points, List<Polygon> conflict)
        {
            if (conflict.Count == 0)
                return;

            var polygon = conflict[0];
            
            // index, distance
            (int, float) furthest = (-1, -1f);
            for (int i = 0; i < points.Count; ++i)
            {
                var p = points[i].position;

                // Note: first tetrahedron will not check this condition,
                // since we didn't define the outside face yet.
                //if (hull.Count > 0)
                //{
                //    var signDis = polygon.plane.GetDistanceToPoint(p);
                //    if (signDis < 0)
                //        continue; // skip point behind this plane.
                //}

                var cpop = polygon.plane.ClosestPointOnPlane(p);
                var disSqrt = (p - cpop).sqrMagnitude;
                if (furthest.Item2 < disSqrt)
                {
                    var cpip = polygon.ClosestPointInPolygon(p);
                    if (!cpip.Approximately(cpop))
                        continue; // skip point outside this polygon.

                    furthest = (i, disSqrt);
                }
            }

            if (furthest.Item1 == -1)
            {
                Debug.LogError($"Fail to execute during state={state}");
                state = eState.Error;
            }

            // We had horizon + far point (4 points)
            var far = points[furthest.Item1];
            points.Remove(far);

            // Define Tetrahedron,
            // 1 triangle = 3 edges + 1 furthest point again triangle, 1 + 3 = 4 faces.
            var centroid = far.position;
            var cnt = 1;
            foreach (var p in polygon.vertices)
            {
                centroid += p.position;
                ++cnt;
            }
            centroid /= (float)cnt;

            foreach ((var p0, var p1) in polygon.GetEdges())
            {
                var tmp = new Polygon(p0, far, p1);
                if (tmp.IsPositiveSide(centroid))
                    tmp.Flip();
                conflict.Add(tmp);
            }

            RemovePointsInsideHull(points, conflict);
        }

        private void RemovePointsInsideHull(List<Vertex> points, List<Polygon> conflict)
        {
            if (conflict.Count == 0)
                return;

            var i = points.Count;
            while (i-- > 0)
            {
                var point   = points[i];
                var outside = false;
                for (int k = 0; k < conflict.Count && !outside; ++k)
                {
                    outside = conflict[k].IsPositiveSide(point.position);
                }
                if (!outside)
                {
                    // unseen point is inside the hull.
                    points.RemoveAt(i);
                }
            }
        }

        private void AddPolygonToHull(Polygon polygon, FEMesh hull)
        {

        }

        private void FindFurthestPointOnPolygon(List<Vertex> points, List<Polygon> conflict)
        {

        }
    }
}
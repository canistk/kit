using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit.MeshKit;
using Kit;

[ExecuteInEditMode]
public class TestPolygon : MonoBehaviour
{
    [SerializeField] Transform[] m_Points;
    [SerializeField] Transform m_Target;

    [System.Flags]
    public enum eGizmos
    {
        Shape           = 1 << 0,
        Normal          = 1 << 1,
        Rect            = 1 << 2,
        ClosestPoint    = 1 << 3,
        Union           = 1 << 4,
        MainEdgeList    = 1 << 5,
        SlaveEdgeList   = 1 << 6,
    }
    [System.Serializable]
    public class Setup
    {
        public eGizmos gizmos;

        public Color m_ClosestColor = Color.yellow;
        [Range(0.01f, 1f)] public float m_ClosestSize = 0.1f;
        public Color m_ShapeColor = Color.green;
        public Color m_InvalidColor = Color.green;
        public Color m_IntersectColor = Color.yellow;

        public Color m_UnionColor = Color.magenta;
        public Color m_MainEdgeColor = Color.yellow;
        public Color m_SlaveEdgeColor = Color.yellow;
    }
    [SerializeField]
    public Setup m_Setup;
    public TestPolygon m_Other;
    [Range(0f, 0.3f)] public float m_Threshold = 0.0001f;

    [Range(0f, 2f)] public float m_Displacement = 0.3f;

    private void ConvertPolygon()
    {
        List<Vertex> vertices = new List<Vertex>();
        for (int i = 0; i < m_Points.Length; i++)
        {
            if (m_Points[i] == null)
                continue;
            vertices.Add(new Vertex(i, m_Points[i].position));
        }
        m_Polygon = new Polygon(vertices.ToArray());
    }

    private Polygon m_Polygon;
    public Polygon GetPolygon()
    {
        ConvertPolygon();
        return m_Polygon;
    }
    private Vector3 targetPos => m_Target ? m_Target.position : Vector3.zero;

    private void Update()
    {
        if (transform.hasChanged)
        {
            ConvertPolygon();
            transform.hasChanged = false;
        }
    }

    private void OnDrawGizmos()
    {
        if (m_Setup.gizmos.HasFlag(eGizmos.Shape))
            DrawPolygon(m_Polygon, m_Setup.m_ShapeColor);
        DrawClosestPoint();
        DrawOtherPolygon();
    }

    private void DrawPolygon(Polygon polygon, Color color, Vector3 offset = default)
    {
        if (!polygon.IsValid())
            return;

        using (new ColorScope(color))
        {
            foreach ((var p0, var p1) in polygon.GetEdges())
            {
                Gizmos.DrawLine(p0.position + offset, p1.position + offset);
            }
        }

        var normal = polygon.normal;
        if (normal == Vector3.zero)
            return;
        var centroid = polygon.Centroid();
        var isClockwise = polygon.IsClockwiseOrAntiClockwise();
        if (m_Setup.gizmos.HasFlag(eGizmos.Normal))
        {
            GizmosExtend.DrawRay(centroid, normal, color);
            DrawRotation(centroid, normal, color, isClockwise);
        }
    }

    private void DrawClosestPoint()
    {
        if (m_Target == null)
            return;
        if (!m_Polygon.IsValid())
            return;

        // Find Closest Point within Polygon.
        var ppp = m_Polygon.ClosestPointInPolygon(targetPos);
        if (m_Setup.gizmos.HasFlag(eGizmos.ClosestPoint))
        {
            var isOutOfArea = (targetPos - ppp).sqrMagnitude > 0.0001f;
            if (isOutOfArea)
            {
                var size = m_Setup.m_ClosestSize;
                var col = m_Setup.m_InvalidColor;
                GizmosExtend.DrawSphere(targetPos, size, col.CloneAlpha(col.a * 0.5f));
                GizmosExtend.DrawLine(targetPos, ppp, col.CloneAlpha(col.a * 0.3f));
                GizmosExtend.DrawSphere(ppp, size, col);
            }
            else
            {
                var col = m_Setup.m_ClosestColor;
                GizmosExtend.DrawSphere(ppp, 0.02f, Color.white);
            }
        }
        // return rst;
    }


    private System.Text.StringBuilder sb = null;
    private void DrawOtherPolygon()
    {
        if (m_Other == null)
            return;

        var self = GetPolygon();
        var other = m_Other.GetPolygon();
        var re0 = new PolygonHelper.AlignReport(self, other, m_Threshold);
        GizmosExtend.DrawSphere(re0.anchor, 0.2f, Color.white.CloneAlpha(0.3f));
        if (sb == null)
            sb = new System.Text.StringBuilder(200);

        sb.Clear()
            .Append("Angle Align :").AppendLine(re0.angleAlign.ToString())
            .Append("on same plane :").AppendLine(re0.isOnSamePlane.ToString());

        PolygonHelper.IntersectReport re1 = re0.isOnSamePlane ?
            new PolygonHelper.IntersectReport(re0) :
            default;
        if (re0.isOnSamePlane)
        {
            sb.Append("Intersect Type :").AppendLine(re1.type.ToString());

            if (m_Setup.gizmos.HasFlag(eGizmos.Rect))
            {
                re1.GetRectsCorners(out var rect0, out var rect1);
                DrawRect(rect0, Color.green.CloneAlpha(0.5f));
                DrawRect(rect1, Color.yellow.CloneAlpha(0.5f));
            }

            //for (int i = 0; i < re1.intersects.Length; ++i)
            //{
            //    re1.GetIntersect(i, out var intersect);
            //    GizmosExtend.DrawSphere(intersect.position, 0.06f, m_Setup.m_IntersectColor);
            //}
        }

        if (re1.type == PolygonHelper.IntersectReport.eIntersectType.PartialIntersect ||
            re1.type == PolygonHelper.IntersectReport.eIntersectType.Touch)
        {
            var quat = re1.alignReport.local2world.GetRotation();
            var n = quat * Vector3.forward;
            var offset = n * 3f * m_Displacement;
            if (PolygonHelper.TryUnionPolygons(re1, out var unionReport, 1e-6f))
            {
                if (m_Setup.gizmos.HasFlag(eGizmos.Union))
                {
                    var polygon = unionReport.result;
                    DrawPolygon(polygon, m_Setup.m_UnionColor, offset);
                }
            }

            sb.AppendLine("Intersects:");
            for (int i = 0; i<re1.intersects.Length; ++i)
            {
                sb.AppendLine(re1.intersects[i].ToString());
            }

            var matrix = re1.alignReport.local2world;
            var f = false;
            if (m_Setup.gizmos.HasFlag(eGizmos.MainEdgeList))
            {
                sb.AppendLine("Main Edge:");
                offset = n * 1f * m_Displacement;
                foreach (var edge in unionReport.main.edges)
                {
                    var fPos = matrix.MultiplyPoint3x4(edge.from.position);
                    var dir = matrix.MultiplyVector(edge.to.position - edge.from.position);
                    sb.AppendLine(edge.ToString());

                    if (!f)
                    {
                        f = true;
                        var str = "Main";
                        if (unionReport.startId == edge.from.id)
                            str += "\nStart";
                        GizmosExtend.DrawLabel(fPos + offset,
                            $"{edge.from.id}\n{str}");
                    }
                    else
                    {
                        GizmosExtend.DrawLabel(fPos + offset,
                            $"{edge.from.id}");
                    }
                    GizmosExtend.DrawArrow(fPos + offset, dir, m_Setup.m_MainEdgeColor);
                }
            }

            if (m_Setup.gizmos.HasFlag(eGizmos.SlaveEdgeList))
            {
                sb.AppendLine("Slave Edge:");
                f = false;
                offset = n * 2f * m_Displacement;
                foreach (var edge in unionReport.slave.edges)
                {
                    var fPos = matrix.MultiplyPoint3x4(edge.from.position);
                    var dir = matrix.MultiplyVector(edge.to.position - edge.from.position);
                    sb.AppendLine(edge.ToString());
                    if (!f)
                    {
                        f = true;
                        var str = "Slave";
                        if (unionReport.startId == edge.from.id)
                            str += "\nStart";
                        GizmosExtend.DrawLabel(fPos + offset,
                            $"{edge.from.id}\n{str}", 0f, 0.2f);
                    }
                    else
                    {
                        GizmosExtend.DrawLabel(fPos + offset,
                            $"{edge.from.id}", 0f, 0.2f);
                    }
                    GizmosExtend.DrawArrow(fPos + offset, dir, m_Setup.m_SlaveEdgeColor);
                }
            }
        }

        GizmosExtend.DrawLabel(transform.position, sb.ToString());

        void DrawRect(Vector3[] arr, Color? color)
        {
            using (new ColorScope(color))
            {
                for (int i = 0; i < arr.Length; ++i)
                {
                    var p0 = arr[i];
                    var p1 = arr[(i + 1) % arr.Length];
                    Gizmos.DrawLine(p0, p1);
                }
            }
        }
    }

    private static void DrawRotation(Vector3 center, Vector3 normal, Color? color, bool clockwise, float radius = 1f, float arc = 90f, float wing = 0.3f)
    {
        var quat = Quaternion.LookRotation(normal);
        var sign = clockwise ? 1f : -1f;
        var v0 = new Vector3(radius * wing * sign, radius * wing, 0f);
        var v1 = new Vector3(radius * wing * sign, radius * -wing, 0f);
        var a1 = center + quat * new Vector3(0f, radius, 0f);
        GizmosExtend.DrawRay(a1, quat * v0, color);
        GizmosExtend.DrawRay(a1, quat * v1, color);
        var a2 = center + quat * new Vector3(0f, -radius, 0f);
        GizmosExtend.DrawRay(a2, quat * -v0, color);
        GizmosExtend.DrawRay(a2, quat * -v1, color);

        var c = color.HasValue ? color.Value : Color.white;
        var tangent = quat.GetUp();
        GizmosExtend.DrawWireArc(center, normal, tangent, clockwise ? -arc : arc, radius, c);
        GizmosExtend.DrawWireArc(center, normal, -tangent, clockwise ? -arc : arc, radius, c);
    }

}

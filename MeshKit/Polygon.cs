using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Kit.MeshKit
{
    /// <summary>
    /// <see cref="PolygonHelper"/> for more function.
    /// <see cref="https://github.com/AngusJohnson/Clipper2/tree/main/CSharp/Clipper2Lib"/>
    /// </summary>
    [System.Serializable]
    public struct Polygon
    {
        public Vertex[] vertices;
        public Plane plane;
        public Vector3 normal => plane.normal;

        private int m_id;
        public int id => m_id;

        public Polygon(params Vertex[] vertices) : this(-1, vertices) { }

        public Polygon(int id, params Vertex[] vertices)
        {
            if (vertices.Length < 3)
                throw new System.ArgumentException("at least 3 vertex to from polygon");
            this.m_id = id;
            this.vertices = vertices;
            TryDefinePlaneFromPolygon(this.vertices, out this.plane);
        }

        public Polygon(Vector3 up, params Vertex[] vertices) :
            this (1, up, vertices) { }

        public Polygon(int id, Vector3 up, params Vertex[] vertices)
        {
            if (vertices.Length < 3)
                throw new System.ArgumentException("at least 3 vertex to from polygon");
            this.m_id = id;
            this.vertices = vertices;
            TryDefinePlaneFromPolygon(this.vertices, out this.plane);
            if (this.plane.normal == Vector3.zero)
                return;
            if (Vector3.Dot(up, this.plane.normal) < 0)
                Flip();
        }

        public Polygon(IEnumerable<Vertex> vertices) : this(-1, vertices) { }

        public Polygon(int id, IEnumerable<Vertex> vertices) : this(id, vertices.ToArray()) { }

        public static bool TryDefinePlaneFromPolygon(Vertex[] vs, out Plane plane)
        {
            plane = default;
            if (vs.Length < 3)
                return false;

            var tried = 0;
            var k = 2;
            do
            {
                plane = new Plane(vs[k - 2].position, vs[k - 1].position, vs[k].position);
                ++k;
                ++tried;
            }
            while (plane.normal == Vector3.zero && k < vs.Length);

            if (plane.normal == Vector3.zero)
            {
                // var sb = new System.Text.StringBuilder();
                for (int i = 0; i < vs.Length; ++i)
                {
                    var p0 = vs[i].position;
                    var p1 = vs[(i + 1) % vs.Length].position;
                    DebugExtend.DrawLine(p1, p0, Color.red, 3f, false);
                    //sb.AppendLine($"point[{i}]={p0:F2}");
                }
                //Debug.LogError($"Polygon : invalid plane normal == Vector3.zero." +
                //    $"Tried = {tried}, vertex count = [{vs.Length}] :\n" +
                //    sb.ToString());
                return false;
            }
            return true;
        }

        public void Flip()
        {
            int fullLen = this.vertices.Length;
            int halfLen = this.vertices.Length / 2;
            Vertex tmp;
            for (int i = 0; i < halfLen; ++i)
            {
                // Swap toward center.
                tmp = this.vertices[i];
                this.vertices[i] = this.vertices[fullLen - 1 - i];
                this.vertices[fullLen - 1 - i] = tmp;
            }
            plane.Flip();
        }

        public void Local2World(Transform transform) => MultiplyVertices(transform.localToWorldMatrix);
        public void World2Local(Transform transform) => MultiplyVertices(transform.worldToLocalMatrix);
        public void Local2World(in Matrix4x4 local2World) => MultiplyVertices(local2World);
        public void World2Local(in Matrix4x4 local2World) => MultiplyVertices(local2World.inverse);
        private void MultiplyVertices(in Matrix4x4 matrix)
        {
            for (int i = 0; i < vertices.Length; ++i)
            {
                vertices[i].position = matrix.MultiplyPoint(vertices[i].position);
                vertices[i].normal = matrix.MultiplyVector(vertices[i].normal);
                vertices[i].tangent = matrix.MultiplyVector(vertices[i].tangent);
            }
            plane = new Plane(vertices[0].position, vertices[1].position, vertices[2].position);
        }
        
        public ref Vertex this[int key]
        {
            get => ref vertices[key];
        }
        public int Count
        {
            get => IsValid() ? vertices.Length : 0;
        }

        public static Polygon operator *(Polygon p, Matrix4x4 matrix)
        {
            var v = new Vertex[p.vertices.Length];
            for (int i = 0; i < p.vertices.Length; ++i)
            {
                v[i].position   = matrix.MultiplyPoint(p.vertices[i].position);
                v[i].normal     = matrix.MultiplyVector(p.vertices[i].normal);
                v[i].tangent    = matrix.MultiplyVector(p.vertices[i].tangent);
                v[i].uv         = p.vertices[i].uv;
            }
            var plane = new Plane(
                v[0].position,
                v[1].position,
                v[2].position);
            return new Polygon
            {
                plane = plane,
                vertices = v
            };
        }

        public bool IsValid()
        {
            return vertices != null && vertices.Length >= 3 && normal != Vector3.zero;
        }

        public bool IsPositiveSide(Vector3 point) => plane.GetSide(point);

        /// <summary>Triangulation</summary>
        /// <returns></returns>
        public IEnumerable<Triangle> ToTriangle()
        {
            var cnt = vertices.Length;
            var pt = 2;
            var origin = vertices[0];
            while (pt < cnt)
            {
                yield return new Triangle(new[] {
                    origin,             // 0
                    vertices[pt - 1],   // 1
                    vertices[pt],       // 2
                });
                ++pt;
            }
        }

        public IEnumerable<(Vertex, Vertex)> GetEdges()
        {
            for (int i = 0; i < vertices.Length; ++i)
            {
                var a = vertices[i];
                var b = vertices[(i + 1) % vertices.Length];
                yield return (a, b);
            }
        }

        public (Vertex, Vertex) GetEdge(int i)
        {
            if (i < 0 || i >= vertices.Length)
                throw new System.ArgumentOutOfRangeException();
            var a = vertices[i];
            var b = vertices[(i + 1) % vertices.Length];
            return (a, b);
        }

        public Vector3 Centroid()
        {
            var rst = Vector3.zero;
            for (var i = 0; i < vertices.Length; ++i)
            {
                rst += vertices[i].position;
            }
            return rst / (float)vertices.Length;
        }

        /// <summary>Sort current polygon vertices in clockwise order.</summary>
        public void SortClockwise(bool anitClockwise = false)
        {
            var centroid = Centroid();
            var list = new List<(Vector3 /* vector */, Vertex)>(vertices.Count());
            foreach (var vert in vertices)
            {
                list.Add((vert.position - centroid, vert));
            }

            var n = normal;
            if (anitClockwise)
                list.Sort((x, y) => Vector3.Dot(Vector3.Cross(x.Item1, y.Item1), n) < 0f ? 1 : -1);
            else
                list.Sort((x, y) => Vector3.Dot(Vector3.Cross(x.Item1, y.Item1), n) > 0f ? 1 : -1);

            for (int i = 0; i < list.Count; i++)
            {
                vertices[i] = list[i].Item2;
            }
        }

        /// <summary>check if current polygon's vertices is sort by clockwise or anti-clockwise</summary>
        /// <returns></returns>
        public bool IsClockwiseOrAntiClockwise()
        {
            int marker = 0; // -1, 0, 1
            if (normal == Vector3.zero)
                return false;
            var centroid = Centroid();
            for (int i = 0; i < vertices.Length; i++)
            {
                var v1 = vertices[i].position - centroid;
                var v2 = vertices[(i + 1) % vertices.Length].position - centroid;
                var step = Vector3.Dot(Vector3.Cross(v1, v2), normal);
                if (marker == 0)
                {
                    marker = step == 0f ? 0 : step > 0f ? 1 : -1;
                }
                else
                {
                    if (marker > 0 && step < 0f)
                        return false;
                    else if (marker < 0 && step > 0f)
                        return false;
                    // else, same as previous result
                }
            }
            return true;
        }

        /// <summary>
        /// Assume the polygons order by clockwise & convex shape,
        /// attempt to locate the point projected within polygon.
        /// Warning : this won't work while polygon not in convex shape.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public Vector3 ClosestPointInPolygon(Vector3 point)
        {
            // projectedPointOnPlane
            var ppp = plane.ClosestPointOnPlane(point);
            // distance, percentage, p0, p1
            (float, float, Vertex, Vertex) closestEdge = (float.MaxValue, 0f, default, default);
            var isOutOfArea = false;
            foreach ((var p0, var p1) in GetEdges())
            {
                var v0 = p1.position - p0.position;
                var v1 = ppp - p0.position;
                var n0 = Vector3.Cross(v1, v0);
                var d0 = Vector3.Dot(normal, n0);
                if (d0 > 0) // outside
                {
                    isOutOfArea = true;
                    var v2 = Vector3.Project(v1, v0.normalized);
                    var l0 = v0.magnitude;
                    var l2 = v2.magnitude;
                    var pt = l2 / l0;
                    var dot = Vector3.Dot(v2, v0);
                    var p2 = p0.position + (v0.normalized * l2);

                    var l3 =
                        dot < 0 ? -l2 : // out of p0
                        l2 < l0 ? 0f :  // within p0~p1
                        l2 - l0;        // out of p1

                    if (closestEdge.Item1 > Mathf.Abs(l3))
                    {
                        closestEdge.Item1 = Mathf.Abs(l3);
                        closestEdge.Item2 = Mathf.Sign(dot) * pt;
                        closestEdge.Item3 = p0;
                        closestEdge.Item4 = p1;
                    }
                }
            }

            var rst = ppp;
            if (isOutOfArea)
            {
                var p0 = closestEdge.Item3;
                var p1 = closestEdge.Item4;
                var tmp = closestEdge.Item2;
                var pt = Mathf.Abs(tmp);
                var sign = Mathf.Sign(tmp);
                rst =
                    (sign < 0) ? p0.position :
                    (pt >= 0 && pt <= 1f) ? Vector3.LerpUnclamped(p0.position, p1.position, pt) :
                    p1.position;
            }
            return rst;
        }

        public override string ToString()
        {
            return $"[Polygon:vCnt:{vertices.Length}]";
        }
    }
}
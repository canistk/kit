using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.MeshKit.FaceEdge
{
    /// <summary>
    /// Create Mesh using FaceEdge method.
    /// copy from Jason Chan, 
    /// - Canis 20240130.
    /// </summary>
    public class FEMesh
    {
        // Note : should never change the order, since the index of item are their id.
        private MyList<Point>       m_Points;
        private MyList<FaceEdge>    m_FaceEdges;
        private MyList<Edge>        m_Edges;
        private MyList<Face>        m_Faces;

        public FEMesh()
        {
            m_Points    = new MyList<Point>();
            m_FaceEdges = new MyList<FaceEdge>();
            m_Edges     = new MyList<Edge>();
            m_Faces     = new MyList<Face>();
        }

        public FEMesh(Mesh src) : this()
        {
            LoadFrom(src, Matrix4x4.identity);
        }

        public FEMesh(BSP_Tree tree) : this()
        {
            LoadFrom(tree);
        }

        public static explicit operator FEMesh(Mesh mesh)
        {
            return new FEMesh(mesh);
        }

        public void Clean()
        {
            m_Points.Clear();
            m_FaceEdges.Clear();
            m_Edges.Clear();
            m_Faces.Clear();
        }

        /// <summary>Add vertex to mesh</summary>
        /// <param name="pos"></param>
        /// <param name="normal"></param>
        /// <param name="uv"></param>
        /// <returns>index of point in <see cref="m_Points"/></returns>
        public int AddPoint(in Vector3 pos, in Vector3 normal = default, in Vector4 tangent = default, in Vector4 uv = default)
        {
            ref var pt = ref m_Points.Push();
            pt.Init(pos, normal, tangent, uv);
            return pt.id;
        }

        public int AddPoint(Vertex vertex)
        {
            return AddPoint(vertex.position, vertex.normal, vertex.tangent, vertex.uv);
        }

        /// <summary>Add face to mesh.</summary>
        /// <param name="pointIndices"></param>
        /// <returns>index of face in <see cref="m_Faces"/></returns>
        public int AddFace(params int[] pointIndices)
        {
            if (pointIndices == null)
                throw new System.ArgumentException("Null is invalid input");
            if (pointIndices.Length < 3)
                throw new System.ArgumentException("Not enough point to from face. at least 3 points.");
            ref var face = ref m_Faces.Push();

            var n = pointIndices.Length;
            var firstFaceEdge = -1;
            var lastFaceEdge = -1;
            for (int i = 0; i < n; ++i)
            {
                ref var p0 = ref m_Points.GetId(pointIndices[i]);
                ref var p1 = ref m_Points.GetId(pointIndices[(i + 1) % n]);

                ref var faceEdge = ref m_FaceEdges.Push();
                
                if (firstFaceEdge == -1)
                    firstFaceEdge = faceEdge.id;

                if (lastFaceEdge != -1)
                    m_FaceEdges.GetId(lastFaceEdge).next = faceEdge.id;


                lastFaceEdge = faceEdge.id;

                faceEdge.face = face.id;
                faceEdge.point = p0.id;

                var edgeId = FindEdge(p0.id, p1.id);
                if (edgeId < 0)
                {
                    // new edge
                    ref var edge = ref m_Edges.Push();
                    edge.AddFaceEdge(this, ref faceEdge);

                    edge.AddToPoint(this, ref p0, ref p1);
                }
                else
                {
                    ref var edge = ref m_Edges.GetId(edgeId);
                    edge.AddFaceEdge(this, ref faceEdge);
                }
            }

            if (firstFaceEdge == -1)
                throw new System.Exception("Logic Error.");
            face.faceEdgeStart = firstFaceEdge;
            if (lastFaceEdge != -1 && firstFaceEdge != -1)
            {
                m_FaceEdges.GetId(lastFaceEdge).next = firstFaceEdge;
            }

            return face.id;
        }

        /// <summary>Find edge id by giving points</summary>
        /// <param name="p0">index of point</param>
        /// <param name="p1">index of point</param>
        /// <returns>index of edge, -1 = not found</returns>
        public int FindEdge(Point p0, Point p1)
        {
            return FindEdge(p0.id, p1.id);
        }
        public int FindEdge(int p0, int p1)
        {
            if (p0 < 0)
                return -1;

            foreach (var edge in m_Edges.GetValues())
            {
                if (!edge.HasPoint(p0, p1))
                    continue;
                return edge.id;
            }

            return -1;
        }

        #region Candy
        // TODO: triangulation for any surface.
        // <see cref="https://www.habrador.com/tutorials/math/"/>
        // <see cref="https://www.habrador.com/tutorials/math/11-delaunay/"/>

        /// <summary>
        /// Create quad by 2 triangle.
        /// 3----2
        /// |  / |
        /// | /  |
        /// 0----1
        /// <see cref="Triangle"/>, <seealso cref="AddTriangle(Triangle)"/>
        /// </summary>
        /// <param name="p4">assume ording in anti-clockwise.</param>
        /// <param name="uv"></param>
        /// <param name="planeNormal"></param>
        public void AddQuad(Vector3[] p4, Vector3 planeNormal, Vector4 tangent, Vector4[] uv)
        {
            if (p4 == null || uv == null)
                throw new System.Exception($".");
            if (p4.Length != 4)
                throw new System.Exception($"Quad had 4 point.");
            if (p4.Length != uv.Length)
                throw new System.Exception($"UVs length should always align points p[{p4.Length}] && uv[{uv.Length}]");
            if (planeNormal == Vector3.zero)
                throw new System.Exception($"Required planeNormal for calculation.");

            AddTriangle(
                new Vector3[] { p4[0], p4[1], p4[2] },
                planeNormal,
                tangent,
                new Vector4[] { uv[0], uv[1], uv[2] }
                );
            AddTriangle(
                new Vector3[] { p4[0], p4[2], p4[3] },
                planeNormal,
                tangent,
                new Vector4[] { uv[0], uv[2], uv[3] }
                );
        }

        /// <summary>
        ///      2
        ///     /|
        ///    / |
        ///   /  |
        ///  /   |
        /// 0----1
        /// </summary>
        /// <param name="points"></param>
        /// <param name="uv"></param>
        /// <param name="planeNormal">auto define triangle's vertex sorting order.</param>
        public void AddTriangle(Vector3[] points, Vector3 planeNormal, Vector4 tangent, Vector4[] uv)
        {
            if (points == null || uv == null)
                throw new System.Exception($".");
            if (points.Length != 3)
                throw new System.Exception($"Triangle had 3 point.");
            if (points.Length != uv.Length)
                throw new System.Exception($"UVs length should always align points p[{points.Length}] && uv[{uv.Length}]");
            if (planeNormal == Vector3.zero)
                throw new System.Exception($"Required planeNormal for calculation.");

            var centroid    = (points[0] + points[1] + points[2]) / 3f;
            List<int> list  = new List<int>(3) { 0, 1, 2 };
            int _antiClockwise(int x, int y)
            {
                var vx      = Vector3.ProjectOnPlane(points[x] - centroid, planeNormal);
                var vy      = Vector3.ProjectOnPlane(points[y] - centroid, planeNormal);
                var cross   = Vector3.Cross(vx, vy);
                var dot     = Vector3.Dot(cross, planeNormal);
                return dot > 0 ? -1 : 1;
            }
            list.Sort(_antiClockwise);

            //var sb = new System.Text.StringBuilder();
            //sb.Append("Triangle > ");
            var pointIds    = new int[3];
            for (int i = 0; i < list.Count; ++i)
            {
                var idx     = list[i];
                pointIds[i] = AddPoint(points[idx], planeNormal, tangent, uv[idx]);

                //sb.Append(idx)
                //    .Append($"+UV{uv[idx]:F1}");
                //if (i < list.Count - 1)
                //    sb.Append(", ");
            }
            //Debug.Log(sb);

            AddFace(pointIds);
        }

        public void AddTriangle(Triangle t)
        {
            t.Push(this);
        }

        // TODO: review
        public void Optimize()
        {
            var pointMapping        = m_Points.OptimizeRemapClear();
            var edgeMapping         = m_Edges.OptimizeRemapClear();
            var faceEdgeMapping     = m_FaceEdges.OptimizeRemapClear();
            var faceMapping         = m_Faces.OptimizeRemapClear();

            foreach (var o in m_Points.GetValues())
            {
                ref var point       = ref m_Points.GetId(o.id);
                point.edgeHead      = pointMapping[point.edgeHead];
                point.edgeTail      = pointMapping[point.edgeTail];
            }

            foreach (var o in m_Edges.GetValues())
            {
                ref var edge        = ref m_Edges.GetId(o.id);
                edge.point0         = pointMapping[edge.point0];
                edge.point1         = pointMapping[edge.point1];
                edge.nextEdge0      = edgeMapping[edge.nextEdge0];
                edge.nextEdge1      = edgeMapping[edge.nextEdge1];
                edge.faceEdgeHead   = faceEdgeMapping[edge.faceEdgeHead];
                edge.faceEdgeTail   = faceEdgeMapping[edge.faceEdgeTail];
            }

            foreach (var o in m_FaceEdges.GetValues())
            {
                ref var faceEdge    = ref m_FaceEdges.GetId(o.id);

                faceEdge.next       = faceEdgeMapping[faceEdge.next];
                faceEdge.adjacent   = faceEdgeMapping[faceEdge.adjacent];
                faceEdge.point      = pointMapping[faceEdge.point];
                faceEdge.edge       = edgeMapping[faceEdge.edge];
                faceEdge.face       = faceMapping[faceEdge.face];
            }

            foreach (var o in m_Faces.GetValues())
            {
                ref var face        = ref m_Faces.GetId(o.id);
                face.faceEdgeStart  = faceEdgeMapping[face.faceEdgeStart];
            }
        }

        #endregion Candy

        #region Convertion
        /// <summary>
        /// Convert current FaceEdge into mesh.
        /// </summary>
        /// <returns></returns>
        public Mesh ToMesh()
        {
            // TODO: optimize point index before convert.

            var mesh        = new Mesh();
            mesh.hideFlags  = HideFlags.DontSave;
            var vertices    = new Vector3[m_Points.Count];
            var normals     = new Vector3[m_Points.Count];
            var uvs         = new Vector4[m_Points.Count];
            var tangents    = new Vector4[m_Points.Count];
            var i = 0;
            foreach (var point in m_Points.GetValues())
            {
                vertices[i] = point.pos;
                uvs[i]      = point.uv;
                normals[i]  = point.normal;
                tangents[i] = point.tangent;
                i++;
            }
            mesh.SetVertices(vertices);

            var triangleIds = new List<int>(4096);
            foreach (var face in m_Faces.GetValues())
            {
                // Triangulate each face
                var p0 = -1; var p1 = -1; var p2 = -1;
                foreach (var edge in face.GetFaceEdges(this))
                {
                    if (p0 == -1)
                    {
                        p0 = edge.point;
                        continue;
                    }

                    if (p1 == -1)
                    {
                        p1 = edge.point;
                        continue;
                    }

                    p2 = edge.point;

                    triangleIds.Add(p0);
                    triangleIds.Add(p1);
                    triangleIds.Add(p2);

                    p1 = p2;
                }
            }

            mesh.SetIndices (triangleIds.ToArray(), MeshTopology.Triangles, 0);
            mesh.SetUVs     (0, uvs.ToArray());
            mesh.SetNormals (normals);
            mesh.SetTangents(tangents);
            mesh.OptimizeReorderVertexBuffer();
            //mesh.RecalculateBounds();
            //mesh.Optimize();
            //mesh.RecalculateNormals();
            return mesh;
        }

        public void LoadFrom(MeshFilter meshFilter)
        {
            LoadFrom(meshFilter.sharedMesh, meshFilter.transform.localToWorldMatrix);
        }

        /// <summary>
        /// Load from any mesh and convert into FaceEdge format.
        /// allow to modify and output by <see cref="ToMesh"/>
        /// </summary>
        /// <param name="mesh"></param>
        public void LoadFrom(Mesh mesh, Matrix4x4 local2World)
        {
            Clean();

            var n = mesh.vertexCount;
            List<Vector3> vertices  = new List<Vector3>(n); mesh.GetVertices(vertices);
            List<Vector3> normals   = new List<Vector3>(n); mesh.GetNormals(normals);
            List<Vector4> tangents  = new List<Vector4>(n); mesh.GetTangents(tangents);
            List<Vector4> uvs0      = new List<Vector4>(n); mesh.GetUVs(0, uvs0);
            // Debug.Log($"Load From Mesh, Vertices={vertices.Count}, normals={normals.Count}, uvs0={uvs0.Count}");

            if (tangents.Count == 0 && n > 0)
                tangents.AddRange(new Vector4[n]);

            bool isLocal = local2World == default || local2World.isIdentity;

            for (int i = 0; i < n; ++i)
            {
                if (!isLocal)
                {
                    vertices[i] = local2World.MultiplyPoint(vertices[i]);
                    normals[i]  = local2World.MultiplyVector(normals[i]);
                    tangents[i] = local2World.MultiplyVector(tangents[i]);
                }
                AddPoint(vertices[i], normals[i], tangents[i], uvs0[i]);
            }

            for (int m = 0; m < mesh.subMeshCount; ++m)
            {
                List<int> triangle = new List<int>(mesh.triangles.Length);
                mesh.GetTriangles(triangle, m);
                for (int t = 0; t < triangle.Count; t+=3)
                {
                    AddFace(
                        triangle[t + 0],
                        triangle[t + 1],
                        triangle[t + 2]
                    );
                }
                //Debug.Log($"Load Triangle on mesh[{m}] = {triangle.Count}, %3= {(int)((float)triangle.Count / 3f)}");
            }
        }

        public void ToGameObject(string meshName, Vector3 pos, Quaternion rot,
            Material material, Transform parent,
            out GameObject go, out MeshFilter meshFilter, out MeshRenderer meshRenderer)
        {
            if (string.IsNullOrEmpty(meshName))
                meshName    = $"Mesh-{GetHashCode()}";
            go              = new GameObject(meshName);
            go.transform.SetPositionAndRotation(pos, rot);
            if (parent != null)
                go.transform.SetParent(parent, true);

            meshFilter      = go.AddComponent<MeshFilter>();
            meshFilter.mesh = ToMesh();

            meshRenderer    = go.AddComponent<MeshRenderer>();
            if (material != null)
                meshRenderer.materials = new Material[] { material };
        }

        public void ToGameObject(string meshName = null, Material material = null, Transform parent = null)
            => ToGameObject(meshName, Vector3.zero, Quaternion.identity, material, parent, out _, out _, out _);
        

        public void LoadFrom(GameObject go)
        {
            if (go == null)
                throw new System.NullReferenceException("Empty target.");

            var mf = go.GetComponentInChildren<MeshFilter>();
            if (mf == null)
            {
                Debug.LogError($"Fail to locate {nameof(MeshFilter)} on gameobject {go.name}", go);
                return;
            }

            LoadFrom(mf.sharedMesh, go.transform.localToWorldMatrix);
        }

        public void LoadFrom(IEnumerable<Triangle> triangles)
        {
            Clean();
            foreach (var tri in triangles)
            {
                AddTriangle(tri);
            }
        }

        public IEnumerable<Polygon> ToPolygons()
        {
            if (m_Points.Count == 0)
                throw new System.Exception("No point to convert.");
            foreach (var face in m_Faces.GetValues())
                //for (int i = 0; i < m_Faces.Count; ++i)
            {
                var vs = new List<Vertex>(4);
                foreach (var edges in face.GetFaceEdges(this))
                {
                    if (edges.point == -1)
                        throw new System.NullReferenceException("Edges.point shouldn't be null");
                    var p = m_Points.GetId(edges.point);
                    vs.Add((Vertex)p);
                }
                yield return new Polygon(vs);
            }
        }

        public IEnumerable<Polygon> ToPolygons(Matrix4x4 matrix)
        {
            if (m_Points.Count == 0)
                throw new System.Exception("No point to convert.");
            foreach(var face in m_Faces.GetValues())
            {
                var vs = new List<Vertex>();
                foreach (var edges in face.GetFaceEdges(this))
                {
                    if (edges.point == -1)
                        throw new System.NullReferenceException("Edges.point shouldn't be null");
                    var p = m_Points.GetId(edges.point);
                    var v = (Vertex)p * matrix;
                    vs.Add(v);
                }
                yield return new Polygon(vs);
            }
        }

        public void LoadFrom(IEnumerable<Polygon> polygons)
        {
            Clean();
            foreach (var p in polygons)
            {
                int[] vIds = new int[p.vertices.Length];
                for (int i = 0; i < p.vertices.Length; ++i)
                {
                    vIds[i] = AddPoint(p.vertices[i]);
                }
                AddFace(vIds);
            }
        }

        internal void ToBSPThread(
            System.Action<BSP_Tree> success,
            System.Action<System.Exception> fail)
        {
            var polygons = ToPolygons().ToArray();
            var share   = new ThreadShare<BSP_Tree>(_Callback);
            var thread  = BSP_Thread.Create_BSP_Tree(polygons, share);
            thread.Start();
            thread.Join();
            share.WaitResult(out _, 3000);
            if (share.IsError)
            {
                fail?.Invoke(share.exception);
                // share.exception.DeepLogInvocationException("ToBSPThread");
                return;
            }
            var rst = share.result;
            success?.Invoke(rst); // waitResult -> dispatch in main-thread 
            void _Callback(BSP_Tree node)
            {
                // Debug.Log("Callback success 2");
            }
        }

        internal void LoadFrom(BSP_Tree tree)
        {
            Clean();
            LoadFrom(tree.Polygons());
        }
        #endregion Convertion

        #region Face Edge Structure
        [System.Serializable]
        public struct Point : IItem
        {
            public static readonly Point Invalid = default(Point);

            private int m_id;
            public int id => m_id;
            public Vector3 pos, normal;
            public Vector4 tangent, uv;

            public int edgeHead, edgeTail;

            void IItem.SetId(int id)
            {
                this.m_id       = id;
                this.pos        = default;
                this.normal     = default;
                this.tangent    = default;
                this.uv         = default;
                this.edgeHead   = -1;
                this.edgeTail   = -1;
            }

            public void Init(Vector3 pos, Vector3 normal, Vector4 tangent, Vector4 uv)
            {
                this.pos    = pos;
                this.normal = normal;
                this.tangent = tangent;
                this.uv     = uv;
            }

            public static explicit operator Vertex(Point p)
            {
                return new Vertex(p.id, p.pos, p.normal, p.tangent, p.uv);
            }
        }

        [System.Serializable]
        public struct Edge : IItem
        {
            private int m_id;
            public int id => m_id;

            public int point0;
            public int point1;

            public int nextEdge0;
            public int nextEdge1;

            public int faceEdgeHead;
            public int faceEdgeTail;

            void IItem.SetId(int id)
            {
                m_id = id;
                point0 = -1;
                point1 = -1;
                nextEdge0 = -1;
                nextEdge1 = -1;
                faceEdgeHead = -1;
                faceEdgeTail = -1;
            }

            public bool HasPoint(Point v0, Point v1)
                => HasPoint(v0.id, v1.id);

            public bool HasPoint(int idx0, int idx1)
            {
                return
                    (point0 == idx0 && point1 == idx1) ||
                    (point1 == idx0 && point0 == idx1);
            }

            public int Next(Point v0)
                => Next(v0.id);

            public int Next(int vertexId)
            {
                if (vertexId == point0) return nextEdge0;
                if (vertexId == point1) return nextEdge1;
                throw new System.Exception();
            }

            public void SetNext(Point vertex, Edge otherEdge)
            {
                SetNext(vertex.id, otherEdge.id);
            }

            public void SetNext(int vertexId, int nextEdgeId)
            {
                if (vertexId == point0) { nextEdge0 = nextEdgeId; return; }
                if (vertexId == point1) { nextEdge1 = nextEdgeId; return; }
                throw new System.Exception();
            }

            public void AddFaceEdge(FEMesh mesh, ref FaceEdge faceEdge)
            {
                faceEdge.edge = id;

                if (faceEdgeHead < 0)
                {
                    // first face edge of this edge.
                    faceEdgeHead = faceEdge.id;
                    faceEdgeTail = faceEdge.id;
                    faceEdge.adjacent = faceEdge.id;
                }
                else
                {
                    ref var tail = ref mesh.m_FaceEdges.GetId(faceEdgeTail);
                    tail.adjacent = faceEdge.id;    // append to current last element of linklist
                    faceEdgeTail = faceEdge.id;     // set this to tail
                    faceEdge.adjacent = faceEdgeHead;   // link-back to head. (loop ref)
                }
            }

            public void AddToPoint(FEMesh mesh, ref Point p0, ref Point p1)
            {
                point0 = p0.id;
                point1 = p1.id;

                if (p0.edgeHead < 0)
                {
                    p0.edgeHead = id;
                }
                else
                {
                    ref var edge = ref mesh.m_Edges.GetId(p0.edgeTail);
                    edge.SetNext(p0.id, id);
                }

                if (p1.edgeHead < 0)
                {
                    p1.edgeHead = id;
                }
                else
                {
                    ref var edge = ref mesh.m_Edges.GetId(p1.edgeTail);
                    edge.SetNext(p1.id, id);
                }

                p0.edgeTail = id;
                p1.edgeTail = id;

                nextEdge0 = p0.edgeHead;
                nextEdge1 = p1.edgeHead;
            }
        }

        [System.Serializable]
        public struct FaceEdge : IItem
        {
            private int m_Id;
            public int id => m_Id;

            public int next;
            public int adjacent; // FaceEdge shared by the same edge

            public int point;
            public int edge;
            public int face;

            void IItem.SetId(int id)
            {
                m_Id = id;
                next = -1;
                adjacent = -1; // FaceEdge share by same edge.
                point = -1;
                edge = -1;
                face = -1;
            }

            public void Init(int id, int face, int next, int point)
            {
                this.m_Id = id;
                this.next = next;
                this.adjacent = -1;
                this.point = point;
                this.edge = -1;
                this.face = face;
            }
        }

        [System.Serializable]
        public struct Face : IItem
        {
            private int m_id;
            public int id => m_id;
            public int faceEdgeStart;

            void IItem.SetId(int id)
            {
                this.m_id = id;
                this.faceEdgeStart = -1;
            }

            public void Init(int faceEdgeId)
            {
                this.faceEdgeStart = faceEdgeId;
            }

            public IEnumerable<FaceEdge> GetFaceEdges(FEMesh mesh)
            {
                if (faceEdgeStart == -1)
                    yield break;
                var startId = faceEdgeStart;
                var id = startId;
                FaceEdge pt;
                do
                {
                    pt = mesh.m_FaceEdges.GetId(id);
                    yield return pt;
                    id = pt.next;
                }
                while (id != startId);
            }
        }
        #endregion Face Edge Structure

        #region Utils
        private interface IItem
        {
            public void SetId(int id);
        }

        private class MyList<T>
            where T : IItem
        {
            private int m_Pt;
            private T[] m_data;
            private Queue<int> m_Removed;
            public int Count => m_Pt - m_Removed.Count;

            public MyList(int capacity = 4)
            {
                m_Pt = 0;
                m_data = new T[System.Math.Max(4, capacity)];
                m_Removed = new Queue<int>(4);
            }

            public ref T Push()
            {
                if (m_Removed.Count > 0)
                {
                    var reuse = m_Removed.Dequeue();
                    m_data[reuse].SetId(reuse);
                    return ref m_data[reuse];
                }
                
                if (m_Pt >= m_data.Length)
                {
                    Reserve(m_Pt + 1);
                }
                var id = m_Pt++;
                m_data[id].SetId(id);
                return ref m_data[id];
            }

            public void Append(MyList<T> src)
            {
                if (src == null)
                    return;
                foreach (var val in src.GetValues())
                {
                    Push() = val;
                }
            }

            public IEnumerable<T> GetValues()
            {
                for (int i = 0; i < m_Pt; ++i)
                {
                    if (m_Removed.Contains(i))
                        continue;
                    yield return m_data[i];
                }
            }

            public bool HasId(int id)
            {
                return id >= 0 && id < m_Pt && !m_Removed.Contains(id);
            }

            public ref T GetId(int id)
            {
                if (!HasId(id))
                    throw new System.NullReferenceException($"Id {id} not exist. use HasId to prevent.");

                return ref m_data[id];
            }

            public bool TryGetId(int id, ref T value)
            {
                if (!HasId(id))
                {
                    value = default(T);
                    return false;
                }
                value = ref m_data[id];
                return true;
            }

            private void Reserve(int n)
            {
                n = NextPow2(n);
                var data = new T[n];
                for (int i = 0; i < m_Pt; i++)
                    data[i] = m_data[i];
                m_data = data;
            }

            public bool MarkRemove(int id)
            {
                if (!HasId(id))
                    return false;

                m_Removed.Enqueue(id);
                return true;
            }

            public Dictionary<int, int> OptimizeRemapClear()
            {
                var mapping = new Dictionary<int, int>(m_data.Length);
                var rst     = new T[NextPow2(Count)];
                var k       = 0;
                for (int i = 0; i < m_data.Length; ++i)
                {
                    if (m_Removed.Contains(i))
                        continue; // skip removed
                    mapping.Add(i, k);
                    rst[k] = m_data[i];
                    (rst[k] as IItem).SetId(k);
                    ++k;
                }

                m_Removed.Clear();
                m_data  = rst;
                m_Pt    = rst.Length - 1;
                return mapping;
            }

            public void Clear()
            {
                m_Pt = 0;
                m_Removed.Clear();
            }

            public void Copy(MyList<T> src)
            {
                Clear();
                Append(src);
            }

            private static int NextPow2(int v)
            {
                if (v <= 0) return 0;
                v--;
                v |= v >> 1;
                v |= v >> 2;
                v |= v >> 4;
                v |= v >> 8;
                v |= v >> 16;
                v++;
                return v;
            }
        }
        #endregion Utils
    }
}
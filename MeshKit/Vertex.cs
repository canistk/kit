using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.MeshKit
{
    [System.Serializable]
    public struct Vertex
    {
        public static readonly Vertex Invalid = new Vertex(-1, Vector3.zero);

        public int id;
        public Vector3 position;
        public Vector3 normal;
        public Vector4 tangent;
        public Vector4 uv;
        
        public Vertex(int id, Vector3 p)
            : this(id, p, Vector3.zero, Vector4.zero, Vector4.zero) { }
        public Vertex(Vector3 p, Vector3 normal)
            : this(-1, p, normal, Vector4.zero, Vector4.zero) { }
        public Vertex(Vector3 p, Vector3 normal, Vector4 tangent, Vector4 uv)
            : this(-1, p, normal, tangent, uv) { }

        public Vertex(int id, Vector3 p, Vector3 normal, Vector4 tangent, Vector4 uv)
        {
            this.id         = id;
            this.position   = p;
            this.normal     = normal;
            this.tangent    = tangent;
            this.uv         = uv;
        }

        public override string ToString()
        {
            return $"[id={id},p:{position:F1},n:{normal:F1},t:{tangent:F1},uv:{uv:F1}]";
        }

        public static Vertex Interpolate(Vertex a, Vertex b, float _t)
        {
            var t01 = Mathf.Clamp01(_t);
            var p = Vector3.LerpUnclamped   (a.position, b.position, t01);
            var n = Vector3.SlerpUnclamped  (a.normal, b.normal, t01).normalized;
            var t = Vector4.LerpUnclamped   (a.tangent, b.tangent, t01);
            var u = Vector2.LerpUnclamped   (a.uv, b.uv, t01);
            return new Vertex(p, n, t, u);
        }

        public static Vertex operator *(Vertex p, Matrix4x4 matrix)
        {
            return new Vertex
            {
                position= matrix.MultiplyPoint(p.position),
                normal  = matrix.MultiplyVector(p.normal),
                tangent = matrix.MultiplyVector(p.tangent),
                uv      = p.uv,
            };
        }

        /*
        public static bool operator ==(Vertex vertex, Vertex other)
        {
            return vertex.id == other.id;
        }
        public static bool operator !=(Vertex vertex, Vertex other)
        {
            return vertex.id != other.id;
        }
        */
    }

}

using Kit.MeshKit.FaceEdge;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.MeshKit
{
    [System.Serializable]
    public class Triangle
    {
        
        [SerializeField] private Vertex[]  m_Vertices;
        public Vertex[] vertices => m_Vertices;
        
        public Vector3 p0       => m_Vertices[0].position;
        public Vector3 p1       => m_Vertices[1].position;
        public Vector3 p2       => m_Vertices[2].position;
        public Vector4 uv0      => m_Vertices[0].uv;
        public Vector4 uv1      => m_Vertices[1].uv;
        public Vector4 uv2      => m_Vertices[2].uv;
        public Vector3 normal0  => m_Vertices[0].normal;
        public Vector3 normal1  => m_Vertices[1].normal;
        public Vector3 normal2  => m_Vertices[2].normal;
        public Vector3 normal   => (normal0 + normal1 + normal2).normalized;
        public Vector4 tangent0 => m_Vertices[0].tangent;
        public Vector4 tangent1 => m_Vertices[1].tangent;
        public Vector4 tangent2 => m_Vertices[2].tangent;

        public Triangle(Vector3[] points, Vector3[] normal, Vector4[] tangent, Vector4[] uv)
        {
            if (points == null || uv == null)
                throw new System.Exception($".");
            if (points.Length != 3)
                throw new System.Exception($"Triangle had 3 point.");
            if (points.Length != normal.Length)
                throw new System.Exception($"Normal's length should always align points p[{points.Length}] && normal[{normal.Length}]");
            if (points.Length != tangent.Length)
                throw new System.Exception($"Tangent's length should always align points p[{points.Length}] && normal[{tangent.Length}]");
            if (points.Length != uv.Length)
                throw new System.Exception($"UV's length should always align points p[{points.Length}] && uv[{uv.Length}]");

            var n = Vector3.Cross(points[0], points[1]);
            if (n == Vector3.zero)
                n = Vector3.Cross(points[1], points[2]);
            if (n == Vector3.zero)
                n = Vector3.up; // bias world up.

            if (normal[0] == Vector3.zero)
                normal[0] = n;
            if (normal[1] == Vector3.zero)
                normal[1] = n;
            if (normal[2] == Vector3.zero)
                normal[2] = n;

            this.m_Vertices      = new []
            {
                new Vertex(points[0], normal[0], tangent[0], uv[0]),
                new Vertex(points[1], normal[1], tangent[1], uv[1]),
                new Vertex(points[2], normal[2], tangent[2], uv[2]),
            } ;
        }

        public Triangle(IList<Vertex> vertices)
        {
            m_Vertices = vertices.ToArray();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public bool IsClockwise()
        {
            return Geometry.IsTriangleOrientedClockwise(p0, p1, p2, normal);
        }

        public void Flip()
        {
            var p           = m_Vertices[2];
            m_Vertices[2]   = m_Vertices[0];
            m_Vertices[0]   = p;

            for (int i=0; i<3; ++i)
            {
                m_Vertices[i].normal = -m_Vertices[i].normal;
            }
        }

        /// <summary>
        /// Reorder Triangle points based on <see cref="SystemInfo.graphicsDeviceVersion"/>
        /// <see cref="https://docs.unity3d.com/ScriptReference/SystemInfo-graphicsDeviceVersion.html"/>
        /// In OpenGL using counterclockwise (CCW) for drawing front face
        /// In DirectX using clockwise (CW) for drawing front face
        /// </summary>
        public void ReorderBasedOnGraphicDeviceAndPlaneNormal()
        {
            var g           = SystemInfo.graphicsDeviceVersion;
            var isOpenGL    = g.Contains(@"OpenGL", System.StringComparison.OrdinalIgnoreCase);
            var isDirectX   = g.Contains(@"Direct", System.StringComparison.OrdinalIgnoreCase);
            var isClockwise = IsClockwise();

            if (isOpenGL && isClockwise ||
                isDirectX && !isClockwise)
                Flip();
        }

        public Vector3 CalcCentroid()
        {
            return (p0 + p1 + p2) / 3f;
        }


        /// <summary>Attempt to push to <see cref="FEMesh"/></summary>
        /// <param name="feMesh"></param>
        /// <returns></returns>
        public void Push(FEMesh feMesh)
        {
            if (feMesh == null)
                throw new System.NullReferenceException();

            var area = Geometry.Area(p0, p1, p2);
            Debug.Log($"Area = {area:F2}");

            ReorderBasedOnGraphicDeviceAndPlaneNormal();

            var idx0    = feMesh.AddPoint(p0, normal0, tangent0, uv0);
            var idx1    = feMesh.AddPoint(p1, normal1, tangent1, uv1);
            var idx2    = feMesh.AddPoint(p2, normal2, tangent2, uv2);
            feMesh.AddFace(idx0, idx1, idx2);
        }

        public Polygon ToPolygon()
        {
            return new Polygon(vertices);
        }

        public override string ToString()
        {
            return $"Triangle:\n{vertices[0]}\n{vertices[1]}\n{vertices[2]}";
        }
    }
}
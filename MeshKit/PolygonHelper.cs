using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
namespace Kit.MeshKit
{
    /// <summary>
    /// <see cref="https://codereview.stackexchange.com/questions/277000/union-polygon-algorithm"/>
    /// </summary>
    public static class PolygonHelper
    {
        /// <summary>Collect 2 polygon information check if 2 polygon's alignment state.</summary>
        public struct AlignReport
        {
            /// <summary>2 polygon's normal is align or not.</summary>
            public readonly bool angleAlign;
            /// <summary>Anchor define by 2 polygon's centroid's middle position</summary>
            public readonly Vector3 anchor;

            /// <summary>Convert polygon into projected local space.</summary>
            public readonly Matrix4x4 world2local, local2world;

            /// <summary>all polygon's vertex position are on the same plane.</summary>
            public readonly bool isOnSamePlane;

            /// <summary>The projected polygon into 2d plane, z-axis should be always == 0</summary>
            public readonly Polygon2d p2d0, p2d1;

            /// <summary>Convert polygon in 2d back into world space.</summary>
            public Polygon GetPolygon0() => p2d0 * local2world;
            public Polygon GetPolygon1() => p2d1 * local2world;

            public AlignReport(Polygon polygon0, Polygon polygon1, float threshold = float.Epsilon)
            {
                isOnSamePlane = false;
                anchor = default;
                world2local = local2world = default;
                p2d0 = p2d1 = default;

                var n0 = polygon0.normal;
                var n1 = polygon1.normal;
                var dot = Vector3.Dot(n0, n1);
                this.angleAlign = dot >= (1f - threshold);
                if (!angleAlign)
                    return;

                var p0 = polygon0.Centroid();
                var p1 = polygon1.Centroid();
                this.anchor = Vector3.Lerp(p0, p1, 0.5f);
                var rot = Quaternion.LookRotation(n0, Vector3.up);
                this.local2world = Matrix4x4.TRS(this.anchor, rot, Vector3.one);
                this.world2local = this.local2world.inverse;

                var p0l = polygon0 * world2local;
                var p1l = polygon1 * world2local;

                // check via all points z-axis, see if the different still within threshold range.
                var z = p0l.vertices[0].position.z;
                foreach (var v in p0l.vertices)
                {
                    var zDiff = Mathf.Abs(z - v.position.z);
                    if (zDiff >= threshold)
                        return;
                }
                foreach (var v in p1l.vertices)
                {
                    var zDiff = Mathf.Abs(z - v.position.z);
                    if (zDiff >= threshold)
                        return;
                }
                this.isOnSamePlane = true;
                this.p2d0 = new Polygon2d(p0l);
                this.p2d1 = new Polygon2d(p1l);
            }
        }

        /// <summary>
        /// Define the <see cref="eIntersectType"/> from it.
        /// and based on align report's polygons, to search and list out all <see cref="IntersectInfo"/>
        /// </summary>
        public struct IntersectReport
        {
            const float TOUCH_THRESHOLD = 1e-6f;

            public IntersectReport(AlignReport data)
            {
                this.alignReport = data;
                var p0e = this.alignReport.p2d0;
                var p1e = this.alignReport.p2d1;
                this.p0Rect = p0e.GetPolygonRect();
                this.p1Rect = p1e.GetPolygonRect();
                this.intersects = new IntersectInfo[0];

                // Define polygon ids
                var id = 0;
                for (int i = 0; i < p0e.Count; ++i)
                {
                    ref var p = ref p0e[i];
                    p.id = id++;
                }
                for (int i = 0; i < p1e.Count; ++i)
                {
                    ref var p = ref p1e[i];
                    bool duplicate = false;
                    for (int k = 0; k < p0e.Count && !duplicate; ++k)
                    {
                        if (p.position.EqualRoughly(p0e[k].position, TOUCH_THRESHOLD))
                        {
                            p.id = p0e[k].id;
                            duplicate = true;
                        }
                    }
                    if (!duplicate)
                    {
                        p.id = id++;
                    }
                }


                if (!data.isOnSamePlane)
                {
                    this.type = eIntersectType.None;
                    return;
                }
                else if (!p0Rect.Overlaps(p1Rect))
                {
                    /// Check if polygon's edge 'touch' each other.
                    /// <see cref="Polygon.ClosestPointInPolygon(Vector3)"/>
                    var touchs = BuildIntersectionPoints(p0e, p1e, ref id);
                    /// bias only 1 touch point is edge case, shouldn't execute union process.
                    if (touchs.Count > 1)
                    {
                        this.intersects = touchs.ToArray();
                        this.type = eIntersectType.Touch;
                        return;
                    }

                    this.type = eIntersectType.None;
                    return;
                }
                else if (p0Rect.IsFullyContain(p1Rect))
                {
                    // check if Encapsulate all points
                    // TODO: in same cases, polygon(s) overlap with hole may fail.
                    bool isEncapsulate = true;
                    foreach (var v in p1e.source.vertices)
                    {
                        var refPt = p0e.source.ClosestPointInPolygon(v.position);
                        var sqrt = (refPt - v.position).sqrMagnitude;
                        if (sqrt > 0.001f)
                        {
                            isEncapsulate = false;
                            break;
                        }
                    }
                    if (isEncapsulate)
                    {
                        this.type = eIntersectType.P0Contain;
                        return;
                    }
                }
                else if (p1Rect.IsFullyContain(p0Rect))
                {
                    // check if Encapsulate all points
                    // TODO: in same cases, polygon(s) overlap with hole may fail.
                    bool isEncapsulate = true;
                    foreach (var v in p0e.source.vertices)
                    {
                        var refPt = p1e.source.ClosestPointInPolygon(v.position);
                        var sqrt = (refPt - v.position).sqrMagnitude;
                        if (sqrt > 0.001f)
                        {
                            isEncapsulate = false;
                            break;
                        }
                    }
                    if (isEncapsulate)
                    {
                        this.type = eIntersectType.P1Contain;
                        return;
                    }
                }

                // Full checking for intersection point(s)
                var list = BuildIntersectionPoints(p0e, p1e, ref id);
                if (list.Count == 0)
                {
                    this.type = eIntersectType.None;
                    return;
                }

                this.type = eIntersectType.PartialIntersect;
                this.intersects = list.ToArray();
            }

            /// <summary>Assume polygon was intersect each other, find the intersection point around polygon(s)</summary>
            /// <param name="p0e"></param>
            /// <param name="p1e"></param>
            /// <returns></returns>
            private static List<IntersectInfo> BuildIntersectionPoints(Polygon2d p0e, Polygon2d p1e, ref int id)
            {
                var list = new List<IntersectInfo>(4);
                var info = default(IntersectInfo);
                for (var i = 0; i < p0e.Count; ++i)
                {
                    ref var p0 = ref p0e[i % p0e.Count];
                    ref var p1 = ref p0e[(i + 1) % p0e.Count];
                    for (var k = 0; k < p1e.Count; ++k)
                    {
                        ref var p2 = ref p1e[k % p1e.Count];
                        ref var p3 = ref p1e[(k + 1) % p1e.Count];
                        if (p0.position.EqualRoughly(p3.position, TOUCH_THRESHOLD) &&
                            p1.position.EqualRoughly(p2.position, TOUCH_THRESHOLD))
                        {
                            // bias slave p2,p3, use main p0,p1's id
                            p3.id = p0.id;
                            p2.id = p1.id;
                            if (CreateInfo(p0, p1, p0.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            if (CreateInfo(p0, p1, p1.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            if (CreateInfo(p2, p3, p2.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            if (CreateInfo(p2, p3, p3.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                        }
                    }
                }

                for (var i = 0; i < p0e.Count; ++i)
                {
                    ref var p0 = ref p0e[i % p0e.Count];
                    ref var p1 = ref p0e[(i + 1) % p0e.Count];
                    for (var k = 0; k < p1e.Count; ++k)
                    {
                        ref var p2 = ref p1e[k % p1e.Count];
                        ref var p3 = ref p1e[(k + 1) % p1e.Count];
                        if (p0.position.EqualRoughly(p3.position, TOUCH_THRESHOLD) &&
                            p1.position.EqualRoughly(p2.position, TOUCH_THRESHOLD))
                        {
                            // already handle in previous step.
                            continue;
                            //if (CreateInfo(p0, p1, p0.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            //if (CreateInfo(p0, p1, p1.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            //if (CreateInfo(p2, p3, p2.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            //if (CreateInfo(p2, p3, p3.position, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                        }

                        var v0 = p1.position - p0.position;
                        var v1 = p3.position - p2.position;
                        // Check if lines are parallel by comparing the cross product of their direction vectors to zero vector
                        // and line(s) touching each other (without gap.)
                        if (Vector3.Cross(v0, v1).sqrMagnitude < TOUCH_THRESHOLD &&
                            Vector3.Cross(v0, p2.position - p0.position).sqrMagnitude < TOUCH_THRESHOLD)
                        {
                            // Overlap
                            if (Vector3Extend.TryGetInnerOverlapPoints(p0.position, p1.position, p2.position, p3.position,
                                out var inner0, out var inner1, TOUCH_THRESHOLD))
                            {
                                if (CreateInfo(p0, p1, inner0, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                                if (CreateInfo(p0, p1, inner1, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                                if (CreateInfo(p2, p3, inner0, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                                if (CreateInfo(p2, p3, inner1, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            }
                            // End-Overlap
                        }
                        // Intersect
                        else if (Vector2Extend.TryIntersection(p0.position.XY(), p1.position.XY(), p2.position.XY(), p3.position.XY(), out var intersect))
                        {
                            // found intersect session
                            var inner2 = (Vector3)intersect;
                            if (CreateInfo(p0, p1, inner2, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                            if (CreateInfo(p2, p3, inner2, ref id, TOUCH_THRESHOLD, out info)) list.Add(info);
                        }
                        // End-Intersect
                    }
                }
                return list;
                bool CreateInfo(in Vertex p0, in Vertex p1, in Vector3 pos, ref int id, float threshold,
                    out IntersectInfo info)
                {
                    var pt = Vector3Extend.InverseLerp(p0.position, p1.position, pos);
                    var isHead = pos.EqualRoughly(p0.position, threshold);
                    var isTail = pos.EqualRoughly(p1.position, threshold);
                    var p2 =
                        isHead ? p0 :
                        isTail ? p1 :
                        Vertex.Interpolate(p0, p1, pt);
                    info =
                        isHead ? new IntersectInfo(p0, p0.id, p1.id, 0f) :
                        isTail ? new IntersectInfo(p1, p0.id, p1.id, 1f) :
                        default;

                    // check duplicate anchor(s)
                    for (var i = 0; i < list.Count; ++i)
                    {
                        if (list[i].anchor.position.EqualRoughly(pos, threshold))
                        {
                            // Found duplicate
                            if (list[i].from == p0.id &&
                                list[i].to == p1.id &&
                                list[i].pt.EqualRoughly(pt, threshold))
                                return false;
                            if (p2.id < 0 || p2.id > list[i].anchor.id)
                                p2.id = list[i].anchor.id; // keep lowest id
                            info = new IntersectInfo(p2, p0.id, p1.id, pt);
                        }
                    }

                    for (var i = 0; i < p0e.Count && p2.id == -1; ++i)
                    {
                        if (!p0e[i].position.EqualRoughly(pos, threshold))
                            continue;
                        p2.id = p0e[i].id;
                    }
                    for (var i = 0; i < p1e.Count && p2.id == -1; ++i)
                    {
                        if (!p1e[i].position.EqualRoughly(pos, threshold))
                            continue;
                        p2.id = p1e[i].id;
                    }

                    if (p2.id == -1)
                    {
                        p2.id = id++;
                        info = new IntersectInfo(p2, p0.id, p1.id, pt);
                    }
                    return true;
                }
            }

            /// <summary>Input data for this report.</summary>
            public readonly AlignReport alignReport;

            public enum eIntersectType
            {
                None = 0,
                P0Contain,
                P1Contain,
                PartialIntersect,
                Touch,
            }
            /// <summary>define which type of intersection</summary>
            public readonly eIntersectType type;

            public struct IntersectInfo
            {
                public Vertex anchor;
                public int from, to;
                public float pt;
                public IntersectInfo(Vertex anchor, int from, int to, float pt)
                {
                    this.anchor = anchor;
                    this.from = from;
                    this.to = to;
                    this.pt = pt;
                }
                public override string ToString()
                {
                    return $"[{anchor.id}] {from}->{to} ~{pt:P0}";
                }
                public override int GetHashCode()
                {
                    return Extensions.GenerateHashCode(from, to);
                }
            }
            /// <summary>record all intersection between 2 polygons.</summary>
            public readonly IntersectInfo[] intersects;

            /// <summary>
            /// The rect based on <see cref="AlignReport.world2local"/> space.
            /// define the 2d bound box(rect)
            /// </summary>
            public readonly Rect p0Rect, p1Rect;

            public IEnumerable<IntersectInfo> GetIntersect(int fromId, int toId)
            {
                for (int i = 0; i < intersects.Length; ++i)
                {
                    if (fromId == intersects[i].from &&
                        toId == intersects[i].to)
                    {
                        yield return intersects[i];
                    }
                }
            }

            public IEnumerable<IntersectInfo> GetIntersectBy(Vector3 anchor, float threshold)
            {
                for (int i = 0; i < intersects.Length; ++i)
                {
                    if (!intersects[i].anchor.position.EqualRoughly(anchor, threshold))
                        continue;
                    yield return intersects[i];
                }
            }

            public void GetRectsCorners(out Vector3[] Rect0, out Vector3[] Rect1)
            {
                GetRectCorners(p0Rect, out Rect0);
                GetRectCorners(p1Rect, out Rect1);
            }

            private void GetRectCorners(Rect rect, out Vector3[] corners)
            {
                var mat = alignReport.local2world;
                var v2 = rect.GetCorners();
                corners = new Vector3[4];
                for (int i = 0; i < 4; ++i)
                {
                    corners[i] = mat.MultiplyPoint(new Vector3(v2[i].x, v2[i].y, 0f));
                }
            }
        }

        /// <summary>
        /// <see cref="https://www.geeksforgeeks.org/weiler-atherton-polygon-clipping-algorithm/#:%7E:text=Weiler%20Atherton%20Polygon%20Clipping%20Algorithm,without%20leaving%20any%20residue%20behind."/>
        /// </summary>
        /// <param name="data"></param>
        /// <param name="polygon"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        [System.Obsolete("To implement.", true)]
        public static void TryClippedPolygons() { }

        public class Edge
        {
            public readonly Vertex from, to;
            public readonly bool inside;
            public readonly float length;
            public readonly Vector3 vector;
            public readonly Vector3 direction;
            public Edge(Vertex from, Vertex to, in Polygon2d slave)
                : this(from, to, slave.IsWithin(Vector3.Lerp(from.position, to.position, 0.5f)))
            { }
            public Edge(Vertex from, Vertex to, bool inside)
            {
                this.from = from;
                this.to = to;
                this.inside = inside;
                this.vector = to.position - from.position;
                this.length = vector.magnitude;
                this.direction = Vector3.Normalize(vector);
            }

            public override int GetHashCode()
                => Extensions.GenerateHashCode(from.id, to.id);
            public override string ToString()
            {
                return $"[E {from.id:00} -> {to.id:00}, len={length}]";
            }
        }

        public class EdgeList
        {
            public List<Edge> edges;
            public int total => edges.Count;
            public EdgeList(List<Edge> edges)
            {
                this.edges = edges;
            }
            public IEnumerable<Edge> GetEdgesFrom(int fromId)
            {
                for (int i = 0; i < edges.Count; ++i)
                {
                    if (edges[i].from.id != fromId)
                        continue;
                    yield return edges[i];
                }
            }
        }

        public struct UnionReport
        {
            public Polygon result;
            public int startId;
            public EdgeList main;
            public EdgeList slave;
        }

        public static bool TryUnionPolygons(Polygon polygon0, Polygon polygon1, out UnionReport report, float threshold = 1e-6f)
        {
            report = default;
            var alignReport = new AlignReport(polygon0, polygon1, threshold);
            if (!alignReport.isOnSamePlane)
                return false;
            var intersectReport = new IntersectReport(alignReport);
            if (intersectReport.type == IntersectReport.eIntersectType.None)
                return false;
            return TryUnionPolygons(intersectReport, out report, threshold);
        }

        /// <summary>
        /// <see cref="https://stackoverflow.com/questions/2667748/how-do-i-combine-complex-polygons"/>
        /// </summary>
        /// <param name="data"></param>
        /// <param name="polygon"></param>
        /// <returns>polygon projected in 2d local space, 
        /// require to convert into world space, using <see cref="AlignReport.local2world"/></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public static bool TryUnionPolygons(IntersectReport data, out UnionReport report, float threshold)
        {
            report = default;

            // Step 1. Check constructed graph
            switch (data.type)
            {
                case IntersectReport.eIntersectType.None:               return false;
                case IntersectReport.eIntersectType.P0Contain:          report.result = data.alignReport.GetPolygon0();   return true;
                case IntersectReport.eIntersectType.P1Contain:          report.result = data.alignReport.GetPolygon1();   return true;
                case IntersectReport.eIntersectType.PartialIntersect:   break;
                case IntersectReport.eIntersectType.Touch:              break;
                default: throw new System.NotImplementedException();
            }

            // Step 2, Find left-bottom vertex.
            var anchor = data.alignReport.anchor;
            (var d0BottomLeft, var d0SqrDis) = data.alignReport.p2d0.FindFarestIndex(anchor);
            (var d1BottomLeft, var d1SqrDis) = data.alignReport.p2d1.FindFarestIndex(anchor);
            if (d0BottomLeft == -1 || d1BottomLeft == -1)
            {
                Debug.LogError($"Logic error, fail to fetch farest vertex on polygon(s), d0={d0BottomLeft}, d1={d1BottomLeft}");
                return false;
            }

            /// <see cref="http://i.stanford.edu/pub/cstr/reports/cs/tr/96/1568/CS-TR-96-1568.pdf"/>
            /// <see cref="https://faculty.cc.gatech.edu/~jarek/graphics/papers/04PolygonBooleansMargalit.pdf"/>
            // Step 2, construct 2 edge list, based on intersect point(s)
            GenEdgeList(data, d0BottomLeft, d1BottomLeft, threshold, out var mainList, out var slaveList);
            if (mainList.Count == 0 || slaveList.Count == 0)
            {
                Debug.LogError("Fail to generate edge list.");
                return false;
            }

            // Step 3, build edge list.
            var main = new EdgeList(mainList);
            var slave = new EdgeList(slaveList);
            report.startId = main.edges[0].from.id;
            List<Vertex> verities = EdgeListRebuildPolygon(
                d0SqrDis > d1SqrDis ? main : slave,
                d0SqrDis > d1SqrDis ? slave : main);
            report.main = main;
            report.slave = slave;
            
            if (verities.Count < 3)
                return false;

            // Step 4, Convert 2d verities back to 3d space.
            for (int i = 0; i < verities.Count; ++i)
            {
                verities[i] *= data.alignReport.local2world;
            }
            report.result = new Polygon(verities);
            return report.result.IsValid();
        }

        private static void GenEdgeList(in IntersectReport data, int mainOffset, int slaveOffset, float threshold, out List<Edge> mainList, out List<Edge> slaveList)
        {
            mainList    = CollectVertexInOrder(data.alignReport.p2d0, data.alignReport.p2d1, mainOffset, data);
            slaveList   = CollectVertexInOrder(data.alignReport.p2d1, data.alignReport.p2d0, slaveOffset, data);

            List<Edge> CollectVertexInOrder(Polygon2d main, Polygon2d slave, int offset, in IntersectReport data)
            {
                var edge = new List<Edge>(main.Count);
                var tmp = new List<Edge>(8);
                for (int i = 0; i < main.Count; ++i)
                {
                    var from    = main[(i + offset) % main.Count];
                    var to      = main[(i + 1 + offset) % main.Count];

                    // got intersect
                    var arr = data.GetIntersect(from.id, to.id).OrderBy(o => o.pt).ToArray();
                    if (arr.Length > 0)
                    {
                        for (int k = 0; k < arr.Length; ++k)
                        {
                            if (from.position.EqualRoughly(arr[k].anchor.position, threshold))
                                continue;
                            if (to.position.EqualRoughly(arr[k].anchor.position, threshold))
                                continue;
                            edge.Add(new Edge(from, arr[k].anchor, slave));
                            from = arr[k].anchor;
                        }
                        if (from.position.EqualRoughly(to.position, threshold))
                            continue;
                        edge.Add(new Edge(from, to, slave));
                    }
                    else
                    {
                        edge.Add(new Edge(from, to, slave));
                    }
                }
                return edge;
            }
        }

        private static List<Vertex> EdgeListRebuildPolygon(EdgeList main, EdgeList slave)
        {
            int maxRetry = main.total + slave.total + 20; // error buffer.
            var visited = new HashSet<int>(main.total + slave.total); // mark visited 'from id'
            var rst = new List<Vertex>(maxRetry);

            var buffer = new List<Edge>();
            var edge = main.edges[0];
            do
            {
                var currId = edge.from.id;
                var nextId = edge.to.id;
                if (visited.Contains(currId))
                {
                    // reach the end
                    return rst;
                }

                visited.Add(currId);
                rst.Add(edge.from);

                buffer.Clear();
                buffer.AddRange(main.GetEdgesFrom(nextId));
                buffer.AddRange(slave.GetEdgesFrom(nextId));
                if (buffer.Count == 0)
                {
                    throw new System.Exception($"Fail to locate next edge {nextId}");
                }
                else if (buffer.Count == 1)
                {
                    edge = buffer[0];
                }
                else // buffer.Count > 1
                {
                    var fromDir = edge.direction;
                    var angle   = (float.NegativeInfinity, 0); // angle, index
                    for (int i = 0; i < buffer.Count; ++i)
                    {
                        var _angle = Vector3.SignedAngle(fromDir, buffer[i].direction, Vector3.back);
                        if (_angle > angle.Item1)
                            angle = (_angle, i);
                    }
                    //if (buffer[angle.Item2].inside)
                    //    throw new System.Exception($"The selected edge was inside another's polygon.");
                    edge = buffer[angle.Item2];
                }
            }
            while (maxRetry-- > 0);
            return rst;
        }
    }
}
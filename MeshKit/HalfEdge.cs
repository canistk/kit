using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.MeshKit.HalfEdge
{
    /// <summary>
    /// <see cref="https://observablehq.com/@esperanc/mesh-data-structures-traversal"/>
    /// <see cref="https://jerryyin.info/geometry-processing-algorithms/half-edge/"/>
    /// </summary>
    public class HEMesh
    {
        #region Constructor + Property
        // Note : should never change the order, since the index of item are their id.
        private MyList<Point>     m_Points;
        private MyList<HalfEdge>  m_HalfEdge;
        private MyList<Face>      m_Faces;

        public HEMesh()
        {
            m_Points    = new MyList<Point>();
            m_HalfEdge  = new MyList<HalfEdge>();
            m_Faces     = new MyList<Face>();
        }
        #endregion Constructor + Property

        #region API
        public void Clean()
        {
            m_Points.Clear();
            m_HalfEdge.Clear();
            m_Faces.Clear();
        }

        public ref Point AddPoint(Vector3 p, Vector3 normal, Vector4 tangent, Vector4 uv)
        {
            ref var point = ref m_Points.Push();
            point.Init(p, normal, tangent, uv);
            return ref point;
        }

        public ref Point AddPoint(Vertex vertex)
        {
            ref var point = ref m_Points.Push();
            point.Init(vertex);
            return ref point;
        }

        public bool TryGetPoint(int id, ref Point point) => m_Points.TryGetId(id, ref point);
        public bool RemovePoint(int id)
        {
            if (!m_Points.HasId(id))
                return false;

            ref var point = ref m_Points.GetId(id);
            foreach(var edge in point.GetHalfEdges(this))
            {
                var f = edge.GetFace(this);
                Debug.LogError($"Point[{id}] still had reference. remove related face[{f.id}] first.");
                return false;
            }
            
            return m_Points.MarkRemove(id);
        }

        private int AddHalfEdge(int headId, int tailId, int faceId, int prevEdge)
        {
            // find twins
            int existId = -1;
            foreach (var edge in m_HalfEdge.GetValues())
            {
                var exist = 
                    (edge.headId == headId && edge.tailId == tailId) ||
                    (edge.headId == tailId || edge.tailId == headId);

                if (exist)
                {
                    existId = edge.id;
                    break;
                }    
            }
            
            // add new half edge
            ref var newEdge = ref m_HalfEdge.Push();
            newEdge.Init(headId, tailId, faceId, prevEdge);
            if (existId == -1)
                return newEdge.id;

            // handle twins double link
            if (!m_HalfEdge.HasId(existId))
                throw new System.Exception("logic error");

            ref HalfEdge prevTwin = ref m_HalfEdge.GetId(existId);
            if (m_HalfEdge.HasId(prevTwin.nextTwinId))
            {
                // insert
                ref var nextTwin = ref m_HalfEdge.GetId(prevTwin.nextTwinId);
                prevTwin.nextTwinId = nextTwin.prevTwinId = newEdge.id;
                newEdge.prevTwinId = prevTwin.id;
                newEdge.nextTwinId = nextTwin.id;
            }
            else
            {
                // first twin
                prevTwin.prevTwinId = prevTwin.nextTwinId = newEdge.id;
                newEdge.prevTwinId = newEdge.nextTwinId = prevTwin.id;
            }

            return newEdge.id;
        }

        public bool TryGetHalfEdge(int id, ref HalfEdge halfEdge)
            => m_HalfEdge.TryGetId(id, ref halfEdge);

        private bool TryMatchPoint(in Vertex vertex, float epsilon, out int pointId)
        {
            var pos = vertex.position;
            var normal = vertex.normal;
            //var tangent = vertex.tangent;
            //var uv = vertex.uv;
            foreach (var p in m_Points.GetValues())
            {
                if (!pos.EqualRoughly(p.vertex.position, epsilon))
                    continue;
                if (!normal.EqualRoughly(p.vertex.normal, epsilon))
                    continue;
                pointId = p.id;
                return true;
            }
            pointId = -1;
            return false;
        }

        /// <summary>
        /// Assume <see cref="Polygon.vertices"/> already had unique id
        /// and added into <see cref="AddPoint(Vertex)"/>
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException"></exception>
        /// <exception cref="System.Exception"></exception>
        [System.Obsolete("Too slow")]
        public ref Face AddFace(Polygon polygon, float epsilon = 0.00001f)
        {
            if (polygon.vertices.Length == 0)
                throw new System.ArgumentNullException("Without any vertices in data base");

            var _points = new Point[polygon.vertices.Length];
            for (int i = 0; i < polygon.vertices.Length; ++i)
            {
                var p = polygon.vertices[i];
                if (m_Points.TryGetId(p.id, ref _points[i]) && 
                    _points[i].vertex.position.EqualRoughly(p.position, epsilon) &&
                    _points[i].vertex.normal.EqualRoughly(p.normal, epsilon))
                {
                    // quick check.
                    continue;
                }
                else if (TryMatchPoint(p, epsilon, out var pid)) // slow o(n) search
                {
                    _points[i] = m_Points.GetId(pid);
                }
                else
                {
                    // new point.
                    _points[i] = AddPoint(p);
                }
            }

            ref var face = ref m_Faces.Push();

            int firstHalfEdgeId = -1, lastHalfEdgeId = -1;
            /// <see cref="Polygon.GetEdges"/>
            {
                var i = _points.Length;
                var p0 = _points[0];
                while (i-- > 0)
                {
                    var p1 = _points[i];
                    {
                        int id = AddHalfEdge(p0.id, p1.id, face.id, lastHalfEdgeId);
                        if (firstHalfEdgeId == -1)
                        {
                            firstHalfEdgeId = id;
                        }
                        if (lastHalfEdgeId != -1)
                        {
                            ref var prevEdge = ref m_HalfEdge.GetId(lastHalfEdgeId);
                            prevEdge.nextEdgeId = id;
                        }
                        lastHalfEdgeId = id;
                    }
                    p0 = p1;
                }
            }

            if (lastHalfEdgeId == -1 || firstHalfEdgeId == -1)
                throw new System.Exception("Logic error, face edge not define yet.");

            face.Init(firstHalfEdgeId);
            ref var firstEdge = ref m_HalfEdge.GetId(firstHalfEdgeId);
            ref var lastEdge = ref m_HalfEdge.GetId(lastHalfEdgeId);
            firstEdge.prevEdgeId    = lastHalfEdgeId;
            lastEdge.nextEdgeId     = firstHalfEdgeId;
            return ref face;
        }

        public ref Face AddFace(params int[] pointIndices)
        {
            if (pointIndices.Length == 0)
                throw new System.ArgumentNullException("Without any vertices in data base");

            var _points = new Point[pointIndices.Length];
            for (int i = 0; i < pointIndices.Length; ++i)
            {
                _points[i] = m_Points.GetId(pointIndices[i]);
            }

            ref var face = ref m_Faces.Push();

            int firstHalfEdgeId = -1, lastHalfEdgeId = -1;
            /// <see cref="Polygon.GetEdges"/>
            {
                Point p0, p1;
                for (int i = 0; i < _points.Length; ++i)
                {
                    p0 = _points[i];
                    p1 = _points[(i + 1) % _points.Length];
                    {
                        int id = AddHalfEdge(p0.id, p1.id, face.id, lastHalfEdgeId);
                        if (firstHalfEdgeId == -1)
                        {
                            firstHalfEdgeId = id;
                        }
                        if (lastHalfEdgeId != -1)
                        {
                            ref var prevEdge = ref m_HalfEdge.GetId(lastHalfEdgeId);
                            prevEdge.nextEdgeId = id;
                        }
                        lastHalfEdgeId = id;
                    }
                }
            }

            if (lastHalfEdgeId == -1 || firstHalfEdgeId == -1)
                throw new System.Exception("Logic error, face edge not define yet.");

            face.Init(firstHalfEdgeId);
            ref var firstEdge = ref m_HalfEdge.GetId(firstHalfEdgeId);
            ref var lastEdge = ref m_HalfEdge.GetId(lastHalfEdgeId);
            firstEdge.prevEdgeId    = lastHalfEdgeId;
            lastEdge.nextEdgeId     = firstHalfEdgeId;
            return ref face;
        }

        public bool TryRemoveFace(int faceId)
        {
            Face face = default;
            if (!m_Faces.TryGetId(faceId, ref face))
                return false;

            var iter = face.Traversals(this);
            foreach (var edge in iter)
            {
                if (edge.prevTwinId == -1 && edge.nextTwinId == -1)
                {
                    // single edge no twin
                }
                else 
                {
                    ref var prev = ref m_HalfEdge.GetId(edge.prevTwinId);
                    ref var next = ref m_HalfEdge.GetId(edge.nextTwinId);
                    if (edge.prevTwinId == edge.nextTwinId)
                    {
                        // last 2 half edge
                        prev.nextTwinId = -1;
                        next.prevTwinId = -1;
                    }
                    else
                    {
                        // >= 3 twin, remove self in middle
                        prev.nextTwinId = next.id;
                        next.prevTwinId = prev.id;
                    }
                }

                if (!m_HalfEdge.MarkRemove(edge.id))
                    throw new System.Exception("Logic error");
            }

            if (!m_Faces.MarkRemove(face.id))
                throw new System.Exception("Logic error");

            return true;
        }
        #endregion API

        #region Convertion
        /// <summary>
        /// Convert current FaceEdge into mesh.
        /// </summary>
        /// <returns></returns>
        public Mesh ToMesh()
        {
            // TODO: optimize point index before convert.

            var mesh        = new Mesh();
            mesh.hideFlags  = HideFlags.DontSave;
            var vertices    = new Vector3[m_Points.Count];
            var normals     = new Vector3[m_Points.Count];
            var uvs         = new Vector4[m_Points.Count];
            var tangents    = new Vector4[m_Points.Count];
            var i = 0;
            foreach (var point in m_Points.GetValues())
            {
                vertices[i] = point.vertex.position;
                normals[i]  = point.vertex.normal;
                uvs[i]      = point.vertex.uv;
                tangents[i] = point.vertex.tangent;
                i++;
            }
            mesh.SetVertices(vertices);

            var triangleIds = new List<int>(4096);
            foreach (var face in m_Faces.GetValues())
            {
                // Triangulate each face
                var p0 = -1; var p1 = -1; var p2 = -1;
                foreach (var halfEdge in face.Traversals(this))
                {
                    if (p0 == -1)
                    {
                        p0 = halfEdge.headId;
                        continue;
                    }

                    if (p1 == -1)
                    {
                        p1 = halfEdge.headId;
                        continue;
                    }

                    p2 = halfEdge.headId;

                    triangleIds.Add(p0);
                    triangleIds.Add(p1);
                    triangleIds.Add(p2);

                    p1 = p2;
                }
            }

            mesh.SetIndices (triangleIds.ToArray(), MeshTopology.Triangles, 0);
            mesh.SetUVs     (0, uvs.ToArray());
            mesh.SetNormals (normals);
            mesh.SetTangents(tangents);
            mesh.OptimizeReorderVertexBuffer();
            //mesh.RecalculateBounds();
            //mesh.Optimize();
            //mesh.RecalculateNormals();
            return mesh;
        }

        public void LoadFrom(MeshFilter meshFilter)
        {
            LoadFrom(meshFilter.sharedMesh, meshFilter.transform.localToWorldMatrix);
        }

        /// <summary>
        /// Load from any mesh and convert into FaceEdge format.
        /// allow to modify and output by <see cref="ToMesh"/>
        /// </summary>
        /// <param name="mesh"></param>
        public void LoadFrom(Mesh mesh, Matrix4x4 local2World = default)
        {
            Clean();

            var n = mesh.vertexCount;
            List<Vector3> vertices  = new List<Vector3>(n); mesh.GetVertices(vertices);
            List<Vector3> normals   = new List<Vector3>(n); mesh.GetNormals(normals);
            List<Vector4> tangents  = new List<Vector4>(n); mesh.GetTangents(tangents);
            List<Vector4> uvs0      = new List<Vector4>(n); mesh.GetUVs(0, uvs0);
            // Debug.Log($"Load From Mesh, Vertices={vertices.Count}, normals={normals.Count}, uvs0={uvs0.Count}");

            if (tangents.Count == 0 && n > 0)
                tangents.AddRange(new Vector4[n]);

            bool isLocal = local2World == default || local2World.isIdentity;

            for (int i = 0; i < n; ++i)
            {
                if (!isLocal)
                {
                    vertices[i] = local2World.MultiplyPoint(vertices[i]);
                    normals[i] = local2World.MultiplyVector(normals[i]);
                    tangents[i] = local2World.MultiplyVector(tangents[i]);
                }
                AddPoint(vertices[i], normals[i], tangents[i], uvs0[i]);
            }

            for (int m = 0; m < mesh.subMeshCount; ++m)
            {
                List<int> triangle = new List<int>(mesh.triangles.Length);
                mesh.GetTriangles(triangle, m);
                for (int t = 0; t < triangle.Count; t += 3)
                {
                    AddFace(
                        triangle[t + 0],
                        triangle[t + 1],
                        triangle[t + 2]
                    );
                }
                //Debug.Log($"Load Triangle on mesh[{m}] = {triangle.Count}, %3= {(int)((float)triangle.Count / 3f)}");
            }
        }

        public void ToGameObject(string meshName, Vector3 pos, Quaternion rot,
            Material material, Transform parent,
            out GameObject go, out MeshFilter meshFilter, out MeshRenderer meshRenderer)
        {
            if (string.IsNullOrEmpty(meshName))
                meshName    = $"Mesh-{GetHashCode()}";
            go              = new GameObject(meshName);
            go.transform.SetPositionAndRotation(pos, rot);
            if (parent != null)
                go.transform.SetParent(parent, true);

            meshFilter      = go.AddComponent<MeshFilter>();
            meshFilter.mesh = ToMesh();

            meshRenderer    = go.AddComponent<MeshRenderer>();
            if (material != null)
                meshRenderer.materials = new Material[] { material };
        }

        public void ToGameObject(string meshName = null, Material material = null, Transform parent = null)
            => ToGameObject(meshName, Vector3.zero, Quaternion.identity, material, parent, out _, out _, out _);
        
        public void LoadFrom(GameObject go)
        {
            if (go == null)
                throw new System.NullReferenceException("Empty target.");

            var mf = go.GetComponentInChildren<MeshFilter>();
            if (mf == null)
            {
                Debug.LogError($"Fail to locate {nameof(MeshFilter)} on gameobject {go.name}", go);
                return;
            }

            LoadFrom(mf.sharedMesh, go.transform.localToWorldMatrix);
        }

        
        public IEnumerable<Polygon> ToPolygons()
        {
            if (m_Points.Count == 0)
                throw new System.Exception("No point to convert.");
            foreach (var face in m_Faces.GetValues())
                //for (int i = 0; i < m_Faces.Count; ++i)
            {
                var vs = new List<Vertex>(4);
                foreach (var halfEdge in face.Traversals(this))
                {
                    if (halfEdge.headId == -1)
                        throw new System.NullReferenceException("Edges.point shouldn't be null");
                    var p = m_Points.GetId(halfEdge.headId);
                    vs.Add(p.vertex);
                }
                yield return new Polygon(vs);
            }
        }

        public IEnumerable<Polygon> ToPolygons(Matrix4x4 matrix)
        {
            if (m_Points.Count == 0)
                throw new System.Exception("No point to convert.");
            foreach(var face in m_Faces.GetValues())
            {
                var vs = new List<Vertex>();
                foreach (var halfEdge in face.Traversals(this))
                {
                    if (halfEdge.headId == -1)
                        throw new System.NullReferenceException("Edges.point shouldn't be null");
                    var p = m_Points.GetId(halfEdge.headId);
                    var v = p.vertex * matrix;
                    vs.Add(v);
                }
                yield return new Polygon(vs);
            }
        }

        public void LoadFrom(IEnumerable<Polygon> polygons)
        {
            Clean();
            foreach (var p in polygons)
            {
                int[] vIds = new int[p.vertices.Length];
                for (int i = 0; i < p.vertices.Length; ++i)
                {
                    vIds[i] = AddPoint(p.vertices[i]).id;
                }
                AddFace(vIds);
            }
        }


        internal void ToBSPThread(
            System.Action<BSP_Tree> success,
            System.Action<System.Exception> fail)
        {
            var polygons    = ToPolygons().ToArray();
            var share       = new ThreadShare<BSP_Tree>(_Callback);
            var thread      = BSP_Thread.Create_BSP_Tree(polygons, share);
            thread.Start();
            thread.Join();
            share.WaitResult(out _, 3000);
            if (share.IsError)
            {
                fail?.Invoke(share.exception);
                // share.exception.DeepLogInvocationException("ToBSPThread");
                return;
            }
            var rst = share.result;
            success?.Invoke(rst); // waitResult -> dispatch in main-thread 
            void _Callback(BSP_Tree node)
            {
                // Debug.Log("Callback success 2");
            }
        }

        internal void LoadFrom(BSP_Tree tree)
        {
            LoadFrom(tree.Polygons());
        }
        #endregion Convertion

        #region Data structure
        [System.Serializable]
        public struct Point : IItem
        {
            public int id => m_Id;
            private int m_Id;
            public Vertex vertex;
            public Vector3 position => vertex.position;
            public Vector3 normal => vertex.normal;
            
            public void Init(Vector3 p, Vector3 normal, Vector4 tangent, Vector4 uv)
            {
                Init(new Vertex(p, normal, tangent, uv));
            }

            public void Init(Vertex vertex)
            {
                this.vertex = vertex;
                this.vertex.id = id;
            }

            void IItem.SetId(int id)
            {
                this.m_Id = id;
                this.vertex = default;
            }

            public IEnumerable<HalfEdge> GetHalfEdges(HEMesh data)
            {
                int first = -1;
                foreach (var edge in data.m_HalfEdge.GetValues())
                {
                    if (edge.headId != this.id)
                        continue;
                    first = edge.id;
                    break;
                }
                if (first == -1)
                    yield break;
                var anchor = data.m_HalfEdge.GetId(first);
                foreach (var twin in anchor.GetTwin(data))
                    yield return twin;
            }

        }

        [System.Serializable]
        public struct HalfEdge : IItem
        {
            private int m_Id;
            public int id => m_Id;
            public int headId, tailId, faceId,
                prevEdgeId, nextEdgeId,
                nextTwinId, prevTwinId;

            public void Init(int head, int tail, int face, int prevEdge)
            {
                this.headId = head;
                this.tailId = tail;
                this.faceId = face;
                this.prevEdgeId = prevEdge;
                this.nextEdgeId = -1;
                this.nextTwinId = -1;
                this.prevTwinId = -1;
            }

            void IItem.SetId(int id)
            {
                this.m_Id = id;
            }

            public IEnumerable<HalfEdge> GetTwin(HEMesh data)
            {
                var startId = nextTwinId;
                var id = startId;
                HalfEdge pt;
                do
                {
                    pt = data.m_HalfEdge.GetId(id);
                    yield return pt;
                    id = pt.nextTwinId;
                }
                while (id != startId);
            }

            public bool TryGetPrevTwin(HEMesh data, ref HalfEdge value) => data.m_HalfEdge.TryGetId(prevTwinId, ref value);
            public bool TryGetNextTwin(HEMesh data, ref HalfEdge value) => data.m_HalfEdge.TryGetId(nextTwinId, ref value);
            public bool TryGetPrevEdge(HEMesh data, ref HalfEdge value) => data.m_HalfEdge.TryGetId(prevEdgeId, ref value);
            public bool TryGetNextEdge(HEMesh data, ref HalfEdge value) => data.m_HalfEdge.TryGetId(nextEdgeId, ref value);
            public ref HalfEdge GetPrevTwin(HEMesh data) => ref data.m_HalfEdge.GetId(prevTwinId);
            public ref HalfEdge GetNextTwin(HEMesh data) => ref data.m_HalfEdge.GetId(nextTwinId);
            public ref HalfEdge GetPrevEdge(HEMesh data) => ref data.m_HalfEdge.GetId(prevEdgeId);
            public ref HalfEdge GetNextEdge(HEMesh data) => ref data.m_HalfEdge.GetId(nextEdgeId);

            public ref Point GetHead(HEMesh data) => ref data.m_Points.GetId(headId);
            public ref Point GetTail(HEMesh data) => ref data.m_Points.GetId(tailId);
            public ref Face GetFace(HEMesh data) => ref data.m_Faces.GetId(faceId);
        }

        [System.Serializable]
        public struct Face : IItem
        {
            private int m_Id;
            public int id           => m_Id;
            public int halfEdgeId;
            public void Init(int halfEdgeId)
            {
                m_Id            = id;
                this.halfEdgeId = halfEdgeId;
            }

            void IItem.SetId(int id)
            {
                this.m_Id = id;
                this.halfEdgeId = -1;
            }

            public ref HalfEdge GetHalfEdge(HEMesh data)
            {
                return ref data.m_HalfEdge.GetId(halfEdgeId);
            }

            public IEnumerable<HalfEdge> Traversals(HEMesh data)
            {
                var startId = halfEdgeId;
                var id = startId;
                HalfEdge pt;
                do
                {
                    pt = data.m_HalfEdge.GetId(id);
                    yield return pt;
                    id = pt.nextEdgeId;
                }
                while (id != startId);
            }
        }
        #endregion Data structure

        #region Utils
        private void OptimizePointIndex()
        {
            var pointMapping    = m_Points.ReorderClear();
            var halfEdgeMapping = m_HalfEdge.ReorderClear();
            var faceMapping     = m_Faces.ReorderClear();
            
            foreach (var o in m_HalfEdge.GetValues())
            {
                ref var edge = ref m_HalfEdge.GetId(o.id);

                edge.headId     = pointMapping[edge.headId];
                edge.tailId     = pointMapping[edge.tailId];
                edge.prevTwinId = halfEdgeMapping[edge.prevTwinId];
                edge.nextTwinId = halfEdgeMapping[edge.nextTwinId];
                edge.prevEdgeId = halfEdgeMapping[edge.prevEdgeId];
                edge.nextEdgeId = halfEdgeMapping[edge.nextEdgeId];
                edge.faceId     = faceMapping[edge.faceId];
            }

            foreach (var o in m_Faces.GetValues())
            {
                ref var face = ref m_Faces.GetId(o.id);

                face.halfEdgeId = halfEdgeMapping[face.halfEdgeId];
            }
        }

        private interface IItem
        {
            public void SetId(int id);
        }

        private class MyList<T>
            where T : IItem
        {
            private int m_Pt;
            private T[] m_data;
            private Queue<int> m_Removed;
            public int Count => m_Pt - m_Removed.Count;

            public MyList(int capacity = 4)
            {
                m_Pt = 0;
                m_data = new T[Math.Max(4, capacity)];
                m_Removed = new Queue<int>(4);
            }

            public ref T Push()
            {
                if (m_Removed.Count > 0)
                {
                    var reuse = m_Removed.Dequeue();
                    m_data[reuse].SetId(reuse);
                    return ref m_data[reuse];
                }

                if (m_Pt >= m_data.Length)
                {
                    Reserve(m_Pt + 1);
                }
                var id = m_Pt++;
                m_data[id].SetId(id);
                return ref m_data[id];
            }

            public void Append(MyList<T> src)
            {
                if (src == null)
                    return;
                foreach (var val in src.GetValues())
                {
                    Push() = val;
                }
            }

            public IEnumerable<T> GetValues()
            {
                for (int i = 0; i < m_Pt; ++i)
                {
                    if (m_Removed.Contains(i))
                        continue;
                    yield return m_data[i];
                }
            }

            public bool HasId(int id)
            {
                return id >= 0 && id < m_Pt && !m_Removed.Contains(id);
            }

            public ref T GetId(int id)
            {
                if (!HasId(id))
                    throw new System.NullReferenceException($"Id {id} not exist. use HasId to prevent.");

                return ref m_data[id];
            }

            public bool TryGetId(int id, ref T value)
            {
                if (!HasId(id))
                {
                    value = default(T);
                    return false;
                }
                value = ref m_data[id];
                return true;
            }

            private void Reserve(int n)
            {
                n = NextPow2(n);
                var data = new T[n];
                for (int i = 0; i < m_Pt; i++)
                    data[i] = m_data[i];
                m_data = data;
            }

            public bool MarkRemove(int id)
            {
                if (!HasId(id))
                    return false;

                m_Removed.Enqueue(id);
                return true;
            }

            public Dictionary<int, int> ReorderClear()
            {
                var mapping = new Dictionary<int, int>(m_data.Length);
                var rst     = new T[NextPow2(Count)];
                var k       = 0;
                for (int i = 0; i < m_data.Length; ++i)
                {
                    if (m_Removed.Contains(i))
                        continue; // skip removed
                    mapping.Add(i, k);
                    rst[k] = m_data[i];
                    (rst[k] as IItem).SetId(k);
                    ++k;
                }

                m_Removed.Clear();
                m_data  = rst;
                m_Pt    = rst.Length - 1;
                return mapping;
            }

            public void Clear()
            {
                m_Pt = 0;
                m_Removed.Clear();
            }

            public void Copy(MyList<T> src)
            {
                Clear();
                Append(src);
            }

            private static int NextPow2(int v)
            {
                if (v <= 0) return 0;
                v--;
                v |= v >> 1;
                v |= v >> 2;
                v |= v >> 4;
                v |= v >> 8;
                v |= v >> 16;
                v++;
                return v;
            }
        }
        #endregion Utils
    }
}
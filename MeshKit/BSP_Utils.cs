using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.MeshKit
{
    public static class BSP_Utils
    {
        #region SplitPolygon
        [System.Flags]
        enum EPolygonType
        {
            Coplanar    = 0,
            Front       = 1 << 0,
            Back        = 1 << 1,
            Spanning    = Front | Back 		/// 3 is Front | Back - not a separate entry
		};

        /// <summary>
        /// Split `polygon` by this plane if needed, then put the polygon or polygon
        /// fragments in the appropriate lists.Coplanar polygons go into either
        ///  `coplanarFront` or `coplanarBack` depending on their orientation with
        /// respect to this plane.Polygons in front or in back of this plane go into
        /// either `front` or `back`.
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="plane"></param>
        /// <param name="coplanarFront"></param>
        /// <param name="coplanarBack"></param>
        /// <param name="front"></param>
        /// <param name="back"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        internal static void SplitPolygon(Polygon polygon, Plane plane, List<Polygon> coplanarFront, List<Polygon> coplanarBack, List<Polygon> front, List<Polygon> back)
        {
            // Classify each point as well as the entire polygon into one of the above
            // four classes.
            EPolygonType polygonType = 0;
            EPolygonType[] types = new EPolygonType[polygon.vertices.Length];
            const float EPSILON = 0.00001f;
            for (int i = 0; i < polygon.vertices.Length; ++i)
            {
                // project vertex position to scaledAxis to find out the projected value in 3D-space
                // subtract `w` will able to define the current vertex is `before` or `behind` the giving plane.
                float t = plane.GetDistanceToPoint(polygon.vertices[i].position);
                types[i] =
                    (t < -EPSILON) ? EPolygonType.Back :
                    (t > EPSILON) ? EPolygonType.Front :
                    EPolygonType.Coplanar;
                polygonType |= types[i];
            }

            // Put the polygon in the correct list, splitting it when necessary.
            switch (polygonType)
            {
                case EPolygonType.Coplanar:
                    {
                        // same surface as roots,
                        //if (Vector3.Dot(plane.scaledAxis, polygon.plane.scaledAxis) >= 0)
                        if (Vector3.Dot(plane.normal * plane.distance, polygon.plane.normal * polygon.plane.distance) >= 0)
                            coplanarFront.Add(polygon);
                        else
                            coplanarBack.Add(polygon);
                    }
                    break;

                case EPolygonType.Front:
                    {
                        front.Add(polygon);
                    }
                    break;

                case EPolygonType.Back:
                    {
                        back.Add(polygon);
                    }
                    break;

                case EPolygonType.Spanning:
                    {
                        // polygon across the anchor line, 
                        var cnt = polygon.vertices.Length;
                        var f = new List<Vertex>(cnt);
                        var b = new List<Vertex>(cnt);

                        for (int i = 0; i < cnt; i++)
                        {
                            int j = (i + 1) % cnt; // next-index, wrap to index-0 at the end

                            EPolygonType ti = types[i], tj = types[j];

                            Vertex vi = polygon.vertices[i], vj = polygon.vertices[j];

                            if (ti != EPolygonType.Back)
                            {
                                f.Add(vi);
                            }

                            if (ti != EPolygonType.Front)
                            {
                                b.Add(vi);
                            }

                            if (((ti | tj) & EPolygonType.Spanning) == EPolygonType.Spanning)
                            {
                                /*
                                 * Project 2 vertices `vi`, `vj` on plane normal
                                 * t = 0 ~ 1 // mean the interpolation point between vi & v2 is across current plane
                                 */
                                float t = (-plane.distance - Vector3.Dot(plane.normal, vi.position)) / Vector3.Dot(plane.normal, vj.position - vi.position);
                                //if (t < 0f || t > 1f)
                                //    throw new System.Exception("it out of the range ");
                                Vertex v = Vertex.Interpolate(vi, vj, t);

                                f.Add(v);
                                b.Add(v);
                            }
                        } // End-loop

                        if (f.Count >= 3)
                        {
                            front.Add(new Polygon(f));
                        }

                        if (b.Count >= 3)
                        {
                            back.Add(new Polygon(b));
                        }
                    }
                    break;

                default: throw new System.NotImplementedException();
            }
        }
        #endregion SplitPolygon

        #region Clip Polygon
        internal static List<Polygon> ClipPolygons(BSP_Tree self, BSP_Tree other, ref List<Polygon> clipped)
        {
            if (self == null || other == null)
                throw new System.NullReferenceException($"{nameof(BSP_Tree)} cannot be null.");
            if (other.plane.normal == Vector3.zero)
            {
                clipped = new List<Polygon>(0);
                return new List<Polygon>(other.Polygons());
                // throw new System.ArgumentException($"BSP tree, Invalid plane fail to {nameof(ClipPolygons)}");
            }

            var polygons = self.Polygons().ToArray();
            return ClipPolygons(other, polygons, ref clipped);
        }


        /// <summary>Recursively remove all polygons in `polygons` that are inside this BSP tree.</summary>
        /// <param name="other">another <see cref="BSP_Tree"/></param>
        /// <param name="list"></param>
        /// <param name="clipped">the clipped polygons from this process</param>
        /// <returns></returns>
        internal static List<Polygon> ClipPolygons(BSP_Tree other, IList<Polygon> list, ref List<Polygon> clipped)
        {
            if (other == null)
                throw new System.NullReferenceException($"{nameof(BSP_Tree)} cannot be null.");
            if (other.plane.normal == Vector3.zero)
            {
                clipped = new List<Polygon>(0);
                return new List<Polygon>(list);
                //throw new System.ArgumentException($"BSP tree, Invalid plane fail to {nameof(ClipPolygons)}");
            }

            var cnt         = list.Count;
            var list_front  = new List<Polygon>(cnt);
            var list_back   = new List<Polygon>(cnt);

            for (int i = 0; i < cnt; ++i)
            {
                SplitPolygon(list[i], other.plane, list_front, list_back, list_front, list_back);
            }

            if (other.front != null)
            {
                list_front = ClipPolygons(other.front, list_front, ref clipped);
            }

            if (other.back != null)
            {
                list_back = ClipPolygons(other.back, list_back, ref clipped);
            }
            else
            {
                if (clipped != null)
                    clipped.AddRange(list_back);
                list_back.Clear();
            }

            // Position [First, Last]
            // list_front.insert(list_front.end(), list_back.begin(), list_back.end());
            list_front.AddRange(list_back);

            return list_front;
        }

        #endregion Clip Polygon
    }
}
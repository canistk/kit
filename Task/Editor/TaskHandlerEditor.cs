using Kit.Node;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
namespace Kit
{
    public class TaskHandlerEditor : EditorWindowBase
    {
        [MenuItem("Kit/Debug/TaskHandler")]
        private static void Init()
        {
            var window = GetWindow<TaskHandlerEditor>();
            window.titleContent = new GUIContent("TaskHandler Editor");
        }
        private System.Text.StringBuilder sb;

        private void OnEnable()
        {
            sb = new System.Text.StringBuilder();
        }

        private void OnDisable()
        {
            sb.Clear();
            sb = null;
        }

        private void OnGUI()
        {
            if (sb == null)
                return;
            sb.Clear();
            sb.Append(nameof(TaskHandler))
                .Append(" Task count :")
                .AppendLine(TaskHandler.TaskCount.ToString())
                .Append(" Executing :")
                .AppendLine(TaskHandler.Executing.ToString())
                .AppendLine();

            sb.Append(nameof(EditorTaskHandler))
                .Append(" Task count :")
                .AppendLine(EditorTaskHandler.TaskCount.ToString())
                .Append(" Executing :")
                .AppendLine(EditorTaskHandler.Executing.ToString());

            using (new EditorGUI.DisabledGroupScope(true))
            {
                EditorGUILayout.TextArea(sb.ToString());
            }

            if (GUI.changed) Repaint();
        }
    }
}
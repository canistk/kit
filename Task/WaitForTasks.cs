using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    /// <summary>
    /// Wait until all tasks completed.
    /// </summary>
    public class WaitForTasks : kTask
    {
        kTask[] tasks;
        bool[] isEnd;
        public WaitForTasks(params kTask[] tasks)
        {
            isEnd = new bool[tasks.Length];
        }

        protected sealed override bool InternalExecute()
        {
            int cnt = tasks.Length;
            bool allCompleted = true;
            for (int i = 0; i < cnt; ++i)
            {
                if (!isEnd[i])
                {
                    isEnd[i] = tasks[i].Execute();
                    allCompleted = false;
                }
            }
            return !allCompleted;
        }

        protected override void OnDisposing()
        {
            for (int i = 0; i < tasks.Length; ++i)
                tasks[i]?.Dispose();
            base.OnDisposing();
        }

        protected override void OnFreeMemory()
        {
            for (int i = 0; i < tasks.Length; ++i)
                tasks[i] = null;
            tasks = null;
            base.OnFreeMemory();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public interface IDrawGizmos
    {
        public void DrawGizmos();
    }

    public abstract class TaskBase
    {
        /// <returns>
        /// true  = continue to execute on next cycle
        /// false = ending the task.
        /// </returns>
        public abstract bool Execute();
    }

    public abstract class kTask : TaskBase, System.IDisposable
    {
        public void Abort()
        {
            if (isDisposed)
                return;
            Dispose(disposing: true);
        }

        public sealed override bool Execute()
        {
            if (isDisposed || isCompleted)
                return false;

            isCompleted = !InternalExecute();
            return !isCompleted;
        }

        protected abstract bool InternalExecute();

        public bool isCompleted { get; private set; } = false;


        /// <summary>
        /// will be call during <see cref="Dispose(bool)"/>
        /// dispose managed state (managed objects)
        /// </summary>
        protected virtual void OnDisposing() { }
        /// <summary>
        /// TODO: free unmanaged resources (unmanaged objects) and override finalizer
        /// TODO: set large fields to null
        /// </summary>
        protected virtual void OnFreeMemory() { }

        #region Dispose
        public bool isDisposed { get; private set; } = false;

        protected void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    OnDisposing();
                }
                OnFreeMemory();
                isDisposed = true;
            }
        }

        ~kTask()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            // System.GC.SuppressFinalize(this);
        }
        #endregion Dispose
    }

    /// <summary>
    /// Design for debug usage,
    /// call <see cref="TryMoveToNextStep"/> to execute custom
    /// state machine. step by step.
    /// usually used with <see cref="IDrawGizmos"/> for editor visual result.
    /// </summary>
    public abstract class TaskDebug : kTask
    {
        protected sealed override bool InternalExecute()
        {
            if (m_RequestToNextStep == 0)
                return true;
            --m_RequestToNextStep;
            return InternalStepExecute();
        }
        protected abstract bool InternalStepExecute();

        #region Step Ctrl
        protected int m_RequestToNextStep = 0;
        public bool TryMoveToNextStep()
        {
            if (m_RequestToNextStep == 0)
            {
                ++m_RequestToNextStep;
                return true;
            }
            return false;
        }
        #endregion Step Ctrl
    }

    public abstract class TaskWithState : kTask
    {
        public enum eState
        {
            Idle,
            Running,
            Abort,
            Completed,
        }
        public eState state { get; private set; } = eState.Idle;

        protected abstract void OnEnter();

        /// <summary>
        /// Task will be process on each <see cref="Execute"/>
        /// </summary>
        /// <returns>
        /// true = Continue next cycle
        /// false = the end of the task.
        /// </returns>
        protected abstract bool ContinueOnNextCycle();

        /// <summary>
        /// will be call at the end of process, 
        /// include <see cref="OnDisposing"/>
        /// </summary>
        protected abstract void OnComplete();

        protected sealed override bool InternalExecute()
        {
            if (state == eState.Abort ||
                state == eState.Completed)
                return false;

            if (state == eState.Idle)
            {
                OnEnter();
                if (state == eState.Idle)
                {
                    // can be abort during on enter.
                    state = eState.Running;
                }
            } // Note: no else case, task should able to be finish within single frame.

            if (state == eState.Running)
            {
                if (!ContinueOnNextCycle())
                {
                    state = eState.Completed;
                    Dispose();
                    return false;
                }
            }

            return state < eState.Completed;
        }

        protected override void OnDisposing()
        {
            if (state >= eState.Running)
            {
                // fire if task being started,
                // even running, abort, Completed
                OnComplete();
            }
            base.OnDisposing();
        }
    }

    public class TaskSequence : kTask
    {
        private Queue<kTask> subTasks;
        public TaskSequence(params kTask[] subTasks)
        {
            this.subTasks = new Queue<kTask>(subTasks);
        }

        protected sealed override bool InternalExecute()
        {
            if (subTasks.Count == 0)
                return false;

            try
            {
                var t = subTasks.Peek();
                if (t.Execute())
                    return true;

                if (subTasks.Count > 0 && t == subTasks.Peek())
                    subTasks.Dequeue();
            }
            catch (System.Exception ex)
            {
                Debug.LogException(ex);
                if (subTasks.Count > 0)
                    subTasks.Dequeue();
            }
            return subTasks.Count > 0;
        }
    }

    public class TaskParallel : kTask
    {
        private List<kTask> subTasks;
        protected TaskParallel(IList<kTask> subTasks)
        { 
            this.subTasks = new List<kTask>(subTasks);
        }

        protected sealed override bool InternalExecute()
        {
            int i = subTasks.Count;
            while (i-- > 0)
            {
                try
                {
                    if (!subTasks[i].Execute())
                        subTasks.RemoveAt(i);
                }
                catch (System.Exception ex)
                { 
                    Debug.LogException(ex);
                    subTasks.RemoveAt(i); // cannot process anyway.
                }
            }
            return subTasks.Count > 0;
        }
    }

    public class TaskFunc : kTask
    {
        private readonly System.Func<bool> func;
        public TaskFunc(System.Func<bool> action)
        {
            this.func = action;
        }

        protected override bool InternalExecute()
        {
            try
            {
                var rst = func.Invoke();
                return rst;
            }
            catch(System.Exception ex)
            {
                Debug.LogException(ex);
                return false;
            }
        }
    }

    public class TaskAction : kTask
    {
        private readonly System.Action callback;
        public TaskAction(System.Action action)
        {
            this.callback = action;
        }

        protected override bool InternalExecute()
        {
            try
            {
                callback.Invoke();
                // only execute one
                return false;
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex);
                return false;
            }
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
using Unity.EditorCoroutines.Editor;
#endif
namespace Kit
{
    /// <summary>
    /// task can be execute without GameObject
    /// </summary>
    public static class TaskHandler
    {
        /// <summary>
        /// task should able to abort or dispose any time by it self.
        /// handle it manually via <see cref="kTask.Abort()"/> <see cref="kTask.Dispose()"/>
        /// </summary>
        /// <param name="task">any type of task.</param>
        public static void Add(TaskBase task)
        {
            if (!Application.isPlaying)
                throw new Exception($"Task should not be run in editor mode. please use <{nameof(EditorTaskHandler)}> instead.");
            if (tasks == null)
                throw new Exception();
            
            s_Tasks.Add(task);
        }

        public static void ClearUp()
        {
            if (!Application.isPlaying)
                throw new Exception($"Task should not be run in editor mode. please use <{nameof(EditorTaskHandler)}> instead.");
            if (tasks == null)
                throw new Exception();

            s_Tasks.Clear();
        }

        #region Runtime
        [RuntimeInitializeOnLoadMethod]
        private static void RuntimeInit()
        {
            if (s_Tasks != null)
                return;
            s_Tasks = new List<TaskBase>(8);
            UpdateManager.instance.Register(RuntimeUpdate);
        }

        private static List<TaskBase> s_Tasks = null;
        public static IReadOnlyList<TaskBase> tasks
        {
            get
            {
                if (s_Tasks == null)
                    RuntimeInit();
                return s_Tasks;
            }
        }
        public static int TaskCount => s_Tasks?.Count ?? 0;
        private static int m_ExecuteIndex;
        public static int Executing => TaskCount > 0 ? m_ExecuteIndex : -1;
        private static void RuntimeUpdate()
        {
            if (tasks == null || tasks.Count == 0)
                return;
            var markDel = new List<int>(8);
            for (int i = 0; i < tasks.Count; ++i)
            {
                var task = tasks[i];
                if (task is null || (task is kTask t0 && t0.isDisposed))
                {
                    markDel.Add(i);
                    continue;
                }

                try
                {
                    if (!task.Execute())
                    {
                        if (task is kTask t1)
                            t1.Dispose();
                        markDel.Add(i);
                    }
                }
                catch (Exception ex)
                {
                    ex.DeepLogInvocationException($"TaskError:{task}");
                    markDel.Add(i);
                }
            }

            if (markDel.Count > 0)
            {
                var i = markDel.Count;
                while (i-- > 0)
                {
                    var idx = markDel[i];
                    s_Tasks.RemoveAt(idx);
                }
            }
        }
        #endregion Runtime

    }

    /// <summary>
    /// task can be execute without GameObject and Editor
    /// </summary>
    public static class EditorTaskHandler
    {
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void Add(TaskBase task)
        {
#if UNITY_EDITOR
            if (s_EditorProgress == null)
                s_EditorProgress = EditorCoroutineUtility.StartCoroutineOwnerless(EditorLoop());
            s_EditorTasks.Add(task);
#endif
        }
        [System.Diagnostics.Conditional("UNITY_EDITOR")]
        public static void ClearUp()
        {
#if UNITY_EDITOR
            Editor_CleanUp();
#endif
        }

#if UNITY_EDITOR
        private static List<TaskBase> s_EditorTasks = new List<TaskBase>(8);
        public static int TaskCount => s_EditorTasks?.Count ?? 0;
        private static int m_ExecuteIndex;
        public static int Executing => TaskCount > 0 ? m_ExecuteIndex : -1;
        private static EditorCoroutine s_EditorProgress = null;
        private static IEnumerator EditorLoop()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode ||
                s_EditorTasks == null ||
                s_EditorTasks.Count == 0)
            {
                Editor_CleanUp();
                yield break;
            }

            int MaxQuota = 10000;

            while (s_EditorTasks.Count > 0)
            {
                var anchor = Time.fixedUnscaledTime;
                int quota = MaxQuota;
                int i = s_EditorTasks.Count;
                if (i == 0)
                {
                    yield return null;
                    continue;
                }

                while (i-- > 0)
                {
                    m_ExecuteIndex = i;
                    if (s_EditorTasks.Count <= i)
                        break; // clean up, or modify by others.

                    var task = s_EditorTasks[i];

                    if (task is null ||
                        (task is kTask t0 && t0.isDisposed))
                    {
                        s_EditorTasks.RemoveAt(i);
                        continue;
                    }

                    try
                    {
                        if (!task.Execute())
                        {
                            if (task is kTask t1)
                                t1.Dispose();
                            s_EditorTasks.RemoveAt(i);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.DeepLogInvocationException($"TaskError:{task}");
                        s_EditorTasks.RemoveAt(i); // broken fail to execute on next cycle.
                    }

                    var diff = Time.fixedUnscaledTime - anchor;
                    if (diff >= 1f)
                    {
                        anchor = Time.fixedUnscaledTime;
                        yield return null;
                    }
                    if (--quota <= 0)
                        yield return null;
                }

            }

            // Clean up handling.
            if (s_EditorTasks.Count == 0)
            {
                Editor_CleanUp();
            }
        }

        private static void Editor_CleanUp()
        {
            Debug.Log("EditorTaskHandler completed.");
            s_EditorTasks.Clear();
            EditorUtility.ClearProgressBar();
            if (s_EditorProgress != null)
                EditorCoroutineUtility.StopCoroutine(s_EditorProgress);
            s_EditorProgress = null;
        }
#endif
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public abstract class IntervalTask : kTask
    {
        protected enum eState
        {
            WaitForStart,
            Loop,
            Completed,
        }
        protected eState state { get; private set; } = eState.WaitForStart;

        private float triggerTime;
        private float interval;
        public IntervalTask(float interval)
        {
            triggerTime = -interval; // allow execute on first frame of the game.
            this.interval = interval;
        }

        protected sealed override bool InternalExecute()
        {
            if (currentTime - triggerTime < interval)
                return !StopInterval();

            triggerTime = currentTime;
            OnInterval();
            return !StopInterval();
        }

        protected virtual float currentTime
        {
            get => Time.timeSinceLevelLoad;
        }

        protected abstract void OnInterval();

        protected abstract bool StopInterval();
    }
}

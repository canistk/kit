using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class DelayTask : TaskWithState
    {
        private float startTime;
        private float duration;
        protected virtual float currentTime { get => Time.timeSinceLevelLoad; }
        private System.Action callback;

        public DelayTask(float duration, System.Action callback)
        {
            this.duration = duration;
            this.callback = callback;
        }

        protected override void OnEnter()
        {
            startTime = currentTime;
        }

        protected override bool ContinueOnNextCycle()
        {
            return currentTime - startTime < duration;
        }

        protected override void OnComplete()
        {
            callback?.Invoke();
        }
    }
}
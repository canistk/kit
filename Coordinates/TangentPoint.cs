﻿using UnityEngine;

namespace Kit
{
	[System.Serializable]
	public class TangentPoint : VirtualPoint, ITangentPoint
	{
		#region Variable's
		/// <summary>
		/// To manage the tangent's rotation together
		/// </summary>
		[SerializeField, EularAngles] private Quaternion m_TangentRotation = Quaternion.identity;
		public Quaternion tangentRotation
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return m_TangentRotation;
			}
			set
			{
				if (m_TangentRotation != value)
				{
					m_TangentRotation = value;
					SetDirty();
				}
			}
		}

		private static int s_CorruptionErrorFrameCount = 0;
		/// <summary>Replace current rotation with tangent's rotation.</summary>
		private Matrix4x4 m_TangentMatrix = Matrix4x4identity;
		private Matrix4x4 tangentMatrix
		{
			get
			{
				if (dirty)
					InternalCalculate();
				Matrix4x4 mat = matrix * m_TangentMatrix;
				if (!mat.ValidTRS())
				{
					if (s_CorruptionErrorFrameCount != Time.frameCount)
					{
						// BUG : usually happen in editor mode + playing + change scene focus
						// the issue will appear across all anchor points at the same time, even the data didn't change.
						// what kind of bug is this ?
						s_CorruptionErrorFrameCount = Time.frameCount;
						// Debug.LogWarning($"{GetType().Name} matrix data corrupted : recalculate by raw data.");
					}
					SetDirty(); // Unknown error detected
					InternalCalculate();
					mat = matrix * m_TangentMatrix;
				}
				return mat;
			}
		}
		[SerializeField] private eTangentType m_TangentType = eTangentType.Connected;
		[SerializeField] private Vector3 m_LocalInTangent;
		[SerializeField] private Vector3 m_LocalOutTangent;
		#endregion Variable's

		#region Constructor
		public TangentPoint(object parent) : base(parent) { }
		public TangentPoint(Vector3 position, Quaternion rotation = default,
			Vector3 inTangent = default, Vector3 outTangent = default,
			object parent = null) : base(parent)
		{
			this.position = position;
			this.rotation = rotation == default? Quaternion.identity : rotation;
			this.inTangentPoint = inTangent;
			this.outTangentPoint = outTangent;
		}
		#endregion Constructor

		#region Tangent's
		/// <summary>To define the relationship of the tangent's and control movement behaviour of them.</summary>
		public eTangentType tangentType
		{
			get => m_TangentType;
			set
			{
				if (m_TangentType != value)
				{
					if (value == eTangentType.None)
					{
						m_LocalInTangent = Vector3zero;
						m_LocalOutTangent = Vector3zero;
					}
					else if (m_TangentType == eTangentType.None && value != eTangentType.None)
					{
						m_LocalInTangent = Vector3.left;
						m_LocalOutTangent = Vector3.right;
					}
					m_TangentType = value;
					SetDirty();
				}
			}
		}

		public Vector3 localInTangentPoint
		{
			get => m_LocalInTangent;
			set
			{
				if (tangentType == eTangentType.None)
					m_LocalInTangent = Vector3zero;
				else if (m_LocalInTangent != value)
				{
					m_LocalInTangent = value;
					if (tangentType == eTangentType.Connected)
						m_LocalOutTangent = -value;
					SetDirty();
				}
			}
		}
		/// <summary>a wrapper for direction vector, <see cref="localInTangentPoint"/></summary>
		public Vector3 inTangent
		{
			get => localInTangentPoint;
			set => localInTangentPoint = value;
		}
		public Vector3 localOutTangentPoint
		{
			get => m_LocalOutTangent;
			set
			{
				if (tangentType == eTangentType.None)
					m_LocalOutTangent = Vector3zero;
				else if (m_LocalOutTangent != value)
				{
					m_LocalOutTangent = value;
					if (tangentType == eTangentType.Connected)
						m_LocalInTangent = -value;
					SetDirty();
				}
			}
		}
		/// <summary>a wrapper for direction vector, <see cref="localOutTangentPoint"/></summary>
		public Vector3 outTangent
		{
			get => localOutTangentPoint;
			set => localOutTangentPoint = value;
		}

		public Vector3 inTangentPoint
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return tangentMatrix.TransformPoint(localInTangentPoint);
			}
			set
			{
				localInTangentPoint = tangentMatrix.InverseTransformPoint(value);
			}
		}

		public Vector3 outTangentPoint
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return tangentMatrix.TransformPoint(localOutTangentPoint);
			}
			set
			{
				localOutTangentPoint = tangentMatrix.InverseTransformPoint(value);
			}
		}
		#endregion Tangent's

		#region Calculation
		protected override void OnCalculate()
		{
			base.OnCalculate();
			if (m_TangentRotation.IsInvalid())
				m_TangentRotation = Quaternion.identity;
			m_TangentRotation.Normalize();
			m_TangentMatrix = Matrix4x4.Rotate(m_TangentRotation);
		}
        #endregion Calculation

        #region Gizmos
		public void DrawGizmos(Color point, Color tangent)
        {
			using(new ColorScope(tangent))
            {
				Gizmos.DrawLine(position, inTangentPoint);
				Gizmos.DrawLine(position, outTangentPoint);
				Gizmos.color = point;
				Gizmos.DrawSphere(position, 0.02f);
			}
        }
        #endregion Gizmos

        /// <summary>Make a copy of current <see cref="TangentPoint"/></summary>
        /// <returns></returns>
        public new TangentPoint Clone()
		{
			return new TangentPoint(parent)
			{
				localPosition = localPosition,
				localRotation = localRotation,
				m_TangentRotation = tangentRotation,
				m_LocalInTangent = localInTangentPoint,
				m_LocalOutTangent = localOutTangentPoint,
			};
		}

		public override int GetHashCode()
		{
			return Extensions.GenerateHashCode(parent, localPosition, localRotation, tangentRotation, localInTangentPoint, localOutTangentPoint);
		}
	}
}
﻿using UnityEngine;

namespace Kit
{
	[System.Serializable]
	public class VirtualPoint
	{
		#region Variables
		public bool dirty { get; private set; } = true;
		public void SetDirty() { dirty = true; }

		// Calibrate
		public event Callback ChangedValue = null;

		[SerializeField] private Vector3 m_LocalPosition = Vector3.zero;
		[SerializeField, EularAngles] private Quaternion m_LocalRotation = Quaternion.identity;
		[SerializeField] private Transform m_ParentTransform = null;
		[SerializeField] private VirtualPoint m_ParentVirtualPoint = null;
		
		protected static /* readonly */ Vector3 Vector3one = Vector3.one;
		protected static /* readonly */ Vector3 Vector3zero = Vector3.zero;
		protected static /* readonly */ Vector3 Vector3forward = Vector3.forward;
		protected static /* readonly */ Matrix4x4 Matrix4x4identity = Matrix4x4.identity;
		#endregion Variables

		#region Constructor
		public VirtualPoint(object parent)
		{
			SetParent(parent);
		}
		#endregion Constructor

		#region Public API
		public Vector3 localPosition
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return m_LocalPosition;
			}
			set
			{
				if (m_LocalPosition != value)
				{
					m_LocalPosition = value;
					dirty = true;
				}
			}
		}
		
		public Quaternion localRotation
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return m_LocalRotation;
			}
			set
			{
				if (m_LocalRotation != value)
				{
					m_LocalRotation = value.normalized;
					dirty = true;
				}
			}
		}

		public Vector3 eulerAngles
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return m_LocalRotation.eulerAngles;
			}
			set
			{
				m_LocalRotation = Quaternion.Euler(value);
			}
		}

		public Vector3 position
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return parentMatrix.TransformPoint(localPosition);
			}
			set
			{
				localPosition = parentMatrix.InverseTransformPoint(value);
			}
		}

		public Quaternion rotation
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return parentMatrix.GetRotation().normalized * localRotation;
			}
			set
			{
				localRotation = (parentMatrix.inverse.GetRotation() * value).normalized;
			}
		}
		#endregion Public API

		#region Matrix dependance
		public void SetParent(object obj)
		{
			if (obj == null)
			{
				m_ParentTransform = null;
				m_ParentVirtualPoint = null;
			}
			else if (obj is VirtualPoint virtualPoint)
			{
				if (virtualPoint != this)
				{
					m_ParentTransform = null;
					m_ParentVirtualPoint = virtualPoint;
				}
			}
			else if (obj is Transform transform)
			{
				m_ParentTransform = transform;
				m_ParentVirtualPoint = null;
			}
			else
				Debug.LogError($"{nameof(VirtualPoint)} should only accept {nameof(VirtualPoint)} and {nameof(Transform)}.");
		}
		protected object parent
		{
			get
			{
				if (m_ParentTransform != null)
					return m_ParentTransform;
				else if (m_ParentVirtualPoint != null)
					return m_ParentVirtualPoint;
				else
					return null;
			}
		}
		public Matrix4x4 parentMatrix
		{
			get
			{
				if (m_ParentTransform != null)
					return m_ParentTransform.localToWorldMatrix;
					// return m_ParentTransform.ToMatrix(false);
				else if (m_ParentVirtualPoint != null)
					return m_ParentVirtualPoint.matrix;
				else
					return Matrix4x4identity;
			}
		}
		private Matrix4x4 m_Matrix;
		public Matrix4x4 matrix
		{
			get
			{
				if (dirty)
					InternalCalculate();
				return parentMatrix * m_Matrix;
			}
		}
		#endregion Matrix dependance

		#region Calculate
		public void Ping()
		{
			if (dirty)
				InternalCalculate();
		}

		protected void InternalCalculate()
		{
			// Note : avoid dead loop, DON'T use localPosition/localRotation
			if (dirty)
			{
				dirty = false;
				if (m_LocalRotation.IsInvalid())
					m_LocalRotation = Quaternion.identity;
				m_Matrix = Matrix4x4.TRS(m_LocalPosition, m_LocalRotation, Vector3one);
				OnCalculate();
				ChangedValue?.Invoke();
			}
		}

		protected virtual void OnCalculate() { }
		#endregion Calculate

		public VirtualPoint Clone()
		{
			return new VirtualPoint(parent)
			{
				m_LocalPosition = localPosition,
				m_LocalRotation = localRotation,
			};
		}

		public override int GetHashCode()
		{
			return Extensions.GenerateHashCode(parent, localPosition, localRotation);
		}
	}
}
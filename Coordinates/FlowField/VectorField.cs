// #define DEBUG_LINE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
namespace Kit.FlowField
{
    /// <summary>
    /// Create integration field toward target node.
    /// </summary>
    public class VectorField : kTask
    {
        private const float s_DebugMs = 0f;
        public readonly FNode       targetNode;
        public readonly FlowFieldBaker   flowField;
        private FFSetting           setting => flowField.setting;
        private readonly int        stepPerFrame;

        public VectorField(FlowFieldBaker fieldField, Vector3 destination, int stepPerFrame)
        {
            if (fieldField == null)
                throw new System.NullReferenceException();
            if (destination.IsNaN() || destination.IsInfinity())
                throw new System.ArgumentException();

            this.stepPerFrame = stepPerFrame;
            this.flowField = fieldField;

            if (!fieldField.TryGetLeafNode(destination, out targetNode))
            {
                // throw new System.ArgumentException($"Flowfield not include target destination {destination:F2}");
                Debug.LogWarning($"Flowfield not include target destination {destination:F2}");
            }
        }

        public enum eState
        {
            Idle,
            PrepareHeatmap,
            Heatmap,      // calculate cost toward destination
            EndHeatmap,
            VectorField,    // find vector toward next node over flowfield.
            Completed,
        }
        public eState state = eState.Idle;
        private Stopwatch m_Stopwatch = new Stopwatch();

        #region Calculate Cost Heatmap
        private class HeatCost : IComparable<HeatCost>
        {
            private readonly FlowFieldBaker  flowField;
            public readonly FNode       node;
            public eNeighbor            flag;
            private Dictionary<eNeighbor, FNode[]> m_NeighborDict;
            
            public HeatCost(FNode node, FlowFieldBaker flowField, HeatCost defaultMin)
            {
                this.node       = node;
                this.flowField  = flowField;
                this.flag       = eNeighbor.Front;
                this.shortestLink = defaultMin;
                this.m_CostDirty = true;
                this.m_NeighborDict = new Dictionary<eNeighbor, FNode[]>(6);
            }

            public FNode[] GetNeighbor(eNeighbor dir)
            {
                if (m_NeighborDict.TryGetValue(dir, out var arr))
                    return arr;
                arr = node.GetNeighbor(flowField, dir, flowField.setting.errorThreshold).ToArray();
                m_NeighborDict.Add(dir, arr);
                return arr;
            }

            public HeatCost shortestLink { get; private set; }
            public bool CalcShortestPath(HeatCost heatCost)
            {
                if (heatCost == null)
                    return false;
                if (heatCost.node == node)
                    return false;

                if (this.shortestLink == null)
                {
                    this.shortestLink = heatCost;
                    this.m_CostDirty = true;
                    return true;
                }
                else if (this.shortestLink.GetAccumulateCost() <= heatCost.GetAccumulateCost())
                {
                    return false;
                }

                this.shortestLink = heatCost;
                this.m_CostDirty = true;
                return true;
            }

            public int GetOrgCost()
            {
                return flowField.costMapping.GetAreaCost(node);
            }

            private int m_AccumulateCost;
            private bool m_CostDirty = false;
            public int GetAccumulateCost()
            {
                if (shortestLink == null)
                    return 0; // target node

                if (m_CostDirty)
                {
                    // recursive
                    m_CostDirty = false;
                    var y = shortestLink.GetAccumulateCost();
                    var x = Mathf.Max(1, GetOrgCost());
                    m_AccumulateCost = x + y;
                }
                return m_AccumulateCost;
            }

            public int CompareTo(HeatCost other)
            {
                return GetAccumulateCost().CompareTo(other.GetAccumulateCost());
            }

            public static explicit operator VectorFieldData(HeatCost heatCost)
            {
                if (heatCost.shortestLink == null)
                    return VectorFieldData.Empty;
                return new VectorFieldData(heatCost);
            }
        }
        private PriorityQueue<HeatCost> m_TaskQueue;
        private Dictionary<FNode, HeatCost> m_OpenSet;
        private HashSet<FNode> m_CloseSet;
        private int m_TotalLeafCnt;
        private bool PrepareHeatmap()
        {
            m_CloseSet      = new HashSet<FNode>(m_TotalLeafCnt);
            m_OpenSet       = new Dictionary<FNode, HeatCost>(m_TotalLeafCnt);
            m_TaskQueue     = new PriorityQueue<HeatCost>();
            m_TaskQueue.Enqueue(new HeatCost(targetNode, flowField, null));
            m_Stopwatch.Start();
            return false;
        }
        private bool CreatingHeatmap()
        {
            if (m_TaskQueue == null)
                throw new System.Exception();

            if (m_TaskQueue.Count == 0)
                return false;

            var process     = m_TaskQueue.Peek();
            var quota       = stepPerFrame;
            while (quota-- > 0 && m_TaskQueue.Count > 0)
            {
#if false
                // Debug case : end condition invalid.
                if (process.flag >= eNeighbor.All)
                {
                    // fill all neighbor cost
                    throw new System.InvalidProgramException();
                }

                // Debug case : re-process the closest. check logic.
                if (m_CloseSet.Contains(process.node))
                {
                    m_TaskQueue.Dequeue();
                    if (m_TaskQueue.Count > 0)
                    {
                        process = m_TaskQueue.Peek();
                    }
                    ++quota;
                    Debug.LogWarning("Closet set, node in process. skip");
                    continue;
                }
#endif

                var selfCost = process.GetOrgCost();

                foreach (var _otherNode in process.GetNeighbor(process.flag))
                {
                    if (m_CloseSet.Contains(_otherNode))
                        continue; // early return to reduce double calculation

                    if (!m_OpenSet.TryGetValue(_otherNode, out var heatCost))
                    {
                        heatCost = new HeatCost(_otherNode, flowField, process);
                        m_OpenSet.Add(_otherNode, heatCost);
                        m_TaskQueue.Enqueue(heatCost);
                    }


                    // cost from another node toward this node;
                    process.CalcShortestPath(heatCost);
                    
#if DEBUG_LINE
                    DebugExtend.DrawPoint(_otherNode.center, Color.yellow, 0.3f, s_DebugMs, true);
#endif
                }

                process.flag++;
                if (process.flag < eNeighbor.All)
                {
#if DEBUG_LINE
                    DebugExtend.DrawBounds(process.node.bounds, Color.yellow, s_DebugMs, true);
#endif
                }
                else
                {
                    // all direction from this node was calculated.
                    m_CloseSet.Add(process.node);
                    m_TaskQueue.Dequeue();
                    if (m_TaskQueue.Count > 0)
                    {
                        process = m_TaskQueue.Peek();
                    }
                }
            }

            return m_TaskQueue.Count > 0;
        }

        private bool EndHeatmap()
        {
            m_CloseSet = null;
            m_TaskQueue = null;
            m_Stopwatch.Stop();
            Debug.Log($"Heatmap used {m_Stopwatch.ToStringDetail()}");
            return false;
        }
        #endregion Calculate Cost Heatmap

        #region Vector Field
        private struct VectorFieldData
        {
            public static readonly VectorFieldData Empty = default;
            public Vector3 anchor;
            public Vector3 direction;
            public int cost;
            public VectorFieldData(HeatCost h)
            {
                if (h.shortestLink == null)
                    throw new Exception("shortestLink not found, fail to create vector.");
                this.anchor = h.node.center;
                this.direction    = (h.shortestLink.node.center - anchor).normalized;
                this.cost   = h.GetAccumulateCost();
            }
        }
        private Dictionary<FNode, VectorFieldData> m_VectorField;
        private bool CreateVectorField()
        {
            m_Stopwatch.Reset();
            m_Stopwatch.Start();
            m_VectorField = new Dictionary<FNode, VectorFieldData>(m_TotalLeafCnt);
            foreach (var h in m_OpenSet.Values)
            {
                if (h.node == targetNode)
                    continue;

                m_VectorField.Add(h.node, (VectorFieldData)h);
#if DEBUG_LINE
                DebugExtend.DrawLine(node.center, heatCost.shortestLink.node.center, Color.green, s_DebugMs, true);
#endif
            }

            Debug.Log($"VectorField used {m_Stopwatch.ToStringDetail()}");
            m_Stopwatch.Stop();
            m_OpenSet = null; // clean up
            m_Stopwatch = null;
            return false;
        }
        #endregion Vector Field

        protected override bool InternalExecute()
        {
            if (targetNode == null)
                return false;

            bool keepRun = state switch
            {
                eState.Idle             => OnEnter(),
                eState.PrepareHeatmap   => PrepareHeatmap(),
                eState.Heatmap          => CreatingHeatmap(),
                eState.EndHeatmap       => EndHeatmap(),
                eState.VectorField      => CreateVectorField(),
                eState.Completed        => OnComplete(),
                _ => throw new System.NotImplementedException()
            };

            if (!keepRun)
            {
                ++state;
            }
            return state < eState.Completed;
        }

        private bool OnEnter()
        {
            m_TotalLeafCnt = flowField.rootNode.GetLeafCount();
            return false;
        }

        private bool OnComplete()
        {
            m_Stopwatch = null;
            m_TaskQueue = null;
            m_OpenSet = null;
            m_CloseSet = null;
            return false;
        }

        protected override void OnDisposing()
        {
            base.OnDisposing();
            m_VectorField.Clear();
        }

        protected override void OnFreeMemory()
        {
            base.OnFreeMemory();
            m_VectorField = null;
        }

        /// <summary>
        /// Get vector result based on giving anchor.
        /// </summary>
        /// <param name="anchor"></param>
        /// <param name="dir"></param>
        /// <param name="nodeCenter"></param>
        /// <returns></returns>
        public bool TryGetPath(Vector3 anchor, out Vector3 dir, out Vector3 nodeCenter)
        {
            dir = default;
            nodeCenter = default;
            if (state < eState.Completed || state < eState.Completed)
                return false;
            if (m_VectorField == null)
                return false;

            if (!flowField.TryGetLeafNode(anchor, out var node))
            {
                Debug.LogError($"FlowField didn't cover position={anchor:F2}");
                return false;
            }

            return TryGetPath(node, out dir, out nodeCenter);
        }

        /// <summary>
        /// Get vector result based on giving node.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="dir"></param>
        /// <param name="nodeCenter"></param>
        /// <returns></returns>
        public bool TryGetPath(FNode node, out  Vector3 dir, out Vector3 nodeCenter)
        {
            if (!m_VectorField.TryGetValue(node, out var data))
            {
                Debug.LogError($"FlowField didn't contain giving node (pos:{node.center:F2},size:{node.size:F2})");
                dir = default;
                nodeCenter = default;
                return false;
            }

            dir = data.direction;
            nodeCenter = data.anchor;
            return true;
        }

        public void DrawDebug(bool useGizmos = false, Color? color = null)
        {
            if (m_VectorField == null)
                return;

            var c = color.HasValue ? color.Value : Color.green.CloneAlpha(.4f);
            if (useGizmos)
            {
                using (new ColorScope(color))
                {
                    foreach ((var node, var o) in m_VectorField)
                    {
                        Gizmos.DrawLine(node.center, o.anchor);
                        GizmosExtend.DrawLabel(node.center, $"{o.cost}");
                    }
                }
            }
            else
            {
                foreach ((var node, var o) in m_VectorField)
                {
                    Debug.DrawLine(o.anchor, o.anchor + o.direction, c);
                    GizmosExtend.DrawLabel(node.center, $"{o.cost}");
                }
            }
        }
    }
}
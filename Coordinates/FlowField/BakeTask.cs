using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.FlowField
{
    [System.Obsolete("FlowFieldBaker obsolete")]
    public class BakeTask : TaskWithState
    {
        public BakeTask(FlowFieldBaker flowField)    => this.flowField = flowField;

        #region Redirection
        public readonly FlowFieldBaker   flowField;
        public FlowFieldData        database    => flowField.m_FlowFieldData;
        private FFSetting           setting     => flowField?.setting;
        private Transform           transform   => flowField?.transform;
        private GameObject          gameObject  => flowField?.gameObject;
        private Vector3             position    => transform.position;
        private Quaternion          rotation    => transform.rotation;
        #endregion Redirection

        #region Bake API
        private PriorityQueue<float /* area magnitude */, kTask> tasks = new PriorityQueue<float, kTask>();
        public int queueTaskCount => tasks.Count;
        private List<kTask> parallelPhyTasks = new List<kTask>();
        public int parallelPhyTaskCount => parallelPhyTasks.Count;
        public int completedTaskCount { get; private set; } = 0;
        public void Request(FNode from, kTask task)
        {
            if (from == null)
                return;

            if (task is FNodeDetectTask)
            {
                parallelPhyTasks.Add(task);
            }
            else // if (task is SplitCellRequest)
            {
                var priority = -from.bounds.size.sqrMagnitude;
                tasks.Enqueue(task, priority);
            }
        }
        #endregion Bake API

        #region Task Life Cycle
        protected override void OnEnter()
        {
            if (database == null)
                throw new System.Exception("Database not found.");

            database.rootNode = new FNode(transform.position, setting.initialWorldSize);
            database.rootNode.Detect(this);
            database.IsBaking = true;
        }

        protected override bool ContinueOnNextCycle()
        {
            // Pick high priority task
            {
                var cnt = 0;
                var task = (tasks.IsEmpty) ? null : tasks.Peek();
                while (task != null &&
                    tasks.Count > 0 &&
                    cnt < setting.splitQuota)
                {
                    if (task.Execute())
                        break; // wait
                    // or put it into parallel tasks.(keep physics together will faster
                    // parallelPhyTasks.Add(task);

                    // task completed.
                    ++cnt;
                    tasks.Dequeue();
                    task = (tasks.IsEmpty) ? null : tasks.Peek();
                }
            }

            // run tasks
            if (parallelPhyTasks != null && parallelPhyTasks.Count > 0)
            {
                int i = parallelPhyTasks.Count;
                while (i-- > 0)
                {
                    if (parallelPhyTasks.Count <= i)
                        break; // clean up, or modify by others.
                    var task = parallelPhyTasks[i];
                    if (!task.Execute())
                    {
                        int cnt = parallelPhyTasks.Count;
                        if (0 <= i && i < cnt &&
                            parallelPhyTasks[i] != null &&
                            parallelPhyTasks[i] == task)
                        {
                            ++completedTaskCount;
                            parallelPhyTasks[i].Dispose();
                            parallelPhyTasks.RemoveAt(i);
                        }
                    }
                }
            }

            // Debug.Log($"RunTasks {tasks.Count}");
            return tasks.Count > 0 || parallelPhyTasks.Count > 0;
        }

        protected override void OnComplete()
        {
            database.IsBaking = false;
        }
        #endregion Task Life Cycle
    }

    public abstract class FNodeTask : TaskWithState
    {
        protected readonly FNode node;
        protected readonly BakeTask bakeTask;
        protected FlowFieldBaker flowField => bakeTask.flowField;
        protected FFSetting setting => flowField?.setting;

        public FNodeTask(BakeTask bakeTask, FNode node)
        {
            this.bakeTask = bakeTask;
            this.node = node;
        }

        protected string TAG => $"[FlowField]-{GetType().Name}";
        protected override void OnComplete()
        {
            //Debug.Log($"{TAG} Completed, {GetHashCode()}");
        }
    }

    public class FNodeDetectTask : FNodeTask
    {
        private static Collider[] _ColliderBuffer = null;
        private Collider[] colliderBuffer
        {
            get
            {
                if (_ColliderBuffer == null || _ColliderBuffer.Length != setting.bakeBuffer)
                {
                    _ColliderBuffer = new Collider[setting.bakeBuffer];
                    staticCollider = new Collider[setting.bakeBuffer];
                }
                return _ColliderBuffer;
            }
        }
        private static Collider[] staticCollider;

        private System.Action<eCost> callback;
        public FNodeDetectTask(BakeTask bakeTask, FNode node, System.Action<eCost> callback) : base(bakeTask, node)
        {
            this.callback = callback;
        }

        protected override void OnEnter()
        {
            if (node == null || callback == null)
            {
                Debug.LogWarning($"{TAG} Request rejected, missing reference : node={node}, output={callback}");
                Abort();
            }
        }

        protected override bool ContinueOnNextCycle()
        {
            if (node == null || callback == null)
            {
                Debug.LogError($"{TAG} interrupt, missing reference : node={node}, output= {callback}");
                return false;
            }

            var hitCnt = Physics.OverlapBoxNonAlloc(node.center, node.bounds.extents, colliderBuffer, Quaternion.identity, setting.layerMask, QueryTriggerInteraction.Collide);
            if (hitCnt == 0)
            {
                callback?.TryCatchDispatchEventError(o => o?.Invoke(eCost.Empty));
                return false; // empty, no need to split.
            }
            var found = 0;
            {
                for (int i = 0; i < colliderBuffer.Length && i < hitCnt; ++i)
                {
                    if (colliderBuffer[i].gameObject.isStatic)
                        staticCollider[found++] = colliderBuffer[i];
                }
            }

            if (found == 0)
            {
                callback?.TryCatchDispatchEventError(o => o?.Invoke(eCost.Empty));
                return false;
            }

            eCost cost = eCost.Empty;
            // cache cost from ISpaceCost within
            for (int i = 0; i < found; ++i)
            {
                var _cost = GetCost(colliderBuffer[i]);
                cost |= _cost;
            }

            callback?.TryCatchDispatchEventError(o => o?.Invoke(cost));

            if (node.size * 0.5f <= setting.minNodeSize)
                return false;

            bool DetectEmptySpace(float factor01)
            {
                var b8 = node.bounds.GetVertices();
                var threshold = Mathf.Max(1E-5f, node.size * (0.25f + 0.25f * factor01));
                for (int i = 0; i < found && i < staticCollider.Length; ++i)
                {
                    var collider = staticCollider[i];
                    var tooManySpace = false;
                    for (int k = 0; k < b8.Length && !tooManySpace; ++k)
                    {
                        var point = b8[k];
                        var p = (collider is MeshCollider mc && !mc.convex) ?
                            collider.bounds.ClosestPoint(point) : // use bounds box instead.
                            collider.ClosestPoint(point);

                        var dis = (p - point).magnitude;
                        if (dis > threshold)
                            tooManySpace = true;
                        // To detect if the content(s) is big enough to hold this grid,
                        // when it's too small, try to split it.
                    }
                    if (!tooManySpace)
                    {
                        // when any of this are big enough for this node.
                        return false;
                    }
                }

                // when all content is too small.
                return true;
            }

            if (!DetectEmptySpace(setting.errorThreshold))
                return false;

            // had space to split.
            bakeTask.Request(node, new SplitCellRequest(bakeTask, node));
            return false;
        }

        private eCost GetCost(Collider collider)
        {
            if (collider == null)
                throw new System.NullReferenceException();
            if (!collider.isTrigger)
            {
                // normal static collider
                return eCost.Obstacle;
            }

            // Trigger
            var costs = collider.GetComponentsInChildren<ISpaceCost>(includeInactive: false);
            if (costs.Length == 0)
                return eCost.Empty;

            var rst = eCost.Empty;
            for (int i = 0; i < costs.Length; ++i)
            {
                rst |= costs[i].GetCost();
            }
            return rst;
        }

    }

    public class SplitCellRequest : FNodeTask
    {
        public SplitCellRequest(in BakeTask bakeTask, FNode node) : base(bakeTask, node) { }
        protected override void OnEnter()
        {
            if (node == null)
            {
                Debug.LogWarning($"{TAG} Request rejected, missing reference : node={node}.");
                Abort();
            }
            if (node.childrens != null)
            {
                Debug.LogWarning($"{TAG} Request rejected, already splitted.");
                Abort();
            }
        }

        protected override bool ContinueOnNextCycle()
        {
            try
            {
                node.SplitNodes(bakeTask);
            }
            catch
            {
                return false;
            }

            for (int i = 0; i < node.childrens.Length; ++i)
            {
                node.childrens[i].Detect(bakeTask);
            }
            return false;
        }

    }
}
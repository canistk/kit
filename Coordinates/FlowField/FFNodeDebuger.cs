using Kit;
using Kit.FlowField;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode]
public class FFNodeDebuger : MonoBehaviour
{
    [SerializeField] FlowFieldBaker m_FlowField = null;
    [SerializeField] eNeighbor m_Neighbor = eNeighbor.Front;
    [SerializeField, Range(1E-5f, 0.1f)] float m_Err = 1E-5f;
    [SerializeField] Color m_TargetColor = Color.red.CloneAlpha(0.5f);
    [SerializeField] Color m_NeighborColor = Color.cyan.CloneAlpha(0.5f);

    public void OnDrawGizmos()
    {
        if (m_FlowField == null)
            return;

        if (!m_FlowField.TryGetLeafNode(transform.position, out var node))
            return;

        GizmosExtend.DrawBounds(node.bounds, m_TargetColor);
        // var str = System.Enum.GetNames(typeof(eCost)).Cast<uint>().Select(v => v.ToString()).ToArray();
        // GizmosExtend.DrawLabel(node.center, $"{string.Join(',', str)}");\
        if (node.IsLeafNode)
            GizmosExtend.DrawLabel(node.center, $"{node.cost.ToString("G")}, size = {node.size:F2}");

        foreach (var n in node.GetNeighbor(m_FlowField, m_Neighbor, m_Err))
        {
            GizmosExtend.DrawBounds(n.bounds, m_NeighborColor);
        }
    }
}

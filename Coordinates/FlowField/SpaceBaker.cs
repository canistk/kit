using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit.MeshKit.FaceEdge;
namespace Kit.FlowField
{
    public class SpaceBaker : MonoBehaviour
    {
        public SpaceData m_Data;

        [System.Serializable]
        public class Gizmos
        {
            public Color area = Color.yellow.CloneAlpha(0.5f);
        }
        [System.Serializable]
        public class Setting
        {
            public Vector3 halfExtents = new Vector3(5f, 5f, 5f);
            public LayerMask layerMask = Physics.DefaultRaycastLayers;
            public Gizmos gizmos;
        }
        [SerializeField] Setting m_Setting;
        public Setting setting
        {
            get
            {
                if (m_Setting == null)
                    m_Setting = new Setting();
                return m_Setting;
            }
        }

        public void Bake()
        {
            if (Application.isPlaying)
                return;
            var task = new SpaceBakerTask(this, m_Data);
            TaskHandler.Add(task);
        }

        private void OnDrawGizmosSelected()
        {
            GizmosExtend.DrawWireCube(transform.position, setting.halfExtents, transform.rotation, setting.gizmos.area);
        }

        private void OnDrawGizmos()
        {
            
        }
    }

    public class SpaceBakerTask : kTask
    {
        private readonly SpaceBaker baker;
        private SpaceBaker.Setting setting => baker.setting;
        private SpaceData data;
        public enum eState
        {
            Idle = 0,
            OverlapTest,
            BuildMeshFromCollider,

            Completed,
            Error
        }
        public eState state = eState.Idle;

        public SpaceBakerTask(SpaceBaker baker, SpaceData data)
        {
            if (baker == null)
                throw new System.NullReferenceException();
            this.baker = baker;
            this.data = data;
        }

        protected override bool InternalExecute()
        {
            if (data == null)
                throw new System.NullReferenceException();

            switch (state)
            {
                case eState.Idle:           OnEnter(); break;
                case eState.OverlapTest:    OverlapTest(); break;
                case eState.BuildMeshFromCollider: BuildMeshFromCollider(); break;
                default:
                    throw new System.Exception($"Invalid cases {state}");
            }

            Debug.Log(nameof(SpaceBakerTask));
            if (state == eState.Completed)
                OnComplete();
            return state < eState.Completed;
        }

        private void OnEnter()
        {
            data.IsBaking = true;
            ++state;
        }

        private struct OverlapRst
        {
            public Collider[] staticColliders, colliders;
        }
        private OverlapRst m_OverlapRst;
        private void OverlapTest()
        {
            var pos         = baker.transform.position;
            var rot         = baker.transform.rotation;
            var halfExtents = setting.halfExtents;
            var colliders   = Physics.OverlapBox(pos, halfExtents, rot, setting.layerMask, QueryTriggerInteraction.Ignore);
            var statics     = new List<Collider>(colliders.Length);
            var nonStatics  = new List<Collider>(colliders.Length);
            for (int i = 0; i < colliders.Length; ++i)
            {
                if (colliders[i].gameObject.isStatic)
                {
                    statics.Add(colliders[i]);
                }
                else
                {
                    nonStatics.Add(colliders[i]);
                }
            }
            m_OverlapRst.staticColliders  = statics.ToArray();
            m_OverlapRst.colliders        = nonStatics.ToArray();
            ++state;
        }

        private struct BuildMeshFromColliderRst
        {
            public int index;
            public List<FEMesh> meshs;
        }
        private BuildMeshFromColliderRst m_BuildMeshFromColliderRst;
        private void BuildMeshFromCollider()
        {
            var cache = m_BuildMeshFromColliderRst;
            var idx = cache.index;
            var cnt = m_OverlapRst.staticColliders.Length;
            if (idx == cnt)
            {
                ++state;
                return;
            }
            if (cache.meshs == null)
            {
                cache.meshs = new List<FEMesh>(m_OverlapRst.staticColliders.Length);
            }
            var collider = m_OverlapRst.staticColliders[cache.index++];

            if (collider is MeshCollider mc)
            {
                var mesh = new FEMesh();
                mesh.LoadFrom(mc.sharedMesh, mc.transform.localToWorldMatrix);
                cache.meshs.Add(mesh);
            }
            else if (collider is SphereCollider sc)
            {
                var mesh = new FEMesh();
                MeshExtend.CreateSphere(mesh, sc.center, sc.radius, 20, 10);
                cache.meshs.Add(mesh);
            }
            else if (collider is BoxCollider bc)
            {
                Debug.LogError($"Not Support collider {collider}");
            }
            else if (collider is CapsuleCollider cc)
            {
                Debug.LogError($"Not Support collider {collider}");
            }
            else if (collider is TerrainCollider tc)
            {
                Debug.LogError($"Not Support collider {collider}");
            }
            else
            {
                Debug.LogError($"Not Support collider {collider}");
            }
        }
        private void OnComplete()
        {
            data.IsBaking = false;
        }
    }
}
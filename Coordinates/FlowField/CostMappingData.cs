using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.FlowField
{
    [CreateAssetMenu(fileName = "CostMappingData", menuName = "Kit/FlowField/CostMappingData")]
    public class CostMappingData : ScriptableObject
    {
        [Range(1,100)]public int Cost01 = 1;
        [Range(1,100)]public int Cost02 = 1;
        [Range(1,100)]public int Cost03 = 1;
        [Range(1,100)]public int Cost04 = 1;
        [Range(1,100)]public int Cost05 = 1;
        [Range(1,100)]public int Cost06 = 1;
        [Range(1,100)]public int Cost07 = 1;
        [Range(1,100)]public int Cost08 = 1;
        [Range(1,100)]public int Cost09 = 1;
        [Range(1,100)]public int Cost10 = 1;
        [Range(1,100)]public int Cost11 = 1;
        [Range(1,100)]public int Cost12 = 1;
        [Range(1,100)]public int Cost13 = 1;
        [Range(1,100)]public int Cost14 = 1;
        [Range(1,100)]public int Cost15 = 1;
        [Range(1,100)]public int Cost16 = 1;
        [Range(1,100)]public int Cost17 = 1;
        [Range(1,100)]public int Cost18 = 1;
        [Range(1,100)]public int Cost19 = 1;
        [Range(1,100)]public int Cost20 = 1;
        [Range(1,100)]public int Cost21 = 1;
        [Range(1,100)]public int Cost22 = 1;
        [Range(1,100)]public int Cost23 = 1;
        [Range(1,100)]public int Cost24 = 1;
        [Range(1,100)]public int Cost25 = 1;
        [Range(1,100)]public int Cost26 = 1;
        [Range(1,100)]public int Cost27 = 1;
        [Range(1,100)]public int Cost28 = 1;
        [Range(1,100)]public int Cost29 = 1;
        [Range(1,100)]public int Cost30 = 1;
        [Range(1,100)]public int Cost31 = 1;
        [Range(1,100)]public int Cost32 = 1;

        public int GetAreaCost(eCost mask)
        {
            int total = 0;
            if ((mask & eCost.Cost01) != 0) total += Cost01;
            if ((mask & eCost.Cost02) != 0) total += Cost02;
            if ((mask & eCost.Cost03) != 0) total += Cost03;
            if ((mask & eCost.Cost04) != 0) total += Cost04;
            if ((mask & eCost.Cost05) != 0) total += Cost05;
            if ((mask & eCost.Cost06) != 0) total += Cost06;
            if ((mask & eCost.Cost07) != 0) total += Cost07;
            if ((mask & eCost.Cost08) != 0) total += Cost08;

            if ((mask & eCost.Cost09) != 0) total += Cost09;
            if ((mask & eCost.Cost10) != 0) total += Cost10;
            if ((mask & eCost.Cost11) != 0) total += Cost11;
            if ((mask & eCost.Cost12) != 0) total += Cost12;
            if ((mask & eCost.Cost13) != 0) total += Cost13;
            if ((mask & eCost.Cost14) != 0) total += Cost14;
            if ((mask & eCost.Cost15) != 0) total += Cost15;
            if ((mask & eCost.Cost16) != 0) total += Cost16;

            if ((mask & eCost.Cost17) != 0) total += Cost17;
            if ((mask & eCost.Cost18) != 0) total += Cost18;
            if ((mask & eCost.Cost19) != 0) total += Cost19;
            if ((mask & eCost.Cost20) != 0) total += Cost20;
            if ((mask & eCost.Cost21) != 0) total += Cost21;
            if ((mask & eCost.Cost22) != 0) total += Cost22;
            if ((mask & eCost.Cost23) != 0) total += Cost23;
            if ((mask & eCost.Cost24) != 0) total += Cost24;

            if ((mask & eCost.Cost25) != 0) total += Cost25;
            if ((mask & eCost.Cost26) != 0) total += Cost26;
            if ((mask & eCost.Cost27) != 0) total += Cost27;
            if ((mask & eCost.Cost28) != 0) total += Cost28;
            if ((mask & eCost.Cost29) != 0) total += Cost29;
            if ((mask & eCost.Cost30) != 0) total += Cost30;
            if ((mask & eCost.Cost31) != 0) total += Cost31;
            if ((mask & eCost.Cost32) != 0) total += Cost32;
            return total;
        }

        public int GetAreaCost(FNode node)
        {
            if (node == null)
                throw new System.NullReferenceException();

            return GetAreaCost(node.cost);
        }
    }
}
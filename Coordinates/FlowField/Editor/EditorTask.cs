using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Kit.FlowField
{
    public class EditorTask : TaskWithState
    {
        private DateTime m_Start;
        private readonly kTask m_Task;
        private readonly System.Action m_CompletedCallback;
        public EditorTask(kTask task, System.Action completedCallback)
        {
            this.m_Start = DateTime.UtcNow;
            this.m_Task = task;
            this.m_CompletedCallback = completedCallback;
        }

        protected override void OnEnter()
        {
            m_Start = DateTime.UtcNow;
        }

        protected override bool ContinueOnNextCycle()
        {
            if (m_Task == null)
                return false;

            var cancel = EditorUtility.DisplayCancelableProgressBar("Baking", $"In progress", 0.5f);
            if (cancel)
            {
                if (m_Task is kTask t)
                    t.Abort();
                return false;
            }
            return m_Task.Execute();
        }

        protected override void OnComplete()
        {
            EditorUtility.ClearProgressBar();
            var ts = DateTime.UtcNow - m_Start;
            Debug.Log($"Editor task end, Total time : {ts.ToStringDetail()}");

            m_CompletedCallback?.TryCatchDispatchEventError(o => o?.Invoke());
        }
    }
}
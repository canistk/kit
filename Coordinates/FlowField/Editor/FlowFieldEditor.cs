using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.EditorCoroutines.Editor;

namespace Kit.FlowField
{
    [System.Obsolete("FlowFieldBaker obsolete")]
    [CustomEditor(typeof(FlowFieldBaker))]
    public class FlowFieldEditor : EditorWithSceneGUI
    {
        protected override void OnAfterDrawGUI()
        {
            base.OnAfterDrawGUI();
            using(new EditorGUI.DisabledGroupScope(flowField?.m_FlowFieldData?.IsBaking ?? false))
            {
                if (GUILayout.Button("Start Bake"))
                {
                    BakeSelectedFlowField();
                }
            }
            EditorGUILayout.Space();

            m_Destination   = (GameObject)EditorGUILayout.ObjectField(m_Destination, typeof(GameObject), allowSceneObjects: true);
            m_QuotaPerFrame = EditorGUILayout.IntSlider(m_QuotaPerFrame, 1, 1024);
            using (new EditorGUI.DisabledGroupScope(m_Destination == null))
            {
                if (GUILayout.Button("Vector Field"))
                {
                    CreateVectorField(m_Destination.transform.position, m_QuotaPerFrame);
                }
            }
            EditorGUILayout.Space();
        }

        private GameObject m_Destination;
        private int m_QuotaPerFrame = 1;

        private void BakeSelectedFlowField()
        {
            if (flowField == null)
                throw new System.NullReferenceException();

            // Create Scriptable
            if (flowField.m_FlowFieldData == null)
            {
                var path = EditorUtility.SaveFilePanelInProject("Save FlowField Data", "FlowFieldData", "asset", "Data to baked");
                if (string.IsNullOrEmpty(path))
                    return;
                var db = ScriptableObject.CreateInstance<FlowFieldData>();
                AssetDatabase.CreateAsset(db, path);
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                var loadDb = AssetDatabase.LoadAssetAtPath<FlowFieldData>(path);
                flowField.m_FlowFieldData = loadDb;
            }

            // ensure scriptableObject is ready.
            if (flowField.m_FlowFieldData != null)
            {
                FlowField.AddTask(
                    new EditorTask(
                        new BakeTask(flowField),
                        () =>
                        {
                            if (flowField.m_FlowFieldData != null)
                            {
                                var db = flowField.m_FlowFieldData;
                                EditorUtility.SetDirty(db);
                                AssetDatabase.SaveAssetIfDirty(db);
                                AssetDatabase.Refresh();
                            }
                        }
                    ));
            }
        }

        private void CreateVectorField(Vector3 destination, int stepPerFrame)
        {
            if (flowField == null)
                throw new System.NullReferenceException();
            
            if (flowField.m_FlowFieldData == null)
            {
                Debug.LogError("Require baked data to create vector field.");
                return;
            }

            FlowField.AddTask(m_VectorField = new VectorField(flowField, destination, stepPerFrame));
        }
        VectorField m_VectorField;

        #region Redirection
        private FlowFieldBaker flowField => (FlowFieldBaker)target;
        private FFSetting setting => flowField?.setting;
        private Transform transform => flowField?.transform;
        private GameObject gameObject => flowField?.gameObject;
        private Vector3 position => transform.position;
        private Quaternion rotation => transform.rotation;
        #endregion Redirection

        private float m_Pass = 0f;
        protected override void OnSceneGUI(SceneView view)
        {
            m_Pass += Time.unscaledDeltaTime;
            if (m_Pass >= 0.08f)
            {

                if (m_VectorField != null && m_VectorField.state == VectorField.eState.Completed)
                {
                    m_VectorField.DrawDebug(false);
                }

                view.Repaint();
                m_Pass = 0f;
            }
        }
    }
}
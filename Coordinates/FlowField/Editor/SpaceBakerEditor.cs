using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit.FlowField
{
    [CustomEditor(typeof(SpaceBaker))]
    public class SpaceBakerEditor : EditorWithSceneGUI
    {
        #region Redirection
        private SpaceBaker  baker       => (SpaceBaker)target;
        private SpaceData   spaceData   => baker?.m_Data;
        private Transform   transform   => baker?.transform;
        private GameObject  gameObject  => baker?.gameObject;
        private Vector3     position    => transform.position;
        private Quaternion  rotation    => transform.rotation;
        #endregion Redirection

        protected override void OnAfterDrawGUI()
        {
            base.OnAfterDrawGUI();
            using (new EditorGUI.DisabledGroupScope(baker?.m_Data?.IsBaking ?? false))
            {
                if (GUILayout.Button("Start Bake"))
                {
                    BakeSelectedSpace();
                }
            }
            EditorGUILayout.Space();
        }
        protected override void OnSceneGUI(SceneView view)
        {
        }

        private void BakeSelectedSpace()
        {
            if (baker == null)
                throw new System.NullReferenceException();

            if (baker.m_Data == null)
            {
                baker.m_Data = SaveScriptableObjectInProject<SpaceData>("Save area data", nameof(SpaceData), "asset", "Save area data in project.");
            }

            if (baker.m_Data == null)
            {
                Debug.LogError($"Invalid : {nameof(SpaceData)} not found.");
                return;
            }

            EditorTaskHandler.Add(new EditorTask(
                new SpaceBakerTask(baker, spaceData),
                OnBakeCompleted));
        }

        private void OnBakeCompleted()
        {

        }

        private T SaveScriptableObjectInProject<T>(string title, string defaultName, string extension, string message, string path = "Assets")
            where T : ScriptableObject
        {
            // path = EditorUtility.SaveFilePanelInProject("Save FlowField Data", "FlowFieldData", "asset", "Data to baked");
            path = EditorUtility.SaveFilePanelInProject(title, defaultName, extension, message, path);
            if (string.IsNullOrEmpty(path))
                return null;
            var db = ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(db, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            var loadDb = AssetDatabase.LoadAssetAtPath<T>(path);
            return loadDb;
        }
    }
}
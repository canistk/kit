using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.FlowField
{
    public interface ISpaceCost
    {
        public eCost GetCost();
    }

    [System.Flags]
    public enum eCost : uint
    {
        Empty  = (uint)0b0000_0000_0000_0000_0000_0000_0000_0000,
        Cost01 = (uint)0b0000_0000_0000_0000_0000_0000_0000_0001,
        Cost02 = (uint)0b0000_0000_0000_0000_0000_0000_0000_0010,
        Cost03 = (uint)0b0000_0000_0000_0000_0000_0000_0000_0100,
        Cost04 = (uint)0b0000_0000_0000_0000_0000_0000_0000_1000,

        Cost05 = (uint)0b0000_0000_0000_0000_0000_0000_0001_0000,
        Cost06 = (uint)0b0000_0000_0000_0000_0000_0000_0010_0000,
        Cost07 = (uint)0b0000_0000_0000_0000_0000_0000_0100_0000,
        Cost08 = (uint)0b0000_0000_0000_0000_0000_0000_1000_0000,

        Cost09 = (uint)0b0000_0000_0000_0000_0000_0001_0000_0000,
        Cost10 = (uint)0b0000_0000_0000_0000_0000_0010_0000_0000,
        Cost11 = (uint)0b0000_0000_0000_0000_0000_0100_0000_0000,
        Cost12 = (uint)0b0000_0000_0000_0000_0000_1000_0000_0000,
        
        Cost13 = (uint)0b0000_0000_0000_0000_0001_0000_0000_0000,
        Cost14 = (uint)0b0000_0000_0000_0000_0010_0000_0000_0000,
        Cost15 = (uint)0b0000_0000_0000_0000_0100_0000_0000_0000,
        Cost16 = (uint)0b0000_0000_0000_0000_1000_0000_0000_0000,
        
        Cost17 = (uint)0b0000_0000_0000_0001_0000_0000_0000_0000,
        Cost18 = (uint)0b0000_0000_0000_0010_0000_0000_0000_0000,
        Cost19 = (uint)0b0000_0000_0000_0100_0000_0000_0000_0000,
        Cost20 = (uint)0b0000_0000_0000_1000_0000_0000_0000_0000,

        Cost21 = (uint)0b0000_0000_0001_0000_0000_0000_0000_0000,
        Cost22 = (uint)0b0000_0000_0010_0000_0000_0000_0000_0000,
        Cost23 = (uint)0b0000_0000_0100_0000_0000_0000_0000_0000,
        Cost24 = (uint)0b0000_0000_1000_0000_0000_0000_0000_0000,

        Cost25 = (uint)0b0000_0001_0000_0000_0000_0000_0000_0000,
        Cost26 = (uint)0b0000_0010_0000_0000_0000_0000_0000_0000,
        Cost27 = (uint)0b0000_0100_0000_0000_0000_0000_0000_0000,
        Cost28 = (uint)0b0000_1000_0000_0000_0000_0000_0000_0000,

        Cost29 = (uint)0b0001_0000_0000_0000_0000_0000_0000_0000,
        Cost30 = (uint)0b0010_0000_0000_0000_0000_0000_0000_0000,
        Cost31 = (uint)0b0100_0000_0000_0000_0000_0000_0000_0000,
        Cost32 = (uint)0b1000_0000_0000_0000_0000_0000_0000_0000,

        Obstacle = (uint)0b1111_1111_1111_1111_1111_1111_1111_1111,
    }

    [RequireComponent(typeof(Collider))]
    public class SpaceCost : MonoBehaviour, ISpaceCost
    {
        [SerializeField] eCost m_Cost = 0;
        eCost ISpaceCost.GetCost() => m_Cost;

        private void OnValidate()
        {
            var c = GetComponent<Collider>();
            if (c == null)
                throw new System.NullReferenceException();

            if (!c.gameObject.isStatic)
            {
                c.gameObject.isStatic = true;
                Debug.LogWarning($"Require Collider(static) on <{nameof(SpaceCost)}>", this);
            }

            if (!c.isTrigger)
            {
                c.isTrigger = true;
                Debug.LogWarning($"Require Collider(Trigger) on <{nameof(SpaceCost)}>", this);
            }
        }
    }
}
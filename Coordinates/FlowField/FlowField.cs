using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.LowLevel;

#if UNITY_EDITOR
using UnityEditor;
using Unity.EditorCoroutines.Editor;
#endif
namespace Kit.FlowField
{
    [System.Obsolete("Use Kit.TaskHandle & Kit.EditorTaskHandler instead.")]
    public static class FlowField
    {
        public static void AddTask(kTask task)
        {
            if (Application.isPlaying)
            {
                AddTaskRuntime(task);
            }
            else
            {
#if UNITY_EDITOR
                AddTaskEditor(task);
#endif
            }
        }

        public static void TaskClearUp()
        {
            if (Application.isPlaying)
            {
                RuntimeCleanUp();
            }
            else
            {
#if UNITY_EDITOR
                EditorTaskCleanUp();
#endif
            }
        }

        #region Runtime
        [RuntimeInitializeOnLoadMethod]
        private static void RuntimeInit()
        {
            /// Easy implementation idea :
            /// <see cref="https://forum.unity.com/threads/how-to-make-static-update-method.1326297/"/>
            /// more detail for this method.
            /// <see cref="https://medium.com/@thebeardphantom/unity-2018-and-playerloop-5c46a12a677"/>
            var root  = PlayerLoop.GetCurrentPlayerLoop();
            root.subSystemList[5].updateDelegate += RuntimeUpdate;
            PlayerLoop.SetPlayerLoop(root);
            s_UpdateInfo = new UpdateInfo();
        }

        private class UpdateInfo
        {
            public Queue<kTask> queue = new Queue<kTask>(8);
            public List<kTask> parallel = new List<kTask>(8);
        }
        private static UpdateInfo s_UpdateInfo;
        private static void RuntimeUpdate()
        {
            if (s_UpdateInfo.parallel != null && s_UpdateInfo.parallel.Count > 0)
            {
                int i = s_UpdateInfo.parallel.Count;
                while (i-- > 0)
                {
                    if (s_UpdateInfo.parallel.Count <= i)
                        break; // clean up, or modify by others.
                    var task = s_UpdateInfo.parallel[i];
                    if (!task.Execute())
                    {
                        if (task is kTask td)
                            td.Dispose();
                        s_UpdateInfo.parallel.RemoveAt(i);
                    }
                }
            }
            if (s_UpdateInfo.queue != null && s_UpdateInfo.queue.Count > 0)
            {
                var task = s_UpdateInfo.queue.Peek();
                if (!task.Execute())
                {
                    // During execute, queueTask may change or deleted.
                    if (s_UpdateInfo.queue.Count > 0 && task == s_UpdateInfo.queue.Peek())
                    {
                        if (task is kTask td)
                            td.Dispose();
                        s_UpdateInfo.queue.Dequeue();
                    }
                }
            }
        }
        private static void AddTaskRuntime(kTask task)
        {
            s_UpdateInfo.queue.Enqueue(task);
            // TODO: ?
            // s_UpdateInfo.parallel.Add(task);
        }
        private static void RuntimeCleanUp()
        {
            s_UpdateInfo = new UpdateInfo();
        }
        #endregion Runtime

#if UNITY_EDITOR
        
        private static Queue<kTask> s_EditorQueue = new Queue<kTask>();
        private static EditorCoroutine s_EditorProgress = null;
        private static IEnumerator EditorLoop()
        {
            if (EditorApplication.isPlayingOrWillChangePlaymode)
            {
                EditorTaskCleanUp();
                yield break;
            }

            while (s_EditorQueue.Count > 0)
            {
                var _task = s_EditorQueue.Peek();
                try
                {
                    if (!_task.Execute())
                    {
                        s_EditorQueue.Dequeue();
                        _task = null;
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    if (s_EditorQueue.Count > 0)
                    {
                        // remove task when throw exception.
                        s_EditorQueue.Dequeue();
                    }
                }
                yield return null;
            }

            // Clean up handling.
            EditorCoroutineUtility.StopCoroutine(s_EditorProgress);
            s_EditorProgress = null;
        }

        private static void AddTaskEditor(kTask task)
        {
            if (s_EditorProgress == null)
                s_EditorProgress = EditorCoroutineUtility.StartCoroutineOwnerless(EditorLoop());
            s_EditorQueue.Enqueue(task);
        }
        private static void EditorTaskCleanUp()
        {
            s_EditorQueue.Clear();
            EditorUtility.ClearProgressBar();
            if (s_EditorProgress != null)
                EditorCoroutineUtility.StopCoroutine(s_EditorProgress);
            s_EditorProgress = null;
        }
#endif
    }
}
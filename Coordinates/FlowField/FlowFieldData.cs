using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.FlowField
{
    [System.Obsolete("Use SpaceData instead.")]
    [CreateAssetMenu(fileName = "FlowFieldData", menuName = "Kit/FlowField/FlowFieldData")]
    public class FlowFieldData : ScriptableObject
    {
        public bool IsBaking;
        public FNode rootNode;
    }
}
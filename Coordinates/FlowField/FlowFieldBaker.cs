#define SIZE_OPTIMIZE
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.FlowField
{
    [System.Obsolete("Use SpaceBaker, octree too slow.")]
    public class FlowFieldBaker : MonoBehaviour
    {
        public FlowFieldData m_FlowFieldData = null;
        [SerializeField] FFSetting m_Setting = new FFSetting();
        public FFSetting setting => m_Setting;

        [SerializeField] CostMappingData m_CostMapping = null;
        internal CostMappingData costMapping => m_CostMapping;

        private void OnDrawGizmos()
        {
            //if (IsBaking)
            //    return;

            if (rootNode == null)
                return;
            rootNode.DrawGizmos(setting);
            var b = new Bounds(transform.position, Vector3.one * setting.initialWorldSize);
            GizmosExtend.DrawBounds(b, setting.boundaryColor);

            var h = rootNode.halfExtents;
            GizmosExtend.DrawWireCube(transform.position, new Vector3(h, h, h), Quaternion.identity, setting.boundaryColor);
        }

        public FNode rootNode
        {
            get
            {
                if (m_FlowFieldData == null)
                    return null;

                if (m_FlowFieldData.rootNode == null)
                    m_FlowFieldData.rootNode = new FNode(transform.position, setting.initialWorldSize);
                return m_FlowFieldData.rootNode;
            }
        }

        public bool TryGetLeafNode(Vector3 point, out FNode node)
        {
            if (m_FlowFieldData == null || m_FlowFieldData.IsBaking)
            {
                node = null;
                return false;
            }
            if (rootNode == null)
            {
                node = null;
                return false;
            }
            return rootNode.TryGetContains(point, out node);
        }
        public IEnumerable<FNode> GetIntersect(Bounds other)
        {
            if (rootNode == null || m_FlowFieldData.IsBaking)
                yield break;

            foreach (var c in rootNode.GetIntersect(other))
                yield return c;
        }
    }

    [System.Flags]
    public enum eGizmos
    {
        Grids       = 1 << 1,
        Contents    = 1 << 2,
    }

    [System.Serializable]
    public class FFSetting
    {
        [Tooltip("Size of the sides of the initial node, in metres. The octree will never shrink smaller than this.")]
        [Min(0.1f)] public float initialWorldSize = 10f;

        [Tooltip("Nodes will stop splitting if the new nodes would be smaller than this (metres).")]
        [Min(0.1f)] public float minNodeSize = 1f;

        [Min(1)] public int splitQuota = 128;

        [Tooltip("The error to avoid engine detect the collided neighbor node(s) during map baking.")]
        [Range(1E-5f, 0.1f)] public float errorThreshold = 0.5f;

        [Min(5)] public int bakeBuffer = 20;

        public LayerMask layerMask = Physics.DefaultRaycastLayers;

        [Header("Gizmos")]
        public eGizmos gizmos = (eGizmos)0;
        public Gradient SizeColor = new Gradient();
        public Color boundaryColor = Color.white.CloneAlpha(0.5f);
    }

    [System.Serializable]
    public class FNode
    {
        public FNode(Vector3 center, float size)
        {
#if SIZE_OPTIMIZE
            this.bytes = new byte[5 * f32];
#endif
            this.center = center;
            this.size = size;
            this.cost = eCost.Empty;
        }


        #region Variables
#if SIZE_OPTIMIZE
        private static int f32 = sizeof(float);
        /// <summary>Optimize data cache for this node, reduce file size.</summary>
        public                  byte[]          bytes;
        /// <summary>World position of this node.</summary>
        public                  Vector3         center
        {
            get
            {
                return new Vector3(
                    BitConverter.ToSingle(new ReadOnlySpan<byte>(bytes, 0 * f32, f32)),
                    BitConverter.ToSingle(new ReadOnlySpan<byte>(bytes, 1 * f32, f32)),
                    BitConverter.ToSingle(new ReadOnlySpan<byte>(bytes, 2 * f32, f32)));
            }
            private set
            {
                Buffer.BlockCopy(BitConverter.GetBytes(value.x), 0, bytes, 0 * f32, f32);
                Buffer.BlockCopy(BitConverter.GetBytes(value.y), 0, bytes, 1 * f32, f32);
                Buffer.BlockCopy(BitConverter.GetBytes(value.z), 0, bytes, 2 * f32, f32);
            }
        }
        /// <summary>World Size of this node</summary>
        public                  float           size        
        {
            get => BitConverter.ToSingle(new ReadOnlySpan<byte>(bytes,      3 * f32, f32));
            private set => Buffer.BlockCopy(BitConverter.GetBytes(value), 0, bytes, 3 * f32, f32);
        }
        private static int i32 = sizeof(uint);
        /// <summary>
        /// Cost for baked terrain
        /// </summary>
        public                  eCost          cost
        {
            get => (eCost)BitConverter.ToUInt32(new ReadOnlySpan<byte>(bytes,       4 * f32, i32));
            private set => Buffer.BlockCopy(BitConverter.GetBytes((uint)value), 0, bytes,   4 * f32, i32);
        }
#else
        public                  Vector3         center;
        public                  float           size;
        public                  UInt32          cost;
#endif
        public                  float           halfExtents => size * 0.5f;
        public                  bool            IsLeafNode  => childrens == null || childrens.Length == 0;
        public                  FNode[]         childrens   = null;
        private                 KeyValuePair<bool, Bounds> m_Bounds;
        public                  Bounds          bounds
        {
            get
            {
                // runtime cache
                if (!m_Bounds.Key)
                {
                    m_Bounds = new (true, new Bounds(center, new Vector3(size, size, size)));
                }
                return m_Bounds.Value;
            }
        }
        #endregion Variables

        #region Bake Task(s)
        /// <summary>Detect world terrain</summary>
        /// <param name="bakeTask"></param>
        internal void Detect(in BakeTask bakeTask)
        {
            bakeTask.Request(this, new FNodeDetectTask(bakeTask, this, _OnTaskCompleted));
            void _OnTaskCompleted(eCost cost)
            {
                this.cost = cost;
            }
        }
        /// <summary>Split this node into 8 octree children(s) nodes</summary>
        /// <param name="bakeTask"></param>
        /// <exception cref="System.Exception"></exception>
        internal void SplitNodes(in BakeTask bakeTask)
        {
            if (!IsLeafNode)
                throw new System.Exception("Rejected, already splitted.");

            var q   = size * 0.25f;
            var h   = size * 0.5f;
            if (h < bakeTask.flowField.setting.minNodeSize)
            {
                throw new System.Exception("Rejected, minNodeSize reach.");
            }

            childrens = new FNode[8]
            {
                new FNode(center + new Vector3(-q, -q, -q), h), // LDF
                new FNode(center + new Vector3( q, -q, -q), h), // RDF
                new FNode(center + new Vector3(-q, -q,  q), h), // LDB
                new FNode(center + new Vector3( q, -q,  q), h), // RDB
                new FNode(center + new Vector3(-q,  q, -q), h), // LUF
                new FNode(center + new Vector3( q,  q, -q), h), // RUF
                new FNode(center + new Vector3(-q,  q,  q), h), // LUB
                new FNode(center + new Vector3( q,  q,  q), h), // RUB
            };

            // don't clean up
            cost = eCost.Empty;
        }
        #endregion Bake Task(s)

        internal void DrawGizmos(in FFSetting setting)
        {
            if (IsLeafNode)
            {
                var drawGrids = (setting.gizmos & eGizmos.Grids) != 0;
                var drawContent = (setting.gizmos & eGizmos.Contents) != 0;
                var pt = FloatExtend.InvLerp(setting.minNodeSize, setting.initialWorldSize, size);
                var sColor = setting.SizeColor.Evaluate(pt);
                var h = bounds.size * 0.5f;
                var r = Quaternion.identity;
                if (cost != eCost.Empty)
                {
                    if (drawContent)
                        GizmosExtend.DrawCube(center, h, r, sColor);
                    else if(drawGrids)
                        GizmosExtend.DrawWireCube(center, h, r, sColor);
                }
                else
                {
                    if (drawGrids)
                        GizmosExtend.DrawWireCube(center, h, r, sColor);
                }
            }
            else
            {
                for (int i = 0; i < childrens.Length; ++i)
                {
                    childrens[i].DrawGizmos(setting);
                }
            }
        }

        public Bounds[] GetChildrenBounds()
        {
            var q       = this.size * 0.25f;
            var h       = this.size * 0.5f;
            var size    = new Vector3(h,h,h);
            /// <see cref="eZOrderCurve"/>
            /// <see cref="https://en.wikipedia.org/wiki/Z-order_curve"/>
            return new Bounds[]
            {
                new Bounds(center + new Vector3(-q, -q, -q), size), // LDF
                new Bounds(center + new Vector3( q, -q, -q), size), // RDF
                new Bounds(center + new Vector3(-q, -q,  q), size), // LDB
                new Bounds(center + new Vector3( q, -q,  q), size), // RDB
                new Bounds(center + new Vector3(-q,  q, -q), size), // LUF
                new Bounds(center + new Vector3( q,  q, -q), size), // RUF
                new Bounds(center + new Vector3(-q,  q,  q), size), // LUB
                new Bounds(center + new Vector3( q,  q,  q), size), // RUB
            };
        }

        public bool IsContains(Vector3 point) => bounds.Contains(point);
        public bool TryGetContains(Vector3 point, out FNode node)
        {
            if (!bounds.Contains(point))
            {
                node = null;
                return false;
            }
            else if (IsLeafNode)
            {
                node = this;
                return true;
            }

            for (int i = 0; i < 8; ++i)
            {
                if (childrens[i].TryGetContains(point, out node))
                    return true;
            }

            node = null;
            return false;
        }

        public bool IsIntersect(Bounds other) => bounds.Intersects(other);
        public IEnumerable<FNode> GetIntersect(Bounds other)
        {
            if (!bounds.Intersects(other))
                yield break;
            
            // had intersect
            if (IsLeafNode)
            {
                yield return this;
            }
            else
            {
                for (int i = 0; i < 8; ++i)
                {
                    foreach (var c in childrens[i].GetIntersect(other))
                        yield return c;
                }
            }
        }

        /// <summary>Locate the neighbor node(s), near by this node.</summary>
        /// <param name="neighbor">the direction or edge which collided with this.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public IEnumerable<FNode> GetNeighbor(FlowFieldBaker flowField, eNeighbor neighbor, float err = 1E-5f)
        {
            // DebugExtend.DrawBounds(bounds, Color.red, 3f, false);
            if (flowField.rootNode == this)
                yield break;

            var setting = flowField.setting;

            // [Err] to void the unity physics engine return overlap bounds,
            // narrow down the size of detection bound instead.
            err = Mathf.Max(1E-5f, err);
            var co = setting.minNodeSize * 0.5f; // center-offset
            var hl = setting.minNodeSize - err;
            var l = size - err;
            var h = halfExtents;

            // Create a bounds(bait) in order to found all intersect nodes with it.
            // octree itself is a quick method to search for that.
            Bounds minBounds = neighbor switch
            {
                eNeighbor.Front => new Bounds(center + new Vector3(0, 0,  h + co), new Vector3(l, l, hl)),
                eNeighbor.Back  => new Bounds(center + new Vector3(0, 0, -h - co), new Vector3(l, l, hl)),
                eNeighbor.Left  => new Bounds(center + new Vector3(-h - co, 0 ,0), new Vector3(hl, l, l)),
                eNeighbor.Right => new Bounds(center + new Vector3( h + co, 0, 0), new Vector3(hl, l, l)),
                eNeighbor.Up    => new Bounds(center + new Vector3(0,  h + co, 0), new Vector3(l, hl, l)),
                eNeighbor.Down  => new Bounds(center + new Vector3(0, -h - co, 0), new Vector3(l, hl, l)),
                eNeighbor.All   => new Bounds(center, new Vector3(l + err, l + err, l + err)),
                _ => throw new System.NotImplementedException(),
            };

            // DebugExtend.DrawBounds(minBounds, Color.yellow, 3f, false);
            foreach (var c in flowField.GetIntersect(minBounds))
            {
                if (c == this)
                    continue;

                // DebugExtend.DrawBounds(c.bounds, Color.cyan, 3f, false);
                yield return c;
            }
        }

        public int GetLeafCount()
        {
            if (IsLeafNode)
                return 1;

            int sum = 0;
            for (int i = 0; i < 8; ++i)
            {
                sum += childrens[i].GetLeafCount();
            }
            return sum;
        }
    }

    public enum eNeighbor
    {
        Front,
        Back,
        Left,
        Right,
        Up,
        Down,

        All,
    }

    /// <summary>
    /// Z-Order curve represent in Octree format
    /// <see cref="https://en.wikipedia.org/wiki/Z-order_curve#/media/File:Lebesgue-3d-step2.png"/>
    /// </summary>
    public enum eZOrderCurve
    {
        LDF = 0,
        RDF,
        LDB,
        RDB,
        LUF,
        RUF,
        LUB,
        RUB,
    }

}
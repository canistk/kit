using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Stopwatch = System.Diagnostics.Stopwatch;
namespace Kit.FlowField
{
    /// <summary>
    /// <see cref="http://idm-lab.org/bib/abstracts/papers/aaai07a.pdf"/>
    /// <see cref="https://arxiv.org/ftp/arxiv/papers/1401/1401.3843.pdf"/>
    /// 
    /// abandon the idea,
    /// reason :
    /// use <see cref="https://www.youtube.com/watch?v=7MOQOvGJyn4"/>
    /// "Theta Any-angle" on "polyana" the navigation meshes.
    /// </summary>
    [System.Obsolete("Not a good idea to do this in Octree tree,")]
    public class ThetaAnyAngle  : kTask
    {
        public  readonly    FNode           fromNode;
        public  readonly    FNode           toNode;
        public  readonly    FlowFieldBaker  flowField;
        private             FFSetting       setting => flowField.setting;
        private readonly    int             stepPerFrame;

        public ThetaAnyAngle(FlowFieldBaker flowField, Vector3 start, Vector3 destination, int stepPerFrame)
        {
            if (!flowField.TryGetLeafNode(start, out fromNode))
            {
                // throw new System.ArgumentException($"Flowfield not include target destination {destination:F2}");
                Debug.LogWarning($"Flowfield not include target destination {destination:F2}");
            }

            if (!flowField.TryGetLeafNode(destination, out toNode))
            {
                // throw new System.ArgumentException($"Flowfield not include target destination {destination:F2}");
                Debug.LogWarning($"Flowfield not include target destination {destination:F2}");
            }
            this.flowField = flowField;
            this.stepPerFrame = stepPerFrame;
        }

        public ThetaAnyAngle(FlowFieldBaker flowField, FNode fromNode, FNode toNode, int stepPerFrame)
        {
            this.fromNode = fromNode;
            this.toNode = toNode;
            this.flowField = flowField;
            this.stepPerFrame = stepPerFrame;
        }

        public enum eState
        {
            Idle,

            Completed,
        }
        public eState state { get; private set; } = eState.Idle;

        private class ThetaNode : IComparable<ThetaNode>
        {
            // G Cost = distance from starting node.
            // H Cost = distance from end node. (heuristic)
            // F Cost = G + H Cost
            public float G;
            public float H;
            public float F => G + H;
            private readonly FlowFieldBaker flowField;
            public readonly FNode node;
            public ThetaNode(FlowFieldBaker flowField, FNode node)
            {
                this.flowField = flowField;
                this.node = node;
            }

            public int GetOrgCost()
            {
                return flowField.costMapping.GetAreaCost(node);
            }


            public ThetaNode parent { get; private set; }

            private int m_AccumulateCost;
            private bool m_CostDirty = false;
            public int GetAccumulateCost()
            {
                if (parent == null)
                    return 0; // target node

                if (m_CostDirty)
                {
                    // recursive
                    m_CostDirty = false;
                    var y = parent.GetAccumulateCost();
                    var x = Mathf.Max(1, GetOrgCost());
                    m_AccumulateCost = x + y;
                }
                return m_AccumulateCost;
            }

            public int CompareTo(ThetaNode other)
            {
                throw new NotImplementedException();
            }
        }

        protected override bool InternalExecute()
        {
            if (fromNode == null || toNode == null)
            {
                Debug.LogError("Invalid coordinate.");
                return false;
            }

            bool keepRun = state switch
            {
                //eState.Idle => OnEnter(),
                //eState.PrepareHeatmap => PrepareHeatmap(),
                //eState.Heatmap => CreatingHeatmap(),
                //eState.EndHeatmap => EndHeatmap(),
                //eState.VectorField => CreateVectorField(),
                //eState.Completed => OnComplete(),
                _ => throw new System.NotImplementedException()
            };

            if (!keepRun)
            {
                ++state;
            }
            return state < eState.Completed;
        }

    }
}
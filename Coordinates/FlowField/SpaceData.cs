using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.FlowField
{
    [CreateAssetMenu(fileName = "SpaceData", menuName = "Kit/FlowField/SpaceData")]
    public class SpaceData : ScriptableObject
    {
        public bool IsBaking;

    }
}
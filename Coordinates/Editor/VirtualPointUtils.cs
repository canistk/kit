﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Kit;

public static class VirtualPointUtils
{
	private static VirtualPoint m_SelectedPoint;
	public static VirtualPoint selectedPoint
	{
		get => m_SelectedPoint;
		set
		{
			m_SelectedPoint = value;
			// if (m_SelectedPoint != null) Debug.LogWarning($"Select : {m_SelectedPoint.position:F1}");
		}
	}

	public const float s_VPSize = .3f;
	public const float s_VPPickSize = .35f;
	private static Color s_White50 = Color.white.CloneAlpha(0.5f);

	public static bool DrawVirtualPoint(int controlId, string suffix, VirtualPoint vp, Callback beforeChange = null)
	{
		if (vp == null)
			return false;
		else if (vp == selectedPoint)
		{
			DrawSelectedVirtualPoint(controlId, suffix, vp, out bool changed, beforeChange);
			return changed;
		}
		else
		{
			DrawSceneViewSelection(controlId, suffix, vp, Event.current.type);
			return false;
		}
	}

	private static void DrawSceneViewSelection(int controlId, string suffix, VirtualPoint vp, EventType eventType)
	{
		float hs = HandleUtility.GetHandleSize(vp.position);
		Handles.color = Color.white;
		if (eventType == EventType.Repaint)
		{
			Handles.SphereHandleCap(0, vp.position, vp.rotation, hs * s_VPSize, EventType.Repaint);
		}
		else
		{
			if (HandlesExtend.Button(controlId,
				vp.position, vp.rotation, hs * s_VPSize, hs * s_VPPickSize, true,
				Handles.SphereHandleCap, Color.red, out bool isHovering))
			{
				selectedPoint = vp;
			}
		}
	}

	private static void DrawSelectedVirtualPoint(int controlId,string suffix, VirtualPoint vp, out bool changed, Callback beforeChange = null)
	{
		Color old = Handles.color;
		float hs = HandleUtility.GetHandleSize(vp.position);
		changed = false;
		if (vp is TangentPoint tp)
		{
			changed |= TangentPointUtils.DrawSelectedTangentPoint(hs, suffix, tp, beforeChange);
		}
		else
		{
			if (Event.current.type == EventType.Repaint)
			{
				Handles.color = s_White50;
				Handles.SphereHandleCap(0, vp.position, vp.rotation, hs * s_VPSize, EventType.Repaint);
			}
		}

		switch (Tools.current)
		{
			case Tool.Move:
				if (HandlesExtend.PositionHandle(
					"Move_" + suffix,
					vp.position,
					vp.rotation,
					out Vector3 pos))
				{
					if (vp.position != pos)
					{
						changed |= true;
						beforeChange?.Invoke();
						vp.position = pos;
					}
				}
				break;
			case Tool.Rotate:
				
				if (vp is TangentPoint tp0)
				{
					TangentPointUtils.DrawTangentRotationControl(suffix, tp0, beforeChange);
				}
				else
				{
					if (HandlesExtend.RotationHandle(
						"Rotate_" + suffix,
						vp.position,
						vp.rotation,
						out Quaternion rotation))
					{
						if (vp.rotation != rotation)
						{
							changed |= true;
							beforeChange?.Invoke();
							vp.rotation = rotation;
						}
					}
				}
				break;
		}
		
		if (changed)
			vp.Ping();
		Handles.color = old;
	}
}
public static class TangentPointUtils // Depend on VirtualPointUtils
{
	private static /* readonly */ Vector3 s_RotateSnap = new Vector3(0.25f, 0.25f, 0.25f);
	private static Color s_White50 = Color.white.CloneAlpha(0.5f);
	private static Color s_Gray50 = Color.gray.CloneAlpha(0.5f);
	public static void DrawTangentRotationControl(string suffix, TangentPoint tp, Callback beforeChange)
	{
		if (Tools.pivotRotation == PivotRotation.Global)
		{
			if (HandlesExtend.RotationHandle(
							"Rotate_" + suffix,
							tp.position,
							tp.rotation,
							out Quaternion rotation))
			{
				beforeChange?.Invoke();
				tp.rotation = rotation;
			}
		}
		else
		{
			if (Event.current.shift)
			{
				if (HandlesExtend.RotationHandle(
								"Rotate_" + suffix,
								tp.position,
								tp.rotation,
								out Quaternion rotation))
				{
					beforeChange?.Invoke();
					Quaternion diff = tp.rotation.Inverse() * rotation;
					tp.tangentRotation *= diff.Inverse();
					tp.rotation = rotation;
				}
			}
			else
			{
				if (HandlesExtend.RotationHandle(
							"Rotate_" + suffix,
							tp.position,
							tp.rotation * tp.tangentRotation,
							out Quaternion modify))
				{
					beforeChange?.Invoke();
					tp.tangentRotation = tp.rotation.Inverse() * modify; // resolve relative rotaion 
				}
			}
		}
	}

	public static bool DrawSelectedTangentPoint(float hs, string suffix, TangentPoint tp, Callback beforeChange = null)
	{
		const float headingSize = 0.3f;
		float coneSize = hs * headingSize;
		Vector3 conePos = tp.rotation * new Vector3(0f, 0f, coneSize) + tp.position;
		if (Event.current.type == EventType.Repaint)
		{
			if (Tools.pivotRotation == PivotRotation.Global)
			{
				Handles.color = Color.yellow;
				Handles.DrawDottedLines(new Vector3[] { tp.inTangentPoint, tp.position, tp.position, tp.outTangentPoint }, 1f);
			}
			else
			{
				if (Event.current.shift)
				{
					Handles.color = Color.gray;
				}
				else
				{
					Handles.color = Color.yellow;
				}
				Handles.DrawDottedLines(new Vector3[] { tp.inTangentPoint, tp.position, tp.position, tp.outTangentPoint }, 1f);
			}

			if (Tools.pivotRotation == PivotRotation.Local &&
				Event.current.shift)
			{
				Handles.color = s_White50;
			}
			else
			{
				Handles.color = Tools.pivotRotation == PivotRotation.Local ? s_Gray50 : s_White50;
			}

			Handles.SphereHandleCap(0, tp.position, tp.rotation, hs * headingSize, EventType.Repaint);
			Handles.ConeHandleCap(0, conePos, tp.rotation, coneSize, EventType.Repaint);

		}
		bool gui_Changed = false;

		if (Tools.pivotMode == PivotMode.Pivot)
		{
			Quaternion rot = Tools.pivotRotation == PivotRotation.Global ?
				tp.rotation :
				tp.rotation * tp.tangentRotation;
			if (tp.localInTangentPoint != Vector3.zero &&
				HandlesExtend.PositionHandle(
						"Tangent_in_" + suffix,
						tp.inTangentPoint,
						rot,
						out Vector3 inTangent))
			{
				if (tp.inTangentPoint != inTangent)
				{
					gui_Changed = true;
					beforeChange?.Invoke();
					tp.inTangentPoint = inTangent;
				}
			}

			if (tp.localOutTangentPoint != Vector3.zero && 
				HandlesExtend.PositionHandle(
						"Tangent_out_" + suffix,
						tp.outTangentPoint,
						rot,
						out Vector3 outTangent))
			{
				if (tp.outTangentPoint != outTangent)
				{
					gui_Changed = true;
					beforeChange?.Invoke();
					tp.outTangentPoint = outTangent;
				}
			}
			
		}
		else // if (Tools.pivotMode == PivotMode.Center)
		{
			if (Tools.pivotRotation == PivotRotation.Local)
			{
				// Color for Handles.SphereHandleCap
				Handles.color = Event.current.shift ? s_Gray50 : s_White50;

				if (tp.localInTangentPoint != Vector3.zero && 
					HandlesExtend.FreeMoveHandle(
						"Tangent_in_" + suffix,
						tp.inTangentPoint,
						tp.tangentRotation,
						hs * 0.2f,
						s_RotateSnap,
						Handles.SphereHandleCap,
						out Vector3 inTangent))
				{
					if (tp.inTangentPoint != inTangent)
					{
						gui_Changed = true;
						beforeChange?.Invoke();
						tp.inTangentPoint = inTangent;
					}
				}
				if (tp.localOutTangentPoint != Vector3.zero && 
					HandlesExtend.FreeMoveHandle(
						"Tangent_out_" + suffix,
						tp.outTangentPoint,
						tp.tangentRotation,
						hs * 0.2f,
						s_RotateSnap,
						Handles.SphereHandleCap,
						out Vector3 outTangent))
				{
					if (tp.outTangentPoint != outTangent)
					{
						gui_Changed = true;
						beforeChange?.Invoke();
						tp.outTangentPoint = outTangent;
					}
				}
			}
			else
			{
				Handles.color = s_Gray50;
				Handles.SphereHandleCap(0, tp.inTangentPoint, Quaternion.identity, hs * 0.15f, EventType.Repaint);
				Handles.SphereHandleCap(0, tp.outTangentPoint, Quaternion.identity, hs * 0.15f, EventType.Repaint);
			}
		}
		return gui_Changed;
	}
}
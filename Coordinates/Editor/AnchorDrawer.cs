using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Kit.Coordinates
{
    // [CustomPropertyDrawer(typeof(Anchor), false)]
    public class AnchorDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            using (new EditorGUI.PropertyScope(position, label, property))
            {
                var posProp = property.FindPropertyRelative("m_LocalPosition");
                var rotProp = property.FindPropertyRelative("m_LocalRotation");
                var ptProp = property.FindPropertyRelative("m_Parent");
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var line = position.Clone(height: EditorGUIUtility.singleLineHeight);
                    var parent = EditorGUI.ObjectField(line, ptProp.displayName, ptProp.objectReferenceValue, typeof(Transform), true);
                    line = line.GetRectBottom();
                    var pos = EditorGUI.Vector3Field(line, posProp.displayName, posProp.vector3Value);
                    line = line.GetRectBottom();
                    var rot = EditorGUI.Vector3Field(line, rotProp.displayName, rotProp.quaternionValue.eulerAngles);
                    if (check.changed)
                    {
                        posProp.vector3Value = pos;
                        rotProp.quaternionValue = Quaternion.Euler(rot);
                        ptProp.objectReferenceValue = parent;
                    }
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 3f;
        }
    }
}
using Kit.Physic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Coordinates
{
    [System.Serializable]
    public class OctreeSetting
    {
        [Tooltip("Size of the sides of the initial node, in metres. The octree will never shrink smaller than this.")]
        [Min(0.1f)] public float initialWorldSize = 10f;

        [Tooltip("Nodes will stop splitting if the new nodes would be smaller than this (metres).")]
        [Min(0.1f)] public float minNodeSize = 1f;

        [Tooltip("Clamped between 1 and 2. Values > 1 let nodes overlap.")]
        [Range(1f, 2f)] public float looseness = 1f;

        [Tooltip("The amount of content (min,max) for per node, over the max will be split, lower then min will be shrink.")]
        [MinMaxSliderInt(1, 10)] public Vector2Int contentVolume = new Vector2Int(1, 2);

        //[Tooltip("When content more then giving setting, split cell request will be trigger. unless reaching the min node size.")]
        public int maxContentPerNode => contentVolume.y;

        //[Tooltip("When all content amount (include childrens) less then giving setting, shrink request will be trigger. unless reach maxContentPerNode.")]
        public int shrinkContentAmount => contentVolume.x;

        public bool SplitCellWhenContentSizeSmaller = false;
        [Range(0f, 1f)] public float SpliteCellWhenContentSizeSmallerThan = 0.25f;

        [System.Flags]
        public enum eDebug
        {
            SpaceNode       = 1 << 0,
            Content         = 1 << 1,
            Label           = 1 << 2,
            LeafNodeContent = 1 << 3,

            All = SpaceNode | Content | Label | LeafNodeContent,
        }
        [Space]
        [Header("Debug")]
        public eDebug debug = eDebug.All;
        [Tooltip("The gizmos debug layer, depend on octree depth from 0~100%.")]
        public Color pointColor = Color.yellow;
        public Color volumeColor = Color.cyan;
        public Color leafContentColor = Color.cyan.CloneAlpha(0.5f);

        [Header("Grid Gizmos")]
        public Gradient depthColor = new Gradient
        {
            colorKeys = new[]{
                new GradientColorKey(Color.white, 0f),
                new GradientColorKey(Color.green, 1f),
            },
            alphaKeys = new[]
            {
                new GradientAlphaKey(0.01f, 0f),
                new GradientAlphaKey(0.01f, 0.5f),
                new GradientAlphaKey(1f, 1f),
            },
            mode = GradientMode.Blend,
        };
        [MinMaxSlider(0f, 1f)] public Vector2 drawGizmosDepthNode = new Vector2(0f, 1f);
    }

    public class MonoOctree : MonoBehaviour
    {
        private Octree m_Octree;
        [SerializeField] private OctreeSetting m_Setting = new OctreeSetting();

        [Header("U3D Refresh")]
        [Tooltip("Auto update the existing content(s) position based on the reference.")]
        [SerializeField] bool m_AutoRefreshOctreeContentPosition = true;
        [DrawIf(nameof(m_AutoRefreshOctreeContentPosition), true, disablingType:DisablingType.DontDraw)]
        [Tooltip("Auto update interval.")]
        [SerializeField, Min(0.001f)] float m_RefreshInterval = 0.2f;
        
        [DrawIf(nameof(m_AutoRefreshOctreeContentPosition), true, disablingType: DisablingType.DontDraw)]
        [Tooltip("Quota to process split,merge,shrink request form spaceNode, performance related.")]
        [SerializeField, Min(1)] int m_PerFrameQuota = 3;

        [Header("Collider Helper")]
        [SerializeField] RaycastHelper m_Adder;
        [SerializeField] RaycastHelper m_Remover;

        [SerializeField, ReadOnly] int m_TotalContent = 0;

        private void Reset()
        {
            {
                var tmp = new GameObject("Adder");
                tmp.transform.SetParent(transform, false);
                m_Adder = tmp.AddComponent<RaycastHelper>();
            }
            {
                var tmp = new GameObject("Remover");
                tmp.transform.SetParent(transform, false);
                m_Remover = tmp.AddComponent<RaycastHelper>();
            }
        }

        private void Awake()
        {
            m_Octree = new Octree(transform.position, m_Setting);
            m_Adder.OnHit.AddListener(OnAddHited);
            m_Remover.OnHit.AddListener(OnRemoveHited);
        }

        private void FixedUpdate()
        {
            if (m_Octree == null)
                return;

            m_Octree.Tick();
            m_TotalContent = m_Octree.Count();
        }

        private void Update()
        {
            if (!m_AutoRefreshOctreeContentPosition)
                return;
            RefreshContent();
        }

        private void OnDrawGizmos()
        {
            if (m_Octree == null)
                return;
            m_Octree.DrawGizmos();
        }

        private void OnAddHited(Transform target)
        {
            IOctreeContent content = target.GetComponent<IOctreeContent>();
            if (content == null) 
                content = target.GetOrAddComponent<GameObjectContent>();
            m_Octree.Add(content);
        }

        private void OnRemoveHited(Transform target)
        {
            IOctreeContent content = target.GetComponent<IOctreeContent>();
            if (content == null)
                return;
            m_Octree.Remove(content);
        }

        #region Refresh Content
        private float m_TriggerTime;
        private Queue<IOctreeContent> m_SnapShot = null;
        private void DumpContent()
        {
            m_TriggerTime = Time.timeSinceLevelLoad;
            m_SnapShot = new Queue<IOctreeContent>(m_Octree.rootNode.GetContents());
        }
        public void RefreshContent()
        {
            if (Time.timeSinceLevelLoad - m_TriggerTime < m_RefreshInterval)
                return;

            if (m_SnapShot == null || m_SnapShot.Count == 0)
                DumpContent();
            
            m_TriggerTime = Time.timeSinceLevelLoad;
            int quota = m_PerFrameQuota;
            while (quota-- > 0 && m_SnapShot.Count > 0)
            {
                var obj = m_SnapShot.Dequeue();
                if (obj == null)
                    continue; // it's being removed

                if (obj is not UnityEngine.Object u3dObj)
                    continue; // skip non-game engine object.

                if (u3dObj is Component comp && comp.gameObject.isStatic)
                    continue; // skip static gameobject

                if (!m_Octree.Contains(obj))
                    continue; // it's being removed.

                m_Octree.Add(obj);
            }

        }
        #endregion Refresh Content
    }
}
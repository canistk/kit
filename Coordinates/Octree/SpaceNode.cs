using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Coordinates
{
    using eDebug = OctreeSetting.eDebug;
    /// <summary>
    /// The node to reparsent the spacing and it's content.
    /// when <see cref="IOctreeContent"/> keep adding to this node,
    /// depend on state will trigger following :
    /// <see cref="Octree.InternalGrowRequest(IOctreeContent)"/>
    /// <see cref="Octree.InternalSplitNode(SpaceNode)"/>
    /// <see cref="Octree.InternalShrinkNode(SpaceNode)"/>
    /// </summary>
    public class SpaceNode
    {
        #region Constructor/Destructor
        /// <summary>Constructor.</summary>
        /// <param name="minSize">Minimum size of nodes in this octree.</param>
        /// <param name="baseLength">Length of this node, not taking looseness into account.</param>
        /// <param name="looseness">Multiplier for baseLengthVal to get the actual size.</param>
        /// <param name="center">Centre position of this node.</param>
        public SpaceNode(
            Octree octree,
            SpaceNode parent,
            Vector3 center,
            float baseLength)
        {
            this.octree = octree;
            this.parent = parent;
            this.baseLength = baseLength;
            this.bounds = new Bounds(center, new Vector3(baseLength, baseLength, baseLength));
            // Debug.Log($"[Octree] {nameof(SpaceNode)} created at {bounds}, Depth={Depth()}");
        }

        ~SpaceNode() { }
        #endregion Constructor/Destructor

        #region Variables
        protected   readonly    Octree      octree;
        protected   readonly    SpaceNode   parent;
        public      readonly    float       baseLength;
        public      readonly    Bounds      bounds;
        public                  Vector3     center      => bounds.center;
        private                 OctreeSetting setting   => octree.setting;
        public                  float       minSize     => setting.minNodeSize;
        public                  float       looseness   => setting.looseness;
        public                  float       looseLength => (setting.minNodeSize * looseness - setting.minNodeSize) + baseLength;
        public                  Bounds      looseBounds => new Bounds(center, new Vector3(looseLength, looseLength, looseLength));
        public                  bool        IsLeafNode  => childrens == null;
        #endregion Variables

        #region Content Handling
        private List<IOctreeContent> m_Contents = new List<IOctreeContent>(8);

        
        /// <summary>Access by <see cref="Octree.Add(IOctreeContent)"/></summary>
        internal void Add(IOctreeContent obj)
        {
            if (this == octree.rootNode)
            {
                // the object is too big to fit in root node
                if (!looseBounds.IsFullyEncapsulate(obj.bounds))
                {
                    // root node, but fail to encapsulate, the target object are too big.
                    // grow and redefine root node
                    octree.InternalGrowRequest(obj);
                    return;
                }

                // object already in octree.
                if (Contains(obj, out var previousNode))
                {
                    if (previousNode.looseBounds.IsFullyEncapsulate(obj.bounds) ||
                        previousNode.looseBounds.Intersects(obj.bounds))
                        return;

                    // hold by another node, remove and re-add
                    previousNode.Remove(obj);
                }
            }

            if (parent != null &&
                !(looseBounds.IsFullyEncapsulate(obj.bounds) || looseBounds.Contains(obj.bounds.center)))
            {
                throw new System.Exception($"Depth[{Depth()}] parent node had responsibility to check content and children's size.");
            }

            if (!IsLeafNode &&
                TryAssignContentIntoChildren(obj))
            {
                // object fit in child.
                return;
            }

            // if no children can fit in content,
            // this node is the best place to put it.
            m_Contents.Add(obj);

            // split children if reach max content amount
            if (!IsLeafNode)
            {
                Debug.LogError($"[Octree] Do we had this case, the {obj} didn't fit in children");
            }
            else if (m_Contents.Count > setting.maxContentPerNode)
            {
                HandleSplitRequest();
                return;
            }

            if (setting.SplitCellWhenContentSizeSmaller)
            {
                var s = setting.minNodeSize * setting.minNodeSize;
                var b = bounds.size.sqrMagnitude;
                if (b <= s)
                    return; // too small to take care with.

                var o = obj.bounds.size.sqrMagnitude;
                var p = setting.SpliteCellWhenContentSizeSmallerThan;
                if (b * p >= o)
                {
                    HandleSplitRequest();
                    return;
                }
            }
            
        }

        /// <summary>Access by <see cref="Octree.Remove(IOctreeContent)"/></summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal bool Remove(IOctreeContent obj)
        {
            if (!Contains(obj, out var containNode))
                return false;
            containNode.InternalRemove(obj);
            return true;
        }

        private void InternalRemove(IOctreeContent obj)
        {
            if (!m_Contents.Remove(obj))
                throw new System.Exception("Logic error");

            if (IsLeafNode)
            {
                if (parent != null)
                    parent.HandleShrinkRequest();
            }
            else
            {
                HandleShrinkRequest();
            }
        }

        /// <summary>
        /// Check if this node (include children) are contain the giving content.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Contains(IOctreeContent obj) => Contains(obj, out _);

        /// <summary>
        /// Check if this node (include children) are contain the giving content.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="container"></param>
        /// <returns>the container of giving content.</returns>
        public bool Contains(IOctreeContent obj, out SpaceNode container)
        {
            container = this;
            if (m_Contents.Contains(obj))
                return true;

            if (!IsLeafNode)
            {
                for (int i = 0; i < 8; ++i)
                {
                    if (childrens[i].Contains(obj, out container))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// The total content amount within this node,
        /// include childrens
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            if (IsLeafNode)
                return m_Contents.Count;

            int cnt = 0;
            for (int i = 0; i < 8; ++i)
            {
                cnt += childrens[i].Count();
            }
            return cnt;
        }

        /// <summary>
        /// The depth of this node,
        /// depth are calculate from root node.
        /// </summary>
        /// <returns></returns>
        public int Depth()
        {
            return parent == null ? 0 : 1 + parent.Depth();
        }

        /// <summary>
        /// The max children depth within this node.
        /// </summary>
        /// <returns>
        /// no children return current depth + 1
        /// had childrens, will return the max depth of childrens.
        /// </returns>
        public int MaxChildDepth()
        {
            if (IsLeafNode)
                return 1 + Depth();

            int max = childrens[0].MaxChildDepth();
            for (int i = 1; i < 8; ++i)
            {
                int tmp = childrens[i].MaxChildDepth();
                if (max < tmp)
                    max = tmp;
            }
            return max;
        }

        /// <summary>
        /// Return all content within this node (include childrens)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IOctreeContent> GetContents()
        {
            {
                var iter = m_Contents.GetEnumerator();
                while (iter.MoveNext())
                    yield return iter.Current;
            }

            if (IsLeafNode)
                yield break;

            for (int i = 0; i < 8; ++i)
            {
                var iter = childrens[i].GetContents().GetEnumerator();
                while (iter.MoveNext())
                    yield return iter.Current;
            }
        }

        /// <summary>
        /// return all leaf node that intersect giving ray.
        /// </summary>
        /// <param name="ray"></param>
        /// <returns>all intersect ray.</returns>
        public IEnumerable<SpaceNode> IntersectRay(Ray ray)
        {
            if (IsLeafNode && bounds.IntersectRay(ray))
                yield return this;

            for (int i = 0; i < 8; ++i)
            {
                foreach (var intersect in childrens[i].IntersectRay(ray))
                {
                    yield return intersect;
                }
            }
        }

        /// <summary>
        /// return all leaf node that intersects giving bounds
        /// </summary>
        /// <param name="other"></param>
        /// <param name="containSurfaceCollision"></param>
        /// <returns></returns>
        public IEnumerable<SpaceNode> Intersects(Bounds other, bool containSurfaceCollision = true)
        {
            if (IsLeafNode && bounds.Intersects(other, containSurfaceCollision))
                yield return this;
            for (int i = 0; i < 8; ++i)
            {
                foreach (var intersect in childrens[i].Intersects(other, containSurfaceCollision))
                {
                    yield return intersect;
                }
            }
        }

        public void DrawGizmos(int depth, in int max)
        {
            var pt          = (float)depth / (float)max;
            var color       = setting.depthColor.Evaluate(pt);
            var dr          = setting.drawGizmosDepthNode;
            var drawRange   = dr.x <= pt && pt <= dr.y;
            var drawNode    = (setting.debug & eDebug.SpaceNode) != 0;
            var drawLabel   = (setting.debug & eDebug.Label) != 0;
            var drawContent = (setting.debug & eDebug.Content) != 0;
            var drawLeafObj = (setting.debug & eDebug.LeafNodeContent) != 0;
            if (childrens != null)
            {
                for (int i = 0; i < 8; ++i)
                {
                    childrens[i].DrawGizmos(1 + depth, max);
                }
            }

            if (drawRange)
            {
                if (drawNode)
                {
                    if (depth < max)
                    {
                        GizmosExtend.DrawBounds(looseBounds, color);
                    }
                    else
                    {
                        Debug.LogError($"[Octree] Out of range on depth={depth}, when max depth {max}");
                    }
                }

                for (int i = 0; i < m_Contents.Count; ++i)
                {
                    var b = m_Contents[i].bounds;
                    if (drawContent)
                    {
                        if (b.extents.sqrMagnitude.Approximately(0f))
                        {
                            GizmosExtend.DrawPoint(b.center, setting.pointColor);
                        }
                        else
                        {
                            GizmosExtend.DrawBounds(b, setting.volumeColor);
                        }
                    }

                    if (drawLabel)
                        GizmosExtend.DrawLabel(b.center, $"{m_Contents[i].name}\nDepth {depth}");
                }
            }

            if (drawLeafObj && IsLeafNode && Count() > 0)
            {
                using (new ColorScope(setting.leafContentColor))
                {
                    Gizmos.DrawCube(looseBounds.center, looseBounds.size);
                }
            }
        }
        #endregion Content Handling

        #region Split Children
        public SpaceNode[] childrens { get; private set; } = null;
        private void HandleSplitRequest()
        {
            using (new SystemExtend.ProfilerScope(nameof(HandleSplitRequest)))
            {
                if (!IsLeafNode)
                    throw new System.Exception("Logic flow error, childrens already created.");

                var q /*quarter*/       = baseLength * 0.25f;
                var halfLen = baseLength * 0.5f;
                childrens = new SpaceNode[8]
                {
                new SpaceNode(octree, this, center + new Vector3(-q,-q,-q), halfLen), // LDF
                new SpaceNode(octree, this, center + new Vector3(-q,-q, q), halfLen), // LDB
                new SpaceNode(octree, this, center + new Vector3(-q, q,-q), halfLen), // LUF
				new SpaceNode(octree, this, center + new Vector3(-q, q, q), halfLen), // LUB
				new SpaceNode(octree, this, center + new Vector3( q,-q,-q), halfLen), // RDF
				new SpaceNode(octree, this, center + new Vector3( q,-q, q), halfLen), // RDB
				new SpaceNode(octree, this, center + new Vector3( q, q,-q), halfLen), // RUF
				new SpaceNode(octree, this, center + new Vector3( q, q, q), halfLen), // RUB
                };

                // Try move content to children node.
                var cache = new List<IOctreeContent>(8);
                var outBoundObjs = new List<IOctreeContent>(8);
                int cnt = m_Contents.Count;

                while (cnt-- > 0)
                {
                    var obj = m_Contents[cnt];
                    var b = obj.bounds;

                    // Check if object content still maintain within this node.
                    if (!(looseBounds.IsFullyEncapsulate(b) || looseBounds.Contains(b.center)))
                    {
                        // content already out bounds, assume it's being moved
                        outBoundObjs.Add(obj);
                        continue;
                    }

                    // check child
                    if (!TryAssignContentIntoChildren(obj))
                    {
                        // not fit for all children, keep content in current node.
                        DebugExtend.DrawBounds(looseBounds, Color.red, 10f, false);
                        var comp = obj as Component;
                        if (comp != null)
                        {
                            DebugExtend.DrawPoint(comp.transform.position, Color.magenta.CloneAlpha(0.5f), 1f, 10f, false);
                        }

                        string msg = "";
                        for (int i = 0; i < 8; ++i)
                        {
                            var loose = childrens[i].looseBounds;
                            var fit = loose.IsFullyEncapsulate(b) || loose.Contains(b.center);
                            DebugExtend.DrawBounds(loose, Color.yellow.CloneAlpha(0.3f), 10f, false);
                            msg += $"Cell[{i}]=" + (fit ? "Fit" : "Not-Fit") + "\n";
                        }
                        Debug.LogError("[Octree]" + msg);

                        throw new System.Exception($"[Octree] {obj.name} {obj.bounds}, fail to add in cell {looseBounds}");
                    }
                }
                m_Contents.Clear();
                m_Contents = cache;
                octree.InternalSplitNode(this);

                if (outBoundObjs.Count > 0)
                {
                    // Debug.LogWarning($"[Octree] Out bounds object detected {outBoundObjs.Count}, renew contents");
                    foreach (var obj in outBoundObjs)
                    {
                        octree.Add(obj);
                    }
                }
            }
        }
        #endregion Split Children

        #region Merge Children
        /// <summary>
        /// We can shrink the octree if:
        /// - This node is >= double minLength in length
        /// - All objects in the root node are within one octant
        /// - This node doesn't have children, or does but 7/8 children are empty
        /// We can also shrink it if there are no objects left at all!
        /// </summary>
        private void HandleShrinkRequest()
        {
            using (new SystemExtend.ProfilerScope(nameof(HandleShrinkRequest)))
            {
                if (IsLeafNode)
                    throw new System.Exception("Impossible cases");

                if (baseLength < (2f * setting.minNodeSize))
                    return;

                int octantHadChild = 0;
                int count = 0;
                for (int i = 0; i < 8; ++i)
                {
                    var tmp = childrens[i].Count();
                    count += tmp;
                    if (tmp > 0)
                        ++octantHadChild;
                }

                if (octantHadChild > 1)
                    return; // reject, multiple octant had content.

                bool tooMuchContent = count > setting.maxContentPerNode; // 3
                if (tooMuchContent)
                    return; // reject, will trigger split after shrink don't merge.

                if (count < setting.shrinkContentAmount) // 1
                    return; // reject, 

                var cache = new List<IOctreeContent>(GetContents());
                m_Contents = cache;
                childrens = null;
                octree.InternalShrinkNode(this);
            }
            // Bubble up the shrink request.
            // cases less few object remain across several depth.
            if (parent != null)
                parent.HandleShrinkRequest();
        }
        #endregion Merge Children

        #region Utils
        /// <summary>
        /// Optimization, reduce the search time via all children one by one.
        /// use the content pivot to guess the best fit area.
        /// <see cref="https://en.wikipedia.org/wiki/Z-order_curve"/>
        /// </summary>
        /// <param name="pivot"></param>
        /// <returns></returns>
        private int QuickGuessIdx(Vector3 pivot)
        {
            // use pivot center, assume children's index
            // based on Z-Curve order
            return
                (pivot.x <= bounds.center.x ? 0 : 4) +
                (pivot.y <= bounds.center.y ? 0 : 2) +
                (pivot.z <= bounds.center.z ? 0 : 1);
        }


        /// <summary>
        /// Attempt to assign <see cref="IOctreeContent"/> to one of this childrens node.
        /// optimization for Z-Curve to guess the most possible children bounds.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        private bool TryAssignContentIntoChildren(IOctreeContent obj)
        {
            if (IsLeafNode)
                throw new System.Exception();

            var tmp = obj.bounds;

            // Check if object content still maintain within this node.
            if (!(looseBounds.IsFullyEncapsulate(tmp) || looseBounds.Contains(tmp.center)))
            {
                // content already out bounds, assume it's being moved
                return false;
            }

            var possibleFitIdx = QuickGuessIdx(tmp.center);
            var possibleBounds = childrens[possibleFitIdx].looseBounds;
            if (possibleBounds.IsFullyEncapsulate(tmp) ||
                possibleBounds.Contains(tmp.center))
            {
                childrens[possibleFitIdx].Add(obj);
                return true;
            }
            else
            {
                // Slow version to search all children for best fit.
                // the object is small enough to encapsulate into this node.
                for (int i = 0; i < 8; ++i)
                {
                    if (i == possibleFitIdx)
                        continue; // skip, it didn't match before.
                    if (childrens[i].looseBounds.IsFullyEncapsulate(tmp) ||
                        childrens[i].looseBounds.Contains(tmp.center))
                    {
                        // check size before assign to child.
                        childrens[i].Add(obj);
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion Utils
    }
}
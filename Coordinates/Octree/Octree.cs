using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Coordinates
{
    public class Octree
    {
        #region Static Variables
        public readonly OctreeSetting setting;
        private float   looseness           => setting.looseness;
        private float   initialSize         => setting.initialWorldSize;
        private float   minSize             => setting.minNodeSize;
        private int     maxContentPerNode   => setting.maxContentPerNode;
        #endregion Static Variables

        public SpaceNode rootNode { get; private set; }
        

        /// <summary>Constructor for the bounds octree.</summary>
		/// <param name="initialWorldSize">Size of the sides of the initial node, in metres. The octree will never shrink smaller than this.</param>
		/// <param name="initialWorldPos">Position of the centre of the initial node.</param>
		/// <param name="minNodeSize">Nodes will stop splitting if the new nodes would be smaller than this (metres).</param>
		/// <param name="looseness">Clamped between 1 and 2. Values > 1 let nodes overlap.</param>
		public Octree(Vector3 initialWorldPos, OctreeSetting setting)
        {
            if (setting.minNodeSize > setting.initialWorldSize)
            {
                Debug.LogWarning($"Minimum node size should not bigger then initial world size.\n" +
                    $"Value changed: {setting.minNodeSize} = {setting.initialWorldSize}");
                setting.minNodeSize = setting.initialWorldSize;
            }
            this.setting = setting;
            setting.looseness = Mathf.Clamp(looseness, 1.0f, 2.0f);
            rootNode = new SpaceNode(this, null, initialWorldPos, setting.minNodeSize);
        }
        ~Octree()
        {

        }

        public void DrawGizmos()
        {
            int max = rootNode.MaxChildDepth();
            rootNode.DrawGizmos(0, max);
        }

        #region Content Handling
        public void Add(IOctreeContent obj)         => rootNode.Add(obj);
        public bool Remove(IOctreeContent obj)      => rootNode.Remove(obj);
        public bool Contains(IOctreeContent obj)    => rootNode.Contains(obj);
        public bool Contains(IOctreeContent obj, out SpaceNode container) => rootNode.Contains(obj, out container);
        public int  Count()                         => rootNode.Count();
        #endregion Content Handling

        #region Task Handle
        private Queue<OctreeTask> m_TaskQueue       = new Queue<OctreeTask>();
        public  void Enqueue(OctreeTask task)       => m_TaskQueue.Enqueue(task);
        /// <summary>
        /// Update task.
        /// </summary>
        public void Tick()
        {
            if (m_TaskQueue.Count == 0)
                return;

            var task = m_TaskQueue.Peek();
            if (task.Execute())
                return;

            // Delete task.
            m_TaskQueue.Dequeue().Dispose();
        }
        #endregion Task Handle

        #region Events
        public delegate void GrowEvent(SpaceNode rootNode);
        public event GrowEvent EVENT_RootNodeChanged;
        internal void InternalGrowRequest(IOctreeContent content)
        {
            using (new SystemExtend.ProfilerScope(nameof(InternalGrowRequest)))
            {
                Bounds newBounds = rootNode.bounds;
                {
                    // Ensure new root node encapsulate new content
                    // Debug.Log($"Before Bounds={newBounds}");
                    Bounds tmp = content.bounds;
                    newBounds.Encapsulate(content.bounds);
                    // Debug.Log($"include content Bounds={newBounds}");
                    int quota = 20;
                    while (quota > 0 &&
                        !newBounds.IsFullyEncapsulate(content.bounds))
                    {
                        --quota;
                        tmp.Expand(0.000001f);
                        newBounds.Encapsulate(tmp);
                    }

                    if (quota == 0)
                    {
                        // unexpect case, need to fix.
                        Debug.LogError($"Growth attempt[{20 - quota}], fail to encapsulate new content {newBounds}");
                    }
                    else if (quota > 1 && quota < 20)
                    {
                        Debug.LogWarning($"Growth attempt[{20 - quota}], fail to encapsulate new content, enlarge new root node size.");
                    }
                    // else, ignore growth attempt 1 case.

                    // Debug.Log($"Growth Bounds={newBounds}");
                }

                var pivot = newBounds.center;
                var maxLength = Mathf.Max(newBounds.size.x, newBounds.size.y, newBounds.size.z);
                var newRoot = new SpaceNode(this, null, pivot, maxLength);
                if (!newRoot.looseBounds.IsFullyEncapsulate(content.bounds))
                {
                    DebugExtend.DrawBounds(newRoot.looseBounds, Color.red.CloneAlpha(0.5f), 10f, false);
                    DebugExtend.DrawBounds(content.bounds, Color.yellow, 10f, false);

                    throw new System.Exception($"{newRoot.looseBounds} fail to encapsulate - {content.bounds}");
                }

                // Clone content to new root.
                {
                    int cnt = rootNode.Count();
                    var list = rootNode.GetContents().ToList(cnt);
                    this.rootNode = newRoot;

                    for (int i = 0; i < list.Count; ++i)
                    {
                        try
                        {
                            this.rootNode.Add(list[i]);
                        }
                        catch (System.Exception ex)
                        {
                            Debug.LogError("throw during growth, " + ex);
                        }
                    }
                    this.rootNode.Add(content);

                }

                // Debug.Log($"{nameof(Octree)} growth {rootNode.bounds}");
                EVENT_RootNodeChanged?.TryCatchDispatchEventError(o => o?.Invoke(rootNode));
            }
        }
        
        public delegate void SpliteEvent(SpaceNode parentNode);
        public event SpliteEvent EVENT_Split;
        internal void InternalSplitNode(SpaceNode parentNode)
        {
            // Debug.Log($"{nameof(Octree)} node on depth={parentNode.Depth()} split childrens");
            EVENT_Split?.TryCatchDispatchEventError(o => o?.Invoke(parentNode));
        }

        public delegate void ShrinkEvent(SpaceNode parentNode);
        public event ShrinkEvent EVENT_Merge;
        internal void InternalShrinkNode(SpaceNode parentNode)
        {
            // Debug.Log($"{nameof(Octree)} node on depth={parentNode.Depth()} shrink childrens");
            EVENT_Merge?.TryCatchDispatchEventError(o => o?.Invoke(parentNode));
        }
        #endregion Events
    }
}
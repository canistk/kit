using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Coordinates
{
    public interface IOctreeContent
    {
        string name { get; }
        Bounds bounds { get; }
    }
}
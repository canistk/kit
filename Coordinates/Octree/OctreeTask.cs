using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Coordinates
{
    public abstract class OctreeTask : TaskWithState
    {
        public readonly Octree octree;
        public OctreeTask(Octree octree)
        {
            this.octree = octree;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit.Coordinates
{
    public class GameObjectContent : MonoBehaviour, IOctreeContent
    {
        public Bounds bounds
        {
            get
            {
                if (renderer != null)
                    return renderer.bounds;

                if (collider!= null)
                    return collider.bounds;

                return new Bounds(transform.position, Vector3.zero);
            }
        }

        private struct FetchComp<COMP> where COMP : Component
        {
            private bool fetch;
            public COMP target;
            public COMP GetOnce(Transform transform)
            {
                if (target == null && !fetch)
                {
                    fetch = true;
                    target = transform.GetComponentInChildren<COMP>(true);
                }
                return target;
            }
        }

        private FetchComp<Collider> m_Collider = default;
        public new Collider collider => m_Collider.GetOnce(transform);

        private FetchComp<Renderer> m_Renderer = default;
        public new Renderer renderer => m_Renderer.GetOnce(transform);
    }
}
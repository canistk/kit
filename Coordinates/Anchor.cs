using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit.Coordinates
{
    [System.Serializable]
    public class Anchor
    {
        [SerializeField]
        private Vector3 m_LocalPosition         = Vector3.zero;
        [SerializeField, EularAngles] 
        private Quaternion m_LocalRotation      = Quaternion.identity;

        #region Constructor
        public Anchor(Transform parent)
            : this()
        {
            SetParent(parent);
        }
        public Anchor(Vector3 position, Quaternion rotation, Transform parent)
            : this(parent)
        {
            m_LocalPosition = m_Parent.InverseTransformPoint(position);
            m_LocalRotation = m_Parent.rotation.Inverse() * rotation;
            this.dirty = dirty;
        }

        public Anchor(Vector3 position, Transform parent)
            : this(parent)
        {
            m_LocalPosition = m_Parent.InverseTransformPoint(position);
            m_LocalRotation = Quaternion.identity;
            this.dirty = dirty;
        }

        public Anchor() { }
        #endregion Constructor

        #region Parent Transform
        [SerializeField]
        private Transform m_Parent;
        public Transform parent => m_Parent;
        public void SetParent(Transform parent)
        {
            dirty = true;
            m_Parent = parent;
        }
        private static /* readonly */ Matrix4x4 Matrix4x4identity = Matrix4x4.identity;
        private Matrix4x4 m_Matrix = Matrix4x4.identity;
        private Matrix4x4 m_InverseMatrix;
        public Matrix4x4 localMatrix
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return m_Matrix;
            }
        }
        public Matrix4x4 localInverseMatrix
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return m_InverseMatrix;
            }
        }
        private Matrix4x4 parentMatrix
        {
            get
            {
                return m_Parent ? m_Parent.localToWorldMatrix : Matrix4x4identity;
            }
        }
        public Matrix4x4 matrix
        {
            get
            {
                return parentMatrix * localMatrix;
            }
        }
        #endregion Parent Transform

        #region Public API
        public Vector3 localPosition
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return m_LocalPosition;
            }
            set
            {
                if (m_LocalPosition == value)
                    return;
                m_LocalPosition = value;
                dirty = true;
            }
        }

        public Vector3 position
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return parentMatrix.MultiplyPoint3x4(localPosition);
            }
            set
            {
                localPosition = parentMatrix.inverse.MultiplyPoint3x4(value);
            }
        }

        public Quaternion localRotation
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return m_LocalRotation;
            }
            set
            {
                if (m_LocalRotation == value)
                    return;
                m_LocalRotation = value.normalized;
                dirty = true;
            }
        }

        public Quaternion rotation
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return parentMatrix.GetRotation().normalized * localRotation;
            }
            set
            {
                localRotation = (parentMatrix.inverse.GetRotation() * value).normalized;
            }
        }

        public Vector3 eulerAngles
        {
            get
            {
                if (dirty)
                    InternalCalculate();
                return m_LocalRotation.eulerAngles;
            }
            set
            {
                m_LocalRotation = Quaternion.Euler(value);
            }
        }
        
        #endregion Public API

        public bool dirty { get; private set; } = true;
        public void SetDirty() { dirty = true; }
        public event Callback EVENT_ChangedValue = null;
        private static /* readonly */ Vector3 Vector3one = Vector3.one;
        private static Quaternion Quaternionidentity = Quaternion.identity;
        private void InternalCalculate()
        {
            if (!dirty)
                return;
            dirty = false;
            if (m_LocalRotation.IsInvalid())
                m_LocalRotation = Quaternionidentity;
            m_Matrix = Matrix4x4.TRS(m_LocalPosition, m_LocalRotation, Vector3one);
            m_InverseMatrix = Matrix4x4.Inverse(m_Matrix);
            EVENT_ChangedValue?.Invoke();
        }

    }
}

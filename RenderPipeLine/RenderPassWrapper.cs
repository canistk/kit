using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Kit
{
    public class RenderPassWrapper : ScriptableRenderPass
    {
        private IRenderPass m_Pass;
        public RenderPassWrapper(IRenderPass renderPass)
        {
            m_Pass = renderPass;
        }

        /// <summary>Called by <see cref="ScriptableRendererFeature.OnCameraPreCull(ScriptableRenderer, in CameraData)"/>,
        /// <seealso cref="ScriptableRenderer.OnPreCullRenderPasses"/>
        /// execute before the feature being execute.
        /// </summary>
        /// <param name="renderer"></param>
        /// <param name="cameraData"></param>
        public virtual void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
        {
            if (ReferenceEquals(null, m_Pass))
                return;
            m_Pass.OnCameraPreCull(renderer, cameraData);
        }

        /// <summary>Called by <see cref="ScriptableRenderer.Execute(ScriptableRenderContext, ref RenderingData)"/>,
        /// <see cref="https://github.com/Unity-Technologies/Graphics/blob/master/Packages/com.unity.render-pipelines.universal/Runtime/ScriptableRenderer.cs"/>
        /// Line 1220
        /// way before all other pass.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="cameraTextureDescriptor"></param>
        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            //base.Configure(cmd, cameraTextureDescriptor);
            if (ReferenceEquals(null, m_Pass))
                return;
            m_Pass.Configure(this, cmd, cameraTextureDescriptor);
        }

        /// <summary>
        /// Called by <see cref="ScriptableRenderer.Execute(ScriptableRenderContext, ref RenderingData)"/>,
        /// <see cref="https://github.com/Unity-Technologies/Graphics/blob/master/Packages/com.unity.render-pipelines.universal/Runtime/ScriptableRenderer.cs"/>
        /// Line 1269
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="renderingData"></param>
        public override void OnCameraSetup(CommandBuffer cmd, ref RenderingData renderingData)
        {
            // base.OnCameraSetup(cmd, ref renderingData);
            if (ReferenceEquals(null, m_Pass))
                return;
            m_Pass.OnCameraSetup(this, cmd, ref renderingData);
        }

        /// <summary>Called by <see cref="ScriptableRenderer.Execute(ScriptableRenderContext, ref RenderingData)"/>
        /// by <see cref="ScriptableRenderer.RenderBlocks"/> across all pass
        /// e.g. BeforeRendering, MainRenderingOpaque, MainRenderingTransparent, AfterRendering
        /// </summary>
        /// <param name="context"></param>
        /// <param name="renderingData"></param>
        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            // base.Execute(context, ref renderingData);
            if (ReferenceEquals(null, m_Pass))
                return;
            m_Pass.Execute(this, context, ref renderingData);
        }

        /// <summary>Called by <see cref="ScriptableRenderer.FinishRendering(CommandBuffer)"/>
        /// finial call on each pass,
        /// all variables should be reset here.</summary>
        /// <param name="cmd"></param>
        public override void OnCameraCleanup(CommandBuffer cmd)
        {
            //base.OnCameraCleanup(cmd);
            if (ReferenceEquals(null, m_Pass))
                return;
            m_Pass.OnCameraCleanup(this, cmd);
        }

        /// <summary>
        /// Called by <see cref="ScriptableRenderer.FinishRendering(CommandBuffer)"/>
        /// Line 2082
        /// </summary>
        /// <param name="cmd"></param>
        public override void OnFinishCameraStackRendering(CommandBuffer cmd)
        {
            // base.OnFinishCameraStackRendering(cmd);
            if (ReferenceEquals(null, m_Pass))
                return;
            m_Pass.OnFinishCameraStackRendering(this, cmd);
        }
    }
}

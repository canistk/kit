using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
namespace Kit
{
	/// <summary>
	/// This scriptableObject will be called by URP pipe line.
	/// we redirect system call to <see cref="MyRenderPassManager"/>
	/// </summary>
	public class MyRendererFeature : ScriptableRendererFeature
	{
		public override void Create()
		{
			// Don't think we can pass the create callback to children.
			// since this only trigger once.
			// Debug.Log("MyPostProcessManager.RendererFeature.Create");
        }

		/// <summary></summary>
		/// <param name="renderer"></param>
		/// <param name="cameraData"></param>
        public override void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
        {
            if (MyRenderPassManager.rawInstance == null)
                return;
            MyRenderPassManager.instance.OnCameraPreCull(renderer, cameraData);
        }

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
		{
			// Debug.Log("MyPostProcessManager.RendererFeature.AddRenderPasses");
			if (MyRenderPassManager.rawInstance == null)
				return;
			MyRenderPassManager.instance.AddRenderPasses(renderer, ref renderingData);
		}
	}

	public class MyRenderPassManager
	{
        private static MyRenderPassManager s_instance;
		public static MyRenderPassManager rawInstance => s_instance;
		public static MyRenderPassManager instance
		{
			get
			{
				if (s_instance == null)
				{
					s_instance = new MyRenderPassManager();
				}
				return s_instance;
			}
        }
        private Dictionary<IRenderPass, RenderPassWrapper> m_volumes = new Dictionary<IRenderPass, RenderPassWrapper>();

        public void Register(IRenderPass vol)
		{
			var _pass = new RenderPassWrapper(vol);
            m_volumes.Add(vol, _pass);
		}

		public void Unregister(IRenderPass vol)
		{
			if (m_volumes.ContainsKey(vol))
			{
				m_volumes.Remove(vol);
			}
		}

		/// <summary>A wrapper based on <see cref="ScriptableRendererFeature.OnCameraPreCull(ScriptableRenderer, in CameraData)"/></summary>
		/// <param name="renderer"></param>
		/// <param name="cameraData"></param>
        internal void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
		{
            foreach ((IRenderPass renderPass, RenderPassWrapper wrapper) in m_volumes)
            {
                if (renderPass == null || wrapper == null)
					continue;

				// TODO: not sure how to implement.
				wrapper.OnCameraPreCull(renderer, cameraData);
            }
        }
        /// <summary>A wrapper based on <see cref="ScriptableRendererFeature.AddRenderPasses(ScriptableRenderer, ref RenderingData)"/></summary>
        /// <param name="renderer"></param>
        /// <param name="renderingData"></param>
        internal void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
		{
            foreach ((IRenderPass renderPass, RenderPassWrapper wrapper) in m_volumes)
            {
                if (renderPass == null || wrapper == null)
                    continue;

				/// from <see cref="ScriptableRenderer.cameraColorTarget"/>
                /// pass camera color target to <see cref="MonoRenderPass.cameraColorTarget"/>
                renderPass.cameraColorTarget = renderer.cameraColorTarget;

                /// set RenderPassEvent based on setting.
                wrapper.renderPassEvent = renderPass.renderPassEvent;
				renderer.EnqueuePass(wrapper);
			}
		}
	}
}
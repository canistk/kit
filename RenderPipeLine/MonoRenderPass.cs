using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
namespace Kit
{
    public interface IRenderPass
    {
        public RenderPassEvent                  renderPassEvent     { get; }
        public RenderTargetIdentifier           cameraColorTarget   { get; set; }
        
        /// <see cref="ScriptableRendererFeature.OnCameraPreCull(ScriptableRenderer, in CameraData)"/>
        public void OnCameraPreCull             (ScriptableRenderer renderer, in CameraData cameraData);

        /// <see cref="ScriptableRenderPass.OnCameraSetup(CommandBuffer, ref RenderingData)"/>
        public void OnCameraSetup               (ScriptableRenderPass render, CommandBuffer cmd, ref RenderingData renderingData);

        /// <see cref="ScriptableRenderPass.Configure(CommandBuffer, RenderTextureDescriptor)"/>
        public void Configure                   (ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor);

        /// <see cref="ScriptableRenderPass.OnCameraCleanup(CommandBuffer)"/>
        public void OnCameraCleanup             (ScriptableRenderPass render, CommandBuffer cmd);

        /// <see cref="ScriptableRenderPass.OnFinishCameraStackRendering(CommandBuffer)"/>
        public void OnFinishCameraStackRendering(ScriptableRenderPass render, CommandBuffer cmd) { }

        /// <see cref="ScriptableRenderPass.Execute(ScriptableRenderContext, ref RenderingData)"/>
        public void Execute                     (ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData);
    }

    /// <summary>
    /// <see cref="https://catlikecoding.com/unity/tutorials/custom-srp/custom-render-pipeline/"/>
    /// <see cref="https://github.com/Unity-Technologies/Graphics/blob/master/Packages/com.unity.render-pipelines.universal/Runtime/Passes/RenderObjectsPass.cs"/>
    /// </summary>
    [ExecuteInEditMode]
    public abstract class MonoRenderPass : MonoBehaviour, IRenderPass
    {
        public abstract RenderPassEvent         renderPassEvent     { get; }
        public abstract RenderTargetIdentifier  cameraColorTarget   { get; set; }
        public virtual void OnCameraPreCull     (ScriptableRenderer renderer, in CameraData cameraData) { }
        public virtual void OnCameraSetup       (ScriptableRenderPass render, CommandBuffer cmd, ref RenderingData renderingData) { }
        public virtual void Configure           (ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor) { }
        public virtual void OnCameraCleanup     (ScriptableRenderPass render, CommandBuffer cmd) { }
        public virtual void OnFinishCameraStackRendering(ScriptableRenderPass render, CommandBuffer cmd) { }
        public virtual void Execute             (ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData) { }
        public virtual void OnEnable()
        {
            MyRenderPassManager.instance.Register(this);
        }

        public virtual void OnDisable()
        {
            MyRenderPassManager.instance.Unregister(this);
        }
    }
}
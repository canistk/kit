using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
namespace Kit
{
	public class MyRenderPipeline : RenderPipeline
	{
		UniversalRenderPipeline basePipeline;

		public MyRenderPipeline(UniversalRenderPipelineAsset asset)
		{
			basePipeline = new UniversalRenderPipeline(asset);
		}

		protected override void Render(ScriptableRenderContext context, Camera[] cameras)
		{
			//basePipeline.Render(context, cameras);
		}
	}

	[CreateAssetMenu(menuName = "Rendering/MyRenderPipelineAsset", fileName = "MyRenderPipelineAsset")]
	public class MyRenderPipelineAsset : UniversalRenderPipelineAsset
	{
		protected override RenderPipeline CreatePipeline() => new MyRenderPipeline(this);
	}
}
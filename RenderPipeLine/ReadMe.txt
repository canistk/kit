URP setup flow.
0) Project Settings > Player > Scripting Define Symbols, add "URP"
1) select : Edit > Project Settings > Graphics > Scriptable Render Pipeline Setting.
2) it should be something like (Universal Render Pipeline Asset), select the scriptable asset
2.1) locate "Renderer List" and locate "ForwardRenderer" or search in project when it's standard setup.
3) select ForwardRenderer > Add Renderer Feature
4) in the drop down list select "MyPostProcessRenderPass"

now you can register all post process via "MyPostProcessManager.Register"
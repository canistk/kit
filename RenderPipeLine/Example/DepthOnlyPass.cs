using Kit;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
namespace Kit
{
    /*
     * https://gist.github.com/Cyanilux/0175cead446155244e720b33dce87740
     * Re-write into MonoRenderPass for better understand.
     * - Canis 20231022
     */

    public class DepthOnlyPass : MonoRenderPass
    {
        [SerializeField] RenderPassEvent m_RenderPassEvent = RenderPassEvent.AfterRenderingPrePasses;
        public override RenderPassEvent renderPassEvent => m_RenderPassEvent;
        /*
        - Set to at least AfterRenderingPrePasses if depth texture is generated via a Depth Prepass,
            or it will just be overriden
        - Set to at least BeforeRenderingSkybox if depth texture is generated via a Copy Depth pass,
              Anything before this is already included in the texture! (Though not for Scene View as that always uses a prepass)
        */

        [SerializeField] CameraType m_RenderCamera = CameraType.Game | CameraType.SceneView;
        [SerializeField] LayerMask m_LayerMask = Physics.AllLayers;
        [SerializeField] Material m_Material;
        public override RenderTargetIdentifier cameraColorTarget { get; set; }
        private static Mesh m_FullScreenTriangle;


        private ProfilingSampler    m_ProfilingSampler;
        private FilteringSettings   m_FilteringSettings;
        private List<ShaderTagId>   m_ShaderTagIdList = new List<ShaderTagId>();
        private RenderTargetHandle  depthTex;
        private RTHandle            depthTexRT;

        private struct RTIF
        {
            public int id;
            public RenderTargetIdentifier rtid;
            public RTIF(string name) : this(Shader.PropertyToID(name))
            { }
            public RTIF(int id)
            {
                this.id = id;
                this.rtid = new RenderTargetIdentifier(this.id);
            }

            public void Init(ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
            {
                var width = cameraTextureDescriptor.width;
                var height = cameraTextureDescriptor.height;

                cmd.GetTemporaryRT(id, width, height, 0, FilterMode.Bilinear, RenderTextureFormat.ARGB32);

                rtid = new RenderTargetIdentifier(id);
            }
        }
        private RTIF m_Tmp;

        private RenderTargetIdentifier m_ColorTex, m_DepthTex;

        private RenderStateBlock    m_RenderStateBlock = new RenderStateBlock(RenderStateMask.Nothing);


        public override void OnEnable()
        {
            base.OnEnable();
            Init();
        }

        private bool m_IsInited = false;
        private void Init()
        {
            if (m_IsInited)
                return;

            this.m_ProfilingSampler = new ProfilingSampler("RenderToDepthTexture");
            this.m_FilteringSettings = new FilteringSettings(RenderQueueRange.opaque, m_LayerMask);
            this.m_ShaderTagIdList = new List<ShaderTagId>(new[] {
                new ShaderTagId("DepthOnly") // Only render DepthOnly pass
            });
            this.depthTex.Init("_CameraDepthTexture");
            
            this.depthTexRT = RTHandles.Alloc(
                new RenderTargetIdentifier(Shader.PropertyToID("_CameraDepthTexture")),
                name: ("_CameraDepthTexture"));


            this.m_RenderStateBlock = new RenderStateBlock(RenderStateMask.Nothing);
        }

        public override void Configure(ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            base.Configure(render, cmd, cameraTextureDescriptor);
            render.ConfigureTarget(m_ColorTex , m_DepthTex);
            // render.ConfigureDepthStoreAction(RenderBufferStoreAction.Store);
            // render.ConfigureTarget(depthTex.Identifier());
        }

        public override void OnCameraSetup(ScriptableRenderPass render, CommandBuffer cmd, ref RenderingData renderingData)
        {
            // base.OnCameraSetup(render, cmd, ref renderingData);
            // PrepareFullScreenMesh();
            // render.ConfigureTarget(depthTex.Identifier());
            // render.ConfigureTarget(depthTexRT);
        }

        public override void Execute(ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData)
        {
            if (m_Material == null)
                return;

            if ((renderingData.cameraData.cameraType & m_RenderCamera) == 0)
                return;

            if (!renderingData.cameraData.requiresDepthTexture)
            {
                Debug.LogWarning($"Not support depth texture on camera {renderingData.cameraData.camera}");
                return;
            }

            var cmd             = CommandBufferPool.Get(GetType().Name);
            //var sortingCriteria = renderingData.cameraData.defaultOpaqueSortFlags;
            //var drawSettings    = render.CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, sortingCriteria);

            //using (new ProfilingScope(cmd, m_ProfilingSampler))
            //{
            //    context.DrawRenderers(renderingData.cullResults, ref drawSettings, ref m_FilteringSettings);
            //}

            if (m_Material)
            {
                cmd.Blit(render.depthAttachment, cameraColorTarget, m_Material);
            }
            else
            {
                cmd.Blit(render.depthAttachment, cameraColorTarget);
            }

            //var cam     = renderingData.cameraData.camera;
            //var matrix  = cam.transform.localToWorldMatrix;
            //var offset  = Matrix4x4.Translate(Vector3.forward * (cam.nearClipPlane + 0.0001f));


            // Read stencil
            // https://forum.unity.com/threads/how-to-read-stencil-bits-from-a-custom-depthstencil-rtexture-in-fragment-shader.1410204/#post-9007771

            // https://forum.unity.com/threads/how-to-call-depthonlypass-after-rendering-transpants-or-is-broken.1424289/
            // Set ZWrite to true, ZTest to LEqual is optional.
            // m_RenderStateBlock.depthState = new DepthState(true, CompareFunction.LessEqual);
            // Tell URP that we changed the depth render state.
            // m_RenderStateBlock.mask |= RenderStateMask.Depth;
            

            // cmd.DrawMesh(m_FullScreenTriangle, matrix * offset, m_Material);
            context.ExecuteCommandBuffer(cmd);
            cmd.Clear();
            CommandBufferPool.Release(cmd);
        }

        private void PrepareFullScreenMesh()
        {
            if (m_FullScreenTriangle != null)
                return;

            m_FullScreenTriangle = new Mesh()
            {
                name = "PostProcessMesh",
                vertices = new Vector3[] {
                    new Vector3(-1, -1, 0),
                    new Vector3( 3, -1, 0),
                    new Vector3(-1,  3, 0),
                },
                triangles = new int[] { 0, 1, 2 }
            };
            m_FullScreenTriangle.UploadMeshData(true);
        }
    }
}
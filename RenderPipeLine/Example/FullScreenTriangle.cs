using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
namespace Kit
{
	public class FullScreenTriangle : MonoRenderPass
	{
		public Material material;
		private static Mesh fullScreenTriangle;

		[SerializeField] RenderPassEvent m_RenderPassEvent = RenderPassEvent.AfterRenderingTransparents;
		public override RenderPassEvent renderPassEvent => m_RenderPassEvent;

        public override RenderTargetIdentifier cameraColorTarget { get; set; }

        public override void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
        { }

        public override void Configure(ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            if (!fullScreenTriangle)
            {
                fullScreenTriangle = new Mesh()
                {
                    name = "fullScreenTriangle",
                    vertices = new Vector3[] {
                    new Vector3(-1, -1, 0),
                    new Vector3( 3, -1, 0),
                    new Vector3(-1,  3, 0),
                },
                    triangles = new int[] { 0, 1, 2 }
                };
                fullScreenTriangle.UploadMeshData(true);
            }
        }

        override public void Execute(ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData)
		{
			if (!material)
                return;

            if ((renderingData.cameraData.cameraType & 
                (CameraType.Game | CameraType.SceneView)) == 0)
                return;

            var cam     = renderingData.cameraData.camera;
            var matrix  = cam.transform.localToWorldMatrix;
            var offset  = Matrix4x4.Translate(Vector3.forward * (cam.nearClipPlane + 0.0001f));
            var cmd     = CommandBufferPool.Get(GetType().Name);

			cmd.Clear();
			cmd.DrawMesh(fullScreenTriangle, matrix * offset, material);
			context.ExecuteCommandBuffer(cmd);
			cmd.Clear();
			CommandBufferPool.Release(cmd);
		}

        public override void OnCameraCleanup(ScriptableRenderPass render, CommandBuffer cmd)
        { }
    }
}
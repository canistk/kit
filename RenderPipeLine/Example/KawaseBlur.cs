using Kit;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class KawaseBlur : MonoRenderPass
{
    public override RenderPassEvent renderPassEvent => RenderPassEvent.AfterRenderingTransparents;
    public Material blurMaterial = null;
    
    [Range(2, 15)]  public int      passes = 2;
    [Range(1, 4)]   public int      downsample = 1;
    public  bool     copyToFramebuffer  = true;
    public  string   targetName         = "_blurTexture";

    private int tmpId1;
    private int tmpId2;

    RenderTargetIdentifier tmpRT1;
    RenderTargetIdentifier tmpRT2;

    public override RenderTargetIdentifier cameraColorTarget { get; set; }
    
    public override void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnCameraPreCull(ScriptableRenderer renderer, in CameraData cameraData)
    { }

    public override void Configure(ScriptableRenderPass render, CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
    {
        var width   = cameraTextureDescriptor.width     / downsample;
        var height  = cameraTextureDescriptor.height    / downsample;

        tmpId1 = Shader.PropertyToID("tmpBlurRT1");
        tmpId2 = Shader.PropertyToID("tmpBlurRT2");
        cmd.GetTemporaryRT(tmpId1, width, height, 0, FilterMode.Bilinear, RenderTextureFormat.ARGB32);
        cmd.GetTemporaryRT(tmpId2, width, height, 0, FilterMode.Bilinear, RenderTextureFormat.ARGB32);

        tmpRT1 = new RenderTargetIdentifier(tmpId1);
        tmpRT2 = new RenderTargetIdentifier(tmpId2);

        render.ConfigureTarget(tmpRT1);
        render.ConfigureTarget(tmpRT2);
    }

    public override void Execute(ScriptableRenderPass render, ScriptableRenderContext context, ref RenderingData renderingData)
    {
        if (blurMaterial == null)
            return;

        cameraColorTarget = renderingData.cameraData.renderer.cameraColorTarget;
        CommandBuffer cmd = CommandBufferPool.Get(GetType().Name);

        RenderTextureDescriptor opaqueDesc = renderingData.cameraData.cameraTargetDescriptor;
        opaqueDesc.depthBufferBits = 0;

        // first pass
        cmd.SetGlobalFloat("_offset", 1.5f);
        cmd.Blit(cameraColorTarget, tmpRT1, blurMaterial);

        for (var i = 1; i < passes - 1; i++)
        {

            cmd.SetGlobalFloat("_offset", 0.5f + i);
            cmd.Blit(tmpRT1, tmpRT2, blurMaterial);

            // pingpong
            (tmpRT1, tmpRT2) = (tmpRT2, tmpRT1);
        }

        // final pass
        cmd.SetGlobalFloat("_offset", 0.5f + passes - 1f);
        if (copyToFramebuffer)
        {
            cmd.Blit(tmpRT1, cameraColorTarget, blurMaterial);
        }
        else
        {
            cmd.Blit(tmpRT1, tmpRT2, blurMaterial);
            cmd.SetGlobalTexture(targetName, tmpRT2);
        }

        context.ExecuteCommandBuffer(cmd);
        cmd.Clear();

        CommandBufferPool.Release(cmd);
    }

    public override void OnCameraCleanup(ScriptableRenderPass render, CommandBuffer cmd)
    { }

}
Shader "Kit/Universal Render Pipeline/RenderTex"
{
    Properties
    {
        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture][noscaleOffset] _BaseMap("Albedo", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            // "RenderType" = "Opaque"
            "RenderType"            = "Opaque"
            "RenderPipeline"        = "UniversalRenderPipeline"
            // "Queue"                 = "Transparent"
            "ForceNoShadowCasting"  = "True"
            "IgnoreProjector"       = "True"
            "PreviewType"           = "Plane"
            "DisableBatching"       = "True"
        }
        LOD     100
        ZWrite  Off
        // ZTest Always
        Cull    Back
        
        Pass
        {
    
            HLSLPROGRAM
            #pragma vertex VertexPass
            #pragma fragment FragmentPass
            // make fog work
            #pragma multi_compile_fog
            #pragma target 3.0

            // Required by all Universal Render Pipeline shaders.
            // It will include Unity built-in shader variables (except the lighting variables)
            // (https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
            // It will also include many utilitary functions. 
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            
            struct Attributes
            {
                float4 positionOS   : POSITION;
                float2 uv           : TEXCOORD0;
            };

            struct Varyings
            {
                float4 positionCS   : SV_POSITION;
                float2 uv           : TEXCOORD0;
            };

            TEXTURE2D(_BaseMap);            SAMPLER(sampler_BaseMap);
            UNITY_INSTANCING_BUFFER_START(UnityPerMaterial)
                UNITY_DEFINE_INSTANCED_PROP(float4, _BaseMap_ST);
                UNITY_DEFINE_INSTANCED_PROP(float4, _BaseColor);
            UNITY_INSTANCING_BUFFER_END(UnityPerMaterial)

            
            Varyings VertexPass (Attributes i)
            {
                Varyings o;
                
                VertexPositionInputs vertexInput = GetVertexPositionInputs(i.positionOS.xyz);
                // VertexNormalInputs normalInput = GetVertexNormalInputs(i.normalOS, i.tangentOS);

                o.positionCS    = vertexInput.positionCS;
                o.uv            = i.uv;
                return o;
            }

            float4 FragmentPass (Varyings i) : SV_Target
            {
                float2 uv       = TRANSFORM_TEX(i.uv, _BaseMap);
                float3 col      = _BaseColor * SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv).rgb;
                return float4(col, 1);
            }
            ENDHLSL
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public abstract class Binding : MonoBehaviour
    {
        public abstract void Assign(object data, bool forceUpdate = false);
        public abstract bool CanHandle(object raw);
    }

    public abstract class Binding<DATA> : Binding
        where DATA : class
    {
        private DATA _data;
        public DATA data
        {
            get => _data;
            private set
            {
                // avoid assign same data.
                if (_data == value)
                    return;

                // Disconnect from old data.
                if (_data != null && _data is IDataUpdatable old)
                    old.EVENT_Updated -= TriggerUpdate;

                // Assign to
                _data = value;

                // Connect to new data.
                if (_data != null && _data is IDataUpdatable src)
                    src.EVENT_Updated += TriggerUpdate;
            }
        }

        /// <summary>Redirect to target <see cref="DATA"/> type.</summary>
        /// <param name="data">when data isn't target type, will be reidrect to 
        /// <see cref="View{BIND, DATA}.OnViewUpdateInvalid(object)"/>
        /// Since data will convert into null when fail.</param>
        /// <param name="forceUpdate">force trigger <see cref="View{BIND, DATA}.OnViewUpdate(DATA)"/></param>
        public sealed override void Assign(object data, bool forceUpdate = false)
        {
            Assign(data as DATA, forceUpdate);
        }

        public void Assign(DATA data, bool forceUpdate = false)
        {
            if (this.data == data && !forceUpdate)
                return;
            this.data = data;
            OnAssign(data);
            TriggerUpdate();
        }

        /// <summary>Allow <see cref="Binding"/> sub-class to override handle</summary>
        /// <param name="data"></param>
        protected virtual void OnAssign(DATA data) { }

        public event System.Action<DATA> EVENT_ViewUpdate;
        /// <summary>Allow sub class to force update data.</summary>
        protected void TriggerUpdate()
        {
            EVENT_ViewUpdate?.TryCatchDispatchEventError(o => o?.Invoke(data));
        }

        /// <summary>
        /// For <see cref="Collection{DATA, CTRL}"/>
        /// to automation choose the prefab based on it's binding.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public sealed override bool CanHandle(object data)
        {
            return data is DATA;
        }
    }
}
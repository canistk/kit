using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public interface IDataUpdatable
    {
        public event System.Action EVENT_Updated;
    }
}
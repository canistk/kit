using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public abstract class View<BIND, DATA> : MonoBehaviour
        where BIND : Binding<DATA>
        where DATA : class
    {
        protected BIND bind;
        protected DATA data => bind.data;
        protected virtual void Awake()
        {
            if (bind == null)
                bind = GetComponentInParent<BIND>();
            if (bind == null)
                throw new System.Exception($"{typeof(BIND)} not found.");
            if (bind.data != null)
                InternalDataUpdate(bind.data);
            bind.EVENT_ViewUpdate += InternalDataUpdate;
        }
        protected virtual void OnDestroy()
        {
            if (bind == null)
                return;
            bind.EVENT_ViewUpdate -= InternalDataUpdate;
        }

        public void InternalDataUpdate(DATA data)
        {
            if (data == null)
                OnViewUpdateInvalid(data);
            else
                OnViewUpdate(data);
        }

        protected virtual void OnViewUpdateInvalid(object dataRef) { }

        protected abstract void OnViewUpdate(DATA data);
    }
}
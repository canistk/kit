using UnityEngine;
using Kit;
// [ExecuteInEditMode]
public class GenerateDebugMipMap : MonoBehaviour
{
    // This script will tint texture's mip levels in different colors
    // (1st level red, 2nd green, 3rd blue). You can use it to see
    // which mip levels are actually used and how.

    [ContextMenu("Mipmap Debug")]
    private void ReplaceDebugMipmap()
    {
        Renderer rend = GetComponent<Renderer>();

        // duplicate the original texture and assign to the material
        Texture2D texture = (Texture2D)Instantiate(rend.sharedMaterial.mainTexture);
        texture.hideFlags = HideFlags.DontSave;
        rend.sharedMaterial.mainTexture = texture;

        var mipCount = Mathf.Min(3, texture.mipmapCount);

        // tint each mip level
        for (var mip = 0; mip < mipCount; ++mip)
        {
            float pt = (float)(mipCount - mip) / (float)mipCount;
            Color32 mipColor = ColorExtend.GetJetColor(pt);
            Debug.Log(ColorExtend.ToRichText(mipColor, $"Mip level {mip}"));
            var cols = texture.GetPixels32(mip);
            for (var i = 0; i < cols.Length; ++i)
            {
                cols[i] = Color32.Lerp(cols[i], mipColor, 0.33f);
            }
            texture.SetPixels32(cols, mip);
        }

        // actually apply all SetPixels32, don't recalculate mip levels
        texture.Apply(false);
    }

    void OnEnable()
    {
        ReplaceDebugMipmap();
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public class MultiFollower : MonoBehaviour
    {
        public enum eUpdate
        {
            Update = 0,
            FixedUpdate,
            Period,
        }
        public eUpdate m_UpdateMethod = eUpdate.FixedUpdate;
        [DrawIf(nameof(m_UpdateMethod), eUpdate.Period, ComparisonType.Equals, DisablingType.DontDraw)]
        public float m_Internal = 0.25f;
        private float m_LastUpdate = 0f;

        public FollowerPreset[] m_Preset = { };

        #region System
        private void Update()
        {
            if (m_UpdateMethod == eUpdate.Update)
                InternalUpdate(Time.deltaTime * Time.timeScale);
        }

        private void FixedUpdate()
        {
            if (m_UpdateMethod == eUpdate.FixedUpdate)
                InternalUpdate(Time.fixedDeltaTime * Time.timeScale);
            if (m_UpdateMethod == eUpdate.Period)
            {
                if ((m_LastUpdate + m_Internal) > Time.timeSinceLevelLoad)
                    return;
                m_LastUpdate = Time.timeSinceLevelLoad;
                InternalUpdate(Time.fixedDeltaTime * Time.timeScale);
            }
        }
        #endregion System

        private void InternalUpdate(float deltaTime)
        {
            foreach (var p in m_Preset)
            {
                p.Follow(deltaTime);
            }
        }

        [ContextMenu("Apply current postiion different as offset")]
        protected void ApplyCurrentPositionDifferentAsOffset()
        {
            foreach (var p in m_Preset)
            {
                if (p != null && p.self && p.target)
                {
                    p.m_Offset = p.target.InverseTransformPoint(p.self.position);
                }
            }
        }

        [ContextMenu("Apply current rotation different as offset")]
        protected void ApplyCurrentRotationDifferenctAsOffset()
        {
            foreach (var p in m_Preset)
            {
                if (p != null && p.self && p.target)
                {
                    Quaternion diff = p.target.rotation.Inverse() * p.self.rotation;
                    p.m_OffsetRotate = diff.eulerAngles;
                }
            }
        }
    }

    [System.Serializable]
    public class FollowerPreset : Follower
    {
        public Transform self;
        public Transform target;

        public void Follow(float deltaTime)
        {
            if (self && target)
                Follow(self, target, deltaTime);
        }
    }
}
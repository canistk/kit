﻿using UnityEngine;

namespace Kit
{
    public class SimpleFollower : MonoBehaviour
    {
        public Transform m_Target = null;
        public enum eUpdate
        { 
            Update = 0,
            FixedUpdate,
            Period,
        }
        public eUpdate m_UpdateMethod = eUpdate.FixedUpdate;
        [DrawIf(nameof(m_UpdateMethod), eUpdate.Period, ComparisonType.Equals, DisablingType.DontDraw)]
        public float m_Internal = 0.25f;
        private float m_LastUpdate = 0f;

        public Follower m_Setting = new Follower();

        [Header("Optional")]
        public bool m_ResetCoordinatesOnEnable = true;
        [SerializeField, MaskField(typeof(eTransformCacheRef))] eTransformCacheRef m_ResetMask = eTransformCacheRef.Full;
#pragma warning disable 649
        private TransformCache m_LocalCoord;
#pragma warning restore 649
        private void Awake()
        {
            m_LocalCoord = new TransformCache(transform, eTransformCacheRef.Full);
        }

        private void OnEnable()
        {
            m_LocalCoord.AssignTo(transform, m_ResetMask);
        }

        private void Update()
        {
            if (m_UpdateMethod == eUpdate.Update)
                InternalUpdate();
        }

        private void FixedUpdate()
        {
            if (m_UpdateMethod == eUpdate.FixedUpdate)
                InternalUpdate();
            if (m_UpdateMethod == eUpdate.Period)
            {
                if ((m_LastUpdate + m_Internal) > Time.timeSinceLevelLoad)
                    return;
                m_LastUpdate = Time.timeSinceLevelLoad;
                InternalUpdate();
            }
        }

        private void InternalUpdate()
        {
            if (!GetTarget(out Transform target))
                return;

            m_Setting.Follow(transform, target, Time.deltaTime * Time.timeScale);
        }

        public void SetTarget(Transform newTarget)
        {
            m_Target = newTarget;
        }

        public virtual bool GetTarget(out Transform target)
        {
            target = m_Target;
            return target != null;
        }

        [ContextMenu("Apply current position different as offset")]
        protected void ApplyCurrentPositionDifferentAsOffset()
        {
            if (!GetTarget(out Transform target))
                return;

            m_Setting.m_Offset = target.InverseTransformPoint(transform.position);
        }

        [ContextMenu("Apply current rotation different as offset")]
        protected void ApplyCurrentRotationDifferenctAsOffset()
        {
            if (!GetTarget(out Transform target))
                return;

            Quaternion diff = target.rotation.Inverse() * transform.rotation;
            m_Setting.m_OffsetRotate = diff.eulerAngles;
        }

        /****
        private void OnDrawGizmos()
        {
            if (!GetTarget(out Transform target))
                return;

            Vector3 pos = target.TransformPoint(m_Offset);
            Quaternion rotate = target.rotation * Quaternion.Euler(m_OffsetRotate);
            GizmosExtend.DrawArrow(target.position, target.forward * 0.3f, Color.gray);
            GizmosExtend.DrawArrow(pos, rotate.GetForward() * 0.2f, Color.green);
        }
        //****/
    }
}
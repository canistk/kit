using UnityEngine;
namespace Kit
{
    [System.Serializable]
    public class Follower
    {
        public enum eTranslateMode
        {
            Snap = 0,
            Lerp,
            SmoothDamp
        }
        public bool m_FollowPosition = true;
        public Vector3 m_Offset = Vector3.zero;
        public eTranslateMode m_TranslateMode = eTranslateMode.Snap;
        // [DrawIf(nameof(m_TranslateMode), eTranslateMode.Snap, ComparisonType.NotEqual, DisablingType.DontDraw)]
        public float m_TranslateSpeed = 1f;

        public enum eRotationMode
        {
            Snap = 0,
            Slerp,
            SmoothDamp,
        }
        [Header("Rotation")]
        public bool m_FollowRotation = true;
        public Vector3 m_OffsetRotate = Vector3.zero;
        public eRotationMode m_RotationMode = eRotationMode.Snap;
        // [DrawIf(nameof(m_RotationMode), eRotationMode.Snap, ComparisonType.NotEqual, DisablingType.DontDraw)]
        public float m_RotationSpeed = 1f;
#pragma warning disable 649
        private struct SmoothDampData
        {
            public float current, currentVelocity;
        }
        private SmoothDampData m_SmoothTranslate, m_SmoothRotation;
#pragma warning restore 649

        private static readonly Vector3 Vector3zero = Vector3.zero;
        public void Follow(Transform self, Transform target, float deltaTime)
        {
            Vector3 pos = target.position;
            Quaternion rotate = target.rotation;

            if (m_FollowPosition && m_Offset != Vector3zero)
                pos = target.TransformPoint(m_Offset);

            if (m_FollowRotation && m_OffsetRotate != Vector3zero)
                rotate *= Quaternion.Euler(m_OffsetRotate);

            if (m_FollowPosition && m_FollowRotation &&
                m_TranslateMode == eTranslateMode.Snap &&
                m_RotationMode == eRotationMode.Snap)
            {
                // Optimize for common case, early return
                self.SetPositionAndRotation(pos, rotate);
                return;
            }

            if (m_FollowPosition)
            {
                switch (m_TranslateMode)
                {
                    case eTranslateMode.Snap:
                        self.position = pos;
                        break;
                    case eTranslateMode.Lerp:
                        self.position = Vector3.Lerp(self.position, pos, deltaTime * m_TranslateSpeed);
                        break;
                    case eTranslateMode.SmoothDamp:
                    {
                        float totalDis = (pos - self.position).magnitude;
                        if (totalDis != 0f)
                        {
                            float moveableDis = m_TranslateSpeed * deltaTime;
                            float dampingDis = deltaTime * Mathf.SmoothDamp(m_SmoothTranslate.current, m_TranslateSpeed, ref m_SmoothTranslate.currentVelocity, deltaTime);
                            float finalDis = Mathf.Min(dampingDis, moveableDis, totalDis);
                            float pt = finalDis / totalDis;
                            self.position = Vector3.Lerp(self.position, pos, pt);
                        }
                    }
                    break;
                }
            }

            if (m_FollowRotation)
            {
                switch (m_RotationMode)
                {
                    case eRotationMode.Snap:
                        self.rotation = rotate;
                        break;
                    case eRotationMode.Slerp:
                        self.rotation = Quaternion.Slerp(self.rotation, rotate, deltaTime * m_RotationSpeed);
                        break;
                    case eRotationMode.SmoothDamp:
                    {
                        float totalAngular = Quaternion.Angle(self.rotation, rotate);
                        if (totalAngular != 0f)
                        {
                            float moveableAngular = m_RotationSpeed * deltaTime;
                            float dampingAngular = deltaTime * Mathf.SmoothDampAngle(m_SmoothRotation.current, m_RotationSpeed, ref m_SmoothRotation.currentVelocity, deltaTime);
                            float finalAngular = Mathf.Min(dampingAngular, moveableAngular, totalAngular);
                            float pt = finalAngular / totalAngular;
                            self.rotation = Quaternion.Slerp(self.rotation, rotate, pt);
                        }
                    }
                    break;
                }
            }
        }
    }
}
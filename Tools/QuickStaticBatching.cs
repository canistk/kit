﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    public class QuickStaticBatching : MonoBehaviour
    {
		[SerializeField] GameObject m_Root;

		[SerializeField] GameObject[] m_Children = { };

		private void Reset()
		{
			m_Root = gameObject;
		}

		private void OnEnable()
		{
			ApplyStaticBatch();
		}

		private void ApplyStaticBatch()
		{
			m_Root.isStatic = true;
			if (m_Children?.Length == 0)
			{
				foreach (Transform child in transform)
					child.gameObject.isStatic = true;
				StaticBatchingUtility.Combine(m_Root);
			}
			else
			{
				foreach (GameObject child in m_Children)
					child.isStatic = true;
				StaticBatchingUtility.Combine(m_Children, m_Root);
			}
		}
	}
}
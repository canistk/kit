using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    [RequireComponent(typeof(Collider))]
    public class IgnoreCollider : MonoBehaviour
    {
        [SerializeField] Collider m_Self = null;
        [SerializeField] Transform m_IgnoreChildColliders = null;
        [SerializeField] Collider[] m_IgnoreColliders = { };

        private void Reset()
        {
            if (m_Self == null)
                m_Self = GetComponent<Collider>();
        }

        private void Awake()
        {
            if (m_Self == null)
                m_Self = GetComponent<Collider>();

            if (m_IgnoreChildColliders != null)
            {
                Collider[] arr = GetComponentsInChildren<Collider>(true);
                foreach(var c in arr)
                {
                    if (c.isTrigger)
                        continue;
                    IgnoreCollision(c, true);
                }

                foreach (var c in m_IgnoreColliders)
                {
                    if (c == null || c.isTrigger)
                        continue;
                    IgnoreCollision(c, true);
                }
            }
        }

        public void IgnoreCollision(Collider other, bool ignore)
        {
            if (other == m_Self)
                return;
            Physics.IgnoreCollision(m_Self, other, ignore);
        }
    }
}
﻿using UnityEngine;
using System;
using System.Collections;

namespace Kit
{
	public class CLogEnableDisableAwakeStart : MonoBehaviour
	{
		
		
		[System.Flags]
		private enum eTrackState
		{
			None		= 0,
			Constructor	= 1 << 0,
			Awake		= 1 << 1,
			Start		= 1 << 2,
			OnEnable	= 1 << 3,
			OnDisable	= 1 << 4,
			OnDestroy	= 1 << 5,
			OnCollisionEnter	= 1 << 6,
			OnCollisionExit		= 1 << 7,
			OnTriggerEnter		= 1 << 8,
			OnTriggerExit		= 1 << 9,
		}

		[SerializeField, MaskField(typeof(eTrackState))] eTrackState m_TrackState = eTrackState.Awake | eTrackState.Start | eTrackState.OnEnable | eTrackState.OnDisable | eTrackState.OnDestroy;

		[Header("Action")]
		[SerializeField] private bool m_giveWarnings = false;

		[SerializeField] private bool m_pause = false;

		[SerializeField] private bool m_activeDetails = false;

		[SerializeField] private bool m_frameCountAndTime = false;

		public CLogEnableDisableAwakeStart()
		{
			if ((m_TrackState & eTrackState.Constructor) != 0)
			{
				// temporarily disable time statistics, since this isn't the Unity thread:
				bool cachedTimePref = m_frameCountAndTime;
				m_frameCountAndTime = false;
				string msg = MessageTemplate("constructor");
				m_frameCountAndTime = cachedTimePref;

				DisplayLog(msg);
				// WARNING: I'm not sure if Debug.Break works outside the Unity thread.
			}
		}

		private void DisplayLog(string msg)
		{
			if (m_giveWarnings)
				Debug.LogWarning(msg, this);
			else
				Debug.Log(msg, this);

			if (m_pause)
				Debug.Break();
		}


		void Awake()
		{
			if ((m_TrackState & eTrackState.Awake) != 0)
			{
				string msg = MessageTemplate("awake");
				DisplayLog(msg);
			}
		}

		void Start()
		{
			if ((m_TrackState & eTrackState.Start) != 0)
			{
				string msg = MessageTemplate("start");
				DisplayLog(msg);
			}
		}

		void OnEnable()
		{
			if ((m_TrackState & eTrackState.OnEnable) != 0)
			{
				string msg = MessageTemplate("enabled");
				DisplayLog(msg);
			}
		}

		void OnDisable()
		{
			if ((m_TrackState & eTrackState.OnDisable) != 0)
			{
				string msg = MessageTemplate("disabled");
				DisplayLog(msg);
			}
		}

		void OnDestroy()
		{
			if ((m_TrackState & eTrackState.OnDestroy) != 0)
			{
				string msg = MessageTemplate("destroyed");
				DisplayLog(msg);
			}
		}

		void OnCollisionEnter(Collision collision)
		{
			if ((m_TrackState & eTrackState.OnCollisionEnter) != 0)
			{
				string msg = MessageTemplate("collision enter " + collision.gameObject.name);
				DisplayLog(msg);
			}
		}

		void OnCollisionExit(Collision collision)
		{
			if ((m_TrackState & eTrackState.OnCollisionExit) != 0)
			{
				string msg = MessageTemplate("collision exit " + collision.gameObject.name);
				DisplayLog(msg);
			}
		}

		void OnTriggerEnter(Collider other)
		{

			if ((m_TrackState & eTrackState.OnTriggerEnter) != 0)
			{
				string msg = MessageTemplate("trigger enter " + other.name);
				DisplayLog(msg);
			}
		}

		void OnTriggerExit(Collider other)
		{

			if ((m_TrackState & eTrackState.OnTriggerExit) != 0)
			{
				string msg = MessageTemplate("trigger exit " + other.name);
				DisplayLog(msg);
			}
		}

		private string MessageTemplate(string _event)
		{
			return name + " " + _event + "." +
				(m_activeDetails ?
					" (Active self/scene: " + gameObject.activeSelf + "/" + gameObject.activeInHierarchy + ")" :
					"") +
					(m_frameCountAndTime ? " (" + Time.frameCount + ", " + Time.timeSinceLevelLoad + " seconds since level load)" : "");
		}
	}
}
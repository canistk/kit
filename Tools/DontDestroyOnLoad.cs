﻿using UnityEngine;

namespace Kit
{
	public class DontDestroyOnLoad : MonoBehaviour
	{
		void Start()
		{
			DontDestroyOnLoad(this);
		}
	}
}

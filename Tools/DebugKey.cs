using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Kit
{
    public class DebugKey : MonoBehaviour
    {
        [SerializeField] KeyCode m_DebugKey = KeyCode.Mouse0;
        public UnityEvent EVENT_Trigger = new UnityEvent();

        private void Update()
        {
            if (Input.GetKeyDown(m_DebugKey))
            {
                EVENT_Trigger.Invoke();
            }
        }
    }
}
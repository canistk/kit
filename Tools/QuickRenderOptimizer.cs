﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Kit
{
    public class QuickRenderOptimizer : MonoBehaviour
    {
		[System.Serializable]
		private class MeshConfig
		{
			public static MeshConfig Default = new MeshConfig();
			public static MeshConfig Min = new MeshConfig()
			{
				m_ShadowCastingMode = ShadowCastingMode.Off,
				m_ReceiveShadow = false,
				m_LightProbeUsage = LightProbeUsage.Off,
				m_ReflectionProbeUsage = ReflectionProbeUsage.Off,
				m_ProbeAnchor = null,
				m_MotionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion,
			};

			public bool m_Apply = true;
			public ShadowCastingMode m_ShadowCastingMode = ShadowCastingMode.On;
			public bool m_ReceiveShadow = true;

			public LightProbeUsage m_LightProbeUsage = LightProbeUsage.BlendProbes;
			public ReflectionProbeUsage m_ReflectionProbeUsage = ReflectionProbeUsage.BlendProbes;
			public Transform m_ProbeAnchor = null;

			public MotionVectorGenerationMode m_MotionVectorGenerationMode = MotionVectorGenerationMode.Object;
		}
		[SerializeField] MeshConfig m_MeshConfig = new MeshConfig();

		[System.Serializable]
		private class SkinnedMeshConfig
		{
			public static SkinnedMeshConfig Default = new SkinnedMeshConfig();
			public static SkinnedMeshConfig Min = new SkinnedMeshConfig()
			{
				m_ShadowCastingMode = ShadowCastingMode.Off,
				m_ReceiveShadow = false,
				m_LightProbeUsage = LightProbeUsage.Off,
				m_ReflectionProbeUsage = ReflectionProbeUsage.Off,
				m_ProbeAnchor = null,
				m_SkinnedMotionVectors = false,
			};
			public bool m_Apply = true;
			public ShadowCastingMode m_ShadowCastingMode = ShadowCastingMode.On;
			public bool m_ReceiveShadow = true;
			
			public LightProbeUsage m_LightProbeUsage = LightProbeUsage.BlendProbes;
			public ReflectionProbeUsage m_ReflectionProbeUsage = ReflectionProbeUsage.BlendProbes;
			public Transform m_ProbeAnchor = null;

			public bool m_SkinnedMotionVectors = true;
		}
		[SerializeField] SkinnedMeshConfig m_SkinnedMeshConfig = new SkinnedMeshConfig();

		[ContextMenu("Set Min")]
		private void SetMin()
		{
			m_MeshConfig = MeshConfig.Min;
			m_SkinnedMeshConfig = SkinnedMeshConfig.Min;
		}

		[ContextMenu("Set Default")]
		private void SetDefault()
		{
			m_MeshConfig = MeshConfig.Default;
			m_SkinnedMeshConfig = SkinnedMeshConfig.Default;
		}

		private void OnEnable()
		{
			ApplySetting();
		}

		[ContextMenu("Apply Setting")]
		private void ApplySetting()
		{
			Renderer[] renderers = transform.GetComponentsInChildren<Renderer>();
			foreach (var renderer in renderers)
			{
				if (m_MeshConfig.m_Apply && renderer is MeshRenderer mr)
				{
					mr.shadowCastingMode = m_MeshConfig.m_ShadowCastingMode;
					mr.receiveShadows = m_MeshConfig.m_ReceiveShadow;
					mr.lightProbeUsage = m_MeshConfig.m_LightProbeUsage;
					mr.reflectionProbeUsage = m_MeshConfig.m_ReflectionProbeUsage;
					mr.probeAnchor = m_MeshConfig.m_ProbeAnchor;
					mr.motionVectorGenerationMode = m_MeshConfig.m_MotionVectorGenerationMode;
				}
				else if (renderer is SkinnedMeshRenderer skin)
				{
					skin.shadowCastingMode = m_SkinnedMeshConfig.m_ShadowCastingMode;
					skin.receiveShadows = m_SkinnedMeshConfig.m_ReceiveShadow;
					skin.lightProbeUsage = m_SkinnedMeshConfig.m_LightProbeUsage;
					skin.reflectionProbeUsage = m_SkinnedMeshConfig.m_ReflectionProbeUsage;
					skin.probeAnchor = m_SkinnedMeshConfig.m_ProbeAnchor;
					skin.skinnedMotionVectors = m_SkinnedMeshConfig.m_SkinnedMotionVectors;
				}
			}
		}
	}
}
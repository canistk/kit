﻿using UnityEngine;
namespace Kit
{
    public class HingeJointCache : JointCache
    {
        public JointMotor motor { get; set; }
        public JointLimits limits { get; set; }
        public JointSpring spring { get; set; }
        public bool useMotor { get; set; }
        public bool useLimits { get; set; }
        public bool useSpring { get; set; }
        public HingeJointCache(HingeJoint hingeJoint) : base(hingeJoint)
        {
            var x = this;
            var y = hingeJoint;
            x.motor = y.motor;
            x.limits = y.limits;
            x.spring = y.spring;
            x.useMotor = y.useMotor;
            x.useLimits = y.useLimits;
            x.useSpring = y.useSpring;
        }

        public override void AssignTo<T>(T joint)
        {
            base.AssignTo(joint);
            HingeJoint x = joint as HingeJoint;
            Debug.Assert(x != null, $"Error {joint} must be {typeof(HingeJoint)}");
            var y = this;
            x.motor = y.motor;
            x.limits = y.limits;
            x.spring = y.spring;
            x.useMotor = y.useMotor;
            x.useLimits = y.useLimits;
            x.useSpring = y.useSpring;
        }
    }
}
﻿using UnityEngine;
namespace Kit
{
    public class ConfigurableJointCache : JointCache
    {
        public float projectionAngle { get; set; }
        public float projectionDistance { get; set; }
        public JointProjectionMode projectionMode { get; set; }
        public JointDrive slerpDrive { get; set; }
        public JointDrive angularYZDrive { get; set; }
        public JointDrive angularXDrive { get; set; }
        public RotationDriveMode rotationDriveMode { get; set; }
        public Vector3 targetAngularVelocity { get; set; }
        public Quaternion targetRotation { get; set; }
        public JointDrive zDrive { get; set; }
        public JointDrive yDrive { get; set; }
        public JointDrive xDrive { get; set; }
        public Vector3 targetVelocity { get; set; }
        public Vector3 targetPosition { get; set; }
        public SoftJointLimit angularZLimit { get; set; }
        public SoftJointLimit angularYLimit { get; set; }
        public SoftJointLimit highAngularXLimit { get; set; }
        public SoftJointLimit lowAngularXLimit { get; set; }
        public SoftJointLimit linearLimit { get; set; }
        public SoftJointLimitSpring angularYZLimitSpring { get; set; }
        public SoftJointLimitSpring angularXLimitSpring { get; set; }
        public SoftJointLimitSpring linearLimitSpring { get; set; }
        public ConfigurableJointMotion angularZMotion { get; set; }
        public ConfigurableJointMotion angularYMotion { get; set; }
        public ConfigurableJointMotion angularXMotion { get; set; }
        public ConfigurableJointMotion zMotion { get; set; }
        public ConfigurableJointMotion yMotion { get; set; }
        public ConfigurableJointMotion xMotion { get; set; }
        public Vector3 secondaryAxis { get; set; }
        public bool configuredInWorldSpace { get; set; }
        public bool swapBodies { get; set; }
        public ConfigurableJointCache(ConfigurableJoint configurableJoint) : base(configurableJoint)
        {
            var x = this;
            var y = configurableJoint;
            x.projectionAngle = y.projectionAngle;
            x.projectionDistance = y.projectionDistance;
            x.projectionMode = y.projectionMode;
            x.slerpDrive = y.slerpDrive;
            x.angularYZDrive = y.angularYZDrive;
            x.angularXDrive = y.angularXDrive;
            x.rotationDriveMode = y.rotationDriveMode;
            x.targetAngularVelocity = y.targetAngularVelocity;
            x.targetRotation = y.targetRotation;
            x.zDrive = y.zDrive;
            x.yDrive = y.yDrive;
            x.xDrive = y.xDrive;
            x.targetVelocity = y.targetVelocity;
            x.targetPosition = y.targetPosition;
            x.angularZLimit = y.angularZLimit;
            x.angularYLimit = y.angularYLimit;
            x.highAngularXLimit = y.highAngularXLimit;
            x.lowAngularXLimit = y.lowAngularXLimit;
            x.linearLimit = y.linearLimit;
            x.angularYZLimitSpring = y.angularYZLimitSpring;
            x.angularXLimitSpring = y.angularXLimitSpring;
            x.linearLimitSpring = y.linearLimitSpring;
            x.angularZMotion = y.angularZMotion;
            x.angularYMotion = y.angularYMotion;
            x.angularXMotion = y.angularXMotion;
            x.zMotion = y.zMotion;
            x.yMotion = y.yMotion;
            x.xMotion = y.xMotion;
            x.secondaryAxis = y.secondaryAxis;
            x.configuredInWorldSpace = y.configuredInWorldSpace;
            x.swapBodies = y.swapBodies;
        }
        public override void AssignTo<T>(T joint)
        {
            base.AssignTo(joint);
            var x = joint as ConfigurableJoint;
            Debug.Assert(x != null, $"Error {joint} must be {typeof(ConfigurableJoint)}");
            var y = this;
            x.projectionAngle = y.projectionAngle;
            x.projectionDistance = y.projectionDistance;
            x.projectionMode = y.projectionMode;
            x.slerpDrive = y.slerpDrive;
            x.angularYZDrive = y.angularYZDrive;
            x.angularXDrive = y.angularXDrive;
            x.rotationDriveMode = y.rotationDriveMode;
            x.targetAngularVelocity = y.targetAngularVelocity;
            x.targetRotation = y.targetRotation;
            x.zDrive = y.zDrive;
            x.yDrive = y.yDrive;
            x.xDrive = y.xDrive;
            x.targetVelocity = y.targetVelocity;
            x.targetPosition = y.targetPosition;
            x.angularZLimit = y.angularZLimit;
            x.angularYLimit = y.angularYLimit;
            x.highAngularXLimit = y.highAngularXLimit;
            x.lowAngularXLimit = y.lowAngularXLimit;
            x.linearLimit = y.linearLimit;
            x.angularYZLimitSpring = y.angularYZLimitSpring;
            x.angularXLimitSpring = y.angularXLimitSpring;
            x.linearLimitSpring = y.linearLimitSpring;
            x.angularZMotion = y.angularZMotion;
            x.angularYMotion = y.angularYMotion;
            x.angularXMotion = y.angularXMotion;
            x.zMotion = y.zMotion;
            x.yMotion = y.yMotion;
            x.xMotion = y.xMotion;
            x.secondaryAxis = y.secondaryAxis;
            x.configuredInWorldSpace = y.configuredInWorldSpace;
            x.swapBodies = y.swapBodies;
        }
    }
}
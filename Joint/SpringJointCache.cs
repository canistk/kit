﻿using UnityEngine;
namespace Kit
{
    public class SpringJointCache : JointCache
    {
        public float spring { get; set; }
        public float damper { get; set; }
        public float minDistance { get; set; }
        public float maxDistance { get; set; }
        public float tolerance { get; set; }
        public SpringJointCache(SpringJoint springJoint) : base(springJoint)
        {
            var x = this;
            var y = springJoint;
            x.spring = y.spring;
            x.damper = y.damper;
            x.minDistance = y.minDistance;
            x.maxDistance = y.maxDistance;
            x.tolerance = y.tolerance;
        }

        public override void AssignTo<T>(T joint)
        {
            base.AssignTo(joint);
            var x = joint as SpringJoint;
            Debug.Assert(x != null, $"Error {joint} must be {typeof(SpringJoint)}");
            var y = this;
            x.spring = y.spring;
            x.damper = y.damper;
            x.minDistance = y.minDistance;
            x.maxDistance = y.maxDistance;
            x.tolerance = y.tolerance;
        }
    }
}
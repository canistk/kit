﻿using UnityEngine;
namespace Kit
{
    public class FixedJointCache : JointCache
    {
        public FixedJointCache(FixedJoint fixedJoint) : base(fixedJoint) { }
    }
}
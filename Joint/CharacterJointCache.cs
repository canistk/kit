﻿using UnityEngine;
namespace Kit
{
    public class CharacterJointCache : JointCache
    {
        public Vector3 swingAxis { get; set; }
        public SoftJointLimitSpring twistLimitSpring { get; set; }
        public SoftJointLimitSpring swingLimitSpring { get; set; }
        public SoftJointLimit lowTwistLimit { get; set; }
        public SoftJointLimit highTwistLimit { get; set; }
        public SoftJointLimit swing1Limit { get; set; }
        public SoftJointLimit swing2Limit { get; set; }
        public bool enableProjection { get; set; }
        public float projectionDistance { get; set; }
        public float projectionAngle { get; set; }
        public CharacterJointCache(CharacterJoint characterJoint) : base(characterJoint)
        {
            var x = this;
            var y = characterJoint;
            x.swingAxis = y.swingAxis;
            x.twistLimitSpring = y.twistLimitSpring;
            x.swingLimitSpring = y.swingLimitSpring;
            x.lowTwistLimit = y.lowTwistLimit;
            x.highTwistLimit = y.highTwistLimit;
            x.swing1Limit = y.swing1Limit;
            x.swing2Limit = y.swing2Limit;
            x.enableProjection = y.enableProjection;
            x.projectionDistance = y.projectionDistance;
            x.projectionAngle = y.projectionAngle;
        }

        public override void AssignTo<T>(T joint)
        {
            base.AssignTo(joint);
            var x = joint as CharacterJoint;
            Debug.Assert(x != null, $"Error {joint} must be {typeof(CharacterJoint)}");
            var y = this;
            x.swingAxis = y.swingAxis;
            x.twistLimitSpring = y.twistLimitSpring;
            x.swingLimitSpring = y.swingLimitSpring;
            x.lowTwistLimit = y.lowTwistLimit;
            x.highTwistLimit = y.highTwistLimit;
            x.swing1Limit = y.swing1Limit;
            x.swing2Limit = y.swing2Limit;
            x.enableProjection = y.enableProjection;
            x.projectionDistance = y.projectionDistance;
            x.projectionAngle = y.projectionAngle;
        }
    }
}
﻿using UnityEngine;

namespace Kit
{
    public abstract class JointCache
    {
        public Rigidbody connectedBody { get; set; }
        public Vector3 axis { get; set; }
        public Vector3 anchor { get; set; }
        public Vector3 connectedAnchor { get; set; }
        public bool autoConfigureConnectedAnchor { get; set; }
        public float breakForce { get; set; }
        public float breakTorque { get; set; }
        public bool enableCollision { get; set; }
        public bool enablePreprocessing { get; set; }
        public float massScale { get; set; }
        public float connectedMassScale { get; set; }

        public JointCache(Joint joint)
        {
            var x = this;
            var y = joint;
            x.connectedBody = y.connectedBody;
            x.axis = y.axis;
            x.anchor = y.anchor;
            x.connectedAnchor = y.connectedAnchor;
            x.autoConfigureConnectedAnchor = y.autoConfigureConnectedAnchor;
            x.breakForce = y.breakForce;
            x.breakTorque = y.breakTorque;
            x.enableCollision = y.enableCollision;
            x.enablePreprocessing = y.enablePreprocessing;
            x.massScale = y.massScale;
            x.connectedMassScale = y.connectedMassScale;
        }

        public static JointCache Record(Joint joint)
        {
            if (joint is ConfigurableJoint configurableJoint)
                return new ConfigurableJointCache(configurableJoint);
            if (joint is CharacterJoint characterJoint)
                return new CharacterJointCache(characterJoint);
            if (joint is SpringJoint springJoint)
                return new SpringJointCache(springJoint);
            if (joint is HingeJoint hingeJoint)
                return new HingeJointCache(hingeJoint);
            if (joint is FixedJoint fixedJoint)
                return new FixedJointCache(fixedJoint);

            // 2D ?
            throw new System.NotImplementedException();
        }

        public Joint Create(GameObject go)
        {
            if (this is ConfigurableJointCache)
            {
                var joint = go.AddComponent<ConfigurableJoint>();
                AssignTo(joint); return joint;
            }
            else if (this is CharacterJointCache)
            {
                var joint = go.AddComponent<CharacterJoint>();
                AssignTo(joint); return joint;
            }
            else if (this is SpringJointCache)
            {
                var joint = go.AddComponent<SpringJoint>();
                AssignTo(joint); return joint;
            }
            else if (this is HingeJointCache)
            {
                var joint = go.AddComponent<HingeJoint>();
                AssignTo(joint); return joint;
            }
            else if (this is FixedJointCache)
            {
                var joint = go.AddComponent<FixedJoint>();
                AssignTo(joint); return joint;
            }
            else
                throw new System.NotImplementedException(); // 2D ?
        }

        public virtual void AssignTo<T>(T joint) where T : Joint
        {
            var x = joint;
            var y = this;
            x.connectedBody = y.connectedBody;
            x.axis = y.axis;
            x.anchor = y.anchor;
            x.connectedAnchor = y.connectedAnchor;
            x.autoConfigureConnectedAnchor = y.autoConfigureConnectedAnchor;
            x.breakForce = y.breakForce;
            x.breakTorque = y.breakTorque;
            x.enableCollision = y.enableCollision;
            x.enablePreprocessing = y.enablePreprocessing;
            x.massScale = y.massScale;
            x.connectedMassScale = y.connectedMassScale;
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Kit
{
    public struct Vector3Damp
    {
        private float m_CurrentSpeed;
        private float m_CurrentVelocity;
        /// <summary>Calculate movement speed based on input with damping</summary>
        /// <param name="vector01"></param>
        /// <param name="targetSpeed"></param>
        /// <param name="damping"></param>
        /// <param name="maxSpeed"></param>
        /// <param name="deltaTime"></param>
        /// <returns></returns>
        public Vector3 SmoothDamp(in Vector3 vector01, float targetSpeed, float damping, float maxSpeed, float deltaTime)
        {
            Vector3 v = Vector3.ClampMagnitude(vector01, 1f);
            float speed = v.magnitude * targetSpeed;
            if (speed > maxSpeed)
                speed = maxSpeed;
            if (float.IsNaN(m_CurrentVelocity))
                m_CurrentVelocity = 0f;
            m_CurrentSpeed = Mathf.SmoothDamp(m_CurrentSpeed, speed, ref m_CurrentVelocity, damping, maxSpeed, deltaTime);
            if (float.IsNaN(m_CurrentSpeed))
                m_CurrentSpeed = 0f;
            return v * m_CurrentSpeed * deltaTime;
        }

        /// <summary>Calculate movement speed based on input with damping</summary>
        /// <param name="vector01"></param>
        /// <param name="targetSpeed"></param>
        /// <param name="damping"></param>
        /// <param name="maxSpeed"></param>
        public Vector3 SmoothDamp(in Vector3 vector01, float targetSpeed, float damping, float maxSpeed)
        {
            Vector3 v = Vector3.ClampMagnitude(vector01, 1f);
            float speed = v.magnitude * targetSpeed;
            if (speed > maxSpeed)
                speed = maxSpeed;
            if (float.IsNaN(m_CurrentVelocity))
                m_CurrentVelocity = 0f;
            m_CurrentSpeed = Mathf.SmoothDamp(m_CurrentSpeed, speed, ref m_CurrentVelocity, damping, maxSpeed);
            if (float.IsNaN(m_CurrentSpeed))
                m_CurrentSpeed = 0f;
            return v * m_CurrentSpeed;
        }
    }
}
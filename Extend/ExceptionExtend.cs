using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Kit
{
    using StackTrace = System.Diagnostics.StackTrace;
    using StackFrame = System.Diagnostics.StackFrame;
    public static class ExceptionExtend
    {
        public static void DeepLogInvocationException(this Exception ex, string eventDispatcherName, System.Delegate delegatehandler, int maxDepth = -1)
        {
            ex.DeepLogInvocationException($"{eventDispatcherName} > {(delegatehandler?.Target ?? "Unknown")}", maxDepth);
        }

        /// <summary>A helper to log exception stack trace in a more readable way.</summary>
        /// <param name="ex"></param>
        /// <param name="delegateName">reference for method's name or any other message.</param>
        /// <param name="maxDepth">-1 mean no limit</param>
        public static void DeepLogInvocationException(this Exception ex, string delegateName, int maxDepth = -1)
        {
            int depth = 0;
            Exception orgEx = ex;
            List<Exception> exStack = new List<Exception>(Mathf.Max(maxDepth, 2));
            while (ex != null && ex.InnerException != null &&
                (depth++ < maxDepth || maxDepth == -1))
            {
                exStack.Add(ex);
                ex = ex.InnerException;
            }

            // Fall back when no exception was logged
            if (exStack.Count == 0)
            {
                if (TryGetException(orgEx, out var stackTraceDetail))
                {
                    Debug.LogError($"{orgEx.GetType().Name} during \"{delegateName}\" > \"{orgEx.Message}\"\n\n{stackTraceDetail}\n-EOF\n");
                }
                else
                {
                    Debug.LogError($"{orgEx.GetType().Name} during \"{delegateName}\" > \"{orgEx.Message}\"\n\n{orgEx.StackTrace}\n-EOF\n");
                }
            }
            else
            {
                PrintInnerException(exStack);
            }

            void PrintInnerException(List<Exception> exStack)
            {
                int i = exStack.Count;
                while (i-- > 0)
                {
                    var ev2 = exStack[i];
                    if (TryGetException(ev2, out var stackTraceDetail))
                    {
                        Debug.LogError($"{ev2.GetType().Name}[{exStack.Count - i}] \"{delegateName}\" > \"{ev2.Message}\"\n\n{stackTraceDetail}\n");
                    }
                    else
                    {
                        Debug.LogError($"{ev2.GetType().Name}[{exStack.Count - i}] \"{delegateName}\" > \"{ev2.Message}\"\n\n{ex.StackTrace}\n");
                    }
                }
            }

            bool TryGetException(Exception exception, out string info)
            {
                StringBuilder sb = new StringBuilder();
                StackTrace trace = new(exception, true);
                for (var k = 0; k < trace.FrameCount; ++k)
                {
                    if (TryGetFrameInfo(trace.GetFrame(k), out var line))
                    {
                        sb.AppendLine(line);
                    }
                }
                info = sb.ToString();
                return info.Length > 0;
            }

            bool TryGetFrameInfo(StackFrame frame, out string info)
            {
                info = null;
                if (frame == null)
                    return false;
                var filePath = frame.GetFileName();
                if (filePath == null || filePath.Length == 0)
                    return false;
                var fileName            = System.IO.Path.GetFileName(filePath);
                var fullDir             = System.IO.Path.GetDirectoryName(filePath);
                var buildInScriptIdx    = fullDir.LastIndexOf("Assets");
                var shortDir            = buildInScriptIdx < 0 ? $"../{fileName}" : fullDir.Substring(buildInScriptIdx);
                var lineNo              = frame.GetFileLineNumber();
                var methodLong          = frame.GetMethod().Name;
                var a0                  = methodLong.IndexOf('<');
                var a1                  = methodLong.IndexOf('>');
                var shortName           = a0 != 1 && a1 != -1 ? methodLong.Substring(a0 + 1, a1 - a0 - 1) : methodLong;
                var lineStr             = $"{shortDir}:{lineNo}";

                info = $"{fileName}:{Color.yellow.ToRichText(shortName)}() (at {lineStr.Hyperlink(filePath, lineNo)})";
                return true;
            }
        }

        public static string Hyperlink(this string tag, string url)
        {
            return $"<a href=\"{url}\">{tag}</a>";
        }
        public static string Hyperlink(this string tag, string url, int line)
        {
            return $"<a href=\"{url}\" line=\"{line}\">{tag}</a>";
        }
    }
}
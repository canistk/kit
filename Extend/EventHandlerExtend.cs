﻿using System;

namespace Kit
{
    public static class EventHandlerExtend
    {
        public static void SafeInvoke<EVENT>(this EventHandler<EVENT> eventHandler, object sender, EVENT args)
            where EVENT : EventArgs
        {
            if (eventHandler == null)
                return;
            eventHandler.Invoke(sender, args);
        }

        /// <summary>A event dispatcher helper to dispatch event with try catch block.</summary>
        /// <typeparam name="EVENT"></typeparam>
        /// <param name="evt"></param>
        /// <param name="singleDispatcher"></param>
        /// <param name="maxDepth"></param>
        /// <exception cref="System.Exception"></exception>
        public static void TryCatchDispatchEventError<EVENT>(this EVENT evt, System.Action<EVENT> singleDispatcher, int maxDepth = -1)
            where EVENT : System.MulticastDelegate
        {
            if (evt == null)
                return;
            if (singleDispatcher == null)
            {
                throw new System.Exception("Must define how dispatcher handle the event.");
            }
            var handles = evt.GetInvocationList();
            for (var i = 0; i < handles.Length; ++i)
            {
                try
                {
                    var dispatcher = handles[i] as EVENT;
                    if (dispatcher == null)
                        continue;
                    singleDispatcher?.Invoke(dispatcher);
                }
                catch (System.Exception ex)
                {
                    ex.DeepLogInvocationException(evt.GetType().Name, handles[i], maxDepth);
                }
            }
        }

    }
}

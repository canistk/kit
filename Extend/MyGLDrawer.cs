using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
#endif

namespace Kit
{
    [ExecuteAlways]
    internal class MyGLDrawer : MonoBehaviour
    {
        #region Singleton
        private static MyGLDrawer _instance = null;
        public static MyGLDrawer Instance
        {
            get
            {
                if (_instance == null)
                {
                    if (Camera.main != null)
                    {
                        _instance = Camera.main.gameObject.GetOrAddComponent<MyGLDrawer>();
                    }
                    if (Camera.current != null)
                    {
                        // _instance = Camera.current.gameObject.GetOrAddComponent<MyGLDrawer>();
                    }
                    // if (_instance) _instance.hideFlags = HideFlags.DontSave;
#if UNITY_EDITOR
                    //var sceneView = UnityEditor.SceneView.currentDrawingSceneView;
                    //if (sceneView != null && sceneView.camera != null)
                    //{
                    //    _instance = Camera.current.gameObject.GetOrAddComponent<MyGLDrawer>();
                    //}
#endif

                    if (_instance)
                        _instance.hideFlags = HideFlags.DontSave;
                }
                return _instance;
            }
        }
        public static MyGLDrawer InstanceRaw = _instance;
        #endregion Singleton

        const int s_MaxRequest = 10000;

        #region System
        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                return;
            }
            else if (_instance.GetInstanceID() == this.GetInstanceID())
                return; // already register

            // duplicate instance detected.
            this.enabled = false;
#if UNITY_EDITOR
            DestroyImmediate(this);
#else
            Destroy(gameObject);
#endif
        }

        private void OnRenderObject()
        {
            // if (m_LineRequests.Count > 0) Log($"{name} : OnRenderObject, {m_LineRequests.Count}");

            DrawLines();
            DrawLineTriangle();
            DrawSolidTriangle();
        }
        #endregion System

        #region Material
        private static Material m_VertexUnlit = null;
        public static Material VertexUnlit
        {
            get
            {
                if (m_VertexUnlit == null)
                {
                    m_VertexUnlit = new Material(Shader.Find("Hidden/Internal-Colored"));
                    m_VertexUnlit.hideFlags = HideFlags.HideAndDontSave;
                }
                return m_VertexUnlit;
            }
        }
        #endregion Material

        #region Line
        private struct LineRequest
        {
            public Color color;
            public Vector3 v0, v1;
        }
        private List<LineRequest> m_LineRequests = new List<LineRequest>(100);
        internal void AddLineJob(in Vector3 from, in Vector3 to, in Color color)
        {
            if (!this.enabled)
                return;
            if (m_LineRequests.Count > s_MaxRequest)
                return;
            m_LineRequests.Add(new LineRequest {
                v0 = from,
                v1 = to,
                color = color
            });
        }
        private void DrawLines()
        {
            if (m_LineRequests.Count > 0)
            {
                GL.PushMatrix();
                // GL.LoadPixelMatrix();
                VertexUnlit.SetPass(0);
                GL.Begin(GL.LINES);
                for (int i = 0; i < m_LineRequests.Count; ++i)
                {
                    GL.Color(m_LineRequests[i].color);
                    GL.Vertex(m_LineRequests[i].v0);
                    GL.Color(m_LineRequests[i].color);
                    GL.Vertex(m_LineRequests[i].v1);
                }
                GL.End();

                GL.PopMatrix();
                m_LineRequests.Clear();
            }
        }
        #endregion Line

        #region Triangle
        private struct TriangleRequest
        {
            public Color c0, c1, c2;
            public Vector3 v0, v1, v2;
        }
        private List<TriangleRequest> m_SolidTriangleRequests = new List<TriangleRequest>(100);
        internal void AddSoildTriangleJob(in Vector3 v0, in Vector3 v1, in Vector3 v2, in Color c0, in Color c1, in Color c2)
        {
            if (!this.enabled)
                return;
            if (m_SolidTriangleRequests.Count > s_MaxRequest)
                return;
            m_SolidTriangleRequests.Add(new TriangleRequest
            {
                v0 = v0, v1 = v1, v2 = v2,
                c0 = c0, c1 = c1, c2 = c2,
            });
        }
        private List<TriangleRequest> m_LineTriangleRequests = new List<TriangleRequest>(100);
        internal void AddLineTriangleJob(in Vector3 v0, in Vector3 v1, in Vector3 v2, in Color c0, in Color c1, in Color c2)
        {
            if (!this.enabled)
                return;
            if (m_LineTriangleRequests.Count > s_MaxRequest)
                return;
            m_LineTriangleRequests.Add(new TriangleRequest
            {
                v0 = v0,
                v1 = v1,
                v2 = v2,
                c0 = c0,
                c1 = c1,
                c2 = c2,
            });
        }
        private void DrawSolidTriangle()
        {
            if (m_SolidTriangleRequests.Count > 0)
            {
                GL.PushMatrix();
                // GL.LoadPixelMatrix();
                VertexUnlit.SetPass(0);
                GL.Begin(GL.TRIANGLES);
                for (int i = 0; i < m_SolidTriangleRequests.Count; ++i)
                {
                    GL.Color(m_SolidTriangleRequests[i].c0);
                    GL.Vertex(m_SolidTriangleRequests[i].v0);
                    GL.Color(m_SolidTriangleRequests[i].c1);
                    GL.Vertex(m_SolidTriangleRequests[i].v1);
                    GL.Color(m_SolidTriangleRequests[i].c2);
                    GL.Vertex(m_SolidTriangleRequests[i].v2);
                }
                GL.End();

                GL.PopMatrix();
                m_SolidTriangleRequests.Clear();
            }
        }

        private void DrawLineTriangle()
        {
            if (m_LineTriangleRequests.Count > 0)
            {
                GL.PushMatrix();
                // GL.LoadPixelMatrix();
                VertexUnlit.SetPass(0);
                for (int i = 0; i < m_LineTriangleRequests.Count; ++i)
                {
                    GL.Begin(GL.LINE_STRIP);
                    GL.Color(m_LineTriangleRequests[i].c0);
                    GL.Vertex(m_LineTriangleRequests[i].v0);
                    GL.Color(m_LineTriangleRequests[i].c1);
                    GL.Vertex(m_LineTriangleRequests[i].v1);
                    GL.Color(m_LineTriangleRequests[i].c2);
                    GL.Vertex(m_LineTriangleRequests[i].v2);
                    GL.Color(m_LineTriangleRequests[i].c0);
                    GL.Vertex(m_LineTriangleRequests[i].v0);
                    GL.End();
                }

                GL.PopMatrix();
                m_LineTriangleRequests.Clear();
            }
        }
        #endregion Triangle

        #region Debug Log
        private double m_LastLogTime = -1f;
        private void PeriodLog(string str)
        {
#if UNITY_EDITOR
            double _time = UnityEditor.EditorApplication.timeSinceStartup;
#else
            double _time = Time.timeSinceLevelLoadAsDouble;
#endif
            if (_time - m_LastLogTime > 3f)
            {
                Debug.Log(str);
                m_LastLogTime = _time;
            }
        }
        #endregion Debug Log
    }
}
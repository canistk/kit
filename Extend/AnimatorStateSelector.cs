﻿using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	[System.Serializable]
	public class AnimatorStateSelector
	{
		public Animator m_Animator;
		public int m_SelectedHash;
		public int m_LayerIndex;
		public string m_DisplayPath;
		public bool Valid
		{
			get
			{
				return m_Animator != null &&
					m_Animator.HasState(m_LayerIndex, m_SelectedHash);
			}
		}
	}
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kit;
namespace Kit.Test
{
    public class TestSignedAngle : MonoBehaviour
    {
        public Transform m_Dir0, m_Dir1;
        public Transform m_Normal;

        public Quaternion v1 => m_Dir0.rotation;
        public Quaternion v2 => m_Dir1.rotation;
        public Vector3 v1f => m_Dir0.forward;
        public Vector3 v2f => m_Dir1.forward;
        public Vector3 normal => m_Normal.up;

        [SerializeField] float m_M1Radius = 1f;
        [SerializeField] Color m_M1Color = Color.gray.CloneAlpha(.5f);
        [SerializeField] Color m_M1BColor = Color.gray.CloneAlpha(.2f);

        [SerializeField] float m_M2Radius = 1f;
        [SerializeField] Color m_M2Color = Color.green.CloneAlpha(.5f);
        [SerializeField] Color m_M2BColor = Color.gray.CloneAlpha(.2f);

        private void OnDrawGizmos()
        {
            if (m_Dir0 == null || m_Dir1 == null || m_Normal == null)
                return;


            Transform t = transform;

            float dot = Vector3.Dot(v1f, v2f);
            float degree = Vector3.Angle(v1f, v2f);
            string msg = $"Dot = {dot:F2} -> Degree {dot.DotToDegree():F2}\n" +
                $"Degree {degree:F2} -> Dot {degree.DegreeToDot():F2}";
            GizmosExtend.DrawLabel(t.position, msg, Vector2.down);


            Vector3 realNormal = Vector3.Cross(v1f, v2f);
            if (Vector3.Dot(normal, realNormal) < 0)
                realNormal = -realNormal;
            Vector3 projectedV1 = Vector3.ProjectOnPlane(v1f, normal);

            GizmosExtend.DrawRay(t.position, realNormal, Color.green.CloneAlpha(.5f));

            GizmosExtend.DrawRay(t.position, normal, Color.green);
            GizmosExtend.DrawRay(t.position, v1f, Color.red);
            GizmosExtend.DrawRay(t.position, v2f, Color.cyan);


            float m1Angle = v1.AngleBetweenDirectionSigned(v2, Vector3.forward, normal);
            GizmosExtend.DrawArc(t.position, realNormal, v1f, m1Angle, m_M1Radius, m_M1Color);
            GizmosExtend.DrawArc(t.position, normal, projectedV1, m1Angle, m_M1Radius, m_M1BColor);

            float m2Angle = v1.AngleProjectOnPlaneSigned(v2, Vector3.forward, normal);
            GizmosExtend.DrawArc(t.position, realNormal, v1f, m2Angle, m_M2Radius, m_M2Color);
            GizmosExtend.DrawArc(t.position, normal, projectedV1, m2Angle, m_M2Radius, m_M2BColor);

        }
    }
}
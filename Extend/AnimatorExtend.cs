﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor.Animations;
#endif
namespace Kit
{
	public static class AnimatorExtend
	{
		public static void CrossFade(this Animator animator,int stateHashName, int layer, AnimatorCrossFadeSetting setting)
        {
			animator.CrossFade(stateHashName,
				setting.normalizedTransitionDuration,
				layer,
				setting.normalizedTimeOffset,
				setting.normalizedTransitionTime);
		}
		public static void CrossFade(this Animator animator, AnimatorStateSelector selector, AnimatorCrossFadeSetting setting)
		{
			if (selector != null &&
				animator == selector.m_Animator)
			{
				animator.CrossFade(selector.m_SelectedHash, selector.m_LayerIndex, setting);
			}
		}
		public static void CrossFadeInFixedTime(this Animator animator, int stateHashName, int layer, AnimatorCrossFadeFixedSetting setting)
		{
			animator.CrossFadeInFixedTime(stateHashName,
				setting.fixedTransitionDuration,
				layer,
				setting.fixedTimeOffset,
				setting.normalizedTransitionTime);
		}
		public static void CrossFadeInFixedTime(this Animator animator, AnimatorStateSelector selector, AnimatorCrossFadeFixedSetting setting)
		{
			if (selector != null &&
				animator == selector.m_Animator)
			{
				animator.CrossFadeInFixedTime(selector.m_SelectedHash, selector.m_LayerIndex, setting);
			}
		}


		public static bool HasParameter(this Animator animator, string parameterName)
		{
			int hash = Animator.StringToHash(parameterName);
			foreach (AnimatorControllerParameter param in animator.parameters)
			{
				if (param.nameHash == hash)
					return true;
			}
			return false;
		}
		public static bool HasParameter(this Animator animator, string parameterName, AnimatorControllerParameterType type)
		{
			int hash = Animator.StringToHash(parameterName);
			foreach (AnimatorControllerParameter param in animator.parameters)
			{
				if (param.type == type && param.nameHash == hash)
					return true;
			}
			return false;
		}

		public static bool HasState(this Animator animator, string stateName, int layerIndex)
		{
			int stateId = Animator.StringToHash(stateName);
			return animator.HasState(layerIndex, stateId);
		}

		public static bool TryGetState(this Animator animator, string stateName, out AnimatorStateStruct state)
		{
			if (animator.isInitialized && !string.IsNullOrEmpty(stateName))
			{
				int hash = Animator.StringToHash(stateName);
				for (int i = 0; i < animator.layerCount; i++)
				{
					if (animator.HasState(i, hash))
					{
						state.layerIndex = i;
						state.name = stateName;
						state.shortNameHash = hash;
						return true;
					}
				}
			}
			state = default;
			return false;
		}

		public static bool Editor_TryGetMotion<T>(List<T> stateList, int hash, int layer, out Motion motion)
			where T : AnimatorStateCache
		{
#if UNITY_EDITOR
			for (int i = 0; i < stateList.Count; i++)
			{
				if (stateList[i].layer == layer &&
					stateList[i].fullNameHash == hash)
				{
					motion = stateList[i].motion;
					return true;
				}
			}
#endif
			motion = null;
			return false;
		}

		/// <summary>
		/// In order to fetch state information from AnimatorController
		/// </summary>
		/// <param name="animatorController"></param>
		/// <param name="stateList"></param>
		public static void Editor_BuildStateInfoList(RuntimeAnimatorController animatorController, out List<AnimatorStateCacheLite> stateList)
        {
#if UNITY_EDITOR
			AnimatorController ac = animatorController as AnimatorController;
			if (ac == null)
				throw new UnityException($"Require {nameof(AnimatorController)}");
			AnimatorControllerLayer[] layers = ac.layers;
			stateList = new List<AnimatorStateCacheLite>(100);
			int layerCnt = layers.Length;
			for (int i = 0; i < layerCnt; i++)
			{
				string layerName = layers[i].name;
				Editor_AddStateFromStateMachine(stateList, layerName, layerName, i, layers[i].stateMachine);
			}
#else
			throw new System.NotImplementedException();
#endif
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		private static void Editor_AddStateFromStateMachine(List<AnimatorStateCacheLite> stateList, string prefix, string displayPrefix, int layerIndex, Object animatorStateMachine)
		{
#if UNITY_EDITOR
			// In order to get rid of compiler error (No editor)
			AnimatorStateMachine asm = animatorStateMachine as AnimatorStateMachine;

			ChildAnimatorState[] childs = asm.states;
			int childCnt = childs.Length;
			for (int x = 0; x < childCnt; x++)
			{
				AnimatorState state = childs[x].state;
				string path = prefix + "." + state.name;
				string displayPath = prefix + "/" + state.name;
				stateList.Add(new AnimatorStateCacheLite
				{
					displayPath = displayPath,
					path = path,
					layer = layerIndex,
					fullNameHash = Animator.StringToHash(path),
					stateName = state.name,
					shortNameHash = Animator.StringToHash(state.name),
				});
				// Debug.Log(path + ", hash = "+ state.nameHash + " == "+ Animator.StringToHash(path));
			}

			ChildAnimatorStateMachine[] subStateMachine = asm.stateMachines;
			int subStateMachineCnt = subStateMachine.Length;
			for (int y = 0; y < subStateMachineCnt; y++)
			{
				ChildAnimatorStateMachine childStateMachine = subStateMachine[y];
				string subLayerName = prefix + "." + childStateMachine.stateMachine.name;
				string subLayerDisplayName = prefix + "/" + childStateMachine.stateMachine.name;
				Editor_AddStateFromStateMachine(stateList, subLayerName, subLayerDisplayName, layerIndex, childStateMachine.stateMachine);
			}
#endif
		}

		[System.Diagnostics.Conditional("UNITY_EDITOR")]
		private static void Editor_AddStateFromStateMachine(List<AnimatorStateCache> stateList, string prefix, string displayPrefix, int layerIndex, Object animatorStateMachine)
		{
#if UNITY_EDITOR
			// In order to get rid of compiler error (No editor)
			AnimatorStateMachine asm = animatorStateMachine as AnimatorStateMachine;

			ChildAnimatorState[] childs = asm.states;
			int childCnt = childs.Length;
			for (int x = 0; x < childCnt; x++)
			{
				AnimatorState state = childs[x].state;
				string path = prefix + "." + state.name;
				string displayPath = prefix + "/" + state.name;
				stateList.Add(new AnimatorStateCache
				{
					motion = state.motion,
					displayPath = displayPath,
					path = path,
					layer = layerIndex,
					fullNameHash = Animator.StringToHash(path),
					stateName = state.name,
					shortNameHash = Animator.StringToHash(state.name),
				});
				// Debug.Log(path + ", hash = "+ state.nameHash + " == "+ Animator.StringToHash(path));
			}

			ChildAnimatorStateMachine[] subStateMachine = asm.stateMachines;
			int subStateMachineCnt = subStateMachine.Length;
			for (int y = 0; y < subStateMachineCnt; y++)
			{
				ChildAnimatorStateMachine childStateMachine = subStateMachine[y];
				string subLayerName = prefix + "." + childStateMachine.stateMachine.name;
				string subLayerDisplayName = prefix + "/" + childStateMachine.stateMachine.name;
				Editor_AddStateFromStateMachine(stateList, subLayerName, subLayerDisplayName, layerIndex, childStateMachine.stateMachine);
			}
#endif
		}
	}
	[System.Serializable]
	public class AnimatorStateCache : AnimatorStateCacheLite
	{
		public Motion motion;
	}
	[System.Serializable]
	public class AnimatorStateCacheLite
    {
		public string displayPath;
		public int layer;
		public string stateName;
		public int shortNameHash;
		public string path;
		public int fullNameHash; // Key

        public override string ToString()
        {
			return $"[{layer}]\"{displayPath}\", F={fullNameHash},S={shortNameHash}";
        }
    }

	public struct AnimatorStateStruct
	{
		public string name;
		public int shortNameHash;
		public int layerIndex;
	}

	[System.Serializable]
	public struct AnimatorCrossFadeSetting
    {
		public float normalizedTransitionDuration;
		public float normalizedTimeOffset;
		public float normalizedTransitionTime;
	}
	[System.Serializable]
	public struct AnimatorCrossFadeFixedSetting
    {
		public float fixedTransitionDuration;
		public float fixedTimeOffset;
		public float normalizedTransitionTime;
	}
}
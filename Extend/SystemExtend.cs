using UnityEngine;
using System;
using System.Reflection;
using UnityEngine.Profiling;

namespace Kit
{
    public static class SystemExtend
    {
        #region DebugFunctions
        /// <summary>Gets the methods of an object.</summary>
        /// <returns>A list of methods accessible from this object.</returns>
        /// <param name='obj'>The object to get the methods of.</param>
        /// <param name='includeInfo'>Whether or not to include each method's method info in the list.</param>
        public static string MethodsOfObject(this System.Object obj, bool includeInfo = false)
        {
            string methods = string.Empty;
            MethodInfo[] methodInfos = obj.GetType().GetMethods();
            for (int i = 0; i < methodInfos.Length; i++)
            {
                if (includeInfo)
                {
                    methods += methodInfos[i] + "\n";
                }
                else
                {
                    methods += methodInfos[i].Name + "\n";
                }
            }
            return (methods);
        }

        /// <summary>Gets the methods of a type.</summary>
        /// <returns>A list of methods accessible from this type.</returns>
        /// <param name='type'>The type to get the methods of.</param>
        /// <param name='includeInfo'>Whether or not to include each method's method info in the list.</param>
        public static string MethodsOfType(this Type type, bool includeInfo = false)
        {
            string methods = string.Empty;
            MethodInfo[] methodInfos = type.GetMethods();
            for (var i = 0; i < methodInfos.Length; i++)
            {
                if (includeInfo)
                {
                    methods += methodInfos[i] + "\n";
                }
                else
                {
                    methods += methodInfos[i].Name + "\n";
                }
            }
           return (methods);
        }

		/// <summary>Use reflection to invoke function by Name</summary>
		/// <param name="obj">This object</param>
		/// <param name="functionName">function name in string</param>
		/// <param name="bindingFlags"><see cref="BindingFlags"/></param>
		/// <param name="args">The values you wanted to pass, will trim out if destination params less than provider.</param>
		/// <returns></returns>
		public static bool InvokeMethod(this object obj, string functionName, BindingFlags bindingFlags, params object[] args)
		{
			Type type = obj.GetType();
			MethodInfo method = type.GetMethod(functionName, bindingFlags);
			if (method != null)
			{
				int length = method.GetParameters().Length;
				if (length > args.Length)
				{
					throw new ArgumentOutOfRangeException("Destination parameter(s) are required " + length + ", but system provided " + args.Length);
				}
				else
				{
					object[] trimArgs = new object[length];
					Array.Copy(args, trimArgs, length);
					method.Invoke(obj, trimArgs);
					return true;
				}
			}
			return false;
		}

        public static T GetCopyOf<T>(this Component comp, T other) where T : Component
        {
            Type type = comp.GetType();
            if (type != other.GetType()) return null; // type mis-match
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
            PropertyInfo[] pinfos = type.GetProperties(flags);
            foreach (var pinfo in pinfos)
            {
                if (pinfo.CanWrite)
                {
                    try
                    {
                        pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                    }
                    catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
                }
            }
            FieldInfo[] finfos = type.GetFields(flags);
            foreach (var finfo in finfos)
            {
                finfo.SetValue(comp, finfo.GetValue(other));
            }
            return comp as T;
        }

        public static T Clone<T>(this UnityEngine.Object obj) where T : UnityEngine.Object
        {
            if (obj == null)
                throw new System.NullReferenceException();
            var type    = obj.GetType();
            T rst       = (T)Activator.CreateInstance(type);
            foreach (var property in type.GetProperties())
            {
                if (property.GetSetMethod() != null && property.GetGetMethod() != null)
                {
                    property.SetValue(rst, property.GetValue(obj, null), null);
                }
            }
            return rst;
        }
        #endregion

        public static string ToStringDetail(this TimeSpan ts)
        {
            return
                $"{(ts.TotalDays            >= 1f ? $"{(int)ts.TotalDays}d " : "")}" +
                $"{(ts.TotalHours           >= 1f ? $"{ts.Hours}h " : "")}" +
                $"{(ts.TotalMinutes         >= 1f ? $"{ts.Minutes}m " : "")}" +
                $"{(ts.TotalSeconds         >= 1f ? $"{ts.Seconds}s " : "")}" +
                $"{(ts.TotalMilliseconds    >= 1f ? $"{ts.Milliseconds}ms" : "")}".Trim();
        }

        public static string ToStringDetail(this System.Diagnostics.Stopwatch stopwatch)
            => new TimeSpan(stopwatch.ElapsedTicks).ToStringDetail();

        public struct ProfilerScope : IDisposable
        {
            private bool m_Disposed;
            public ProfilerScope(string name)
            {
                m_Disposed = false;
                Profiler.BeginSample(name);
            }

            public void Dispose()
            {
                if (m_Disposed)
                    return;

                m_Disposed = true;
                Profiler.EndSample();
            }
        }
    }
}
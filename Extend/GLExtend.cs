using UnityEngine;

namespace Kit
{
    /// <summary>
    /// <see cref="MyGLDrawer"/>, which will auto attempt to draw on main camera.
    /// </summary>
    public static class GLExtend
    {
        public enum GLType
        {
            TRIANGLES       = GL.TRIANGLES,
            TRIANGLE_STRIP  = GL.TRIANGLE_STRIP,
            QUADS           = GL.QUADS,
            LINES           = GL.LINES,
            LINE_STRIP      = GL.LINE_STRIP,
        }
        private static Material m_Diffuse = null;
        public static Material Diffuse
        {
            get
            {
                if (m_Diffuse == null)
                {
                    m_Diffuse = new Material(Shader.Find("Hidden/Internal-Colored"));
                    m_Diffuse.hideFlags = HideFlags.HideAndDontSave;
                }
                return m_Diffuse;
            }
        }
        public static void DrawLine(in Vector3 from, in Vector3 to, in Color color = default)
        {
            var drawer = MyGLDrawer.Instance;
            if (drawer)
                drawer.AddLineJob(from, to, color);
        }

        public static void DrawSoildTriangle(in Vector3 v0, in Vector3 v1, in Vector3 v2, in Color c0)
        {
            var drawer = MyGLDrawer.Instance;
            if (drawer)
                drawer.AddSoildTriangleJob(v0, v1, v2, c0, c0, c0);
        }

        public static void DrawSoildTriangle(in Vector3 v0, in Vector3 v1, in Vector3 v2, in Color c0, in Color c1, in Color c2)
        {
            var drawer = MyGLDrawer.Instance;
            if (drawer)
                drawer.AddSoildTriangleJob(v0, v1, v2, c0, c1, c2);
        }

        public static void DrawLineTriangle(in Vector3 v0, in Vector3 v1, in Vector3 v2, in Color c0)
        {
            var drawer = MyGLDrawer.Instance;
            if (drawer)
                drawer.AddLineTriangleJob(v0, v1, v2, c0, c0, c0);
        }
        public static void DrawLineTriangle(in Vector3 v0, in Vector3 v1, in Vector3 v2, in Color c0, in Color c1, in Color c2)
        {
            var drawer = MyGLDrawer.Instance;
            if (drawer)
                drawer.AddLineTriangleJob(v0, v1, v2, c0, c1, c2);
        }
    }
}
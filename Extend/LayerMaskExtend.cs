﻿using UnityEngine;

namespace Kit
{
	public static class LayerMaskExtend
	{
		public static bool Contain(this LayerMask layerMask, GameObject gameObject)
		{
			return layerMask.Contain(gameObject.layer);
		}

		/// <summary>
		/// Extension method to check if a layer is in a layermask
		/// <see cref="http://answers.unity3d.com/questions/50279/check-if-layer-is-in-layermask.html"/>
		/// </summary>
		/// <param name="layerMask"></param>
		/// <param name="layer"></param>
		/// <returns></returns>
		public static bool Contain(this LayerMask layerMask, int layer)
		{
			int bit = 1 << layer;
			int mask = layerMask.value | bit;
			return layerMask.value == mask;
		}

		public static bool IsInLayerMask(this GameObject gameObject, LayerMask layerMask)
		{
            // Convert the object's layer to a bitfield for comparison
            int objLayer = 1 << gameObject.layer;
			return (layerMask.value & objLayer) != 0;
		}

		/// <summary>A dirty way to convert LayerMask into single layer</summary>
		/// <param name="layerMask"></param>
		/// <returns></returns>
		public static int ConvertToSingleLayer(this LayerMask layerMask)
		{
			return Mathf.CeilToInt(Mathf.Log(layerMask.value, 2));
		}

		public static LayerMask Combine(LayerMask x, LayerMask y)
		{
			LayerMask mask = new LayerMask();
			mask.value = x.value |= y.value;
			return mask;
		}

		public static void Add(this ref LayerMask layerMask, LayerMask other)
		{
			layerMask.value = (layerMask.value |= other.value);
		}

		public static void Remove(this ref LayerMask layerMask, LayerMask other)
		{
			layerMask.value = (layerMask.value &= ~other.value);
		}
	}
}
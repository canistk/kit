﻿using UnityEngine;

namespace Kit
{
	public struct RayHit
	{
		public Vector3 point;
		public Vector3 normal;
		public float distance;
		public object rendergeometry;

		public static implicit operator RayHit(RaycastHit info)
        {
			return new RayHit
			{
				point = info.point,
				normal = info.normal,
				distance = info.distance,
			};
        }
	}

	public static class RayExtend
	{
		#region Intersect
		/// <summary>
		/// Determines whether there is an intersection between a <see cref="Ray"/> and a <see cref="Ray"/>.
		/// </summary>
		/// <param name="ray1">The first ray to test.</param>
		/// <param name="ray2">The second ray to test.</param>
		/// <param name="point">When the method completes, contains the point of intersection,
		/// or <see cref="Vector3.Zero"/> if there was no intersection.</param>
		/// <returns>Whether the two objects intersect.</returns>
		/// <remarks>
		/// This method performs a ray vs ray intersection test based on the following formula
		/// from Goldman.
		/// <code>s = det([o_2 - o_1, d_2, d_1 x d_2]) / ||d_1 x d_2||^2</code>
		/// <code>t = det([o_2 - o_1, d_1, d_1 x d_2]) / ||d_1 x d_2||^2</code>
		/// Where o_1 is the position of the first ray, o_2 is the position of the second ray,
		/// d_1 is the normalized direction of the first ray, d_2 is the normalized direction
		/// of the second ray, det denotes the determinant of a matrix, x denotes the cross
		/// product, [ ] denotes a matrix, and || || denotes the length or magnitude of a vector.
		/// </remarks>
		public static bool RayIntersectsRay(in Ray ray1, in Ray ray2, out Vector3 point)
		{
			//Source: Real-Time Rendering, Third Edition
			//Reference: Page 780

			Vector3 cross = Vector3.Cross(ray1.direction, ray2.direction);
			float denominator = cross.magnitude;

			//Lines are parallel.
			if (denominator < float.Epsilon)
			{
				//Lines are parallel and on top of each other.
				if (ray2.origin.Approximately(ray1.origin))
				{
					point = Vector3.zero;
					return true;
				}
			}

			denominator = denominator * denominator;

			//3x3 matrix for the first ray.
			float m11 = ray2.origin.x - ray1.origin.x;
			float m12 = ray2.origin.y - ray1.origin.y;
			float m13 = ray2.origin.z - ray1.origin.z;
			float m21 = ray2.direction.x;
			float m22 = ray2.direction.y;
			float m23 = ray2.direction.z;
			float m31 = cross.x;
			float m32 = cross.y;
			float m33 = cross.z;

			//Determinant of first matrix.
			float dets =
				m11 * m22 * m33 +
				m12 * m23 * m31 +
				m13 * m21 * m32 -
				m11 * m23 * m32 -
				m12 * m21 * m33 -
				m13 * m22 * m31;

			//3x3 matrix for the second ray.
			m21 = ray1.direction.x;
			m22 = ray1.direction.y;
			m23 = ray1.direction.z;

			//Determinant of the second matrix.
			float dett =
				m11 * m22 * m33 +
				m12 * m23 * m31 +
				m13 * m21 * m32 -
				m11 * m23 * m32 -
				m12 * m21 * m33 -
				m13 * m22 * m31;

			//t values of the point of intersection.
			float s = dets / denominator;
			float t = dett / denominator;

			//The points of intersection.
			Vector3 point1 = ray1.origin + (s * ray1.direction);
			Vector3 point2 = ray2.origin + (t * ray2.direction);

			//If the points are not equal, no intersection has occurred.
			if (!point2.Approximately(point2))
			{
				point = Vector3.zero;
				return false;
			}

			point = point1;
			return true;
		}

		/// <summary>
		/// Not yet tested
		/// </summary>
        public static bool LineLineIntersection(Vector3 x0, Vector3 x1, Vector3 y0, Vector3 y1)
        {
			var x10 = x1 - x0;
			var xray = new Ray(x0, x10);
			var y10 = y1 - y0;
			var yray = new Ray(y0, y10);
			if (!RayIntersectsRay(xray, yray, out var point))
				return false;

			var px = point - x0;
			var dotpx10 = Vector3.Dot(x10, px);
            if (dotpx10 > 0f)
			{
				var a = x10.sqrMagnitude;
				var b = px.sqrMagnitude;
				return a >= b;
			}
			
			var py = point - y0;
			var dotpy10 = Vector3.Dot(y10, py);
			if (dotpy10 > 0f)
			{
				var a = y10.sqrMagnitude;
				var b = py.sqrMagnitude;
				return a >= b;
			}
			return false;
        }

        /// <summary>
        /// Detect if ray are intersect the target Sphere.
        /// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection"/>
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="origin">sphere origin</param>
        /// <param name="radius">sphere radius</param>
        /// <param name="rayHit">return result</param>
        /// <param name="detectInsideSphere">Assign true = when <paramref name="ray"/> are start inside the sphere, also define hitted.</param>
        /// <returns>hit = true</returns>
        public static bool IntersectRaySphere(this in Ray ray, Vector3 origin, float radius, float maxDistance, ref RayHit rayHit,
			bool detectInsideSphere = false)
        {
			float t0 = 0f, t1 = 0f; // solutions for "t" if the ray intersects;
			float radius2 = radius * radius;
			Vector3 rayDirNormalized = ray.direction.normalized;
#if GEOMETRIC_SOLUTION || true
			// Geometric solution
			Vector3 L = origin - ray.origin; // Notes : L := known as the long side of triangle

			float tca = Vector3.Dot(L, rayDirNormalized); // Notes: tca := L project into ray.direction
			if (tca < 0f)
				return false; // inverse direction.

			float d2 = Vector3.Dot(L, L) - tca * tca; // d := based on Pythagorean theorem, L^2 - tca^2 = d^2
			if (d2 > radius2)
				return false;
			// Notes : thc := the distance between sphere center & intersect points
			float thc = Mathf.Sqrt(radius2 - d2);
			t0 = tca - thc;
			t1 = tca + thc;
			if (t0 > t1)
				MathKit.Swap(ref t0, ref t1);
#else
			// #elif ANALYTIC_SOLUTION //|| true
			// Analytic solution
			Vector3 L = ray.origin - origin;
			float a = Vector3.Dot(ray.direction, ray.direction); // a = ray.magnitude^2
			float b = 2f * Vector3.Dot(L, ray.direction);
			float c = Vector3.Dot(L, L) - radius2;
			// Notes: since intersect point - origin should always equal to radius.
			// : because distance(t) should always land on (L) vector
			// intersect point === ray.origin + ray.direction * distance(t) = L
			// L.Dot(L) = L.magnitude^2 = "intersect point to origin distance"^2 - radius^2 = 0
			if (!MathKit.SolveQuadratic(a, b, c, ref t0, ref t1))
				return false;
#endif
			if (t0 > maxDistance)
				return false;
			if (t0 < 0f) // If t0 is negative, ray started inside sphere
            {
				if (detectInsideSphere)
				{
					t0 = t1; // started inside sphere, let's use t1 instead
					if (t0 < 0f)
						return false; // both t0 & t1 are negative, behind ray
				}
				else
				{
					// ray started inside sphere.
					return false;
				}
			}

			Vector3 point = ray.origin + (ray.direction.normalized * t0);
			Vector3 normal = (point - origin).normalized;
			rayHit = new RayHit
			{
				point = point,
				normal = normal,
				distance = t0,
			};
			return true;
		}

		[System.Obsolete("Without handle max distance issue, use Ray.direction as input.", true)]
		public static bool IntersectRaySphere(this Ray ray, Vector3 origin, float radius)
		{
			RayHit rayHit = default;
			float distance = ray.direction.magnitude;
			return IntersectRaySphere(ray, origin, radius, distance, ref rayHit);
		}

		/// <summary>Giving point and ray to calculate the intersect point on target surface normal.</summary>
		/// <param name="ray"></param>
		/// <param name="pointOnSurface">Any point on surface</param>
		/// <param name="surfaceNormal">plane normal</param>
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection"/>
		/// <see cref="https://www.gamedev.net/articles/programming/math-and-physics/practical-use-of-vector-math-in-games-r2968/"/>
		/// <returns></returns>
		[System.Obsolete("May return invalid vector(NaN), due to parallel ray/plane issue.", true)]
		public static Vector3 IntersectPointOnPlane(this in Ray ray, in Vector3 pointOnSurface, in Vector3 surfaceNormal)
		{
			float dot2 = Vector3.Dot(surfaceNormal, ray.direction);
			float dot1 = Vector3.Dot(surfaceNormal, pointOnSurface - ray.origin);
			float distance = dot1 / dot2;
			return ray.origin + (ray.direction * distance);
		}

		/// <summary>Giving point and ray to calculate the intersect point on target surface normal.
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection"/>
		/// <see cref="https://www.youtube.com/watch?v=JKTHQvkXmnY&list=PLeGk08zVu454f47YNid_bRTGbo9_kIU5c&index=2"/>
		/// </summary>
		/// <param name="ray">unit vector is required</param>
		/// <param name="p0">any point on plane, used for triangle calculation</param>
		/// <param name="surfaceNormal">normal vector of plane</param>
		/// <param name="rayHit">return ray hit detail.</param>
		/// <returns>Hit = true</returns>
		public static bool IntersectPointOnPlane(this in Ray ray, float maxDistance, in Vector3 p0, Vector3 surfaceNormal, ref RayHit rayHit)
		{
#if LONG_VERSION
			float denominator = Vector3.Dot(surfaceNormal, ray.direction);
			if (denominator.Approximately(0f))
				return false; // parallel to the plane;
			if (denominator > 0f)
				return false; // cull back face.
			float planeDistance = Vector3.Dot(surfaceNormal, p0);
			float distance = (planeDistance - Vector3.Dot(ray.origin, surfaceNormal)) / denominator;
			if (0f < distance && distance <= maxDistance)
			{
				rayHit.point = ray.origin + (ray.direction * distance);
				rayHit.normal = surfaceNormal;
				rayHit.distance = distance;
				return true;
			}
#else
#if UNITY_EDITOR
			Debug.Assert(ray.direction.sqrMagnitude.Approximately(1f), "Ray.Direction, Require unit verter");
			Debug.Assert(surfaceNormal.sqrMagnitude.Approximately(1f), "SurfaceNormal, Require unit verter");
#endif
			float denominator = -Vector3.Dot(surfaceNormal, ray.direction); // reverse to intersect
			if (denominator > 1e-6) // avoid too close to 0, float point error.
			{
				float distance = Vector3.Dot(surfaceNormal, ray.origin - p0) / denominator;
				if (0f < distance && distance <= maxDistance)
				{
					rayHit.point = ray.origin + (ray.direction * distance);
					rayHit.normal = surfaceNormal;
					rayHit.distance = distance;
					return true;
				}
			}
#endif
			return false;
		}

		/// <summary>Determines whether there is an intersection between a <see cref="Ray"/> and a <see cref="Plane"/>.</summary>
		/// <param name="ray">The ray to test.</param>
		/// <param name="plane">The plane to test.</param>
		/// <param name="distance">When the method completes, contains the distance of the intersection,
		/// or 0 if there was no intersection.</param>
		/// <returns>Whether the two objects intersect.</returns>
		public static bool IntersectPointOnPlane(in Ray ray, in Plane plane, out float distance)
		{
			return IntersectPointOnPlane(ray, plane.normal, plane.distance, out distance);
		}

		/// <summary>Determines whether there is an intersection between a <see cref="Ray"/> and a plane.</summary>
		/// <param name="ray">The ray to test.</param>
		/// <param name="planeNormal">Normal vector of the plane.</param>
		/// <param name="planeDistance">Distance from the origin to the plane.</param>
		/// <param name="distance">When the method completes, contains the distance of the intersection,
		/// or 0 if there was no intersection.</param>
		/// <returns>Whether the two objects intersect.</returns>
		public static bool IntersectPointOnPlane(in Ray ray, in Vector3 planeNormal, in float planeDistance, out float distance)
		{
			//Source: Real-Time Collision Detection by Christer Ericson
			//Reference: Page 175

			float directionDot = Vector3.Dot(planeNormal, ray.direction);

			if (directionDot == 0f)
			{
				distance = 0f;
				return false;
			}

			float positionDot = Vector3.Dot(planeNormal, ray.origin);
			distance = (-planeDistance - positionDot) / directionDot;

			if (distance < 0f)
			{
				distance = 0f;
				return false;
			}

			return true;
		}

		/// <summary>
		/// Determines whether there is an intersection between a <see cref="Ray"/> and a <see cref="Plane"/>.
		/// </summary>
		/// <param name="ray">The ray to test.</param>
		/// <param name="plane">The plane to test</param>
		/// <param name="point">When the method completes, contains the point of intersection,
		/// or <see cref="Vector3.Zero"/> if there was no intersection.</param>
		/// <returns>Whether the two objects intersected.</returns>
		public static bool IntersectPointOnPlane(in Ray ray, in Plane plane, out Vector3 point)
		{
			//Source: Real-Time Collision Detection by Christer Ericson
			//Reference: Page 175

			if (!IntersectPointOnPlane(ray, plane, out float distance))
			{
				point = Vector3.zero;
				return false;
			}

			point = ray.origin + (ray.direction * distance);
			return true;
		}

		/// <summary>Determines whether there is an intersection between a <see cref="Ray"/> and a triangle.</summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance"></param>
		/// <param name="triangle"></param>
		/// <param name="rayHit"></param>
		/// <returns>Whether the two objects intersected.</returns>
		public static bool IntersectPointOnTriangle(this in Ray ray, float maxDistance, in Triangle triangle, ref RayHit rayHit)
			=> ray.IntersectPointOnTriangle(maxDistance, triangle[0], triangle[1], triangle[2], ref rayHit);

		/// <summary>Determines whether there is an intersection between a <see cref="Ray"/> and a triangle.
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/geometry-of-a-triangle"/>
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance"></param>
		/// <param name="p0"></param>
		/// <param name="p1"></param>
		/// <param name="p2"></param>
		/// <param name="rayHit"></param>
		/// <returns>Whether the two objects intersected.</returns>
		public static bool IntersectPointOnTriangle(this in Ray ray, float maxDistance, in Vector3 p0, in Vector3 p1, in Vector3 p2, ref RayHit rayHit)
        {
			// Triangle side, p0>p1>p2, assume clockwise order
			Vector3 
				edge01 = p0 - p1,
				edge12 = p1 - p2,
				edge20 = p2 - p0;
			Vector3 surfaceNormal = Vector3.Cross(edge20, edge01).normalized;

			RayHit hit = default;
			if (!ray.IntersectPointOnPlane(maxDistance, p0, surfaceNormal, ref hit))
				return false;

			bool IsOutsideEdge(in Vector3 edge, in Vector3 point2hitPoint, in Vector3 _surfaceNormal)
			{
				// compare (edge vs point2hitPoint) vector & compareEdge
				Vector3 v = Vector3.Cross(edge, point2hitPoint);
				// is in front or behind the edge. assume pointing forward is outside.
				return Vector3.Dot(_surfaceNormal, v) < 0f;
			}
			if (IsOutsideEdge(edge01, p0 - hit.point, surfaceNormal) ||
				IsOutsideEdge(edge12, p1 - hit.point, surfaceNormal) ||
				IsOutsideEdge(edge20, p2 - hit.point, surfaceNormal))
				return false;
			
			/// assume <see cref="IsOutsideEdge(in Vector3, in Vector3, in Vector3, in Vector3)"/> correct
			/// now we are within the triangle.
			rayHit.point = hit.point;
			rayHit.normal = surfaceNormal;
			rayHit.distance = hit.distance;
			return true;
		}

		/// <summary>Find out if the giving circle disk can be hit by this ray.
		/// <see cref="https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-plane-and-ray-disk-intersection"/></summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance">ray max distance</param>
		/// <param name="center">disk center point</param>
		/// <param name="radius">disk radius</param>
		/// <param name="surfaceNormal">disk normal</param>
		/// <param name="rayHit">return intersect info</param>
		/// <returns></returns>
		public static bool IntersectPointOnDisk(this in Ray ray, float maxDistance, in Vector3 center, float radius, in Vector3 surfaceNormal, ref RayHit rayHit)
        {
			if (ray.IntersectPointOnPlane(maxDistance, center, surfaceNormal, ref rayHit) &&
				rayHit.distance >= 0f)
            {
				// Hit plance, check if it within radius.
				Vector3 v = rayHit.point - center; // hit-point > center radius.
				return Vector3.Dot(v, v) < radius * radius; // compare 2 radius.
            }
			return false;
        }

		/// <summary>
		/// A wrapper class for
		/// <see cref="IntersectPointOnMesh(in Ray, float, Mesh, ref RayHit, Matrix4x4)"/>
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance"></param>
		/// <param name="meshFilter"></param>
		/// <param name="rayHit"></param>
		/// <returns></returns>
		public static bool IntersectPointOnMesh(this in Ray ray, float maxDistance, MeshFilter meshFilter, ref RayHit rayHit)
		=> ray.IntersectPointOnMesh(maxDistance, meshFilter.sharedMesh, ref rayHit, meshFilter.transform.localToWorldMatrix);

		/// <summary>
		/// To detect if giving ray can interact the giving mesh model.
		/// </summary>
		/// <param name="ray"></param>
		/// <param name="maxDistance"></param>
		/// <param name="mesh"></param>
		/// <param name="rayHit"></param>
		/// <param name="matrix"></param>
		/// <returns></returns>
		public static bool IntersectPointOnMesh(this in Ray ray, float maxDistance, Mesh mesh, ref RayHit rayHit, Matrix4x4 matrix = default)
        {
			if (matrix == default)
				matrix = Matrix4x4.identity;
			Vector3[] vertices = mesh.vertices;
			bool alreadyHit = false;
			for (int submesh = 0; submesh < mesh.subMeshCount; submesh++)
			{
				int triCnt = (int)mesh.GetIndexCount(submesh) / 3;
				int[] indices = mesh.GetIndices(submesh);
				uint subMeshVertexStart = mesh.GetBaseVertex(submesh);
				for (int tri = 0; tri < triCnt; tri++)
				{
					int i = tri * 3;
					Triangle triangle = new Triangle(
						matrix.MultiplyPoint(vertices[subMeshVertexStart + indices[i]]),
						matrix.MultiplyPoint(vertices[subMeshVertexStart + indices[i + 1]]),
						matrix.MultiplyPoint(vertices[subMeshVertexStart + indices[i + 2]])
					);
					if (ray.IntersectPointOnTriangle(maxDistance, triangle, ref rayHit))
					{
						maxDistance = rayHit.distance;
						alreadyHit = true;
					}
				}
			}
			return alreadyHit;
		}
		#endregion Intersect
    }
}
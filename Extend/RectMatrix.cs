using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    /// <summary>
    /// Create a Rect with Matrix,
    /// since <see cref="RectTransform"/> is not support in gizmos drawing,
    /// provide tools to easy calculate and draw gizmos directly in world space (on canvas)
    /// </summary>
    public class RectMatrix
    {
        public RectMatrix(RectTransform src)
            : this(src.rect, src.localToWorldMatrix, src.worldToLocalMatrix, src.pivot)
        { }

        public RectMatrix(Rect rect, Matrix4x4 local2World, Matrix4x4 world2Local, Vector2 pivot)
        {
            this.rect = rect;
            this.local2World = local2World;
            this.world2Local = world2Local;
            this.pivot = pivot;
        }

        [System.Obsolete("Don't think this is possible, use FromWorld2Local instead")]
        public static RectMatrix FromWorld2Local(Rect rect, Matrix4x4 world2Local, Vector2 pivot)
        {
            return new RectMatrix(rect, world2Local.inverse, world2Local, pivot);
        }

        public static RectMatrix FromLocal2World(Rect rect, Matrix4x4 local2World, Vector2 pivot)
        {
            return new RectMatrix(rect, local2World, local2World.inverse, pivot);
        }

        public static RectMatrix FromSrc2Pos(RectMatrix src, Vector3 worldPosition)
        {
            var quat = src.local2World.rotation;
            var scale = src.local2World.GetScale();
            var l2w = Matrix4x4.TRS(worldPosition, quat, scale);
            var w2l = l2w.inverse;
            return new RectMatrix(src.rect, l2w, w2l, src.pivot);
        }

        public Vector2 pivot { get; private set; }
        public Rect rect { get; private set; }
        public Matrix4x4 local2World { get; private set; }
        public Matrix4x4 world2Local { get; private set; }

        public Vector2[] localCorners
        {
            get
            {
                return rect.GetCorners();
            }
        }
        public Vector2 leftBottom => new Vector2(rect.x, rect.y);
        public Vector2 leftTop => new Vector2(rect.x, rect.yMax);
        public Vector2 rightTop => new Vector2(rect.xMax, rect.yMax);
        public Vector2 rightBottom => new Vector2(rect.xMax, rect.y);

        public Vector3[] worldCorners
        {
            get
            {
                var lc = localCorners;
                return new Vector3[4]
                {
                    local2World.MultiplyPoint((Vector3)lc[0]),
                    local2World.MultiplyPoint((Vector3)lc[1]),
                    local2World.MultiplyPoint((Vector3)lc[2]),
                    local2World.MultiplyPoint((Vector3)lc[3])
                };

            }
        }

        public Vector3 Xaxis => this.local2World * Vector3.left;
        public Vector3 Yaxis => this.local2World * Vector3.up;
        public Vector3 Zaxis => this.local2World * Vector3.forward;

        [System.Obsolete("Please remove, cannot project 3d to 2d struct")]
        public Rect worldRect
        {
            get
            {
                // This is wrong
                //var c = new Vector3[4]
                //{
                //    local2World.MultiplyPoint(leftBottom),
                //    local2World.MultiplyPoint(leftTop),
                //    local2World.MultiplyPoint(rightTop),
                //    local2World.MultiplyPoint(rightBottom)
                //};

                var c = worldCorners;
                var xMin = Mathf.Min(c[0].x, c[1].x, c[2].x, c[3].x);
                var xMax = Mathf.Max(c[0].x, c[1].x, c[2].x, c[3].x);
                var yMin = Mathf.Min(c[0].y, c[1].y, c[2].y, c[3].y);
                var yMax = Mathf.Max(c[0].y, c[1].y, c[2].y, c[3].y);
                return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
            }
        }

        public Vector3 worldCenter
        {
            get => local2World.MultiplyPoint(rect.center);
        }

        /// <summary>
        /// world position based on pivot point,
        /// in UnityEngine.RectTransform, pivot is based game object's position.
        /// </summary>
        public Vector3 worldPivot
        {
            get
            {
                var wc = worldCorners;
                return new Vector3(
                    Mathf.LerpUnclamped(wc[0].x, wc[2].x, pivot.x),
                    Mathf.LerpUnclamped(wc[0].y, wc[2].y, pivot.y),
                    Mathf.LerpUnclamped(wc[0].z, wc[2].z, 0.5f));

                // Issue : lack of matrix rotation info.
                // return GetPivot(worldRect, pivot);
            }
        }
        public static Vector2 GetPivot(Rect rect, Vector2 pivot)
        {
            var wc = rect.GetCorners();
            return new Vector2(
                Mathf.LerpUnclamped(wc[0].x, wc[2].x, pivot.x),
                Mathf.LerpUnclamped(wc[0].y, wc[2].y, pivot.y));
        }
        public void DrawGizmos(Color? color = null, float size = 0.05f)
        {
            var wc = worldCorners;
            using (new ColorScope(color))
            {
                foreach (var p in wc)
                {
                    GizmosExtend.DrawPoint(p, null, size, true);
                }
                GizmosExtend.DrawPoint(worldPivot, null, size, true);

                Gizmos.DrawLine(wc[0], wc[1]);
                Gizmos.DrawLine(wc[1], wc[2]);
                Gizmos.DrawLine(wc[2], wc[3]);
                Gizmos.DrawLine(wc[3], wc[0]);
            }
        }

        public void DrawDebug(Color? color = null, float size = 0.05f)
        {
            var wc = worldCorners;
            if (color.HasValue)
            {
                Debug.DrawLine(wc[0], wc[1], color.Value);
                Debug.DrawLine(wc[1], wc[2], color.Value);
                Debug.DrawLine(wc[2], wc[3], color.Value);
                Debug.DrawLine(wc[3], wc[0], color.Value);
            }
            else
            {
                Debug.DrawLine(wc[0], wc[1]);
                Debug.DrawLine(wc[1], wc[2]);
                Debug.DrawLine(wc[2], wc[3]);
                Debug.DrawLine(wc[3], wc[0]);
            }
        }

        #region Utils
        public bool Overlaps(RectMatrix other)                                  => this.rect.Overlaps(ConvertLocalRect(this.world2Local, other));
        public bool IsFullyContain(RectMatrix other)                            => rect.IsFullyContain(ConvertLocalRect(this.world2Local, other));
        public RectMatrix SnapLeft(RectMatrix target, float margin = 0f)        => RectMatrix.FromLocal2World(rect.SnapLeft(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapRight(RectMatrix target, float margin = 0f)       => RectMatrix.FromLocal2World(rect.SnapRight(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapTop(RectMatrix target, float margin = 0f)         => RectMatrix.FromLocal2World(rect.SnapTop(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapBottom(RectMatrix target, float margin = 0f)      => RectMatrix.FromLocal2World(rect.SnapBottom(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapInnerLeft(RectMatrix target, float margin = 0f)   => RectMatrix.FromLocal2World(rect.SnapInnerLeft(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapInnerRight(RectMatrix target, float margin = 0f)  => RectMatrix.FromLocal2World(rect.SnapInnerRight(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapInnerTop(RectMatrix target, float margin = 0f)    => RectMatrix.FromLocal2World(rect.SnapInnerTop(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);
        public RectMatrix SnapInnerBottom(RectMatrix target, float margin = 0f) => RectMatrix.FromLocal2World(rect.SnapInnerBottom(ConvertLocalRect(this.world2Local, target), margin), local2World, pivot);

        /// <summary>
        /// Convert another <see cref="RectMatrix"/> into current local space.
        /// in other to use <see cref="Rect"/> calculation method.
        /// considered transfrom's rotation and scale.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public static Rect ConvertLocalRect(Matrix4x4 world2Local, RectMatrix other)
        {
            var w2l = world2Local;
            var o = other.worldCorners;
            var c = new Vector3[4]
            {
                // convert other's world corners to local corners.
                // 3D -> 2D assume Z-axis can be ignore.
                w2l.MultiplyPoint(o[0]),
                w2l.MultiplyPoint(o[1]),
                w2l.MultiplyPoint(o[2]),
                w2l.MultiplyPoint(o[3])
            };
            var xMin = Mathf.Min(c[0].x, c[1].x, c[2].x, c[3].x);
            var xMax = Mathf.Max(c[0].x, c[1].x, c[2].x, c[3].x);
            var yMin = Mathf.Min(c[0].y, c[1].y, c[2].y, c[3].y);
            var yMax = Mathf.Max(c[0].y, c[1].y, c[2].y, c[3].y);

            bool biasCondition =
                Mathf.Approximately(c[0].x, xMin) &&
                Mathf.Approximately(c[0].y, yMin) &&
                Mathf.Approximately(c[2].x, xMax) &&
                Mathf.Approximately(c[2].y, yMax);

            if (!biasCondition)
                Debug.LogError("input points, had different rotation");

            return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
        }

        /// <summary>
        /// Based on giving 2 points, calculate the rect. based on giving world to local space matrix
        /// </summary>
        /// <param name="world2Local"></param>
        /// <param name="leftBottom"></param>
        /// <param name="rightTop"></param>
        /// <returns></returns>
        [System.Obsolete("Review, due to not support rotation issue")]
        public static Rect ConvertLocalRect(Matrix4x4 world2Local, Vector3 leftBottom, Vector3 rightTop)
        {
            var w2l = world2Local;
            var lb = w2l.MultiplyPoint(leftBottom);
            var rt = w2l.MultiplyPoint(rightTop);
            var xMin = Mathf.Min(lb.x, rt.x);
            var xMax = Mathf.Max(lb.x, rt.x);
            var yMin = Mathf.Min(lb.y, rt.y);
            var yMax = Mathf.Max(lb.y, rt.y);
            return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
        }

        /// <summary>
        /// Assume all points are in same local space and rectangle shape.
        /// convert local points into rect.
        /// bias : assume Z-axis can be ignore. since 2D rect didn't have Z-axis.
        /// </summary>
        /// <param name="localPointXY"></param>
        /// <returns></returns>
        public static Rect ConvertLocalRect(params Vector3[] localPointXY)
        {
            const int size = 4;
            Debug.Assert(localPointXY.Length == size);
            var v2 = new Vector2[size];
            for (int i = 0; i < size; ++i)
                v2[i] = (Vector2)localPointXY[i];
            return ConvertLocalRect(v2);
        }

        /// <summary>
        /// Assume all points are in same local space and rectangle shape.
        /// convert local points into rect.
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static Rect ConvertLocalRect(params Vector2[] points)
        {
            if (points.Length != 4)
                throw new System.Exception("Input only support 4 points.");

            var p = points;
            var xMin = Mathf.Min(p[0].x, p[1].x, p[2].x, p[3].x);
            var xMax = Mathf.Max(p[0].x, p[1].x, p[2].x, p[3].x);
            var yMin = Mathf.Min(p[0].y, p[1].y, p[2].y, p[3].y);
            var yMax = Mathf.Max(p[0].y, p[1].y, p[2].y, p[3].y);

            bool biasCondition =
                Mathf.Approximately(p[0].x, xMin) &&
                Mathf.Approximately(p[0].y, yMin) &&
                Mathf.Approximately(p[2].x, xMax) &&
                Mathf.Approximately(p[2].y, yMax);

            if (!biasCondition)
                Debug.LogError("input points, had different rotation");
            return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
        }

        /// <summary>To calculate the possible area for UI tips to show up without overlapping with target rect.</summary>
        /// <param name="tips">the rect itself.</param>
        /// <param name="target">target rect that didn't want to overlap</param>
        /// <param name="visibleArea">the area required to fully contain <see cref="tips"/></param>
        /// <param name="rst">this final position of rect</param>
        /// <param name="margin">the margin to calculate snap position.</param>
        /// <returns></returns>
        public static bool TryGetVisableRect(RectMatrix tips, RectMatrix target, RectMatrix visibleArea, out RectMatrix rst, float margin = 0f)
        {
            if (!tips.Overlaps(target) && visibleArea.IsFullyContain(tips))
            {
                rst = tips;
                return true;
            }

            #region Outter align
            if (TrySnapRight(tips, target, margin, visibleArea, out var snapRight))
            {
                rst = snapRight;
                return true;
            }

            if (TrySnapLeft(tips, target, margin, visibleArea, out var snapLeft))
            {
                rst = snapLeft;
                return true;
            }

            if (TrySnapTop(tips, target, margin, visibleArea, out var snapTop))
            {
                rst = snapTop;
                return true;
            }

            if (TrySnapBottom(tips, target, margin, visibleArea, out var snapBn))
            {
                rst = snapBn;
                return true;
            }
            #endregion Outter align

            #region Outter corner
            if (TrySnapLeftTop(tips, target, margin, visibleArea, out var snapLeftTop))
            {
                rst = snapLeftTop;
                return true;
            }

            if (TrySnapLeftBottom(tips, target, margin, visibleArea, out var snapLeftBn))
            {
                rst = snapLeftBn;
                return true;
            }

            if (TrySnapRightTop(tips, target, margin, visibleArea, out var snapRightTop))
            {
                rst = snapRightTop;
                return true;
            }

            if (TrySnapRightBottom(tips, target, margin, visibleArea, out var snapRightBn))
            {
                rst = snapRightBn;
                return true;
            }
            #endregion Outter corner


            // visible area is not fully contain tips, try to find a position that tips is fully contain in visible area.
            #region Inner corner
            if (TrySnapInnerLeftTop(tips, target, margin, visibleArea, out var snapInnerLeftTop))
            {
                rst = snapInnerLeftTop;
                return true;
            }

            if (TrySnapInnerLeftBottom(tips, target, margin, visibleArea, out var snapInnerLeftBn))
            {
                rst = snapInnerLeftBn;
                return true;
            }

            if (TrySnapInnerRightTop(tips, target, margin, visibleArea, out var snapInnerRightTop))
            {
                rst = snapInnerRightTop;
                return true;
            }

            if (TrySnapInnerRightBottom(tips, target, margin, visibleArea, out var snapInnerRightBn))
            {
                rst = snapInnerRightBn;
                return true;
            }
            #endregion Inner corner

            #region Inner align
            if (TrySnapInnerLeft(tips, target, margin, visibleArea, out var snapInnerLeft))
            {
                rst = snapInnerLeft;
                return true;
            }

            if (TrySnapInnerRight(tips, target, margin, visibleArea, out var snapInnerRight))
            {
                rst = snapInnerRight;
                return true;
            }

            if (TrySnapInnerTop(tips, target, margin, visibleArea, out var snapInnerTop))
            {
                rst = snapInnerTop;
                return true;
            }

            if (TrySnapInnerBottom(tips, target, margin, visibleArea, out var snapInnerBn))
            {
                rst = snapInnerBn;
                return true;
            }

            #endregion Inner align


            rst = default;
            return false;
        }

        #region Snapping
        static List<RectMatrix> _cleanSnaps = new List<RectMatrix>(16);
        static List<RectMatrix> _uncleanSnaps = new List<RectMatrix>(16);

        #region First Approach
        const float ERROR_THRESHOLD = 0.1f;
        public static bool TryGetNearestVisibleArea(RectMatrix popup, RectMatrix target, RectMatrix viewport, out RectMatrix result)
        {
            // All calculations are done in world space
            #region Cases
            if (TrySnapRight(popup, target, 0f, viewport, out var snapRight))
                _cleanSnaps.Add(snapRight);
            else
                _uncleanSnaps.Add(snapRight);

            if (TrySnapLeft(popup, target, 0f, viewport, out var snapLeft))
                _cleanSnaps.Add(snapLeft);
            else
                _uncleanSnaps.Add(snapLeft);

            if (TrySnapTop(popup, target, 0f, viewport, out var snapTop))
                _cleanSnaps.Add(snapTop);
            else
                _uncleanSnaps.Add(snapTop);

            if (TrySnapBottom(popup, target, 0f, viewport, out var snapBottom))
                _cleanSnaps.Add(snapBottom);
            else
                _uncleanSnaps.Add(snapBottom);

            if (TrySnapLeftTop(popup, target, 0f, viewport, out var snapLeftTop))
                _cleanSnaps.Add(snapLeftTop);
            else
                _uncleanSnaps.Add(snapLeftTop);

            if (TrySnapLeftBottom(popup, target, 0f, viewport, out var snapLeftBn))
                _cleanSnaps.Add(snapLeftBn);
            else
                _uncleanSnaps.Add(snapLeftBn);

            if (TrySnapRightTop(popup, target, 0f, viewport, out var snapRightTop))
                _cleanSnaps.Add(snapRightTop);
            else
                _uncleanSnaps.Add(snapRightTop);

            if (TrySnapRightBottom(popup, target, 0f, viewport, out var snapRightBn))
                _cleanSnaps.Add(snapRightBn);
            else
                _uncleanSnaps.Add(snapRightBn);
            #endregion

            // TODO:

            if (_cleanSnaps.Count > 0)
            {
                result = _cleanSnaps[0];
                _cleanSnaps.Clear();
                _uncleanSnaps.Clear();
                return true;
            }

            float maxOverlapArea = float.MinValue;
            RectMatrix bestSnap = default;
            foreach (var snap in _uncleanSnaps)
            {
                Rect rect1 = snap.worldRect;
                Rect rect2 = viewport.worldRect;

                Rect overlap = Rect.zero;
                overlap.xMin = Mathf.Max(rect1.xMin, rect2.xMin);
                overlap.yMin = Mathf.Max(rect1.yMin, rect2.yMin);
                overlap.xMax = Mathf.Min(rect1.xMax, rect2.xMax);
                overlap.yMax = Mathf.Min(rect1.yMax, rect2.yMax);

                // Calculate the area of overlap
                float overlapArea = overlap.width * overlap.height;
                if (overlapArea - ERROR_THRESHOLD > maxOverlapArea)
                {
                    maxOverlapArea = overlapArea;
                    bestSnap = snap;
                }
            }

            _cleanSnaps.Clear();
            _uncleanSnaps.Clear();
            result = bestSnap;
            return false;
        }
        #endregion

        #region Second Approach
        public static bool TryGetNearestVisibleAreaEx(RectMatrix popup, RectMatrix target, RectMatrix viewport, out RectMatrix result, out Vector3 position)
        {
            // All calculations are done in world space
            TrySnapRight(popup, target, 0f, viewport, out var snapRight);
            snapRight.DrawDebug(Color.green);
            if (snapRight.CastUpward(viewport, out result, out position))
            {
                result.DrawDebug(Color.red);
                DebugExtend.DrawPoint(position);
                return true;
            }
                
            if (snapRight.CastDownward(viewport, out result, out position))
            {
                result.DrawDebug(Color.red);
                DebugExtend.DrawPoint(position);
                return true;
            }

            TrySnapLeft(popup, target, 0f, viewport, out var snapLeft);
            if (snapLeft.CastUpward(viewport, out result, out position))
                return true;
            if (snapLeft.CastDownward(viewport, out result, out position))
                return true;

            TrySnapTop(popup, target, 0f, viewport, out var snapTop);
            if (snapTop.CastLeft(viewport, out result, out position))
                return true;
            if (snapTop.CastRight(viewport, out result, out position))
                return true;

            TrySnapBottom(popup, target, 0f, viewport, out var snapBottom);
            if (snapBottom.CastLeft(viewport, out result, out position))
                return true;
            if (snapBottom.CastRight(viewport, out result, out position))
                return true;

            return false;
        }
        #endregion

        #region Local corner intersect
        public struct HitResult
        {
            public float distance;
            public Vector3 direction;
            public Vector3 point;
            public RectMatrix hitObj;
        }

        public struct LocalCorner
        {
            public Vector2[] points;
            public LocalCorner(Vector2[] points)
            {
                if (points == null || points.Length != 4)
                    throw new System.Exception();
                this.points = points;
            }

            /// <summary>Bound box != actually size, since we don't calucalte rotation.</summary>
            /// <returns></returns>
            public Rect GetBoundBox()
            {
                var p = points;
                var xMin = Mathf.Min(p[0].x, p[1].x, p[2].x, p[3].x);
                var xMax = Mathf.Max(p[0].x, p[1].x, p[2].x, p[3].x);
                var yMin = Mathf.Min(p[0].y, p[1].y, p[2].y, p[3].y);
                var yMax = Mathf.Max(p[0].y, p[1].y, p[2].y, p[3].y);
                return new Rect(xMin, yMin, xMax - xMin, yMax - yMin);
            }

            public (Vector2 lb, Vector2 lt) EdgeLeft    => (points[0], points[1]);
            public (Vector2 lt, Vector2 rt) EdgeTop     => (points[1], points[2]);
            public (Vector2 rb, Vector2 rt) EdgeRight   => (points[3], points[2]);
            public (Vector2 lb, Vector2 rb) EdgeBottom  => (points[0], points[3]);

            public Ray  EdgeLeftVector      => new Ray(points[0], points[1] - points[0]);
            public Ray  EdgeTopVector       => new Ray(points[1], points[2] - points[1]);
            public Ray  EdgeRightVector     => new Ray(points[2], points[3] - points[2]);
            public Ray  EdgeBottomVector    => new Ray(points[3], points[0] - points[3]);

            public Ray[] edges => new Ray[4]{ EdgeLeftVector, EdgeTopVector, EdgeRightVector, EdgeBottomVector };

            public Vector2 leftBottom   => points[0];
            public Vector2 leftTop      => points[1];
            public Vector2 rightTop     => points[2];
            public Vector2 rightBottom  => points[3];

            public void DrawDebug(Matrix4x4 local2World, Color? color = null, float duration = 0f, bool depthTest = false)
            {
                var p = new Vector3[4]
                {
                    local2World.MultiplyPoint(leftBottom),
                    local2World.MultiplyPoint(leftTop),
                    local2World.MultiplyPoint(rightTop),
                    local2World.MultiplyPoint(rightBottom),
                };

                var _color = color.HasValue ? color.Value : Color.white;
                for (int i = 1; i < 4; ++i)
                {
                    DebugExtend.DrawLine(p[i - 1], p[i], _color, duration, depthTest);
                }
            }

            public bool Cast(Vector2 dir, in float maxDistance, LocalCorner target,
                out float distance)
            {
                dir = dir.normalized;
                distance = 0f;
                for (int x = 0; x < edges.Length; ++x)
                {
                    for (int y = 0; y < target.edges.Length; ++y)
                    {
                        _RayIntersects(edges[x], target.edges[y], out var point, out bool isParallel);
                    }
                }
                return false;

                bool _RayIntersects(Ray lhs, Ray rhs, out Vector3 point, out bool isParallel)
                {
                    const float epsilon = float.Epsilon;
                    var rst = RayExtend.RayIntersectsRay(lhs, rhs, out point);
                    var dot = Vector3.Dot(lhs.direction, rhs.direction);
                    isParallel = dot > Mathf.Max(1f - epsilon, 0f);
                    return rst;
                }
            }
        }

        public static LocalCorner Convert2LocalCorners(Matrix4x4 world2Local, RectMatrix other)
        {
            var w2l = world2Local;
            var o = other.worldCorners;
            var wc = new Vector2[4]
            {
                // convert other's world corners to local corners.
                // 3D -> 2D assume Z-axis can be ignore.
                (Vector2)w2l.MultiplyPoint(o[0]),
                (Vector2)w2l.MultiplyPoint(o[1]),
                (Vector2)w2l.MultiplyPoint(o[2]),
                (Vector2)w2l.MultiplyPoint(o[3])
            };

            return new LocalCorner(wc);
        }

        public bool Cast(Vector3 direction, out HitResult rst,
            IEnumerable<RectMatrix> others = null,
            IEnumerable<RectMatrix> bound = null)
        {
            rst = default;
            Vector3 bias = this.world2Local.MultiplyVector(direction);
            bias.z = 0f;
            if (bias == Vector3.zero)
                return false;

            return Cast((Vector2)bias, out rst, others, bound);
        }

        public bool Cast(Vector2 dir, float maxDistance, out HitResult rst,
            IEnumerable<RectMatrix> others = null,
            IEnumerable<RectMatrix> bound = null)
        {
            if (dir == Vector2.zero)
            {
                rst = default;
                return false;
            }
            dir         = dir.normalized;
            var self    = new LocalCorner(this.localCorners);
            var lbRay   = new Ray(self.leftBottom, dir);
            var ltRay   = new Ray(self.leftTop, dir);
            var rtRay   = new Ray(self.rightTop, dir);
            var rbRay   = new Ray(self.rightBottom, dir);
            var matrix  = this.local2World;

            // Convert into local space.
            if (others != null)
            {
                foreach (var rm in others)
                {
                    var obj = Convert2LocalCorners(world2Local, rm);
                }
            }

            if (bound != null)
            {
                foreach (var rm in bound)
                {
                    var obj = Convert2LocalCorners(world2Local, rm);
                }
            }

            rst = default;
            return false;
        }
        #endregion Local corner intersect

        private bool CastUpward(RectMatrix boundMatrix, out RectMatrix result, out Vector3 position)
        {
            var selfRect = worldRect;
            var boundRect = boundMatrix.worldRect;
            
            if (boundMatrix.IsFullyContain(this))
            {
                result = this;
                position = worldPivot;
                return true;
            }

            result = default;
            position = default;
            if (selfRect.height > boundRect.height || selfRect.width > boundRect.width)
                return false;
            if (selfRect.xMin < boundRect.xMin || selfRect.xMax > boundRect.xMax)
                return false;
            if (selfRect.yMin < boundRect.yMin)
                return false;

            var deltaY = selfRect.yMax - boundRect.yMax;
            var endPoint = worldPivot - new Vector3(0f, deltaY, 0f);
            result = RectMatrix.FromSrc2Pos(this, endPoint);
            position = endPoint;
            boundMatrix.DrawDebug();
            DebugExtend.DrawPoint(endPoint, Color.white, 2f) ;
            return true;
        }

        private bool CastDownward(RectMatrix boundMatrix, out RectMatrix result, out Vector3 position)
        {
            var selfRect = worldRect;
            var boundRect = boundMatrix.worldRect;

            if (boundMatrix.IsFullyContain(this))
            {
                result = this;
                position = worldPivot;
                return true;
            }

            result = default;
            position = default;
            if (selfRect.height > boundRect.height || selfRect.width > boundRect.width)
                return false;
            if (selfRect.xMin < boundRect.xMin || selfRect.xMax > boundRect.xMax)
                return false;
            if (selfRect.yMax > boundRect.yMax)
                return false;

            var deltaY = boundRect.yMin - selfRect.yMin;
            var endPoint = worldPivot + new Vector3(0f, deltaY, 0f);
            result = RectMatrix.FromSrc2Pos(this, endPoint);
            position = endPoint;
            return true;
        }

        private bool CastLeft(RectMatrix boundMatrix, out RectMatrix result, out Vector3 position)
        {
            var selfRect = worldRect;
            var boundRect = boundMatrix.worldRect;

            if (boundMatrix.IsFullyContain(this))
            {
                result = this;
                position = worldPivot;
                return true;
            }

            result = default;
            position = default;
            if (selfRect.height > boundRect.height || selfRect.width > boundRect.width)
                return false;
            if (selfRect.yMin < boundRect.yMin || selfRect.yMax > boundRect.yMax)
                return false;
            if (selfRect.xMin < boundRect.xMin)
                return false;

            var deltaX = selfRect.xMax - boundRect.xMax;
            var endPoint = worldPivot - new Vector3(deltaX, 0f, 0f);
            result = RectMatrix.FromSrc2Pos(this, endPoint);
            position = endPoint;
            return true;
        }

        private bool CastRight(RectMatrix boundMatrix, out RectMatrix result, out Vector3 position)
        {
            var selfRect = worldRect;
            var boundRect = boundMatrix.worldRect;

            if (boundMatrix.IsFullyContain(this))
            {
                result = this;
                position = worldPivot;
                return true;
            }

            result = default;
            position = default;
            if (selfRect.height > boundRect.height || selfRect.width > boundRect.width)
                return false;
            if (selfRect.yMin < boundRect.yMin || selfRect.yMax > boundRect.yMax)
                return false;
            if (selfRect.xMax > boundRect.xMax)
                return false;

            var deltaX = selfRect.xMin - boundRect.xMin;
            var endPoint = worldPivot + new Vector3(deltaX, 0f, 0f);
            result = RectMatrix.FromSrc2Pos(this, endPoint);
            position = endPoint;
            return true;
        }
        #endregion

        #region Raycast
        //private static bool Raycast(RectMatrix from, Vector3 direction, Action<RectMatrix> )
        //{
        //    Ray ray = new Ray(from.worldPivot, direction);

        //    var normal = from.local2World.GetRotation() * Vector3.forward;
        //    var fixedDirection = Vector3.ProjectOnPlane(direction, normal);

        //    if (fixedDirection == Vector3.zero)
        //        throw new Exception($"fixed direction is zero, input direction={direction}");


        //}
        #endregion

        private static bool TrySnapRight(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapRight(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapLeft(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapLeft(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapTop(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapTop(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapBottom(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapBottom(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapLeftTop(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapLeft(target, margin).SnapTop(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapLeftBottom(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapLeft(target, margin).SnapBottom(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapRightTop(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapRight(target, margin).SnapTop(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapRightBottom(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapRight(target, margin).SnapBottom(target, margin);
            return visibleArea.IsFullyContain(rst);
        }

        private static bool TrySnapInnerLeft(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerLeft(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerRight(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerRight(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerTop(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerTop(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerBottom(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerBottom(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerLeftTop(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerLeft(target, margin).SnapInnerTop(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerLeftBottom(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerLeft(target, margin).SnapInnerBottom(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerRightTop(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerRight(target, margin).SnapInnerTop(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        private static bool TrySnapInnerRightBottom(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst)
        {
            rst = tips.SnapInnerRight(target, margin).SnapInnerBottom(target, margin);
            return visibleArea.IsFullyContain(rst) && target.Overlaps(rst);
        }

        public enum eSnapType
        {
            Left,
            Right,
            Top,
            Bottom,
            LeftTop,
            LeftBottom,
            RightTop,
            RightBottom,
            InnerLeft,
            InnerRight,
            InnerTop,
            InnerBottom,
            InnerLeftTop,
            InnerLeftBottom,
            InnerRightTop,
            InnerRightBottom,
        }

        public delegate bool TrySnapDelegate(RectMatrix tips, RectMatrix target, float margin, RectMatrix visibleArea, out RectMatrix rst);

        public static readonly TrySnapDelegate[] trySnapDelegates = new TrySnapDelegate[]
        {
            TrySnapLeft,
            TrySnapRight,
            TrySnapTop,
            TrySnapBottom,
            TrySnapLeftTop,
            TrySnapLeftBottom,
            TrySnapRightTop,
            TrySnapRightBottom,
            TrySnapInnerLeft,
            TrySnapInnerRight,
            TrySnapInnerTop,
            TrySnapInnerBottom,
            TrySnapInnerLeftTop,
            TrySnapInnerLeftBottom,
            TrySnapInnerRightTop,
            TrySnapInnerRightBottom,
        };

        public static TrySnapDelegate GetMethod(eSnapType type)
        {
            return trySnapDelegates[(int)type];
        }

        /// <summary>Assume start end RectMatrix are same size, but different position.</summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static RectMatrix LerpUnclamped(RectMatrix start, RectMatrix end, float t)
        {
            var point = Vector3.LerpUnclamped(start.worldPivot, end.worldPivot, t);
            return RectMatrix.FromSrc2Pos(start, point);
        }

        /// <summary>Assume start end RectMatrix are same size, but different position.</summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static RectMatrix Lerp(RectMatrix start, RectMatrix end, float t)
        {
            var point = Vector3.Lerp(start.worldPivot, end.worldPivot, t);
            return RectMatrix.FromSrc2Pos(start, point);
        }
        #endregion Utils
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AI;

namespace Kit
{
	public static class NavMeshAgentExtend
	{
		public static void Information(this NavMeshAgent agent, ref StringBuilder sb)
		{
			if (sb == null)
				sb = new StringBuilder(200);
			sb.AppendLine($"> Update Position : {agent.updatePosition} / Rotation : {agent.updateRotation} / UpAxis : {agent.updateUpAxis}");
			sb.AppendLine($"> Obstacle avoidance priority :{agent.avoidancePriority}");
			if (!agent.isOnNavMesh)
			{
				sb.AppendLine($"[Not on nav mesh.]");
			}
			else if (agent.isOnOffMeshLink)
			{
				sb.AppendLine($"[Is on off mesh link]");
				sb.AppendLine($"link type = {agent.currentOffMeshLinkData.linkType}");
			}
			else
			{
				sb.AppendLine($"[Is on NavMesh]");
				if (agent.hasPath)
				{
					sb.AppendLine($"- Path : Status : {agent.pathStatus}");
					sb.AppendLine($"IsStopped : {(agent.isStopped ? "Stopped" : "Active")}");
					sb.AppendLine($"IsPathStale :{(agent.isPathStale ? "Change" : "Normal")}");
					sb.AppendLine($"Pending : {agent.pathPending}");
					sb.AppendLine($"- Corners amount : {agent.path.corners.Length}");
					sb.AppendLine($"- Remain distance {agent.remainingDistance:F2}");
				}
				else
				{
					sb.AppendLine($"- Path : No path");
				}
			}
		}

		public static void DrawGizmos(this NavMeshAgent agent, Color colorAgentPath = default, Color colorSteeringTarget = default)
		{
			if (agent.isOnNavMesh && !agent.isOnOffMeshLink && agent.hasPath)
			{
				if (colorAgentPath == default)
					colorAgentPath = Color.green;

				if (colorSteeringTarget == default)
					colorSteeringTarget = Color.magenta;

				Vector3[] points = agent.path.corners;
				int cnt = points.Length;
				for (int i = 0; i < cnt - 1; i++)
				{
					GizmosExtend.DrawLine(points[i], points[i + 1], colorAgentPath);
				}
				GizmosExtend.DrawLine(agent.transform.position, agent.steeringTarget, colorSteeringTarget);
			}
			else if (agent.isOnOffMeshLink)
            {
				GizmosExtend.DrawLine(
					agent.currentOffMeshLinkData.startPos,
					agent.currentOffMeshLinkData.endPos, colorAgentPath);
			}
		}
	}
}
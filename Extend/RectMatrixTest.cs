using Kit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
    using LocalCorner = RectMatrix.LocalCorner;
    public class RectMatrixTest : MonoBehaviour
    {
        [SerializeField] RectTransform m_Card0;
        [SerializeField] RectTransform m_Card1;

        [SerializeField] Transform m_Dir;
        [SerializeField] float m_MaxDistance = 1f;

        private void OnDrawGizmos()
        {
            if (m_Card0 == null || m_Card1 == null)
                return;
            if (m_Dir == null)
                return;

            var card0 = new RectMatrix(m_Card0);
            var card1 = new RectMatrix(m_Card1);

            var matrix = card0.world2Local;
            //card0.DrawGizmos(Color.green);
            //card1.DrawGizmos(Color.white);

            var c0 = new LocalCorner(card0.localCorners);
            var c1 = RectMatrix.Convert2LocalCorners(matrix, card1);

            var l2w = card0.local2World;
            //c0.DrawDebug(l2w, Color.green);
            //c1.DrawDebug(l2w, Color.red);

            Vector2 localRayDir;
            {
                var tmp = matrix.MultiplyVector(m_Dir.up);
                tmp.z = 0f;
                localRayDir = tmp.normalized;
            }

            //{
            //    var invM = card0.local2World;
            //    var org = card0.worldCorners[1];
            //    var ddir = invM * localRayDir * 10000f;
            //    DebugExtend.DrawRay(org, ddir, Color.gray, 0f, false);
            //}

            Cast(card0, localRayDir, m_MaxDistance, card1, out float hitDistance);


        }
        private static bool Cast(RectMatrix from, Vector2 localDir, in float maxDistance, RectMatrix target,
            out float distance)
        {
            localDir = localDir.normalized;
            distance = 0f;

            var _corners = new Vector3[4]
            {
                (Vector3)from.leftBottom,
                (Vector3)from.leftTop,
                (Vector3)from.rightTop,
                (Vector3)from.rightBottom,
            };

            var _otherCorners = new Vector3[4]
            {
                (Vector3)from.world2Local.MultiplyPoint( target.local2World.MultiplyPoint(target.leftBottom)    ),
                (Vector3)from.world2Local.MultiplyPoint( target.local2World.MultiplyPoint(target.leftTop)       ),
                (Vector3)from.world2Local.MultiplyPoint( target.local2World.MultiplyPoint(target.rightTop)      ),
                (Vector3)from.world2Local.MultiplyPoint( target.local2World.MultiplyPoint(target.rightBottom)   ),
            };

            const float DRAW_LEN = 100f;
            const int TEST_IDX = 3;
            var _forward = from.local2World.rotation * Vector3.forward;
            var _dir = (from.local2World.rotation * (Vector3)localDir).normalized;

            for (int x = 0; x < _corners.Length; ++x)
            {
                var lastIdx = (x - 1 + _corners.Length) % _corners.Length;
                var nextIdx = (x + 1) % _corners.Length;
                var p0 = _corners[x];
                var p1 = _corners[nextIdx];
                var p2 = _corners[lastIdx];
                var wp0 = World(p0);
                var wp2 = World(p2);
                var wp1 = World(p1);
                var lastEdge = p2 - p0;
                var nextEdge = p1 - p0;
                var tangent = Vector3.Cross(_forward, p1 - p0).normalized;

                var dot0 = Vector3.Dot(lastEdge.normalized, _dir);
                var dot1 = Vector3.Dot(nextEdge.normalized, _dir);

                var cross0 = Vector3.Dot(_forward, Vector3.Cross(lastEdge, _dir)); // 1 = right, -1 = left
                var cross1 = Vector3.Dot(_forward, Vector3.Cross(nextEdge, _dir));
                var isWithinAngle = dot0 > 0 && dot1 > 0 && (Mathf.Sign(cross0) != Mathf.Sign(cross1));

                GizmosExtend.DrawLine(wp0, wp0 + _forward * DRAW_LEN, Color.black); // forward
                GizmosExtend.DrawLine(wp0, wp0 + tangent * DRAW_LEN, Color.red.CloneAlpha(0.5f)); // tangent
                GizmosExtend.DrawLine(wp0, wp0 + _dir * DRAW_LEN, !isWithinAngle ? Color.green : Color.red);
                GizmosExtend.DrawLabel(wp0, $"[{lastIdx}>{x}>{nextIdx}]\ncross-last {c2BesignChar(cross0)}\ncross-next {c2BesignChar(cross1)},\n\ndot-last {dot0:F2}\ndot-next {dot1:F2}");
                if (isWithinAngle)
                    continue;

                for (int y = 0; y < _otherCorners.Length; ++y)
                {
                    var t0 = _otherCorners[y];
                    var t1 = _otherCorners[(y + 1) % _otherCorners.Length];

                    var wt0 = World(t0);
                    var wt1 = World(t1);
                    var edge = wt1 - wt0;

                    var anchorRay = new Ray(wp0, _dir);
                    var nextRay = new Ray(World(p1), _dir);
                    var edgeRay = new Ray(World(t0), edge);
                    if (!RayExtend.RayIntersectsRay(anchorRay, edgeRay, out var intersect0))
                        continue;

                    if (!RayExtend.RayIntersectsRay(nextRay, edgeRay, out var intersect1))
                        continue;

                    var blackwardIntersect = Vector3.Dot(intersect0 - wp0, _dir) < 0;
                    if (blackwardIntersect)
                        continue;

#if true
                    ClosestPointOnLineSegment(intersect0, wt0, wt1, out var between0, out var bp0);
                    ClosestPointOnLineSegment(intersect1, wt0, wt1, out var between1, out var bp1);
                    ClosestPointOnLineSegment(wt0, intersect0, intersect1, out var between2, out var bp2);
                    ClosestPointOnLineSegment(wt1, intersect0, intersect1, out var between3, out var bp3);
                    if (y == TEST_IDX || TEST_IDX == -1)
                    {
                        if (between0) GizmosExtend.DrawPoint(bp0, Color.black, 0.05f, true);
                        if (between1) GizmosExtend.DrawPoint(bp1, Color.green, 0.05f, true);
                        if (between2) GizmosExtend.DrawPoint(bp2, Color.blue, 0.05f, true);
                        if (between3) GizmosExtend.DrawPoint(bp3, Color.magenta, 0.05f, true);

                    }
#else
                    PointBetweenLineSegmentWithBias(intersect0, wt0, wt1,         out var between0);
                    PointBetweenLineSegmentWithBias(intersect1, wt0, wt1,         out var between1);
                    PointBetweenLineSegmentWithBias(wt0, intersect0, intersect1,  out var between2);
                    PointBetweenLineSegmentWithBias(wt1, intersect0, intersect1,  out var between3);
#endif
                    int cnt = 0;
                    cnt += between0 ? 1 : 0;
                    cnt += between1 ? 1 : 0;
                    cnt += between2 ? 1 : 0;
                    cnt += between3 ? 1 : 0;

                    bool collision = cnt >= 2;
                    if (!collision)
                        continue;

                    var hitDis = (intersect0 - wp0).magnitude;
                    var hitVec = _dir * hitDis;

                    Color color = y switch
                    {
                        0 => Color.black,
                        1 => Color.green,
                        2 => Color.blue,
                        3 => Color.magenta,
                        _ => Color.red,
                    };

                    if (y == TEST_IDX || TEST_IDX == -1)
                    {
                        //GizmosExtend.DrawLabel(intersect0, $"Collision : {cnt}, between {between0},{between1},{between2},{between3}");
                        GizmosExtend.DrawLine(wt0, wt1, color.CloneAlpha(1f));
                        GizmosExtend.DrawLine(intersect0, wp0, Color.white.CloneAlpha(0.3f));
                        GizmosExtend.DrawPoint(intersect0, color, 0.1f, true);

                        for (int z = 0; z < _corners.Length; ++z)
                        {
                            var a = World(_corners[z]) + hitVec;
                            var b = World(_corners[(z + 1) % _corners.Length]) + hitVec;
                            GizmosExtend.DrawLine(a, b, color.CloneAlpha(0.4f));
                        }
                    }

                }
            }
            return false;


            string c2BesignChar(float cross_dot)
            {
                var s = Mathf.Sign(cross_dot);
                if (Mathf.Approximately(s, 0f))
                    return "P"; // parallel
                if (s > 0f)
                    return "R"; // right
                return "L"; // left
            }

            Vector3 World(in Vector3 localpoint) => from.local2World.MultiplyPoint(localpoint);

            void PointBetweenLineSegmentWithBias(Vector3 p, Vector3 a, Vector3 b, out bool withIn)
            {
                var ba = b - a;
                var pa = p - a;
                var axis = ba.normalized;
                var v = Vector3.Project(pa, axis);
                if (v == Vector3.zero)
                {
                    withIn = p == a;
                    return;
                }
                if (Vector3.Dot(axis, v.normalized) < 0f)
                {
                    withIn = false;
                    return;
                }

                withIn = v.sqrMagnitude < ba.sqrMagnitude;
            }

            void ClosestPointOnLineSegment(Vector3 p, Vector3 a, Vector3 b, out bool withIn, out Vector3 closest)
                => InternalClosestPointOnLineSegment(p, a, b, out withIn, out closest, out var _);

            void InternalClosestPointOnLineSegment(Vector3 p, Vector3 a, Vector3 b, out bool withIn, out Vector3 closest, out float projectedLength)
            {
                var ba = b - a;
                var pa = p - a;
                var lineSegmentLength = ba.magnitude;
                if (lineSegmentLength < float.Epsilon)
                {
                    // A,B points too close.
                    withIn = p == a;
                    projectedLength = 0f;
                    closest = a;
                    return;
                }

                var axis = ba.normalized;
                var v = Vector3.Project(pa, axis);
                if (v == Vector3.zero)
                {
                    withIn = true;
                    projectedLength = 0f;
                    closest = a;
                    return;
                }
                projectedLength = v.magnitude;
                var dot = Vector3.Dot(axis, v.normalized); // -1 ~ 1
                Debug.Log($"They are dot normalized ={dot:F4}, v's length ={projectedLength} ");

                if (dot < 0f)
                {
                    // out bound ab, closest a
                    withIn = false;
                    closest = a;
                    return;
                }
                else if (dot > 1f)
                {
                    // out bound ab, closest b
                    withIn = false;
                    closest = b;
                    return;
                }

                // dot within 0 ~ 1f
                withIn = v.sqrMagnitude < ba.sqrMagnitude; // dot >= 0 && dot <= 1f;
                closest = Vector3.LerpUnclamped(a, b, dot);
            }
        }
    }
}
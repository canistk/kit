﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kit
{
	public static class Extensions
	{
		// TODO: rename to IsRefNull
		public static bool IsNull(this object obj) => ReferenceEquals(obj, null);
		public static bool NotNull(this object obj) => !IsNull(obj);
		public static bool RefEquals(this object obj, object other) => ReferenceEquals(obj, other);

		/// <summary>
		/// generate effective hash code
		/// - Non unique combie hash. Same hashcode didn't mean same object.
		/// - quick collision checking.
		/// <see cref="https://stackoverflow.com/questions/11742593/what-is-the-hashcode-for-a-custom-class-having-just-two-int-properties"/>
		/// </summary>
		/// <param name="args">another hashcode for reference type fields</param>
		/// <returns>the hash code of the sum of all those args.</returns>
		public static int GenerateHashCode(params int[] args)
		{
			// 17 & 31 are prime
			int hash = 17;
			int i = args.Length;
			while (i--> 0)
			{
				hash = hash * 31 + args[i];
			}
			return hash;
		}

		/// <summary>
		/// generate effective hash code
		/// - Non unique combie hash. Same hashcode didn't mean same object.
		/// - quick collision checking.
		/// <see cref="https://stackoverflow.com/questions/11742593/what-is-the-hashcode-for-a-custom-class-having-just-two-int-properties"/>
		/// </summary>
		/// <param name="args">object, null == 0</param>
		/// <returns>the hash code of the sum of all those args.</returns>
		public static int GenerateHashCode(params object[] args)
		{
			int hash = 17;
			int i = args.Length;
			while (i-- > 0)
			{
				hash = hash * 31 + (args[i].IsNull() ? 0 : args[i].GetHashCode());
			}
			return hash;
		}

		public static T[] ToArrayClear<T>(this Queue<T> src)
        {
			T[] arr = new T[src.Count];
			int i = 0;
			while (src.Count > 0)
				arr[i++] = src.Dequeue();
			src.Clear();
			return arr;
        }

		public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source, int allocMemorySize)
		{
			if (source == null)
				throw new System.NullReferenceException();
			if (allocMemorySize < 0)
				throw new System.ArgumentOutOfRangeException();
			var list = new List<TSource>(allocMemorySize);
			foreach (var item in source)
			{
				list.Add(item);
			}
			return list;
		}

		public static T[] ToArray<T>(this IEnumerable<T> source, int allocMemorySize = 100)
        {
            if (source == null)
                throw new System.NullReferenceException();
			var list = new List<T>(allocMemorySize);
			foreach(var item in source)
            {
				list.Add(item);
            }
			return list.ToArray();
        }
	}
}
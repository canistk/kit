using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Kit
{
    public static class AudioExtend
    {
        /// <summary>
        /// fix to prevent the sensitive slider issue
        /// - instead of -80db � 0db (the 0.0001 is important, and stops the slider breaking at zero)
        /// <see cref="https://gamedevbeginner.com/the-right-way-to-make-a-volume-slider-in-unity-using-logarithmic-conversion/"/>
        /// </summary>
        /// <param name="pName"></param>
        /// <param name="value01"></param>
        public static float ConvertFloat2DB(float value01)
        {
            float tmp = Mathf.Clamp(value01, 0.0001f, 1f);
            float db = Mathf.Log10(tmp) * 20f;
            return db;
        }

        public static void SetChannelVolume(this AudioMixer mixer, string parameter, float value)
        {
            if (mixer)
                mixer.SetFloat(parameter, ConvertFloat2DB(value));
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Kit
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceEx : MonoBehaviour
    {
        public AudioSource m_AudioSource = null;
		public UnityEvent EVENT_AudioComplete = new UnityEvent();

		private void Reset()
		{
			m_AudioSource = GetComponent<AudioSource>();
		}
		private void Awake()
		{
			if (m_AudioSource==null)
				m_AudioSource = GetComponent<AudioSource>();
		}

		private bool loseFocus => !Application.isFocused;

		private void OnEnable()
		{
			if (m_AudioSource.clip == null)
				return;
			else if (m_AudioSource.playOnAwake)
				StartCoroutine(WaitForClipEnd());
			else
				StartCoroutine(WaitForPlay());
		}

		private IEnumerator WaitForPlay()
		{
			while (!m_AudioSource.isPlaying)
				yield return null;
			yield return WaitForClipEnd();
		}

		private IEnumerator WaitForClipEnd()
		{
			while (m_AudioSource.isPlaying || loseFocus)
				yield return null;

			EVENT_AudioComplete?.Invoke();
		}
	}
}